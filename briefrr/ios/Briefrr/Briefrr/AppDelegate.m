//
//  AppDelegate.m
//  Briefrr
//
//  Created by Shashank Singh on 2/1/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "AppDelegate.h"
#import "RootViewController.h"
#import "Macros.h"
#import "EventManager.h"
#import "UserPreferenceService.h"
#import "RemoteControlEventHandler.h"
#import "EventLogger.h"
#import "Mixpanel.h"
#import "BackgroundFetchService.h"
#import "IdlingManagementService.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "MotionDetectionService.h"

static NSString* const MIXPANEL_TOKEN = @"e472c041fb89d12e23c4aa3482e19381";

@interface AppDelegate ()
@property (nonatomic) BOOL uiInitialized;
@end

@implementation AppDelegate

- (instancetype)init {
    self = [super init];
    if (self) {
        self.uiInitialized = NO;
    }
    return self;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Fabric with:@[CrashlyticsKit]];
    
    // TODO (shashank): remove following code, only for testing
    //NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    //[[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    [Mixpanel sharedInstanceWithToken:MIXPANEL_TOKEN];
    
    BOOL isLaunchedInBackground = application.applicationState == UIApplicationStateBackground;
    [[EventLogger getInstance] logEvent:@"app_init" properties:@{@"launched_in_background": @(isLaunchedInBackground)}];
    if (!isLaunchedInBackground) {
        [self initializeUIIfNotAlready:application];
    }
    return YES;
}

- (void)initializeUIIfNotAlready:(UIApplication*)application {
    if (self.uiInitialized) {
        return;
    }
    self.uiInitialized = YES;
    
    // needs to be called before any UI decisions are made
    [[UserPreferenceService getInstance] updateOnOpen];
    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    [application setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [MotionDetectionService getInstance];
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = UIColorFromRGB(COLOR_CODE_WHITE);
    self.window.rootViewController = [[RootViewController alloc] init];
    
    [self.window makeKeyAndVisible];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[IdlingManagementService getInstance] stop];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if (self.uiInitialized) {
        [[EventLogger getInstance] logEvent:@"ui_initialized_on_foreground"];
    }
    [self initializeUIIfNotAlready:application];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[EventManager getInstance] fireEvent:APP_BECAME_ACTIVE];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[IdlingManagementService getInstance] stop];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [[EventManager getInstance] fireEvent:USER_NOTIFICATION_SETTING_CHANGED];
}

-(void)application:(UIApplication *)app didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [[EventLogger getInstance] logEvent:@"notification_received" properties:@{@"app_state": @(app.applicationState)}];
    if([app applicationState] == UIApplicationStateInactive) {
        
    }
}

- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    [[RemoteControlEventHandler getInstance] handleRemoteControlEvent:event];
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    [[BackgroundFetchService getInstance] fetchWithCompletionHandler:completionHandler];
}

@end
