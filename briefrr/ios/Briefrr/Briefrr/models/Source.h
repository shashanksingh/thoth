//
//  Source.h
//  Briefrr
//
//  Created by Shashank Singh on 2/8/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Source : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *logoURL;
@property (nonatomic) BOOL selected;

- (instancetype)initWithJSONDescriptor:(NSDictionary*)descriptor;
- (BOOL)isPublisher;
- (BOOL)isTopic;
- (BOOL)isLogoLocallyAvailable;
@end
