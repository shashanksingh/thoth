//
//  Story.m
//  Briefrr
//
//  Created by Shashank Singh on 2/17/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "Story.h"
#import "Util.h"
#import "SourcesService.h"

static NSString* const DEFAULT_IMAGE = @"assets/images/source-logos/nyt.png";


@implementation Story
- (instancetype)initWithJSONDescriptor:(NSDictionary*)descriptor {
    self = [super init];
    if (self) {
        self.descriptor = descriptor;
        self.guid = [Util nsNullToNil:descriptor[@"guid"]];
        self.link = [Util nsNullToNil:descriptor[@"link"]];
        self.title = [self getProcessedText:descriptor[@"title"]];
        self.summary = [self getProcessedText:descriptor[@"summary"]];
        self.sourceId = [Util nsNullToNil:descriptor[@"source_id"]];
        
        self.ttsSummary = [Util nsNullToNil:descriptor[@"tts_summary"]];
        if (!self.ttsSummary) {
            self.ttsSummary = self.summary;
        }
        
        NSString *thumbnail = [Util nsNullToNil:descriptor[@"thumbnail"]];
        self.thumbnail = [self getProcessedThumbnailURL:thumbnail sourceId:self.sourceId];
        
        NSNumber *timestamp = (NSNumber*)descriptor[@"timestamp"];
        timestamp = [Util nsNullToNil:timestamp];
        if (!timestamp) {
            self.timestamp = 0;
        } else {
            self.timestamp = timestamp.longValue;
        }
    }
    return self;
}

- (NSString*)getProcessedThumbnailURL:(NSString*)thumbnailURL sourceId:(NSString*)sourceId {
    // use thumbnail if available, source logo otherwise
    if (!thumbnailURL || thumbnailURL.length == 0) {
        Source *source = [[SourcesService getInstance] getSourceById:sourceId];
        if (source) {
            thumbnailURL = source.logoURL;
        } else {
            // TODO (shashank): log error
        }
    }
    return thumbnailURL;
}

- (NSString*)getProcessedText:(NSString*)rawText {
    rawText = [Util nsNullToNil:rawText];
    if (!rawText) {
        return rawText;
    }
    NSString *rv = [Util trimString:rawText];
    rv = [Util capitalizeFirstLetter:rv];
    return rv;
}

@end
