//
//  Story.h
//  Briefrr
//
//  Created by Shashank Singh on 2/17/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Story : NSObject
@property (nonatomic, strong) NSDictionary *descriptor;
@property (nonatomic, strong) NSString *guid;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *summary;
@property (nonatomic, strong) NSString *ttsSummary;
@property (nonatomic, strong) NSString *thumbnail;
@property (nonatomic) long timestamp;
@property (nonatomic, strong) NSString *sourceId;

- (instancetype)initWithJSONDescriptor:(NSDictionary*)descriptor;
@end
