//
//  Source.m
//  Briefrr
//
//  Created by Shashank Singh on 2/8/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "Source.h"
#import "Macros.h"

static NSString* const BUNDLED_PUBLISHER_LOGO_FILE_PATH_TEMPLATE = @"assets/images/source-logos/%@.png";
static NSString* const BUNDLED_TOPIC_LOGO_FILE_PATH_TEMPLATE = @"assets/images/topic-logos/%@.jpg";

@interface Source()
@property (nonatomic, strong) NSString *type;
@property (nonatomic) BOOL localLogo;
@end

@implementation Source

- (instancetype)initWithJSONDescriptor:(NSDictionary*)descriptor {
    self = [super init];
    if (self) {
        self.id = descriptor[@"id"];
        self.name = descriptor[@"name"];
        self.logoURL = descriptor[@"logo"];
        self.type = descriptor[@"type"];
        self.selected = NO;
        self.localLogo = NO;
        
        NSString *bundlePath = [[NSBundle mainBundle] resourcePath];
        NSString *relativePath;
        if ([self isPublisher]) {
            relativePath = [NSString stringWithFormat:BUNDLED_PUBLISHER_LOGO_FILE_PATH_TEMPLATE, self.id];
        } else {
            relativePath = [NSString stringWithFormat:BUNDLED_TOPIC_LOGO_FILE_PATH_TEMPLATE, self.id];
        }
        
        NSString *bundledLogoPath = [bundlePath stringByAppendingPathComponent:relativePath];
        if ([[NSFileManager defaultManager] fileExistsAtPath:bundledLogoPath]) {
            self.localLogo = YES;
            self.logoURL = bundledLogoPath;
        } else {
            if (![self.logoURL hasPrefix:@"http://"] && ![self.logoURL hasPrefix:@"https://"]) {
                self.logoURL = [SERVER_BASE_URL stringByAppendingPathComponent:self.logoURL];
            }
        }
    }
    return self;
}

- (BOOL)isPublisher {
    return [self.type isEqualToString:@"publisher"];
}

- (BOOL)isTopic {
    return [self.type isEqualToString:@"topic"];
}

- (BOOL)isLogoLocallyAvailable {
    return self.localLogo;
}

@end
