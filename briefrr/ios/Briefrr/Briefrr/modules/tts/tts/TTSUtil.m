//
//  TTSUtil.m
//  tts
//
//  Created by Shashank Singh on 12/16/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import "TTSUtil.h"
#import <AVFoundation/AVFoundation.h>
#import <CommonCrypto/CommonDigest.h>


@implementation TTSUtil

+(NSString*)getMD5:(NSString*)plainText
{
    // Create pointer to the string as UTF8
    const char *ptr = [plainText UTF8String];
    
    // Create byte array of unsigned chars
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    // Create 16 byte MD5 hash value, store in buffer
    CC_MD5(ptr, strlen(ptr), md5Buffer);
    
    // Convert MD5 value in the buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}

+ (NSDate*)getFileModificationTime:(NSString*)filePath {
    NSError *error;
    NSDictionary* fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
    if (error) {
        NSLog(@"Error: NetworkTTSProvider:: error in getting attributes of file at: %@, error: %@", filePath, error);
        return NULL;
    }
    return [fileAttribs objectForKey:NSFileModificationDate];
}

+(int)getAudioSamplesFromFile:(NSString*)filePath outputBuffer:(SInt16**)outputBuffer outputBufferLength:(unsigned long*)outputBufferLength {
    
    AudioStreamBasicDescription asbd_mono_aac_16khz;
    memset(&asbd_mono_aac_16khz, 0, sizeof(AudioStreamBasicDescription));
    
    
    asbd_mono_aac_16khz.mFormatID = kAudioFormatLinearPCM;
    asbd_mono_aac_16khz.mFormatFlags = kAudioFormatFlagsNativeEndian |
    kAudioFormatFlagIsPacked | kAudioFormatFlagIsSignedInteger;
    asbd_mono_aac_16khz.mBytesPerFrame = asbd_mono_aac_16khz.mBytesPerPacket = 2;
    asbd_mono_aac_16khz.mBitsPerChannel = 16;
    asbd_mono_aac_16khz.mSampleRate = 16000.0;
    asbd_mono_aac_16khz.mChannelsPerFrame = 1;
    asbd_mono_aac_16khz.mFramesPerPacket = 1;
    
    NSURL *url = [NSURL fileURLWithPath:filePath];
    ExtAudioFileRef fileRef;
    
    OSStatus result = ExtAudioFileOpenURL((__bridge CFURLRef)url, &fileRef);
    if (result != noErr) {
        NSLog(@"Error: NetworkTTSProvider:: in opening cached audio file with url: %@, %d", url, (int)result);
        *outputBuffer = NULL;
        *outputBufferLength = 0;
        return 1;
    }
    
    
    result =  ExtAudioFileSetProperty(fileRef,
                                      kExtAudioFileProperty_ClientDataFormat,
                                      sizeof(asbd_mono_aac_16khz),
                                      &asbd_mono_aac_16khz);
    
    if (result != noErr) {
        NSLog(@"Error: NetworkTTSProvider:: in setting audio file property: %d", (int)result);
        *outputBuffer = NULL;
        *outputBufferLength = 0;
        return 1;
    }
    
    SInt64 numFrames = 0;
    UInt32 size = sizeof(numFrames);
    result = ExtAudioFileGetProperty(fileRef, kExtAudioFileProperty_FileLengthFrames, &size, &numFrames);
    if (result != noErr) {
        NSLog(@"Error: NetworkTTSProvider:: in getting number of frames in audio file: %d", (int)result);
        *outputBuffer = NULL;
        *outputBufferLength = 0;
        return 1;
    }
    
    NSLog(@"numFrames: %u", numFrames);
    
    UInt32 bufferSize = (UInt32)(sizeof(char) * numFrames * asbd_mono_aac_16khz.mBytesPerPacket);
    char *buffer =(char*) malloc(bufferSize);
    
    AudioBufferList audioBufferList2;
    audioBufferList2.mNumberBuffers = 1;
    audioBufferList2.mBuffers[0].mNumberChannels = asbd_mono_aac_16khz.mChannelsPerFrame;
    audioBufferList2.mBuffers[0].mDataByteSize = bufferSize;
    audioBufferList2.mBuffers[0].mData = buffer;
    
    ExtAudioFileRead(fileRef, &numFrames, &audioBufferList2);
    *outputBufferLength = bufferSize/(sizeof(SInt16));
    *outputBuffer = (SInt16*)audioBufferList2.mBuffers[0].mData;
    return 0;
}
@end
