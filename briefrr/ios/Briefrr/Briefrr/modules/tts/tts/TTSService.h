//
//  TTSService.h
//  tts
//
//  Created by Shashank Singh on 10/16/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SpeechUtterance.h"

typedef enum TTSEngine {
    DEFAULT,
    NEOSPEECH_EMBEDDED,
    NEOSPEECH_NETWORK,
    NUANCE
} TTSEngine;


@interface TTSService : NSObject
+(TTSService*)sharedInstance;
- (NSString*)getLicenseFilePathForEngine:(TTSEngine)engine;
- (int)getAudioSamplesForUtterance:(SpeechUtterance*)utterance ttsEngine:(TTSEngine)ttsEngine outputBuffer:(SInt16**)outputBuffer outputBufferLength:(unsigned long*)outputBufferLength;
- (void)prefetchAudioSamplesForUtterances:(NSArray*)utterances withCallback:(void(^)(BOOL))callback;
@end
