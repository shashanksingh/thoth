//
//  TTSUtil.h
//  tts
//
//  Created by Shashank Singh on 12/16/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TTSUtil : NSObject
+ (NSString*)getMD5:(NSString*)plainText;
+ (NSDate*)getFileModificationTime:(NSString*)filePath;
+ (int)getAudioSamplesFromFile:(NSString*)filePath outputBuffer:(SInt16**)outputBuffer outputBufferLength:(unsigned long*)outputBufferLength;
@end
