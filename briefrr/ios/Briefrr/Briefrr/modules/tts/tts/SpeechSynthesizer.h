//
//  SpeechSynthesizer.h
//  OpenEars
//
//  Created by Shashank Singh on 9/14/14.
//  Copyright (c) 2014. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SpeechUtterance.h"

@protocol SpeechSynthesizerDelegate <NSObject>

- (void)speechSyntheSizerStartedUtterance:(SpeechUtterance*)utterance;
- (void)speechSyntheSizerFinishedUtterance:(SpeechUtterance*)utterance wasPartiallyUttered:(BOOL)partial wasSkipped:(BOOL)skipped queuedUtteranceCount:(NSUInteger)queuedUtteranceCount;

@end

@interface SpeechSynthesizer : NSObject
@property (strong, nonatomic) id<SpeechSynthesizerDelegate> delegate;


+ (SpeechSynthesizer*)sharedInstance;
- (void)queueUtterance:(SpeechUtterance*)utterance;
- (void)speakImmediately:(SpeechUtterance*)utterance;
- (NSUInteger)clearUtteranceQueue;
- (void)pause;
- (void)resume;

-(void)getAudioSamples:(UInt32)numSamples inArray:(SInt16**)samplesArray withLength:(UInt32*)samplesCount;
-(void)onAudioSamplesRenderd:(UInt32)numSamples;
-(void)prefetchAudioSamplesForUtterances:(NSArray*)utterances withCallback:(void(^)(BOOL))callback;
@end
