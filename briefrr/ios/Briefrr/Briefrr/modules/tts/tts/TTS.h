//
//  tts.h
//  tts
//
//  Created by Shashank Singh on 10/16/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for tts.
FOUNDATION_EXPORT double ttsVersionNumber;

//! Project version string for tts.
FOUNDATION_EXPORT const unsigned char ttsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <tts/PublicHeader.h>
#import "SpeechSynthesizer.h"


