//
//  SpeechUtterance.h
//  tts
//
//  Created by Shashank Singh on 10/25/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpeechUtterance : NSObject
@property (strong, nonatomic) NSString* speechText;
@property (nonatomic) double volume;
@property (nonatomic) double pitch;
@property (nonatomic) double speed;
@property (nonatomic) double preDelay;
@property (nonatomic) double postDelay;
@property (strong, nonatomic) NSString* voiceToUse;
@end
