//
//  NeoSpeechNetworkTTSProvider.m
//  tts
//
//  Created by Shashank Singh on 12/12/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import "NeoSpeechNetworkTTSProvider.h"
#import "AFNetworking.h"
#import "UserPreferenceService.h"

static NSString * const kNeoSpeechTTSServiceURL = @"http://briefrr.com:14002/tts";

static NSString * const kDefaultTTSLang = @"en_US";
static NSString * const kDefaultMaleVoice = @"James";
static NSString * const kDefaultFemaleVoice = @"Ashley";

@implementation NeoSpeechNetworkTTSProvider

-(NSURLRequest*)getRequestForUtterance:(SpeechUtterance*)utterance {
    NSLog(@"getRequestForUtterance neospeech");
    NSString *voice = utterance.voiceToUse;
    if (!voice) {
        voice = @"default";
    } else if ([voice caseInsensitiveCompare:@"@male"] == 0) {
        voice = kDefaultMaleVoice;
    } else if ([voice caseInsensitiveCompare:@"@female"] == 0) {
        voice = kDefaultFemaleVoice;
    }
    
    NSDictionary *parameters = @{
                                 @"id": [[UserPreferenceService getInstance] getUserId],
                                 @"voice": voice,
                                 @"volume": @(utterance.volume),
                                 @"speed": @(utterance.speed),
                                 @"pitch": @(utterance.pitch),
                                 @"text": utterance.speechText
                                 };
    
    NSError *error = NULL;
    NSMutableURLRequest * request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:kNeoSpeechTTSServiceURL parameters:parameters error:&error];
    if (error) {
        NSLog(@"Error: NeoSpeechNetworkTTSProvider:: error in creating post request: %@, skipping utterance", error);
        return nil;
    }
    
    // TODO (shashank): what if we want to call this in foreground?
    // does setting this flag have a negative impact?
    [request setNetworkServiceType:NSURLNetworkServiceTypeBackground];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    return request;
}
@end
