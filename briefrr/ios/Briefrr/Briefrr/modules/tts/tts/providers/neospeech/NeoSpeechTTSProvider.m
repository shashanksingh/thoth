//
//  NeoSpeechTTSProvider.m
//  tts
//
//  Created by Shashank Singh on 10/16/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import "NeoSpeechTTSProvider.h"
#import "API_Define.h"
#import "TTSUtil.h"
#import "TTSService.h"
#import "RemoteConfigService.h"
#import "EventLogger.h"

#define MAX_FRAMES 100
#define THREAD_ID 0
#define AUDIO_FORMAT VT_BUFFER_API_FMT_S16PCM
#define MIN_PITCH 50
#define MAX_PITCH 200
#define MIN_SPEED 50
#define MAX_SPEED 400
#define MIN_VOLUME 0
#define MAX_VOLUME 500

static NSString* const TTS_ENGINE_PATH = @"TTSResources.bundle/providers/neospeech";

@implementation NeoSpeechTTSProvider
-(NeoSpeechTTSProvider*) init {
    self = [super init];
    if (self) {
        NSString *licenseFilePath = [self getLicenseFilePath];
        if (licenseFilePath) {
            NSLog(@"license content: |%@|", [NSString stringWithContentsOfFile:licenseFilePath encoding:NSUTF8StringEncoding error:NULL]);
        }
        
        NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
        resourcePath = [resourcePath stringByAppendingPathComponent:TTS_ENGINE_PATH];
        NSLog(@"Neospeech:: looking up voice database at: %@", resourcePath);
        
        char *dbPath = (char*)resourcePath.UTF8String;
        char *licensePath = licenseFilePath ? (char*)licenseFilePath.UTF8String : NULL;
        short ret = VT_LOADTTS_ENG_James((int)NULL, -1, dbPath, licensePath);
        if (ret) {
            NSLog(@"Neospeech:: fatal error in loading tts engine: %d", ret);
            self = nil;
        } else {
            NSLog(@"Neospeech:: successfully loaded TTS engine");
        }
    }
    return self;
}

- (NSString*)getLicenseFilePath {
    NSString *licenseFilePath = [[TTSService sharedInstance] getLicenseFilePathForEngine:NEOSPEECH_EMBEDDED];
    NSString *licenseBody = [[RemoteConfigService getInstance] getNeospeechEmbeddedTTSLicense];
    BOOL licenseExists = [[NSFileManager defaultManager] fileExistsAtPath:licenseFilePath];
    if (!licenseBody) {
        if (!licenseExists) {
            [[EventLogger getInstance] logEvent:@"neo_tts_license_file_not_found"];
            licenseFilePath = nil;
        }
    } else {
        NSError *error;
        [licenseBody writeToFile:licenseFilePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
        if (error) {
            [[EventLogger getInstance] logEvent:@"neo_tts_error_in_writing_license_to_disk" properties:@{@"error": error.localizedDescription}];
            if (!licenseExists) {
                licenseFilePath = nil;
            }
        }
    }
    return licenseFilePath;
}

-(void)prefetchAudioSamplesForUtterances:(NSMutableArray*)utterances withCallback:(void(^)(BOOL))callback {
    NSLog(@"NeoSpeechTTSProvider does not support prefetching");
    if (callback) {
        callback(YES);
    }
}

-(BOOL)hasLocalAudioSamplesForUtterance:(SpeechUtterance*)utterance {
    // local tts providers always have samples available
    return YES;
}

-(int)getAudioSamplesForUtterance:(SpeechUtterance*)utterance outputBuffer:(SInt16**)outputBuffer outputBufferLength:(unsigned long*)outputBufferLength {
    
    char *text = (char*)[utterance.speechText UTF8String];
    
    int frameLength = 0;
    int ret = VT_TextToBuffer_ENG_James(AUDIO_FORMAT, NULL, NULL,
                                      &frameLength, -1, THREAD_ID, -1, -1,
                                    -1, -1, -1, -1, -1);
    if (ret < 0) {
        VT_UNLOADTTS_ENG_James(-1);
        NSLog(@"Neospeech:: error in getting frame size: %d", ret);
        return ret;
    }
    
    NSLog(@"Neospeech:: frame length: %d", frameLength);
    
    char* frames[MAX_FRAMES];
    unsigned int frameLengths[MAX_FRAMES];
    
    unsigned int frameIndex = -1;
    unsigned int totalBufferSize = 0;
    int flag = 0;
    
    // valid range is [MIN_PITCH, MAX_PITCH], utterance value is [0, 1]
    int pitch = utterance.pitch;
    if (pitch >= 0) {
        pitch = MIN_PITCH + floor(utterance.pitch * (MAX_PITCH - MIN_PITCH));
        pitch = MAX(MIN_PITCH, MIN(pitch, MAX_PITCH));
    }
    
    int speed = utterance.speed;
    if (speed >= 0) {
        speed = MIN_SPEED + floor(utterance.speed * (MAX_SPEED - MIN_SPEED));
        speed = MAX(MIN_SPEED, MIN(speed, MAX_SPEED));
    }
    
    int volume = utterance.volume;
    if (volume >= 0) {
        volume = MIN_VOLUME + floor(utterance.volume * (MAX_VOLUME - MIN_VOLUME));
        volume = MAX(MIN_VOLUME, MIN(volume, MAX_VOLUME));
    }
    
    NSLog(@"Neospeech:: pitch: %d, speed: %d, volume:%d", pitch, speed, volume);
    
    while (++frameIndex < MAX_FRAMES) {
        char *frameBuffer = (char*)malloc(frameLength);
        if (!frameBuffer) {
            // TODO (shashank): clear the memory claimed by successful frames
            // already created
            NSLog(@"Neospeech:: error in allocating memory for frame buffer");
            return 1;
        }
        
        int thisFrameSize = 0;
        ret = VT_TextToBuffer_ENG_James(AUDIO_FORMAT, text, frameBuffer, &thisFrameSize,
                                    flag, THREAD_ID, -1, pitch, speed, -1, -1, -1, -1);
        if (ret < 0) {
            // TODO (shashank): clear the memory claimed by successful frames
            // already created
            NSLog(@"Neospeech:: there was an error in converting text to speech: %d", ret);
            return ret;
        }
        
        frames[frameIndex] = frameBuffer;
        frameLengths[frameIndex] = thisFrameSize;
        totalBufferSize += thisFrameSize;
        
        if (ret == 1) {
            // all frames done
            break;
        }
        
        // after the first frame (frame >= 2), flag is set to 1
        if (flag == 0) {
            flag = 1;
        }
    }
    
    if (frameIndex >= MAX_FRAMES) {
        NSLog(@"Neospeech:: the input text was too long, truncated after %d frames", MAX_FRAMES);
    }
    
    char* finalBuffer = (char*)calloc(totalBufferSize, sizeof(char));
    // go through each frame and append it to the end of the final buffer
    for (unsigned int i=0, finalBufferSize = 0; i <= frameIndex; ++i) {
        memcpy(finalBuffer + finalBufferSize, frames[i], frameLengths[i]);
        finalBufferSize += frameLengths[i];
        free(frames[i]);
    }
    
    /*NSString *tempPath = NSTemporaryDirectory();
    NSString *fileName = [[NSUUID UUID] UUIDString];
    fileName = [fileName stringByAppendingPathExtension:@"wav"];
    NSString *tempFile = [tempPath stringByAppendingPathComponent:fileName];
    
    //char *fileNameCString = [tempFile UTF8String];
    NSData *data = [NSData dataWithBytes:(char*)finalBuffer length:totalBufferSize];
    [data writeToFile:tempFile atomically:YES];
    return [TTSUtil getAudioSamplesFromFile:tempFile outputBuffer:outputBuffer outputBufferLength:outputBufferLength];*/
    
    *outputBufferLength = totalBufferSize/(sizeof(SInt16));
    *outputBuffer = (SInt16*)finalBuffer;
    return 0;
    
}

@end
