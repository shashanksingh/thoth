//
//  NuanceTTSProvider.m
//  tts
//
//  Created by Shashank Singh on 11/29/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h>
#import "NuanceTTSProvider.h"
#import "AFNetworking.h"

static NSString * const kNuanceTTSServiceURL = @"https://tts.nuancemobility.net/NMDPTTSCmdServlet/tts";
static NSString * const kNuanceAppId = @"NMDPTRIAL_adisomani20141109161135";
static NSString * const kNuanceAppKey = @"149e8892e8f6e136a5851ae543098b34a60c47924b6cede6327c69322ca7bd404545d22d3bb364e1387ec67b8851af2e5df7c1a863ae8e952af7c7569f8596e4";

static NSString * const kNuanceAcceptHeader = @"audio/x-wav;codec=pcm;bit=16;rate=16000";

static NSString * const kDefaultTTSLang = @"en_US";
static NSString * const kDefaultMaleVoice = @"Tom";
static NSString * const kDefaultFemaleVoice = @"Ava";

@interface NuanceTTSProvider()
@property (strong, nonatomic) AFHTTPSessionManager *downloadSessionManager;
@end

@implementation NuanceTTSProvider
@synthesize downloadSessionManager;

-(NSString*)getDeviceId {
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

-(NSURLRequest*)getRequestForUtterance:(SpeechUtterance*)utterance {
    NSLog(@"getRequestForUtterance nuance");
    NSDictionary *parameters = @{
                                 @"appId": kNuanceAppId,
                                 @"appKey": kNuanceAppKey,
                                 @"id": [self getDeviceId],
                                 // TODO (shashank): allow changing voice
                                 @"voice": kDefaultFemaleVoice
                                 };
    
    NSError *error = NULL;
    // initing with GET so that the url is created with parameters set
    // switching to post afterwards as required by the API
    NSMutableURLRequest * request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:kNuanceTTSServiceURL parameters:parameters error:&error];
    request.HTTPMethod = @"POST";
    request.HTTPBody = [utterance.speechText dataUsingEncoding:NSUTF8StringEncoding];
    
    if (error) {
        NSLog(@"Error: NuanceTTSProvider:: error in creating post request: %@, skipping utterance", error);
        return nil;
    }
    
    // TODO (shashank): what if we want to call this in foreground?
    // does setting this flag have a negative impact?
    [request setNetworkServiceType:NSURLNetworkServiceTypeBackground];
    [request setValue:kNuanceAcceptHeader forHTTPHeaderField:@"Accept"];
    [request setValue:@"text/plain" forHTTPHeaderField:@"Content-Type"];
    
    return request;
}

@end
