#import "vt_eng_julie.h"
#import "vt_eng_paul.h"
#import "vt_eng_james.h"
#import "vt_internal_all.h"

//Language & TTS Voice Setting///////////////////
// TTS LANGUAGE (ENG, BRE, CRF, SPA, JPN, CHI, KOR)
// TTS VOICE(ENG:JAMES, JULIE, KATE, PAUL, ASHLEY)
//          (BRE:BRIDGET, HUGH)
//          (CFR:CHLOE)
//          (SPA:VIOLETA, FRANCISCO)
//          (JPN:HARUKA, HIKARI, SAYAKA, MISAKI, SHOW, RYO, TAKERU)
//          (CHI:HUI, LIANG)
//          (KOR:JUNWOO, YUMI, HYERYUN)

#define __ENG__
//#define __JULIE__

/////////////////////////////////////////////////
// eng
#if defined (__ENG__)
#if defined (__JULIE__)
#define VT_TextToBuffer_Julie       VT_TextToBuffer_ENG_Julie
#define VT_LOADTTS_Julie            VT_LOADTTS_ENG_Julie
#define VT_UNLOADTTS_Julie          VT_UNLOADTTS_ENG_Julie
#define VT_SetTextTypeForHighlight_Julie    VT_SetTextTypeForHighlight_ENG_Julie
#define VT_TextToBufferEX_Julie             VT_TextToBufferEX_ENG_Julie
#define VT_GetDefVersion_Julie      VT_GetDefVersion_ENG_Julie
#define VT_gVersionFirst_Julie      VT_gVersionFirst_ENG_Julie
#define VT_gVersionSecond_Julie     VT_gVersionSecond_ENG_Julie
#define VT_gVersionThird_Julie      VT_gVersionThird_ENG_Julie
#define VT_gVersionFourth_Julie     VT_gVersionFourth_ENG_Julie
#define VT_GetTTSInfo_Julie         VT_GetTTSInfo_ENG_Julie
#endif

#if defined (__PAUL__)
#define VT_TextToBuffer_Paul    VT_TextToBuffer_ENG_Paul
#define VT_LOADTTS_Paul         VT_LOADTTS_ENG_Paul
#define VT_UNLOADTTS_Paul       VT_UNLOADTTS_ENG_Paul
#define VT_SetTextTypeForHighlight_Paul     VT_SetTextTypeForHighlight_ENG_Paul
#define VT_TextToBufferEX_Paul              VT_TextToBufferEX_ENG_Paul
#define VT_GetDefVersion_Paul   VT_GetDefVersion_ENG_Paul
#define VT_gVersionFirst_Paul   VT_gVersionFirst_ENG_Paul
#define VT_gVersionSecond_Paul  VT_gVersionSecond_ENG_Paul
#define VT_gVersionThird_Paul   VT_gVersionThird_ENG_Paul
#define VT_gVersionFourth_Paul  VT_gVersionFourth_ENG_Paul
#define VT_GetTTSInfo_Paul      VT_GetTTSInfo_ENG_Paul
#endif

#if defined (__JAMES__)
#define VT_TextToBuffer_James       VT_TextToBuffer_ENG_James
#define VT_LOADTTS_James            VT_LOADTTS_ENG_James
#define VT_UNLOADTTS_James          VT_UNLOADTTS_ENG_James
#define VT_SetTextTypeForHighlight_James    VT_SetTextTypeForHighlight_ENG_James
#define VT_TextToBufferEX_James             VT_TextToBufferEX_ENG_James
#define VT_GetDefVersion_James      VT_GetDefVersion_ENG_James
#define VT_gVersionFirst_James      VT_gVersionFirst_ENG_James
#define VT_gVersionSecond_James     VT_gVersionSecond_ENG_James
#define VT_gVersionThird_James      VT_gVersionThird_ENG_James
#define VT_gVersionFourth_James     VT_gVersionFourth_ENG_James
#define VT_GetTTSInfo_James         VT_GetTTSInfo_ENG_James
#endif

#endif
