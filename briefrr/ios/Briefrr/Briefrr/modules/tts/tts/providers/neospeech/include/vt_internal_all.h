#ifndef VT_INTERNAL_ALL_H
#define VT_INTERNAL_ALL_H

#if defined(__cplusplus)
extern "C" {
#endif
    
    
    
    /*===========================================================================*/
    /* SAPI5.1 \xbf\xa1\xbc\xad \xbb\xe7\xbf\xeb\xc7\xcf\xb4\xc2 \xc7\xd4\xbc\xf6 */
#if defined(__TTS_LIP_SYNC_MODE__)
#if !defined(VT_INTERNAL_DEFINE)
    /* new syncinfo */
    typedef struct _phonesync_new
    {
        int			m_nLength;
        short		m_nId;
    } PHONESYNC_NEW;
    
    typedef struct _wordsync_new
    {
        short		m_nNumberOfPhones;
        PHONESYNC_NEW	*m_pPhone;
        int			m_nLength;
        int			m_nWordStartPosInText;
        int			m_nWordEndPosInText;
        int			m_nSentStartPosInText;
        int			m_nSentEndPosInText;
        int			m_nNumberOfWordsInSent;
        int			m_nIndexOfWordsInSent;
    } WORDSYNC_NEW;
    
    typedef struct _buffersync
    {
        int			m_nOffsetInStream;
        int			m_nIndexOfWordSync;
        int			m_nOffsetInWord;
        int			m_nIndexOfPhoneSync;
        int			m_nOffsetInPhone;
    } BUFFERSYNC;
    
    typedef struct _syncinfo_new
    {
        WORDSYNC_NEW	*m_pWord;
        int			m_nMaxWordForSync;
        int			m_nMaxPhoneForSync;
        int			m_nNumberOfValidWordSync;
        BUFFERSYNC	m_BufferStart;
        BUFFERSYNC	m_BufferEnd;
    } SYNCINFO_NEW, *PSYNCINFO_NEW;
    
    /* mark array structure for mark tag */
#define		MAX_MARK_NAME				(512)
#define		MARK_STATUS_GENERAL			(1)
#define		MARK_STATUS_EMPTY_STRING	(2)
#define		MARK_STATUS_TRIMMED_STRING	(3)
    typedef struct
    {
        int		m_nOffsetInStream;
        int		m_nOffsetInBuffer;
        int		m_nPosInText;
        char	m_sMarkName[MAX_MARK_NAME];
        char	m_bMarkStatus;
    } TTSMark;
    
    typedef struct
    {
        int		m_nSize;
        TTSMark *m_Marks;
        int		m_nStartIndexOfMarks;
        int		m_nEndIndexOfMarks;
    } TTSMarkArray;
    
#endif
    // eng
     _DllMode(int) VT_TextToBufferEX_ENG_Julie(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    _DllMode(int) VT_TextToBufferEX_ENG_Kate(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    _DllMode(int) VT_TextToBufferEX_ENG_Paul(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    _DllMode(int) VT_TextToBufferEX_ENG_James(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    _DllMode(int) VT_TextToBufferEX_ENG_Ashley(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    
    // bre
    _DllMode(int) VT_TextToBufferEX_BRE_Bridget(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    _DllMode(int) VT_TextToBufferEX_BRE_Hugh(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);

    // spa
    _DllMode(int) VT_TextToBufferEX_SPA_Violeta(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    _DllMode(int) VT_TextToBufferEX_SPA_Francisco(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    
    // cfr
    _DllMode(int) VT_TextToBufferEX_CFR_Chloe(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    
    // jpn
    _DllMode(int) VT_TextToBufferEX_JPN_Haruka(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    _DllMode(int) VT_TextToBufferEX_JPN_Hikari(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    _DllMode(int) VT_TextToBufferEX_JPN_Sayaka(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    _DllMode(int) VT_TextToBufferEX_JPN_Misaki(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    _DllMode(int) VT_TextToBufferEX_JPN_Show(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    _DllMode(int) VT_TextToBufferEX_JPN_Ryo(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    _DllMode(int) VT_TextToBufferEX_JPN_Takeru(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    
    // chi
    _DllMode(int) VT_TextToBufferEX_CHI_Hui(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    _DllMode(int) VT_TextToBufferEX_CHI_Liang(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    
    // kor
    _DllMode(int) VT_TextToBufferEX_KOR_Junwoo(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    _DllMode(int) VT_TextToBufferEX_KOR_Yumi(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    _DllMode(int) VT_TextToBufferEX_KOR_Hyeryun(int fmt, char *tts_text, char *output_buff, int *output_len, int flag, int nThreadID, int nSpeakerID, SYNCINFO_NEW **ppSyncInfoNew, TTSMarkArray **ppMarkArray, int pitch, int speed, int volume, int pause, int dictidx, int texttype);
    
    /*
     _DllMode(PSYNCINFO_NEW) VT_AllocSyncInfo_New_ENG(void);
     _DllMode(void) VT_FreeSyncInfo_New_ENG(PSYNCINFO_NEW pSyncInfo);
     _DllMode(void) VT_InitSyncInfo_New_ENG(PSYNCINFO_NEW pSyncInfo);
     */
    
#endif
#if !defined(VT_INTERNAL_DEFINE)
#if !defined(VT_MULTI_BYTE_FOR_HIGHLIGHT)
#define VT_MULTI_BYTE_HIGHLIGHT     0
#endif
#if !defined(VT_UNICODE_FOR_HIGHLIGHT)
#define VT_UNICODE_HIGHLIGHT        1
#endif
    // eng
    _DllMode(void) VT_SetTextTypeForHighlight_ENG_Julie(int iTextType);
    _DllMode(void) VT_SetTextTypeForHighlight_ENG_Kate(int iTextType);
    _DllMode(void) VT_SetTextTypeForHighlight_ENG_Paul(int iTextType);
    _DllMode(void) VT_SetTextTypeForHighlight_ENG_James(int iTextType);
    _DllMode(void) VT_SetTextTypeForHighlight_ENG_Ashley(int iTextType);
    
    // bre
    _DllMode(void) VT_SetTextTypeForHighlight_BRE_Bridget(int iTextType);
    _DllMode(void) VT_SetTextTypeForHighlight_BRE_Hugh(int iTextType);
    
    // spa
    _DllMode(void) VT_SetTextTypeForHighlight_SPA_Violeta(int iTextType);
    _DllMode(void) VT_SetTextTypeForHighlight_SPA_Francisco(int iTextType);
    
    // cfr
    _DllMode(void) VT_SetTextTypeForHighlight_CFR_Chloe(int iTextType);
    
    // jpn
    _DllMode(void) VT_SetTextTypeForHighlight_JPN_Haruka(int iTextType);
    _DllMode(void) VT_SetTextTypeForHighlight_JPN_Sayaka(int iTextType);
    _DllMode(void) VT_SetTextTypeForHighlight_JPN_Misaki(int iTextType);
    _DllMode(void) VT_SetTextTypeForHighlight_JPN_Show(int iTextType);
    _DllMode(void) VT_SetTextTypeForHighlight_JPN_Ryo(int iTextType);
    _DllMode(void) VT_SetTextTypeForHighlight_JPN_Takeru(int iTextType);
    _DllMode(void) VT_SetTextTypeForHighlight_JPN_Hikari(int iTextType);
    
    // chi
    _DllMode(void) VT_SetTextTypeForHighlight_CHI_Hui(int iTextType);
    _DllMode(void) VT_SetTextTypeForHighlight_CHI_Liang(int iTextType);
    
    // kor
    _DllMode(void) VT_SetTextTypeForHighlight_KOR_Junwoo(int iTextType);
    _DllMode(void) VT_SetTextTypeForHighlight_KOR_Yumi(int iTextType);
    _DllMode(void) VT_SetTextTypeForHighlight_KOR_Hyeryun(int iTextType);
#endif
    
    /* 기본버전을 알아내는 함수 (예: Yumi-M16-FileIO) */
    // eng
    _DllMode(char *) VT_GetDefVersion_ENG_Julie(void);
    _DllMode(char *) VT_GetDefVersion_ENG_James(void);
    _DllMode(char *) VT_GetDefVersion_ENG_Kate(void);
    _DllMode(char *) VT_GetDefVersion_ENG_Paul(void);
    _DllMode(char *) VT_GetDefVersion_ENG_Ashley(void);
    
    // bre
    _DllMode(char *) VT_GetDefVersion_BRE_Bridget(void);
    _DllMode(char *) VT_GetDefVersion_BRE_Hugh(void);

    // spa
    _DllMode(char *) VT_GetDefVersion_SPA_Violeta(void);
    _DllMode(char *) VT_GetDefVersion_SPA_Francisco(void);
    
    // cfr
    _DllMode(char *) VT_GetDefVersion_CFR_Chloe(void);

    // jpn
    _DllMode(char *) VT_GetDefVersion_JPN_Haruka(void);
    _DllMode(char *) VT_GetDefVersion_JPN_Sayaka(void);
    _DllMode(char *) VT_GetDefVersion_JPN_Misaki(void);
    _DllMode(char *) VT_GetDefVersion_JPN_Show(void);
    _DllMode(char *) VT_GetDefVersion_JPN_Ryo(void);
    _DllMode(char *) VT_GetDefVersion_JPN_Takeru(void);
    _DllMode(char *) VT_GetDefVersion_JPN_Hikari(void);
    
    // chi
    _DllMode(char *) VT_GetDefVersion_CHI_Hui(void);
    _DllMode(char *) VT_GetDefVersion_CHI_Liang(void);

    // kor
    _DllMode(char *) VT_GetDefVersion_KOR_Yumi(void);
    _DllMode(char *) VT_GetDefVersion_KOR_Junwoo(void);
    _DllMode(char *) VT_GetDefVersion_KOR_Hyeryun(void);
    
    /* Version Number를 나타내는 변수 선언 */
    // eng
    _DllMode(int) VT_gVersionFirst_ENG_Julie;
    _DllMode(int) VT_gVersionSecond_ENG_Julie;
    _DllMode(int) VT_gVersionThird_ENG_Julie;
    _DllMode(int) VT_gVersionFourth_ENG_Julie;
    
    _DllMode(int) VT_gVersionFirst_ENG_James;
    _DllMode(int) VT_gVersionSecond_ENG_James;
    _DllMode(int) VT_gVersionThird_ENG_James;
    _DllMode(int) VT_gVersionFourth_ENG_James;
    
    _DllMode(int) VT_gVersionFirst_ENG_Kate;
    _DllMode(int) VT_gVersionSecond_ENG_Kate;
    _DllMode(int) VT_gVersionThird_ENG_Kate;
    _DllMode(int) VT_gVersionFourth_ENG_Kate;
    
    _DllMode(int) VT_gVersionFirst_ENG_Paul;
    _DllMode(int) VT_gVersionSecond_ENG_Paul;
    _DllMode(int) VT_gVersionThird_ENG_Paul;
    _DllMode(int) VT_gVersionFourth_ENG_Paul;
    
    _DllMode(int) VT_gVersionFirst_ENG_Ashley;
    _DllMode(int) VT_gVersionSecond_ENG_Ashley;
    _DllMode(int) VT_gVersionThird_ENG_Ashley;
    _DllMode(int) VT_gVersionFourth_ENG_Ashley;
    
    // bre
    _DllMode(int) VT_gVersionFirst_BRE_Bridget;
    _DllMode(int) VT_gVersionSecond_BRE_Bridget;
    _DllMode(int) VT_gVersionThird_BRE_Bridget;
    _DllMode(int) VT_gVersionFourth_BRE_Bridget;
    
    _DllMode(int) VT_gVersionFirst_BRE_Hugh;
    _DllMode(int) VT_gVersionSecond_BRE_Hugh;
    _DllMode(int) VT_gVersionThird_BRE_Hugh;
    _DllMode(int) VT_gVersionFourth_BRE_Hugh;
    
    // spa
    _DllMode(int) VT_gVersionFirst_SPA_Francisco;
    _DllMode(int) VT_gVersionSecond_SPA_Francisco;
    _DllMode(int) VT_gVersionThird_SPA_Francisco;
    _DllMode(int) VT_gVersionFourth_SPA_Francisco;
    
    _DllMode(int) VT_gVersionFirst_SPA_Violeta;
    _DllMode(int) VT_gVersionSecond_SPA_Violeta;
    _DllMode(int) VT_gVersionThird_SPA_Violeta;
    _DllMode(int) VT_gVersionFourth_SPA_Violeta;
    
    // cfr
    _DllMode(int) VT_gVersionFirst_CFR_Chloe;
    _DllMode(int) VT_gVersionSecond_CFR_Chloe;
    _DllMode(int) VT_gVersionThird_CFR_Chloe;
    _DllMode(int) VT_gVersionFourth_CFR_Chloe;
    
    // jpn
    _DllMode(int) VT_gVersionFirst_JPN_Haruka;
    _DllMode(int) VT_gVersionSecond_JPN_Haruka;
    _DllMode(int) VT_gVersionThird_JPN_Haruka;
    _DllMode(int) VT_gVersionFourth_JPN_Haruka;
    
    _DllMode(int) VT_gVersionFirst_JPN_Sayaka;
    _DllMode(int) VT_gVersionSecond_JPN_Sayaka;
    _DllMode(int) VT_gVersionThird_JPN_Sayaka;
    _DllMode(int) VT_gVersionFourth_JPN_Sayaka;
    
    _DllMode(int) VT_gVersionFirst_JPN_Misaki;
    _DllMode(int) VT_gVersionSecond_JPN_Misaki;
    _DllMode(int) VT_gVersionThird_JPN_Misaki;
    _DllMode(int) VT_gVersionFourth_JPN_Misaki;
    
    _DllMode(int) VT_gVersionFirst_JPN_Show;
    _DllMode(int) VT_gVersionSecond_JPN_Show;
    _DllMode(int) VT_gVersionThird_JPN_Show;
    _DllMode(int) VT_gVersionFourth_JPN_Show;
    
    _DllMode(int) VT_gVersionFirst_JPN_Ryo;
    _DllMode(int) VT_gVersionSecond_JPN_Ryo;
    _DllMode(int) VT_gVersionThird_JPN_Ryo;
    _DllMode(int) VT_gVersionFourth_JPN_Ryo;
    
    _DllMode(int) VT_gVersionFirst_JPN_Takeru;
    _DllMode(int) VT_gVersionSecond_JPN_Takeru;
    _DllMode(int) VT_gVersionThird_JPN_Takeru;
    _DllMode(int) VT_gVersionFourth_JPN_Takeru;
    
    _DllMode(int) VT_gVersionFirst_JPN_Hikari;
    _DllMode(int) VT_gVersionSecond_JPN_Hikari;
    _DllMode(int) VT_gVersionThird_JPN_Hikari;
    _DllMode(int) VT_gVersionFourth_JPN_Hikari;
    
    //chi
    _DllMode(int) VT_gVersionFirst_CHI_Hui;
    _DllMode(int) VT_gVersionSecond_CHI_Hui;
    _DllMode(int) VT_gVersionThird_CHI_Hui;
    _DllMode(int) VT_gVersionFourth_CHI_Hui;
    
    _DllMode(int) VT_gVersionFirst_CHI_Liang;
    _DllMode(int) VT_gVersionSecond_CHI_Liang;
    _DllMode(int) VT_gVersionThird_CHI_Liang;
    _DllMode(int) VT_gVersionFourth_CHI_Liang;
    
    // kor
    _DllMode(int) VT_gVersionFirst_KOR_Yumi;
    _DllMode(int) VT_gVersionSecond_KOR_Yumi;
    _DllMode(int) VT_gVersionThird_KOR_Yumi;
    _DllMode(int) VT_gVersionFourth_KOR_Yumi;
    
    _DllMode(int) VT_gVersionFirst_KOR_Junwoo;
    _DllMode(int) VT_gVersionSecond_KOR_Junwoo;
    _DllMode(int) VT_gVersionThird_KOR_Junwoo;
    _DllMode(int) VT_gVersionFourth_KOR_Junwoo;
    
    _DllMode(int) VT_gVersionFirst_KOR_Hyeryun;
    _DllMode(int) VT_gVersionSecond_KOR_Hyeryun;
    _DllMode(int) VT_gVersionThird_KOR_Hyeryun;
    _DllMode(int) VT_gVersionFourth_KOR_Hyeryun;

    
#if !defined(VT_INTERNAL_DEFINE)
#define VT_INTERNAL_DEFINE
#endif
    
#if defined(__cplusplus)
}
#endif

#endif /* VT_INTERNAL_ALL_H */