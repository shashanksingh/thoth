//
//  SpeechSynthesizer.m
//  OpenEars
//
//  Created by Shashank Singh on 9/14/14.
//  Copyright (c) 2014 Politepix. All rights reserved.
//

#import "SpeechSynthesizer.h"
#import "TTSService.h"
#import "Macros.h"

static const char* BACKGROUND_QUEUE_NAME = "com.briefrr.speech_synthesizer_background_q";

@interface QueuedSpeechUtterance : SpeechUtterance
@property (nonatomic) double preUtteranceDeadline;
@property (nonatomic) SInt16 *audioSamples;
@property (nonatomic) UInt32 audioSamplesCount;
@property (nonatomic) UInt32 currentSampleIndex;
@property (nonatomic) BOOL samplesFilled;
@end

@implementation QueuedSpeechUtterance

-(id)initWithSpeechUtterance:(SpeechUtterance*)speechUtterance {
    self = [super init];
    if (self) {
        self.speechText = speechUtterance.speechText;
        self.volume = speechUtterance.volume;
        self.pitch = speechUtterance.pitch;
        self.speed = speechUtterance.speed;
        self.preDelay = speechUtterance.preDelay;
        self.postDelay = speechUtterance.postDelay;
        self.voiceToUse = speechUtterance.voiceToUse;
        self.preUtteranceDeadline = -1;
        self.audioSamples = NULL;
        self.audioSamplesCount = -1;
        self.currentSampleIndex = 0;
        self.samplesFilled = NO;
    }
    return self;
}

-(void)dealloc {
    if (self.samplesFilled && self.audioSamples) {
        free(self.audioSamples);
    }
}

-(BOOL)wasPartiallyUttered {
    if (!self.samplesFilled) {
        return NO;
    }
    return self.audioSamplesCount > self.currentSampleIndex;
}

-(BOOL)wasFullyUttered {
    if (!self.samplesFilled) {
        return NO;
    }
    return self.audioSamplesCount <= self.currentSampleIndex;
}

-(BOOL)wasFullySkipped {
    return !self.samplesFilled || self.currentSampleIndex == 0;
}
@end

@interface SpeechSynthesizer ()
@property (nonatomic) BOOL speaking;
@property (strong, nonatomic) NSMutableArray* speechTextQueue;
@property (nonatomic) double preDelay;
@end

@implementation SpeechSynthesizer

+(SpeechSynthesizer*)sharedInstance {
    static SpeechSynthesizer *sharedInstance = nil;
    @synchronized(self) {
        if (sharedInstance == nil)
            sharedInstance = [[self alloc] init];
    }
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.speechTextQueue = [NSMutableArray array];
        self.speaking = NO;
        self.delegate = NULL;
        self.preDelay = 0;
    }
    return self;
}

-(dispatch_queue_t)getBackgroundQueue {
    static dispatch_once_t queueCreationGuard;
    static dispatch_queue_t queue;
    dispatch_once(&queueCreationGuard, ^{
        queue = dispatch_queue_create(BACKGROUND_QUEUE_NAME, DISPATCH_QUEUE_SERIAL);
    });
    return queue;
}

-(void)queueUtterance:(SpeechUtterance*)utterance {
    __block typeof(self) bself = self;
    QueuedSpeechUtterance *queuedUtterance = [[QueuedSpeechUtterance alloc] initWithSpeechUtterance:utterance];
    dispatch_async([self getBackgroundQueue], ^{
        [bself.speechTextQueue addObject:queuedUtterance];
        [bself resume];
    });
}

-(void)speakImmediately:(SpeechUtterance*)utterance {
    __block typeof(self) bself = self;
    QueuedSpeechUtterance *queuedUtterance = [[QueuedSpeechUtterance alloc] initWithSpeechUtterance:utterance];
    dispatch_async([self getBackgroundQueue], ^{
        [bself.speechTextQueue insertObject:queuedUtterance atIndex:0];
        [bself resume];
    });
}

-(void)pause {
    __block typeof(self) bself = self;
    dispatch_async([self getBackgroundQueue], ^{
        bself.speaking = NO;
    });
}

-(void)resume {
    __block typeof(self) bself = self;
    dispatch_async([self getBackgroundQueue], ^{
        bself.speaking = YES;
    });
}

-(NSUInteger)clearUtteranceQueue {
    __block NSUInteger queueSize = 0;
    __block typeof(self) bself = self;
    dispatch_async([self getBackgroundQueue], ^{
        [bself pause];
        
        queueSize = bself.speechTextQueue.count;
        
        while (bself.speechTextQueue.count > 0) {
            QueuedSpeechUtterance *utterance = [bself.speechTextQueue objectAtIndex:0];
            [bself notifyDelegateOnUtteranceDone:utterance];
            [bself.speechTextQueue removeObjectAtIndex:0];
        }
        
        // setting it to 0 causes "next" story to read right
        // after the feedback for next command
        //bself.preDelay = 0;
    });
    
    return queueSize;
}

-(void)notifyDelegateOnUtteranceDone:(QueuedSpeechUtterance*)utterance {
    if (!self.delegate) {
        return;
    }
    BOOL partial = [utterance wasPartiallyUttered];
    BOOL skipped = [utterance wasFullySkipped];
    
    __block typeof(self) bself = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [bself.delegate speechSyntheSizerFinishedUtterance:utterance wasPartiallyUttered:partial wasSkipped:skipped queuedUtteranceCount:bself.speechTextQueue.count];
    });
}

-(void)notifyDelegateOnUtteranceBegin:(QueuedSpeechUtterance*)utterance {
    if (!self.delegate) {
        return;
    }
    
    __block typeof(self) bself = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [bself.delegate speechSyntheSizerStartedUtterance:utterance];
    });
}

-(void)getAudioSamples:(UInt32)numSamples inArray:(SInt16**)samplesArray withLength:(UInt32*)samplesCount {
    
    __block typeof(self) bself = self;
    dispatch_sync([self getBackgroundQueue], ^{
        //NSLog(@"getAudioSamples: %d", self.speaking);
        // while paused we simply render silence
        if (!bself.speaking) {
            *samplesArray = NULL;
            *samplesCount = 0;
            return;
        }
        
        //NSLog(@"not empty: %lu", (unsigned long)bself.speechTextQueue.count);
        // queue is empty, render silence and toggle speaking flag
        if (bself.speechTextQueue.count == 0) {
            *samplesArray = NULL;
            *samplesCount = 0;
            bself.speaking = NO;
            return;
        }
        
        double now = [[NSDate date] timeIntervalSince1970];
        
        QueuedSpeechUtterance *currentUtterance = [bself.speechTextQueue objectAtIndex:0];
        // if this is the first time we are seeing this utterance make sure we
        // respect its own preDelay and the postDelay of any previous utterance
        if (currentUtterance.preUtteranceDeadline < 0) {
            double silenceInterval = MAX(currentUtterance.preDelay, bself.preDelay);
            bself.preDelay = 0;
            currentUtterance.preUtteranceDeadline = now + silenceInterval;
            [self notifyDelegateOnUtteranceBegin:currentUtterance];
        }
        [bself fillAudioSamplesForUtterance:currentUtterance];
        
        // we have already sent all the samples for the current utterance over
        // we'll now just wait for all the samples to be rendered before we
        // pick up the next one. this helps us have pre, post utterance delays
        // if required
        if ([currentUtterance wasFullyUttered]) {
            *samplesArray = NULL;
            *samplesCount = 0;
            return;
        }
        
        if (now < currentUtterance.preUtteranceDeadline) {
            //NSLog(@"delay inmposed silence: %f, %f", now, currentUtterance.preUtteranceDeadline);
            *samplesArray = NULL;
            *samplesCount = 0;
            return;
        }
        
        UInt32 moreSamplesAvailableInCurrentUtterance =
        currentUtterance.audioSamplesCount - currentUtterance.currentSampleIndex;
        UInt32 samplesToCopy = MIN(numSamples, moreSamplesAvailableInCurrentUtterance);
        
        // we don't copy the audio samples for the caller, the ownership of the
        // samples is retained here and the samples will freed when the utterance
        // is removed from the queue (in its dealloc)
        //NSLog(@"sampleArray: %u, %u", (unsigned int)currentUtterance.currentSampleIndex, (unsigned int)samplesToCopy);
        *samplesArray = &currentUtterance.audioSamples[currentUtterance.currentSampleIndex];
        *samplesCount = samplesToCopy;
        
        currentUtterance.currentSampleIndex += samplesToCopy;
    });
}

-(void)fillAudioSamplesForUtterance:(QueuedSpeechUtterance*)utterance {
    //NSLog(@"filling audio samples");
    if (utterance.samplesFilled) {
        return;
    }
    utterance.samplesFilled = YES;
    
    
    SInt16 *wavBuffer;
    unsigned long wavBufferSize;
    TTSService *ttsService = [TTSService sharedInstance];
    int ret = [ttsService getAudioSamplesForUtterance:utterance ttsEngine:DEFAULT outputBuffer:&wavBuffer outputBufferLength:&wavBufferSize];
    
    if (ret) {
        NSLog(@"error in getting audio samples for utterance: %d", (unsigned int)ret);
        utterance.audioSamples = NULL;
        utterance.audioSamplesCount = 0;
        return;
    }
    
    utterance.audioSamples = wavBuffer;
    utterance.audioSamplesCount = (UInt32)(wavBufferSize * sizeof(SInt16)/NUM_BYTES_PER_SAMPLE);
    
    //NSLog(@"audio samples for utterance: %@, %d", utterance.speechText,
    //(unsigned int)utterance.audioSamplesCount);
}

-(void)onAudioSamplesRenderd:(UInt32)numSamples {
    if (numSamples == 0) {
        return;
    }
    
    __block typeof(self) bself = self;
    dispatch_async([self getBackgroundQueue], ^{
        if (bself.speechTextQueue.count == 0) {
            return;
        }
        
        QueuedSpeechUtterance *currentUtterance;
        
        while (bself.speechTextQueue.count > 0) {
            currentUtterance = bself.speechTextQueue[0];
            // fill samples if not already filled
            [bself fillAudioSamplesForUtterance:currentUtterance];
            
            // all the text for this utterance hasn't fully been processed
            // yet
            if (![currentUtterance wasFullyUttered]) {
                break;
            }
            
            [bself.speechTextQueue removeObjectAtIndex:0];
            [bself notifyDelegateOnUtteranceDone:currentUtterance];
            
            bself.preDelay = currentUtterance.postDelay;
            // in case of a speakImmediate utterance we need to reset
            // the deadline of the utterance that was cut short so
            // that it is recalculated and any postDelay of an immediate
            // utterance can be supported.
            if (bself.speechTextQueue.count > 0) {
                QueuedSpeechUtterance *nextUtterance = bself.speechTextQueue[0];
                nextUtterance.preUtteranceDeadline = -1;
            }
        }
    });
}

-(void)prefetchAudioSamplesForUtterances:(NSArray*)utterances withCallback:(void(^)(BOOL))callback {
    [[TTSService sharedInstance] prefetchAudioSamplesForUtterances:utterances withCallback:callback];
}

@end
