//
//  TTSProvider.m
//  tts
//
//  Created by Shashank Singh on 10/16/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTSProvider.h"

@implementation TTSProvider
-(TTSProvider*)init {
    self = [super init];
    return self;
}

-(int)getAudioSamplesForUtterance:(SpeechUtterance*)utterance outputBuffer:(SInt16**)outputBuffer outputBufferLength:(unsigned long*)outputBufferLength {
    *outputBuffer = NULL;
    *outputBufferLength = 0;
    return 0;
}

-(void)prefetchAudioSamplesForUtterances:(NSMutableArray*)utterances withCallback:(void(^)(BOOL))callback {
    if (callback) {
        callback(NO);
    }
}

-(BOOL)hasLocalAudioSamplesForUtterance:(SpeechUtterance*)utterance {
    return NO;
}
@end