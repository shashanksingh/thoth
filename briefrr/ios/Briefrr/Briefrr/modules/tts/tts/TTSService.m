//
//  TTSService.m
//  tts
//
//  Created by Shashank Singh on 10/16/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import "TTSService.h"
#import "TTSProvider.h"
#import "NeoSpeechTTSProvider.h"
#import "NuanceTTSProvider.h"
#import "NeoSpeechNetworkTTSProvider.h"

@interface TTSService()
@property (nonatomic, strong) NSMutableDictionary* ttsEngineToProvider;
@end

@implementation TTSService
@synthesize ttsEngineToProvider;

-(TTSService*)init {
    self = [super init];
    if (self) {
        self.ttsEngineToProvider = [NSMutableDictionary dictionary];
    }
    return self;
}

+(TTSService*)sharedInstance {
    static TTSService *sharedInstance = nil;
    @synchronized(self) {
        if (sharedInstance == nil) {
            NSLog(@"TTSService:: initing");
            sharedInstance = [[self alloc] init];
        }
        
    }
    return sharedInstance;
}

-(TTSProvider*)getProviderForEngine:(TTSEngine)engine {
    if (engine == NEOSPEECH_EMBEDDED) {
        return [[NeoSpeechTTSProvider alloc] init];
    }
    
    if (engine == NEOSPEECH_NETWORK) {
        return [[NeoSpeechNetworkTTSProvider alloc] init];
    }
    
    if (engine == NUANCE) {
        return [[NuanceTTSProvider alloc] init];
    }
    
    if (engine != DEFAULT) {
        NSLog(@"TTSService:: unknown TTSEngine: %d, using default", engine);
    }
    
    // default is neospeech
    return [[NeoSpeechTTSProvider alloc] init];
}

-(TTSProvider*)getOrCreateProviderForEngine:(TTSEngine)ttsEngine {
    TTSProvider *provider = self.ttsEngineToProvider[@(ttsEngine)];
    if (!provider) {
        NSLog(@"TTSService:: creating engine for provider: %d, %@.", ttsEngine, self.ttsEngineToProvider);
        provider = [self getProviderForEngine:ttsEngine];
        self.ttsEngineToProvider[@(ttsEngine)] = provider;
    }
    return provider;
}

-(int)getAudioSamplesForUtterance:(SpeechUtterance*)utterance ttsEngine:(TTSEngine)ttsEngine outputBuffer:(SInt16**)outputBuffer outputBufferLength:(unsigned long*)outputBufferLength {
    
    
    // if the engine is not specified and a remote engine has cached audio samples
    // for this utterance, assuming remote audio is always of better quality
    // return remote audio
    if (ttsEngine == DEFAULT) {
        // TODO(shashank): This logic means one instance of each provider will
        // always be creatd which can be costly
        for (NSNumber *engine in @[@(NEOSPEECH_NETWORK)/*, @(NUANCE)*/]) {
            TTSProvider *remoteProvider = [self getOrCreateProviderForEngine:engine.intValue];
            if ([remoteProvider hasLocalAudioSamplesForUtterance:utterance]) {
                NSLog(@"TTSService: already cached audio found for utterance: using it: %@", utterance.speechText);
                return [remoteProvider getAudioSamplesForUtterance:utterance outputBuffer:outputBuffer outputBufferLength:outputBufferLength];
            }
        }
    }
    
    NSLog(@"TTSService: no cached audio found, using embedded tts engine");
    TTSProvider *provider = [self getOrCreateProviderForEngine:ttsEngine];
    return [provider getAudioSamplesForUtterance:utterance outputBuffer:outputBuffer outputBufferLength:outputBufferLength];
    
}

-(void)prefetchAudioSamplesForUtterances:(NSArray*)utterances withCallback:(void(^)(BOOL))callback {
    NSLog(@"prefetch in ttsservice");
    for (SpeechUtterance *utterance in utterances) {
        NSLog(@"uttr text: %@", utterance.speechText);
    }
    TTSProvider *provider = [self getOrCreateProviderForEngine:NEOSPEECH_NETWORK];
    [provider prefetchAudioSamplesForUtterances:utterances withCallback:callback];
}

- (NSString*)getNameForEngine:(TTSEngine)engine {
    switch (engine) {
        case NEOSPEECH_EMBEDDED:
            return @"neospeech_embedded";
        case NEOSPEECH_NETWORK:
            return @"neospeech_network";
        case NUANCE:
            return @"nuance";
        default:
            return NULL;
    }
}

- (NSString*)getLicenseFilePathForEngine:(TTSEngine)engine {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryDirectory = [paths objectAtIndex:0];
    
    NSString *engineName = [self getNameForEngine:engine];
    if (!engineName) {
        return NULL;
    }
    NSString *fileName = [engineName stringByAppendingString:@"_verification.txt"];
    return [libraryDirectory stringByAppendingPathComponent:fileName];
}
@end
