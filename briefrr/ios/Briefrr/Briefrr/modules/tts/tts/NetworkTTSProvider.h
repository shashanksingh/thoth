//
//  NetworkTTSProvider.h
//  tts
//
//  Created by Shashank Singh on 12/12/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import "TTSProvider.h"

@interface NetworkTTSProvider : TTSProvider
-(NSURLRequest*)getRequestForUtterance:(SpeechUtterance*)utterance;
@end
