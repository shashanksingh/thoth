//
//  NetworkTTSProvider.m
//  tts
//
//  Created by Shashank Singh on 12/12/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import "NetworkTTSProvider.h"
#import "AFNetworking.h"
#import "TTSUtil.h"
#import "EventLogger.h"

static NSString * const kBackgroundDownloadSessionId = @"2d97e112-5de8-446d-89d0-30f0a1b07d27";
static NSString * const kUtteranceIdJoinKey = @"c42c7f23-6e54-4daa-84f1-f1ddd8e4ab38";

// Note(shashank): 2 while in trial
static NSTimeInterval const kMaxParallelFetches = 2;

static NSString * const kCacheDirectoryName = @"network_tts_cache";
static UInt16 const kMaxAudioSamplesCacheFiles = 100;

// min size of a valid audio samples file, nuance can in some cases return 8 bytes
// of invalid data with a status of 200
static long long const kMinAudioSamplesFileSize = 10;


@interface NetworkTTSProvider()
@property (strong, nonatomic) AFHTTPSessionManager *downloadSessionManager;
@end

@implementation NetworkTTSProvider
@synthesize downloadSessionManager;

-(NetworkTTSProvider*)init {
    self = [super init];
    if (self) {
        [self initDownloadSessionManager];
    }
    return self;
}

-(void)dealloc {
    [self stopMonitoringNetwork];
}

-(int)getAudioSamplesForUtterance:(SpeechUtterance*)utterance outputBuffer:(SInt16**)outputBuffer outputBufferLength:(unsigned long*)outputBufferLength {
    NSString *audioFilePath = [self getAudioFilePathForSpeechUtterance:utterance];
    if (!audioFilePath || ![self fileExistsAtPath:audioFilePath]) {
        NSLog(@"Error: NetworkTTSProvider:: no cached samples found for utterance: %@", utterance);
        *outputBuffer = NULL;
        *outputBufferLength = 0;
        return 1;
    }
    return [TTSUtil getAudioSamplesFromFile:audioFilePath outputBuffer:outputBuffer outputBufferLength:outputBufferLength];
    
}

-(NSURLRequest*)getRequestForUtterance:(SpeechUtterance*)utterance {
    NSLog(@"Error: NetworkTTSProvider:: base class does not defined getRequestForUtterance, use a concrete subclass");
    return nil;
}

-(void)stopMonitoringNetwork {
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
}

-(void)prefetchAudioSamplesForUtterances:(NSArray*)utterances withCallback:(void(^)(BOOL))callback {
    AFNetworkReachabilityStatus status = [AFNetworkReachabilityManager sharedManager].networkReachabilityStatus;
    if (status == AFNetworkReachabilityStatusReachableViaWiFi || status == AFNetworkReachabilityStatusReachableViaWWAN) {
        [self processUtterancePrefetchQueue:utterances withCallback:callback];
    } else if (status == AFNetworkReachabilityStatusUnknown) {
        __block typeof(self) bself = self;
        [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            if (!bself) {
                return;
            }
            [bself processUtterancePrefetchQueue:utterances withCallback:callback];
        }];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    } else {
        NSLog(@"network reachability known but not WIFI, skipping prefetching, status: %d", (int)status);
        callback(NO);
    }
}

-(BOOL)hasLocalAudioSamplesForUtterance:(SpeechUtterance*)utterance {
    NSString *audioFilePath = [self getAudioFilePathForSpeechUtterance:utterance];
    BOOL exists = [self validAudioSamplesFileExistsAtPath:audioFilePath];
    NSLog(@"cached audio file existance: %@, %d", audioFilePath, exists);
    return exists;
}

-(void)initDownloadSessionManager {
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:kBackgroundDownloadSessionId];
    // round about way of limiting parallel downloads: http://stackoverflow.com/a/21422293
    config.HTTPMaximumConnectionsPerHost = kMaxParallelFetches;
    
    self.downloadSessionManager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:config];
    NSLog(@"Initializing downloadSessionManager, %lu tasks found", (unsigned long)self.downloadSessionManager.downloadTasks.count);
    for (NSURLSessionDownloadTask *downloadTask in self.downloadSessionManager.downloadTasks) {
        [downloadTask resume];
    }
    
    
    [self.downloadSessionManager setDownloadTaskDidFinishDownloadingBlock:^NSURL *(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, NSURL *location) {
        
        NSString *utteranceHash = downloadTask.taskDescription;
        NSString *path = [NetworkTTSProvider getCachedAudioFilePathForUtteranceHash:utteranceHash];
        NSLog(@"download finished: %@, saving at: %@", utteranceHash, path);
        return [NSURL fileURLWithPath:path];
    }];
}

-(BOOL)validAudioSamplesFileExistsAtPath:(NSString*)audioFilePath {
    return audioFilePath && [self fileExistsAtPath:audioFilePath] && [self isAudioSamplesFileValid:audioFilePath];
}

-(NSString*)getAudioFilePathForSpeechUtterance:(SpeechUtterance*)utterance {
    NSString *hash = [self.class getSpeechUtteranceHash:utterance];
    if (!hash) {
        return NULL;
    }
    return [self.class getCachedAudioFilePathForUtteranceHash:hash];
}

+(NSString*)getApplicationSupportDirectory {
    // NSApplicationSupportDirectory
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

-(BOOL)fileExistsAtPath:(NSString*)filePath {
    return [[NSFileManager defaultManager] fileExistsAtPath:filePath];
}

-(BOOL)removeFileAtPath:(NSString*)filePath {
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
    if (error) {
        NSLog(@"Error: NetworkTTSProvider:: error in deleting file at path: %@, error: %@", filePath, error);
        return NO;
    }
    return YES;
}

-(long long)getFileSizeAtPath:(NSString*)filePath {
    NSError *error;
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:&error];
    if (error) {
        NSLog(@"Error: NetworkTTSProvider:: error in getting file attributes: %@", error);
        return -1;
    }
    
    NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
    return [fileSizeNumber longLongValue];
}

+(NSString*)getCacheDirectoryPath {
    NSString *appSuppDirPath = [self getApplicationSupportDirectory];
    return [appSuppDirPath stringByAppendingPathComponent:kCacheDirectoryName];
}

-(BOOL)ensureCacheDirectory {
    NSString *cacheDir = [self.class getCacheDirectoryPath];
    BOOL dirExists = [self fileExistsAtPath:cacheDir];
    if (dirExists) {
        return YES;
    }
    NSError *error;
    [[NSFileManager defaultManager] createDirectoryAtPath:cacheDir withIntermediateDirectories:YES attributes:nil error:&error];
    if (error) {
        NSLog(@"Warning:: NetworkTTSProvider: error in creating tts cache directory: %@", error);
        return NO;
    }
    return YES;
}

+(NSString*)getSpeechUtteranceHash:(SpeechUtterance*)utterance {
    NSString *voiceToUse = utterance.voiceToUse;
    if (!voiceToUse) {
        voiceToUse = @"default";
    }
    
    NSArray *parts = @[utterance.speechText, voiceToUse, @(utterance.speed), @(utterance.pitch)];
    NSString *plainTextRepresentation = [parts componentsJoinedByString:kUtteranceIdJoinKey];
    NSString *hash = [TTSUtil getMD5:plainTextRepresentation];
    NSLog(@"utterance hash: %@, %@", hash, plainTextRepresentation);
    return hash;
}

+(NSString*)getCachedAudioFilePathForUtteranceHash:(NSString*)utteranceHash {
    NSString *cacheDirPath = [self getCacheDirectoryPath];
    NSString *fileName = [NSString stringWithFormat:@"%@.m4a", utteranceHash];
    return [cacheDirPath stringByAppendingPathComponent:fileName];
}

-(void)processUtterancePrefetchQueue:(NSArray *)utterances withCallback:(void(^)(BOOL))callback {
    NSLog(@"prefetch utterances: %@", utterances);
    [[EventLogger getInstance] logEvent:@"tts_background_prefetch_inited" properties:@{@"num_utterances": @(utterances.count)}];
    
    for (SpeechUtterance *utterance in utterances) {
        
        NSLog(@"prefetching utterance: %@", utterance.speechText);
        if (utterance.speechText.length == 0) {
            continue;
        }
        NSString *hash = [self.class getSpeechUtteranceHash:utterance];
        NSString *audioFilePath = [self.class getCachedAudioFilePathForUtteranceHash:hash];
        if ([self validAudioSamplesFileExistsAtPath:audioFilePath]) {
            NSLog(@"NetworkTTSProvider:: utterance samples already cached, skipping: %@", audioFilePath);
            continue;
        }
        
        NSURLRequest *request = [self getRequestForUtterance:utterance];
        if (!request) {
            continue;
        }
        
        NSURLSessionDownloadTask *downloadTask = [self.downloadSessionManager downloadTaskWithRequest:request progress:nil destination:nil completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
            if (error) {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                [[EventLogger getInstance] logEvent:@"tts_background_prefetch_failed" properties:@{@"error": error.localizedDescription, @"utterance": utterance.speechText, @"url": request.URL ? request.URL.absoluteString : @"<nil>", @"status_code": @(httpResponse.statusCode), @"utterance_text": utterance.speechText ? utterance.speechText : @"<nil>"}];
            }
        }];
        downloadTask.taskDescription = hash;
        [downloadTask resume];
    }
    
    // Note (shashank): This is not the most optimal place to trim
    // the cache but since we are allowing downloads to spill over
    // to background this is the only time we can reliably do it
    [self trimAudioSampleFilesCache];
    callback(YES);
}

-(BOOL)isAudioSamplesFileValid:(NSString*)filePath {
    return [self getFileSizeAtPath:filePath] >= kMinAudioSamplesFileSize;
}

-(void)trimAudioSampleFilesCache {
    if (![self ensureCacheDirectory]) {
        NSLog(@"Warning:: NetworkTTSProvider:: failed to ensureCacheDirectory, skipping initTTSCacheIndex");
        return;
    }
    NSString *cacheDir = [self.class getCacheDirectoryPath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    NSArray * fileNames = [fileManager contentsOfDirectoryAtPath:cacheDir error:&error];
    if (error) {
        NSLog(@"Error: NetworkTTSProvider:: error in getting contents of cache dir at: %@, error: %@", cacheDir, error);
    }
    
    NSMutableDictionary *utteranceHashToModificationTime = [NSMutableDictionary dictionary];
    UInt16 numAudioSampleFiles = 0;
    NSMutableSet *removedUtteranceHashes = [NSMutableSet set];
    
    for (NSString *fileName in fileNames) {
        //NSLog(@"file in tts cache: %@", fileName);
        
        if ([fileName characterAtIndex:0] != 0 && [fileName.pathExtension isEqualToString:@"m4a"]) {
            NSString *utteranceHash = [fileName stringByDeletingPathExtension];
            NSString *fullPath = [cacheDir stringByAppendingPathComponent:fileName];
            NSDate *modificationTime = [TTSUtil getFileModificationTime:fullPath];
            if (!modificationTime) {
                [removedUtteranceHashes addObject:utteranceHash];
                [self removeFileAtPath:fullPath];
                continue;
            }
            
            utteranceHashToModificationTime[utteranceHash] = modificationTime;
            numAudioSampleFiles++;
        }
    }
    
    if (numAudioSampleFiles <= kMaxAudioSamplesCacheFiles) {
        return;
    }
    
    // sort older to newer
    NSArray *sortedUtteranceHashes = [utteranceHashToModificationTime keysSortedByValueUsingComparator:^NSComparisonResult(NSDate *dateA, NSDate *dateB) {
        return dateA.timeIntervalSince1970 - dateB.timeIntervalSince1970;
    }];
    
    UInt16 toDelete = sortedUtteranceHashes.count - kMaxAudioSamplesCacheFiles;
    for (UInt16 i=0; i<toDelete; i++) {
        NSString *utteranceHash = sortedUtteranceHashes[i];
        NSString *filePath = [self.class getCachedAudioFilePathForUtteranceHash:utteranceHash];
        [self removeFileAtPath:filePath];
    }
}

@end
