//
//  TTSProvider.h
//  tts
//
//  Created by Shashank Singh on 10/16/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SpeechUtterance.h"

@interface TTSProvider : NSObject
-(TTSProvider*)init;
-(int)getAudioSamplesForUtterance:(SpeechUtterance*)utterance outputBuffer:(SInt16**)outputBuffer outputBufferLength:(unsigned long*)outputBufferLength;
-(void)prefetchAudioSamplesForUtterances:(NSArray*)utterances withCallback:(void(^)(BOOL))callback;
-(BOOL)hasLocalAudioSamplesForUtterance:(SpeechUtterance*)utterance;
@end
