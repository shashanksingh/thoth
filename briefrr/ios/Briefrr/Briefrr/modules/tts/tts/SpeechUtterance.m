//
//  SpeechUtterance.m
//  tts
//
//  Created by Shashank Singh on 10/25/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import "SpeechUtterance.h"

@implementation SpeechUtterance
- (void)setSpeechText:(NSString *)speechText {
    _speechText = [self normalizeTextForSpeechSynthesizer:speechText];
}

- (NSString*)normalizeTextForSpeechSynthesizer:(NSString*)text {
    NSUInteger length = text.length;
    NSMutableString *normalizedString = [NSMutableString stringWithCapacity:length];
    unichar buffer[length + 1];
    [text getCharacters:buffer range:(NSRange){0, length}];
    for(NSUInteger i = 0; i < length; i++) {
        unichar current = buffer[i];
        switch(current) {
            case 8216:
            case 8217:
            case 96:
                [normalizedString appendString:@"'"];
                break;
            case 8220:
            case 8221:
            case 34:
                [normalizedString appendString:@"\""];
                break;
            case 8211:
                [normalizedString appendString:@"-"];
                break;
            default:
                [normalizedString appendFormat:@"%c", current];
                break;
        }
    }
    return normalizedString;
    
}
@end
