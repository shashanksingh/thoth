//
//  RecognitionModelManager.m
//  SpeechRec
//
//  Created by Shashank Singh on 11/3/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import "RecognitionModelManager.h"

#import <CommonCrypto/CommonDigest.h>
#import "FileSearcher.h"

#define kModelVersion @"21" // used to bust gram/dic cache

#define kSpeechRecBundleRootName @"SpeechRecResources_"
#define kLanguageModelDictFileName @"LanguageModel.dic"
#define kLanguageCode @"en_US" // fixed for now, should be an incoming param in the future
#define kResourcesBundleAcousticModelPath @"AcousticModel_ptm"

#define LANG_MODEL_GIBBERISH_KEY @"G"
#define ALL_PHONEMES_ENGLISH @[@"IY",@"AW",@"DH",@"Y",@"HH",@"CH",@"JH",@"ZH",@"D",@"NG",@"TH",@"AA",@"B",@"AE",@"EH",@"G",@"F",@"AH",@"K",@"M",@"L",@"AO",@"N",@"P",@"S",@"R",@"EY",@"T",@"W",@"V",@"AY",@"Z",@"ER",@"IH",@"UW",@"SH",@"UH",@"OY",@"OW"];


@implementation RecognitionModelManager

+(RecognitionModelManager*)sharedInstance {
    static RecognitionModelManager *sharedInstance = nil;
    @synchronized(self) {
        if (sharedInstance == nil)
            sharedInstance = [[self alloc] init];
    }
    return sharedInstance;
}

-(NSString*)getApplicationSupportDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) lastObject];
}

-(BOOL) fileExistsAtPath:(NSString*)filePath {
    return [[NSFileManager defaultManager] fileExistsAtPath:filePath];
}

-(NSString*) getMD5Hash:(NSString*)clearText {
    // Create pointer to the string as UTF8
    const char *ptr = [clearText UTF8String];
    
    // Create byte array of unsigned chars
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    // Create 16 byte MD5 hash value, store in buffer
    CC_MD5(ptr, strlen(ptr), md5Buffer);
    
    // Convert MD5 value in the buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}

-(NSString*)getModelFileNameForCommands:(NSArray*)commands {
    NSMutableArray *capitalizedCommands = [NSMutableArray array];
    for (NSString *command in commands) {
        [capitalizedCommands addObject:[command uppercaseString]];
    }
    
    // TODO (Shashank): use case sensitive comparison
    NSArray *normalizedCommands = [capitalizedCommands sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSString *hashInput = [normalizedCommands componentsJoinedByString:@"_"];
    hashInput = [hashInput stringByAppendingString:kModelVersion];
    return [self getMD5Hash:hashInput];
}

-(NSString*)getFilePathInAppSupportDirForFileName:(NSString*)fileName andExtension:(NSString*)extension {
    fileName = [fileName stringByAppendingString:[NSString stringWithFormat:@".%@", extension]];
    return [[self getApplicationSupportDirectory] stringByAppendingPathComponent:fileName];
}

-(NSString*)getExistingModelFilePathWithFileName:(NSString*)fileName andExtension:(NSString*)extension {
    NSString *resourceFilePath = [NSString stringWithFormat:@"grammars/%@", fileName];
    
    NSBundle *resourceBundle = [NSBundle mainBundle];
    NSString *filePath = [resourceBundle pathForResource:resourceFilePath ofType: extension];
    if ([self fileExistsAtPath:filePath]) {
        NSLog(@"found existing grammar file in resources at path: %@", filePath);
        return filePath;
    }
    
    filePath = [self getFilePathInAppSupportDirForFileName:fileName andExtension:extension];
    if ([self fileExistsAtPath:filePath]) {
        NSLog(@"found existing grammar file in app support dir at path: %@", filePath);
        return filePath;
    }
    return NULL;
}

-(NSDictionary*)getGarbagePhones {
    NSArray *allPhones = ALL_PHONEMES_ENGLISH;
    NSMutableDictionary *rv = [NSMutableDictionary dictionary];
    
    for (int i=0; i<allPhones.count; i++) {
        NSString *phone = allPhones[i];
        if (phone.length == 0) {
            continue;
        }
        
        NSString *name = [NSString stringWithFormat:@"%@_%d", LANG_MODEL_GIBBERISH_KEY, i];
        rv[name] = phone;
    }
    
    return rv;
}

-(NSString*)generateGrammarFileContentForCommands:(NSArray*)commands {
    NSMutableSet *allWords = [NSMutableSet set];
    
    for (NSString *command in commands) {
        NSArray *words = [command componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [allWords addObjectsFromArray:words];
    }
    
    return [[allWords allObjects] componentsJoinedByString:@"\n"];
    
    NSMutableArray *lines = [NSMutableArray array];
    [lines addObject:@"#JSGF V1.0;"];
    [lines addObject:@"grammar dynamically_generated_briefrr_grammar;"];
    
    NSDictionary *garbagePhones = [self getGarbagePhones];
    NSArray *garbageNames = [garbagePhones allKeys];
    garbageNames = [garbageNames sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
    NSString *joinedGarbageNames = [garbageNames componentsJoinedByString:@" | "];
    
    NSString *joinedCommands = [commands componentsJoinedByString:@" | "];
    [lines addObject:[NSString stringWithFormat:@"<command> = ( %@ );", joinedCommands]];
    [lines addObject:[NSString stringWithFormat:@"<gibberish> = ( %@ );", joinedGarbageNames]];
    [lines addObject:@"public <commands> = ( /1/<command> | /1e-40/<gibberish>);"];
    
    return [lines componentsJoinedByString:@"\n"];
}

-(NSString*)getResourcesBundlePathForLanguage:(NSString*)languageCode {
    NSString *suffix = [languageCode uppercaseString];
    NSString *bundleName = [NSString stringWithFormat:@"%@%@", kSpeechRecBundleRootName, suffix];
    NSString *pathToBundle = [[[NSBundle mainBundle] URLForResource:bundleName withExtension:@"bundle"] path];
    
    if (![self fileExistsAtPath:pathToBundle]) {
        NSLog(@"Error: unable to find resources bundle at :%@", pathToBundle);
        return NULL;
    }
    return pathToBundle;
}

-(NSString*)getLanguageModelDictionaryForLanguage:(NSString*)languageCode {
    NSString *bundlePath = [self getResourcesBundlePathForLanguage:languageCode];
    if (!bundlePath) {
        return NULL;
    }
    NSString *langModelPath = [bundlePath stringByAppendingPathComponent:kLanguageModelDictFileName];
    if (![self fileExistsAtPath:langModelPath]) {
        NSLog(@"Error: missing langugage model dictionary at: %@", langModelPath);
        return NULL;
    }
    return langModelPath;
}

-(NSDictionary*)getPronunciationsForWords:(NSArray*)words {
    NSString *langModelDictPath = [self getLanguageModelDictionaryForLanguage:kLanguageCode];
    if (!langModelDictPath) {
        return NULL;
    }
    
    NSMutableDictionary *wordToPronunciationsMap= [NSMutableDictionary dictionary];
    
    // TODO (shashank): handle all pronunciations (andMatchAllLinesWithSameRoot = TRUE)
    // TODO (shashank): handle OOV words
    NSDictionary *knownWordToLines = [FileSearcher getLinesStartingWith:words
                                        inFileAtPath:langModelDictPath
                                           andMatchAllLinesWithSameRoot:NO];
    
    for (NSString *word in knownWordToLines) {
        NSArray *lines = [knownWordToLines objectForKey:word];
        NSMutableArray *pronunciations = [NSMutableArray array];
        for (NSString *line in lines) {
            NSString *pronunciation = [line componentsSeparatedByString:@"\t"][1];
            [pronunciations addObject:pronunciation];
        }
        [wordToPronunciationsMap setObject:pronunciations forKey:word];
    }
    return wordToPronunciationsMap;
}

-(NSString*)generateDictionaryFileContentForCommands:(NSArray*)commands {
    
    NSMutableSet *commandWordSet = [NSMutableSet set];
    for (NSString *command in commands) {
        
        NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
        NSString *upperCaseCommand = [command uppercaseString];
        NSArray *words = [upperCaseCommand componentsSeparatedByCharactersInSet:whitespaces];
        
        words = [words filteredArrayUsingPredicate: [NSPredicate predicateWithFormat:@"SELF != ''"]];
        [commandWordSet addObjectsFromArray:words];
    }
    
    NSArray *commandWords = [commandWordSet allObjects];
    NSDictionary *wordToPronunciationsMaps = [self getPronunciationsForWords:commandWords];
    NSMutableArray *lines = [NSMutableArray array];
    for (NSString *wordToMatch in commandWords) {
        NSArray *pronunciations = [wordToPronunciationsMaps objectForKey:wordToMatch];
        if (!pronunciations) {
            NSLog(@"Error: no pronunciations found for word: %@, skipping it", wordToMatch);
            continue;
        }
        
        for (uint i=0; i<pronunciations.count; ++i) {
            NSString *pronunciation = [pronunciations objectAtIndex:i];
            NSString *word = wordToMatch;
            if (i > 0) {
                NSString *suffix = [NSString stringWithFormat:@"(%d)", i + 1];
                word = [word stringByAppendingString:suffix];
            }
            
            NSString *line = [NSString stringWithFormat:@"%@\t%@", word, pronunciation];
            [lines addObject:line];
        }
    }
    
    // add garbage phoneme mapping
    NSDictionary *grabagePhones = [self getGarbagePhones];
    for (NSString *name in grabagePhones) {
        NSString *value = grabagePhones[name];
        [lines addObject:[NSString stringWithFormat:@"%@\t%@", name, value]];
    }
    
    return [lines componentsJoinedByString:@"\n"];
}

-(NSString*)getModelFilePathForCommands:(NSArray*)commands andType:(NSString*)type {
    NSString *fileName = [self getModelFileNameForCommands:commands];
    NSString *extension = type;
    
    NSString *existingPath = [self getExistingModelFilePathWithFileName:fileName andExtension:extension];
    if (FALSE && existingPath) {
        return existingPath;
    }
    
    NSString *fileContent;
    if ([type isEqualToString:@"gram"]) {
        fileContent = [self generateGrammarFileContentForCommands:commands];
    } else {
        fileContent = [self generateDictionaryFileContentForCommands:commands];
    }
    
    NSString *filePath = [self getFilePathInAppSupportDirForFileName:fileName andExtension:extension];
    NSData *fileData = [fileContent dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error;
    [fileData writeToFile:filePath options:NSDataWritingAtomic error:&error];
    if (error != NULL) {
        NSLog(@"Error: failed to write model file at path: %@, %@", filePath, error);
        return NULL;
    }
    return filePath;
}

-(NSString*)getGrammarFilePathForCommands:(NSArray*)commands {
    return [self getModelFilePathForCommands:commands andType:@"gram"];
}

-(NSString*)getDictionaryFilePathForCommands:(NSArray*)commands {
    return [self getLanguageModelDictionaryForLanguage:kLanguageCode];
    //return [self getModelFilePathForCommands:commands andType:@"dic"];
}

-(NSString*)getAcousticModelPath {
    NSString *resourcesBundlePath = [self getResourcesBundlePathForLanguage:kLanguageCode];
    if (!resourcesBundlePath) {
        return NULL;
    }
    return [resourcesBundlePath stringByAppendingPathComponent:kResourcesBundleAcousticModelPath];
}

@end
