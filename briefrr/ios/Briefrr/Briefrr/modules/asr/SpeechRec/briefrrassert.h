//
//  briefrrassert.h
//  SpeechRec
//
//  Created by Shashank Singh on 11/2/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#ifndef SpeechRec_briefrrassert_h
#define SpeechRec_briefrrassert_h

#include <sys/cdefs.h>
#ifdef __cplusplus
#include <stdlib.h>
#endif /* __cplusplus */

#ifdef SHOW64BITCOMPLAINTS
#else
#if __LP64__ // This is only significant for 64-bit compilations -- prefer to keep it limited.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wshorten-64-to-32" // We are turning off warnings about 64 bit shortening because it isn't harming behavior, but it's reversible for troubleshooting.
#endif
#endif

#import "err.h"
/*
 * Unlike other ANSI header files, <assert.h> may usefully be included
 * multiple times, with and without NDEBUG defined.
 */

#undef briefrr_assert
#undef __briefrr_assert

#ifdef NDEBUG
// this isn't compiled
#define	briefrr_assert(e)	((void)0)
#else
// this is compiled
#ifndef __GNUC__
// this isn't compiled
__BEGIN_DECLS
#ifndef __cplusplus
// this isn't compiled
void abort(void) __dead2;
#endif /* !__cplusplus */
int  printf(const char * __restrict, ...);
__END_DECLS
// this isn't compiled
#define briefrr_assert(e)  \
((void) ((e) ? 0 : __briefrr_assert (#e, OELINEMACRO)))
#define __briefrr_assert(e, line) \
((void)printf ("%u: failed briefrr_assertion `%s'\n", line, e), (void)0))

#else /* __GNUC__ */
// this is compiled
__BEGIN_DECLS
// this is compiled
void __briefrr_assert_rtn(const char *, int, const char *) __dead2;
#if defined(__ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__) && ((__ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__-0) < 1070)
// this is not compiled
void __eprintf(const char *, unsigned, const char *) __dead2;
#endif
__END_DECLS

#if defined(__ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__) && ((__ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__-0) < 1070)
// this isn't compiled
#define __briefrr_assert(e, line) \
__eprintf ("%u: failed briefrr_assertion `%s'\n", line, e)
#else
// this is compiled
/* 8462256: modified __assert_rtn() replaces deprecated __eprintf() */
#define __briefrr_assert(e, line) \
__briefrr_assert_rtn ((const char *)-1L, line, e)
#endif

#if __DARWIN_UNIX03
// this is compiled
#define briefrr_assert(e)  \
((void) ((e) ? 0 : __briefrr_assert (#e, OELINEMACRO)))
#undef __briefrr_assert
#define __briefrr_assert(e, line) \
((void)printf ("%u: failed briefrr_assertion `%s'\n", line, e), ((void)0))
#else /* !__DARWIN_UNIX03 */
// this isn't compiled
#define briefrr_assert(e)  \
(__builtin_expect(!(e), 0) ? __briefrr_assert (#e, OELINEMACRO) : (void)0)
#endif /* __DARWIN_UNIX03 */

#endif /* __GNUC__ */
#endif /* NDEBUG */


#endif
