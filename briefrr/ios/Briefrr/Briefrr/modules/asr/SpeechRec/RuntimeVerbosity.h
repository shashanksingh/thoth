//
//  RuntimeVerbosity.h
//  SpeechRec
//
//  Created by Shashank Singh on 11/2/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#ifndef SpeechRec_RuntimeVerbosity_h
#define SpeechRec_RuntimeVerbosity_h

int verbose_pocketsphinx = 1; // The only place this can be altered is in a single method in PocketsphinxController. Every other class and module just reads it.
int verbose_cmuclmtk = 0;// The only place this can be altered is in a single method in LangugeModelGenerator. Every other class and module just reads it.

#endif
