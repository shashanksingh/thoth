//
//  SpeechManager.h
//  SpeechRec
//
//  Created by Shashank Singh on 11/2/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VCRService : NSObject

+(VCRService*)sharedInstance;
-(void)setCommandsToRecognize:(NSArray*)commands withWakeUpPhrase:(NSString*)wakeUpPhrase;
-(void)pauseRecognition;
-(void)resumeRecognition;
-(double)getInputGainLevel;
@end
