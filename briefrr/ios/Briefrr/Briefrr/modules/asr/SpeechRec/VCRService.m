//
//  SpeechManager.m
//  SpeechRec
//
//  Created by Shashank Singh on 11/2/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import "VCRService.h"
#import "RuntimeVerbosity.h"
#import "briefrrassert.h"
#import "AudioSubsystem.h"
#import "RecognitionModelManager.h"
#import "Logging.h"
#import "AudioRecorder.h"
#import "EventLogger.h"
#import "Util.h"
#import "UserPreferenceService.h"
#import <AFNetworking/AFNetworking.h>
#import "EventManager.h"
#import "Macros.h"
#import "PocketSphinxVCR.h"
#import "NuanceVCR.h"

static const NSTimeInterval MAX_COMMAND_RECOGNITION_TIME = 3.0;

@interface CommandInfo : NSObject
@property (nonatomic, retain) NSString *command;
@property (nonatomic) SInt32 score;
@property (nonatomic, strong) NSString *recognizerName;
@end

@implementation CommandInfo
- (NSString*)description {
    return [NSString stringWithFormat:@"command: %@; score: %d, recognizer: %@", self.command, self.score, self.recognizerName];
}
@end


@interface VCRService()<VCRDelegate>
@property (nonatomic, strong) AudioSubsystem *audioSubsys;
@property (nonatomic) BOOL isRecognitionPaused;
@property (nonatomic, strong) NSArray *recognizers;
@property (nonatomic) int currentVoiceActivityId;
@property (nonatomic, strong) NSTimer *commandRecognitionTimer;
@property (nonatomic) BOOL commandRecognitionHasBegun;
@property (nonatomic, strong) NSMutableDictionary *recognizerNameToRecognizedCommandInfo;
@end

@implementation VCRService

+(VCRService*)sharedInstance {
    static VCRService *sharedInstance = nil;
    @synchronized(self) {
        if (sharedInstance == nil)
            sharedInstance = [[self alloc] init];
    }
    return sharedInstance;
}

-(NSString*)getApplicationSupportDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) lastObject];
}

-(void)initializeApplicationSupportDirectory {
    NSString *appSupportDir = [self getApplicationSupportDirectory];
    if ([[NSFileManager defaultManager] fileExistsAtPath:appSupportDir isDirectory:NULL]) {
        return;
    }
    
    NSError *error = nil;
    // create dir if not already there
    if (![[NSFileManager defaultManager] createDirectoryAtPath:appSupportDir withIntermediateDirectories:YES attributes:nil error:&error]) {
        DDLogError(@"Error: Unabled to create application support directory: %@", error);
        return;
    }
    
    NSURL *url = [NSURL fileURLWithPath:appSupportDir];
    // prevent the dic and gram files from getting synced to icloud
    if (![url setResourceValue:@(YES) forKey:NSURLIsExcludedFromBackupKey error:&error]) {
        DDLogError(@"Error: unable to exclude app support dir from iCloud backup: %@", error);
    }
}

- (void)setUpEvents {
    __weak typeof(self) bself = self;
    
    [[EventManager getInstance] addEventListener:VOICE_ACTIVITY_BEGIN withCallback:^(va_list args) {
        if (!bself) {
            return;
        }
        [bself onVoiceActivityBegin];
    }];
    
    [[EventManager getInstance] addEventListener:VOICE_ACTIVITY_END withCallback:^(va_list args) {
        if (!bself) {
            return;
        }
        
        NSData *data = va_arg(args, NSData*);
        [bself onVoiceActivityEndWithData:data];
    }];
    
    [[EventManager getInstance] addEventListener:VOICE_ACTIVITY_AUDIO_SAMPLES_AVAILABLE withCallback:^(va_list args) {
        if (!bself) {
            return;
        }
        
        NSData *data = va_arg(args, NSData*);
        [bself onVoiceActivitySamplesAvailable:data];
    }];
}

-(VCRService*)init {
    self = [super init];
    if (self) {
        
        self.currentVoiceActivityId = 0;
        self.commandRecognitionHasBegun = NO;
        self.recognizerNameToRecognizedCommandInfo = [NSMutableDictionary dictionary];
        
        PocketSphinxVCR *psVCR = [[PocketSphinxVCR alloc] init];
        NuanceVCR *nuanceVCR = [[NuanceVCR alloc] init];
        
        
        // Note (shashank): the order here is the order in which
        // commands recognized are trusted, the first one being
        // the most trusted
        self.recognizers = @[nuanceVCR, psVCR];
        for (BaseVCR *vcr in self.recognizers) {
            vcr.delegate = self;
        }
        
        
        [DDLog addLogger:[DDTTYLogger sharedInstance]];
        [self initializeApplicationSupportDirectory];
        
        self.isRecognitionPaused = NO;
        
        [self setUpEvents];
        self.audioSubsys = [[AudioSubsystem alloc] init];
    }
    return self;
}

-(void)setCommandsToRecognize:(NSArray*)commands withWakeUpPhrase:(NSString*)wakeUpPhrase {
    if (commands.count == 0) {
        DDLogError(@"Error: list of commands to recognize is empty");
        return;
    }
    for (BaseVCR *vcr in self.recognizers) {
        [vcr setCommandsToRecognize:commands withWakeUpPhrase:wakeUpPhrase];
    }
}

-(void)pauseRecognition {
    self.isRecognitionPaused = YES;
    [self.audioSubsys pause];
}

-(void)resumeRecognition {
    self.isRecognitionPaused = NO;
    [self.audioSubsys resume];
}

-(double)getInputGainLevel {
    return [self.audioSubsys getInputGainLevel];
}

- (void)onVoiceActivityBegin {
    ++self.currentVoiceActivityId;
    self.commandRecognitionHasBegun = NO;
    self.recognizerNameToRecognizedCommandInfo = [NSMutableDictionary dictionary];
    
    int voiceActivityId = self.currentVoiceActivityId;
    for (BaseVCR *vcr in self.recognizers) {
        [vcr onVoiceActivityBeginWithId:voiceActivityId];
    }
}

- (void)onVoiceActivityEndWithData:(NSData*)data {
    int numSamples = data.length/NUM_BYTES_PER_SAMPLE;
    DDLogVerbose(@"onVoiceActivityEndInAudioSamples:: numSamples: %d", numSamples);
    if (self.isRecognitionPaused) {
        return;
    }
    
    DDLogVerbose(@"recording %d samples2", numSamples);
    [[AudioRecorder sharedInstance] recordFrames2:data];
    
    for (BaseVCR *vcr in self.recognizers) {
        [vcr onVoiceActivityEndWithSamples:data];
    }
}

- (void)onVoiceActivitySamplesAvailable:(NSData*)data {
    for (BaseVCR *vcr in self.recognizers) {
        //[[AudioRecorder sharedInstance] recordFrames2:data];
        [vcr onVoiceActivityAudioSamples:data];
    }
}

- (void)commandRecognitionBeganByRecognizer:(BaseVCR*)recognizer forVoiceActivityId:(int)voiceActivityId {
    if (voiceActivityId != self.currentVoiceActivityId) {
        return;
    }
    if (self.commandRecognitionHasBegun) {
        return;
    }
    self.commandRecognitionHasBegun = YES;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[EventManager getInstance] fireEvent:VOICE_ACITIVITY_COMMAND_RECOGNITION_BEGIN];
    });
    
    __weak typeof(self) bself = self;
    int currentId = self.currentVoiceActivityId;
    dispatch_time_t fireTime = dispatch_time(DISPATCH_TIME_NOW, MAX_COMMAND_RECOGNITION_TIME * NSEC_PER_SEC);
    dispatch_after(fireTime, dispatch_get_current_queue(), ^(void){
        if (!bself || currentId != bself.currentVoiceActivityId) {
            return;
        }
        
        [[EventLogger getInstance] logEvent:@"vcr_timed_out" properties:@{@"recognition_state": bself.recognizerNameToRecognizedCommandInfo.description}];
        [bself reportRecognizedCommandForCurrentVoiceActivity];
    });
    
}

- (void)commandRecognitionEndedByRecognizer:(BaseVCR*)recognizer forVoiceActivityId:(int)voiceActivityId {
    
}

- (void)commandRecognized:(NSString*)command withScore:(SInt32)score byRecognizer:(BaseVCR*)recognizer forVoiceActivityId:(int)voiceActivityId {
    if (voiceActivityId != self.currentVoiceActivityId) {
        if (!command) {
            command = @"<nil>";
        }
        [[EventLogger getInstance] logEvent:@"vcr_recognized_too_late" properties:@{@"recognizer": recognizer.name, @"command": command, @"score": @(score), @"voice_activity_id": @(voiceActivityId)}];
        return;
    }
    
    CommandInfo *info = [CommandInfo new];
    info.command = command;
    info.score = score;
    info.recognizerName = recognizer.name;
    self.recognizerNameToRecognizedCommandInfo[recognizer.name] = info;
    
    if ((recognizer == self.recognizers[0] && command != nil) ||
        self.recognizerNameToRecognizedCommandInfo.allKeys.count == self.recognizers.count)
    {
        [self reportRecognizedCommandForCurrentVoiceActivity];
    }
}

- (void)reportRecognizedCommandForCurrentVoiceActivity {
    // increment the voice activity id so that any late in-coming
    // callback are ignored. note that this means that every other
    // voice activity id value will be unused as an id
    ++self.currentVoiceActivityId;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[EventManager getInstance] fireEvent:VOICE_ACITIVITY_COMMAND_RECOGNITION_END];
    });
    
    CommandInfo *bestCommandInfo = nil;
    for (BaseVCR *vcr in self.recognizers) {
        NSString *name = vcr.name;
        CommandInfo *commandInfo = self.recognizerNameToRecognizedCommandInfo[name];
        if (commandInfo) {
            NSString *command = commandInfo.command;
            if (command && command.length > 0) {
                bestCommandInfo = commandInfo;
                break;
            }
        }
    }
    
    if (!bestCommandInfo) {
        [[EventLogger getInstance] logEvent:@"vcr_all_recognizers_timedout"];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[EventManager getInstance] fireEvent:VOICE_ACTIVITY_NO_COMMAND_RECOGNIZED];
        });
        return;
    }
    
    NSString *command = bestCommandInfo.command;
    SInt32 score = bestCommandInfo.score;
    NSString *recognizerName = bestCommandInfo.recognizerName;
    
    [[EventLogger getInstance] logEvent:@"command_recognized" properties:@{@"command": command, @"score": @(score), @"recognizer": recognizerName}];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[EventManager getInstance] fireEvent:COMMAND_RECOGNIZED, command, @(score)];
    });
    
}

@end
