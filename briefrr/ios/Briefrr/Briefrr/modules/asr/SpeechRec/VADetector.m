//
//  VADetector.m
//  SpeechRec
//
//  Created by Shashank Singh on 11/13/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import "VADetector.h"
#import "TPCircularBuffer.h"
#import "Logging.h"
#import "brfrCircularBuffer.h"
#import "FrequencyVADetector.h"
#import "AudioRecorder.h"
#import "EventManager.h"
#import "EventLogger.h"

#define kAudioBufferMaxBytes 120000 // 6 seconds worth of samples at 16k Hz
#define kMinSpeechPrefixSamples 8192 // 64ms seconds worth of samples before start of speech
#define kBytesPerSample 2

#define kVADWindowSize 4096
#define kVADShortHistorySize 1
#define kVADLongHistorySize 16

#define kVADSpeechMinTTSOn 200
#define kVADSpeechMinTTSOff 125
#define kVADSilenceMax 50
#define kVADLongHistoryNonIncreasingCountForSilence 15

static const char* BACKGROUND_QUEUE_NAME = "com.briefrr.vad_background_q";

@interface VADetector()
@property (nonatomic) TPCircularBuffer audioSamplesCircularBuffer;
@property (nonatomic) BrfrCircularBuffer shortVADWindowAvgHistory;
@property (nonatomic) BrfrCircularBuffer longVADWindowAvgHistory;
@property (nonatomic) long shortVADWindowAvgHistorySum;
@property (nonatomic) long longVADWindowAvgHistorySum;
@property (nonatomic) long numLeftOverSamplesFromLastVAD;
@property (nonatomic) long lastWindowAvg;
@property (nonatomic) BOOL isSilence;
@property (nonatomic) long nonIncreasingWindowCount;
@property (nonatomic) long speechStartSampleIndex;
@property (nonatomic) long preSpeechNoiseLevel;
@property (nonatomic, strong) FrequencyVADetector *frequencyVADetector;
@end

@implementation VADetector

-(VADetector*)init {
    self = [super init];
    if (self) {
        TPCircularBufferInit(&(self->_audioSamplesCircularBuffer), kAudioBufferMaxBytes);
        brfrCbInit(&(self->_shortVADWindowAvgHistory), kVADShortHistorySize);
        brfrCbInit(&(self->_longVADWindowAvgHistory), kVADLongHistorySize);
        
        self.shortVADWindowAvgHistorySum = 0;
        self.longVADWindowAvgHistorySum = 0;
        self.numLeftOverSamplesFromLastVAD = 0;
        self.lastWindowAvg = 0;
        self.isSilence = YES;
        self.nonIncreasingWindowCount = 0;
        self.speechStartSampleIndex = -1;
        
        self.frequencyVADetector = [[FrequencyVADetector alloc] init];
    }
    return self;
}

-(void)dealloc {
    TPCircularBufferCleanup(&(self->_audioSamplesCircularBuffer));
    brfrCbFree(&(self->_shortVADWindowAvgHistory));
    brfrCbFree(&(self->_longVADWindowAvgHistory));
}

-(dispatch_queue_t)getBackgroundQueue {
    static dispatch_once_t queueCreationGuard;
    static dispatch_queue_t queue;
    dispatch_once(&queueCreationGuard, ^{
        queue = dispatch_queue_create(BACKGROUND_QUEUE_NAME, DISPATCH_QUEUE_SERIAL);
    });
    return queue;
}

-(void)addAudioSamples:(SInt16*)samples OfSize:(UInt32)numBytes withTTSOn:(BOOL)isTTSOn {
    __block typeof(self) bself = self;
    dispatch_async([self getBackgroundQueue], ^{
        if (!bself) {
            return;
        }
        [bself addAudioSamplesInBackgroundThread:samples ofSize:numBytes withTTSOn:isTTSOn];
    });
}

-(void)addAudioSamplesInBackgroundThread:(SInt16 *)samples ofSize:(UInt32)numBytes withTTSOn:(BOOL)isTTSOn {
    if (!samples) {
        return;
    }
    
    int value = arc4random() % 1000;
    if (value <= 1) {
        DDLogVerbose(@"incoming samples: %d", (unsigned int)numBytes);
    }
    
    int32_t spaceLeft;
    TPCircularBufferHead(&(self->_audioSamplesCircularBuffer), &spaceLeft);
    int32_t bytesToRemove = numBytes - spaceLeft;
    int32_t samplesToRemove = bytesToRemove/kBytesPerSample;
    
    if (samplesToRemove > 0) {
        if (!self.isSilence && self.speechStartSampleIndex < samplesToRemove) {
            DDLogWarn(@"Warning: ran out of circular buffer waiting for speech utterance to finish");
            // this will clear the entire buffer so there is no need to call consume
            [self onSpeechEnd];
        } else {
            //DDLogVerbose(@"emptySpaceInBuffer: %d, bytesToRemove: %d", spaceLeft, bytesToRemove);
            TPCircularBufferConsume(&(self->_audioSamplesCircularBuffer), bytesToRemove);
        }
    }
    
    TPCircularBufferProduceBytes(&(self->_audioSamplesCircularBuffer), samples, numBytes);
    
    // some samples were removed, index of the speech start
    // will need to be shifted for that
    if (!self.isSilence && samplesToRemove > 0) {
        self.speechStartSampleIndex -= samplesToRemove;
    }
    // we have run out of circular buffer while waiting for utterance
    // end. we'll just call this to be end of utterance
    //DDLogVerbose(@"updating speech sample index: %ld", self.speechStartSampleIndex);
    
    UInt32 numSamples = numBytes/kBytesPerSample;
    [self lookForVoiceActivityInCurrentBufferWithNewSamples:numSamples withTTSOn:isTTSOn];
    
    if (!self.isSilence) {
        NSData *data = [NSData dataWithBytes:samples length:numBytes];
        if (!data) {
            [[EventLogger getInstance] logEvent:@"error_nil_data" properties:@{@"location": @"audio_samples_available", @"num_bytes": @(numBytes)}];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[EventManager getInstance] fireEvent:VOICE_ACTIVITY_AUDIO_SAMPLES_AVAILABLE, data];
            });
        }
    }
}

-(void)updateVADWindowAvgHistory:(long)newWindowAverage {
    
    if (brfrCbIsFull(&(self->_shortVADWindowAvgHistory))) {
        long oldestVADWindowAvg = brfrCbPeek(&(self->_shortVADWindowAvgHistory));
        self.shortVADWindowAvgHistorySum -= oldestVADWindowAvg;
    }
    //DDLogVerbose(@"VADetector:: short oldestVADWindowAvg: %ld", oldestVADWindowAvg);
    brfrCbWrite(&(self->_shortVADWindowAvgHistory), newWindowAverage);
    self.shortVADWindowAvgHistorySum += newWindowAverage;
    
    if (brfrCbIsFull(&(self->_longVADWindowAvgHistory))) {
        long oldestVADWindowAvg = brfrCbPeek(&(self->_longVADWindowAvgHistory));
        self.longVADWindowAvgHistorySum -= oldestVADWindowAvg;
    }
    //DDLogVerbose(@"VADetector:: long oldestVADWindowAvg: %ld", oldestVADWindowAvg);
    brfrCbWrite(&(self->_longVADWindowAvgHistory), newWindowAverage);
    self.longVADWindowAvgHistorySum += newWindowAverage;
}


-(void)onSpeechBegin {
    self.isSilence = NO;
    dispatch_async(dispatch_get_main_queue(), ^{
        [[EventManager getInstance] fireEvent:VOICE_ACTIVITY_BEGIN];
    });
}

-(void)onSpeechEnd {
    DDLogVerbose(@"onSpeechEnd");
    
    self.isSilence = YES;
    SInt16 *utteranceBuffer = NULL;
    int32_t numUtteranceSamples = 0;
    
    if (self.speechStartSampleIndex < 0) {
        DDLogError(@"Error: speechStartSampleIndex < 0 at end of utterance: %ld", self.speechStartSampleIndex);
        return;
    }
    
    int32_t bytes;
    SInt16 *bufferTail = TPCircularBufferTail(&(self->_audioSamplesCircularBuffer), &bytes);
    
    numUtteranceSamples =  (int32_t)((bytes/kBytesPerSample) - self.speechStartSampleIndex);
    if (numUtteranceSamples <= 0) {
        DDLogError(@"Error: speech start sample is lost from the circular buffer: %ld, %ld, %d", self.speechStartSampleIndex, self.speechStartSampleIndex * kBytesPerSample, bytes);
        return;
    }
    
    SInt16 *utteranceBufferStart = bufferTail + self.speechStartSampleIndex;
    DDLogVerbose(@"before removing wakeup keyword: buffer: %p, numUtteranceSamples: %d, speechStartSampleIndex: %ld", utteranceBufferStart, numUtteranceSamples, self.speechStartSampleIndex);
    
    int32_t utteranceBufferSize = numUtteranceSamples * kBytesPerSample;
    utteranceBuffer = (SInt16*)malloc(utteranceBufferSize);
    if (!utteranceBuffer) {
        DDLogError(@"Error: out of memory when trying to alloc buffer for detected utterance");
        return;
    }
    memset(utteranceBuffer, 0, utteranceBufferSize);
    memcpy(utteranceBuffer, utteranceBufferStart , utteranceBufferSize);
    
    self.speechStartSampleIndex = -1;
    self.numLeftOverSamplesFromLastVAD = 0;
    TPCircularBufferClear(&(self->_audioSamplesCircularBuffer));
    
    if (utteranceBuffer && numUtteranceSamples > 0) {
        NSData *data = [NSData dataWithBytes:utteranceBuffer length:numUtteranceSamples * kBytesPerSample];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[EventManager getInstance] fireEvent:VOICE_ACTIVITY_END, data];
        });
    }
}

-(void)lookForVoiceActivityInCurrentBufferWithNewSamples:(UInt32)numNewSamples withTTSOn:(BOOL)isTTSOn {
    int32_t bytes;
    SInt16 *bufferTail = TPCircularBufferTail(&(self->_audioSamplesCircularBuffer), &bytes);
    
    //DDLogVerbose(@"readable bytes: %d", bytes);
    
    SInt16 *bufferEnd = bufferTail + (bytes/kBytesPerSample);
    // go to the end of buffer
    SInt16 *vadStart = bufferEnd;
    // go to the point we were at before we added the last batch
    vadStart -= numNewSamples;
    // go back enough to include the samples we didn't process the
    // the last time because the number of samples was not a multiple
    // of vad window size
    vadStart -= self.numLeftOverSamplesFromLastVAD;
    
    //DDLogVerbose(@"vadStart: %p, isTTSOn: %d", vadStart, isTTSOn);
    //DDLogVerbose(@"numLeftOverSamplesFromLastVAD: %ld", self.numLeftOverSamplesFromLastVAD);
    
    UInt32 bytesPerVADWindow = kVADWindowSize * kBytesPerSample;
    long sampleIndex = (vadStart - bufferTail)/kBytesPerSample;
    
    //DDLogVerbose(@"bufferEnd - vadStart: %ld, %d", bufferEnd - vadStart, bytesPerVADWindow);
    
    while (bufferEnd - vadStart >= kVADWindowSize) {
        long sum = 0;
        UInt32 i;
        
        for (i=0; i<kVADWindowSize; i++) {
            SInt16 sample = vadStart[i];
            sum += sample * sample;
        }
        if (i == 0) {
            break;
        }
        
        //DDLogVerbose(@"VADetector:: window sum: %ld", sum);
        
        // TODO (shashank): optimized sqaure root computation
        // for integers (e.g. http://www.finesse.demon.co.uk/steven/sqrt.html)
        long windowAvg = sqrt(sum/i);
        //DDLogVerbose(@"VADetector:: window avg: %ld, is high: %d", windowAvg, windowAvg > 2000);
        [self updateVADWindowAvgHistory:windowAvg];
        
        // initially the number of samples will be less than the full history
        // size but that is only for a few 10s of millis so we assume that
        // the history buffers are always full
        long shortAvgAvg = self.shortVADWindowAvgHistorySum/kVADShortHistorySize;
        long longAvgAvg = self.longVADWindowAvgHistorySum/kVADLongHistorySize;
        
        //DDLogVerbose(@"VADetector: shortAvgAvg: %ld, longAvgAvg: %ld, isSilence: %d", shortAvgAvg, longAvgAvg, self.isSilence);
        
        if (shortAvgAvg >= (isTTSOn ? kVADSpeechMinTTSOn : kVADSpeechMinTTSOff)) {
            if (self.isSilence) {
                BOOL isVoice = [self.frequencyVADetector samplesHaveVoiceActivity:vadStart numSamples:kVADWindowSize withOutputMuted:!isTTSOn];
                NSData *data = [NSData dataWithBytes:vadStart length:kVADWindowSize];
                [[AudioRecorder sharedInstance] recordFrames3:data :isVoice];
                
                if (!isVoice) {
                    DDLogVerbose(@"power based VA detected, determined by frequency based VAD not be speech");
                } else {
                    // utterance start detected
                    // we mark the start of utterance to be a little before the point
                    // we detected it to account for small delays in processing
                    self.speechStartSampleIndex = MAX(0, sampleIndex - kMinSpeechPrefixSamples);
                    self.preSpeechNoiseLevel = longAvgAvg;
                    DDLogVerbose(@"speech detected with shortAvgAvg: %ld, speechStartSampleIndex: %ld", shortAvgAvg, self.speechStartSampleIndex);
                    [self onSpeechBegin];
                }
                
            }
            self.nonIncreasingWindowCount = 0;
        } else if (!self.isSilence) {
            if (windowAvg <= self.lastWindowAvg) {
                self.nonIncreasingWindowCount++;
            } else {
                self.nonIncreasingWindowCount = 0;
            }
            
            if (longAvgAvg < kVADSilenceMax
                || self.nonIncreasingWindowCount >= kVADLongHistoryNonIncreasingCountForSilence) {
                // utterance end detected
                DDLogVerbose(@"speech end detected with longAvgAvg: %ld, nonIncreasingWindowCount: %ld, preSpeechNoiseLevel: %ld;", longAvgAvg, self.nonIncreasingWindowCount, self.preSpeechNoiseLevel);
                [self onSpeechEnd];
            }
        }
        
        vadStart += i;
        sampleIndex += kVADWindowSize;
        self.lastWindowAvg = windowAvg;
    }
    
    self.numLeftOverSamplesFromLastVAD = (bufferEnd - vadStart);
    
}
@end
