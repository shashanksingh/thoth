//
//  AudioRecorder.m
//  SpeechRec
//
//  Created by Shashank Singh on 11/2/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import "AudioRecorder.h"
#import <AVFoundation/AVFoundation.h>
#import "Logging.h"
#import "Util.h"

static NSString* kServerURL = @"http://192.168.0.105:14003";
static const unsigned int kTimerInterval = 4;

AudioUnit *audioUnit;
ExtAudioFileRef outExtAudioFile2;
ExtAudioFileRef outExtAudioFile3;

@interface AudioRecorder()
@property (strong, nonatomic) NSMutableData *buffer;
@property (strong, nonatomic) NSTimer *timer;
@end

@implementation AudioRecorder
@synthesize buffer, timer;

-(AudioRecorder*)init {
    self = [super init];
    if (self) {
        self.buffer = [NSMutableData data];
        
        NSDate *currDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"dd.MM.YY HH:mm:ss"];
        NSString *dateString = [dateFormatter stringFromDate:currDate];
        NSString *fileName = [NSString stringWithFormat:@"%@.caf", dateString];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        //make a file name to write the data to using the documents directory:
        NSString *onDiskRecordPath = [documentsDirectory stringByAppendingPathComponent:fileName];
        
        AudioStreamBasicDescription asbd_mono_aac_16khz_;
        AudioStreamBasicDescription asbd_mono_canonical_;
        memset(&asbd_mono_aac_16khz_, 0, sizeof(AudioStreamBasicDescription));
        
        
        //        asbd_mono_aac_16khz_.mFormatID = kAudioFormatMPEG4AAC;
        //        asbd_mono_aac_16khz_.mFormatFlags = kMPEG4Object_AAC_Main;
        asbd_mono_aac_16khz_.mFormatID = kAudioFormatLinearPCM;
        asbd_mono_aac_16khz_.mFormatFlags = kAudioFormatFlagsNativeEndian |
        kAudioFormatFlagIsPacked | kAudioFormatFlagIsSignedInteger;
        asbd_mono_aac_16khz_.mBytesPerFrame = asbd_mono_aac_16khz_.mBytesPerPacket = 2;
        asbd_mono_aac_16khz_.mBitsPerChannel = 16;
        asbd_mono_aac_16khz_.mSampleRate = 16000.0;
        asbd_mono_aac_16khz_.mChannelsPerFrame = 1;
        asbd_mono_aac_16khz_.mFramesPerPacket = 1;
        
        size_t bytesPerSample = sizeof (AudioSampleType);
        memset(&asbd_mono_canonical_, 0, sizeof(AudioStreamBasicDescription));
        
        asbd_mono_canonical_.mFormatID          = kAudioFormatLinearPCM;
        asbd_mono_canonical_.mFormatFlags       = kAudioFormatFlagsCanonical;
        asbd_mono_canonical_.mBytesPerPacket    = bytesPerSample;
        asbd_mono_canonical_.mBytesPerFrame     = bytesPerSample;
        asbd_mono_canonical_.mFramesPerPacket   = 1;
        asbd_mono_canonical_.mBitsPerChannel    = 8 * bytesPerSample;
        asbd_mono_canonical_.mChannelsPerFrame  = 1;
        asbd_mono_canonical_.mSampleRate        = 16000.0;
        
        NSURL *url = [NSURL fileURLWithPath:onDiskRecordPath];
        OSStatus result = ExtAudioFileCreateWithURL((__bridge CFURLRef)url,
                                                    //kAudioFileMPEG4Type,
                                                    kAudioFileCAFType,
                                                    &asbd_mono_aac_16khz_,
                                                    NULL,
                                                    kAudioFileFlags_EraseFile,
                                                    &outExtAudioFile);
        
        if (result != noErr) {
            DDLogError(@"error in creating file with url: %@, %d", url, result);
        } else {
            result =  ExtAudioFileSetProperty(outExtAudioFile,
                                              kExtAudioFileProperty_ClientDataFormat,
                                              sizeof(asbd_mono_canonical_),
                                              &asbd_mono_canonical_);
            
            if (result != noErr) {
                DDLogError(@"error in set audio file property: %d", result);
            } else {
                // initializes async writes
                result = ExtAudioFileWrite(outExtAudioFile, 0, NULL);
                if (result != noErr) {
                    DDLogError(@"error in initing async write");
                }
            }
        }
        
        url = [NSURL fileURLWithPath:[onDiskRecordPath stringByReplacingOccurrencesOfString:@".caf" withString:@"_2.caf"]];
        result = ExtAudioFileCreateWithURL((__bridge CFURLRef)url,
                                           //kAudioFileMPEG4Type,
                                           kAudioFileCAFType,
                                           &asbd_mono_aac_16khz_,
                                           NULL,
                                           kAudioFileFlags_EraseFile,
                                           &outExtAudioFile2);
        
        if (result != noErr) {
            DDLogError(@"error in creating file with url2: %@, %d", url, result);
        } else {
            result =  ExtAudioFileSetProperty(outExtAudioFile2,
                                              kExtAudioFileProperty_ClientDataFormat,
                                              sizeof(asbd_mono_canonical_),
                                              &asbd_mono_canonical_);
            
            if (result != noErr) {
                DDLogError(@"error in set audio file property2: %d", result);
            } else {
                // initializes async writes
                result = ExtAudioFileWrite(outExtAudioFile2, 0, NULL);
                if (result != noErr) {
                    DDLogError(@"error in initing async write2");
                }
            }
        }
        
        url = [NSURL fileURLWithPath:[onDiskRecordPath stringByReplacingOccurrencesOfString:@".caf" withString:@"_3.caf"]];
        result = ExtAudioFileCreateWithURL((__bridge CFURLRef)url,
                                           //kAudioFileMPEG4Type,
                                           kAudioFileCAFType,
                                           &asbd_mono_aac_16khz_,
                                           NULL,
                                           kAudioFileFlags_EraseFile,
                                           &outExtAudioFile3);
        
        if (result != noErr) {
            DDLogError(@"error in creating file with url3: %@, %d", url, result);
        } else {
            result =  ExtAudioFileSetProperty(outExtAudioFile3,
                                              kExtAudioFileProperty_ClientDataFormat,
                                              sizeof(asbd_mono_canonical_),
                                              &asbd_mono_canonical_);
            
            if (result != noErr) {
                DDLogError(@"error in set audio file property3: %d", result);
            } else {
                // initializes async writes
                result = ExtAudioFileWrite(outExtAudioFile3, 0, NULL);
                if (result != noErr) {
                    DDLogError(@"error in initing async write3");
                }
            }
        }
    }
    return self;
}

-(void)dealloc {
    DDLogVerbose(@"deallic being called: %p", outExtAudioFile);
    if (self.timer) {
        [self.timer invalidate];
        self.timer = NULL;
    }
    [self closeFiles];
}

-(void)closeFiles {
    if (outExtAudioFile) {
        DDLogVerbose(@"disposing off ext audio file");
        OSStatus result = ExtAudioFileDispose(outExtAudioFile);
        if (result != noErr) {
            DDLogError(@"error in disposing extaudio file: %d", result);
        }
        outExtAudioFile = nil;
    }
    if (outExtAudioFile2) {
        DDLogVerbose(@"disposing off ext audio file2");
        OSStatus result = ExtAudioFileDispose(outExtAudioFile2);
        if (result != noErr) {
            DDLogError(@"error in disposing extaudio file2: %d", result);
        }
        outExtAudioFile2 = nil;
    }
    if (outExtAudioFile3) {
        DDLogVerbose(@"disposing off ext audio file3");
        OSStatus result = ExtAudioFileDispose(outExtAudioFile3);
        if (result != noErr) {
            DDLogError(@"error in disposing extaudio file3: %d", result);
        }
        outExtAudioFile3 = nil;
    }
}

+(AudioRecorder*)sharedInstance {
    static AudioRecorder *sharedInstance = nil;
    @synchronized(self) {
        if (sharedInstance == nil)
            sharedInstance = [[self alloc] init];
    }
    return sharedInstance;
}

-(void)addData:(char*)data dataLength:(UInt32)length {
    __block typeof(self) bself = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [bself.buffer appendBytes:data length:length];
    });
}

-(void)recordSamples:(NSData*)data file:(ExtAudioFileRef)file marker:(SInt16)marker {
    SInt16* samples = (SInt16*)data.bytes;
    UInt32 numSamples = (UInt32)(data.length/NUM_BYTES_PER_SAMPLE);
    
    AudioBuffer buff;
    buff.mDataByteSize = data.length;
    buff.mNumberChannels = 1;
    buff.mData = samples;
    
    SInt16 firstSample = samples[0];
    samples[0] = marker;
    
    AudioBufferList bufferList;
    bufferList.mNumberBuffers = 1;
    bufferList.mBuffers[0] = buff;
    
    OSStatus result = ExtAudioFileWrite(file,
                                        numSamples,
                                        &bufferList);
    
    samples[0] = firstSample;
    if (result != noErr) {
        DDLogError(@"error in async audio write: %d", result);
    }
}

-(void)recordFrames:(AudioBufferList *)bufferList :(UInt32)numFrames {
    return;
    OSStatus result = ExtAudioFileWrite(outExtAudioFile,
                                             numFrames,
                                             bufferList);
    if (result != noErr) {
        DDLogError(@"error in async audio write: %d", result);
    }
}

-(void)recordFrames2:(NSData*)data {
    return;
    [self recordSamples:data file:outExtAudioFile2 marker:30000];
}

-(void)recordFrames3:(NSData*)data :(BOOL)isSpeech {
    return;
    [self recordSamples:data file:outExtAudioFile3 marker:isSpeech ? 30000 : -30000];
}

-(void)send {
    
    DDLogVerbose(@"AudioRecorder:: sending recorded data to server");
    
    NSData *bufferedData = self.buffer;
    if (bufferedData.length == 0) {
        DDLogVerbose(@"AudioRecorder:: recorded data buffer is empty, doing nothing");
        return;
    }
    
    NSString *contentLength = [NSString stringWithFormat:@"%lu", (unsigned long)bufferedData.length];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"POST"];
    [request setURL:[Util urlFromString:kServerURL]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:contentLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:bufferedData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (connectionError) {
            DDLogError(@"AudioRecorder:: error in posting data to server: %@", connectionError);
        } else {
            DDLogVerbose(@"AudioRecorder:: successfully sent buffer of size %lu to server", (unsigned long)bufferedData.length);
        }
    }];
    
    self.buffer = [NSMutableData data];
}

static OSStatus renderCallback(void *userData, AudioUnitRenderActionFlags *actionFlags,
                               const AudioTimeStamp *audioTimeStamp, UInt32 busNumber,
                               UInt32 numFrames, AudioBufferList *buffers) {
    OSStatus status = AudioUnitRender(*audioUnit, actionFlags, audioTimeStamp,
                                      1, numFrames, buffers);
    if(status != noErr) {
        return status;
    }
    
    OSStatus result = ExtAudioFileWriteAsync(outExtAudioFile2,
                                             numFrames,
                                             buffers);
    if (result != noErr) {
        DDLogError(@"error in async audio write: %d", result);
    }
    
    return noErr;
}

-(int) initRemoteIO {
    AudioComponentDescription componentDescription;
    componentDescription.componentType = kAudioUnitType_Output;
    componentDescription.componentSubType = kAudioUnitSubType_RemoteIO;
    componentDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
    componentDescription.componentFlags = 0;
    componentDescription.componentFlagsMask = 0;
    AudioComponent component = AudioComponentFindNext(NULL, &componentDescription);
    if(AudioComponentInstanceNew(component, audioUnit) != noErr) {
        return 1;
    }
    
    UInt32 enable = 1;
    if(AudioUnitSetProperty(*audioUnit, kAudioOutputUnitProperty_EnableIO,
                            kAudioUnitScope_Input, 1, &enable, sizeof(UInt32)) != noErr) {
        return 1;
    }
    
    AURenderCallbackStruct callbackStruct;
    callbackStruct.inputProc = renderCallback; // Render function
    callbackStruct.inputProcRefCon = NULL;
    if(AudioUnitSetProperty(*audioUnit, kAudioUnitProperty_SetRenderCallback,
                            kAudioUnitScope_Input, 0, &callbackStruct,
                            sizeof(AURenderCallbackStruct)) != noErr) {
        return 1;
    }
    
    AudioStreamBasicDescription streamDescription;
    // You might want to replace this with a different value, but keep in mind that the
    // iPhone does not support all sample rates. 8kHz, 22kHz, and 44.1kHz should all work.
    streamDescription.mSampleRate = 16000;
    // Yes, I know you probably want floating point samples, but the iPhone isn't going
    // to give you floating point data. You'll need to make the conversion by hand from
    // linear PCM <-> float.
    streamDescription.mFormatID = kAudioFormatLinearPCM;
    // This part is important!
    streamDescription.mFormatFlags = kAudioFormatFlagIsSignedInteger |
    kAudioFormatFlagsNativeEndian |
    kAudioFormatFlagIsPacked;
    // Not sure if the iPhone supports recording >16-bit audio, but I doubt it.
    streamDescription.mBitsPerChannel = 16;
    // 1 sample per frame, will always be 2 as long as 16-bit samples are being used
    streamDescription.mBytesPerFrame = 2;
    // Record in mono. Use 2 for stereo, though I don't think the iPhone does true stereo recording
    streamDescription.mChannelsPerFrame = 1;
    streamDescription.mBytesPerPacket = streamDescription.mBytesPerFrame *
    streamDescription.mChannelsPerFrame;
    // Always should be set to 1
    streamDescription.mFramesPerPacket = 1;
    // Always set to 0, just to be sure
    streamDescription.mReserved = 0;
    
    // Set up input stream with above properties
    if(AudioUnitSetProperty(*audioUnit, kAudioUnitProperty_StreamFormat,
                            kAudioUnitScope_Input, 0, &streamDescription, sizeof(streamDescription)) != noErr) {
        return 1;
    }
    
    // Ditto for the output stream, which we will be sending the processed audio to
    if(AudioUnitSetProperty(*audioUnit, kAudioUnitProperty_StreamFormat,
                            kAudioUnitScope_Output, 1, &streamDescription, sizeof(streamDescription)) != noErr) {
        return 1;
    }
    return 0;
}
@end
