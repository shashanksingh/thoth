//
//  brfrCircularBuffer.h
//  SpeechRec
//
//  Source: http://en.wikipedia.org/wiki/Circular_buffer
//

#ifndef __SpeechRec__brfrCircularBuffer__
#define __SpeechRec__brfrCircularBuffer__

#include <stdio.h>


typedef struct {
    int         size;   /* maximum number of elements           */
    int         start;  /* index of oldest element              */
    int         end;    /* index at which to write new element  */
    long   *elems;  /* vector of elements                   */
} BrfrCircularBuffer;

void brfrCbInit(BrfrCircularBuffer *cb, int size);
void brfrCbFree(BrfrCircularBuffer *cb);
int brfrCbIsFull(BrfrCircularBuffer *cb);
int brfrCbIsEmpty(BrfrCircularBuffer *cb);
void brfrCbWrite(BrfrCircularBuffer *cb, long value);
long brfrCbRead(BrfrCircularBuffer *cb);
long brfrCbPeek(BrfrCircularBuffer *cb);


#endif /* defined(__SpeechRec__brfrCircularBuffer__) */
