//
//  AudioSubsystem.h
//  SpeechRec
//
//  Created by Shashank Singh on 11/2/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AudioSubsystem : NSObject
- (double)getInputGainLevel;
- (void)pause;
- (void)resume;
@end
