//
//  VADetector.h
//  SpeechRec
//
//  Created by Shashank Singh on 11/13/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface VADetector : NSObject
-(void)addAudioSamples:(SInt16*)samples OfSize:(UInt32)numBytes withTTSOn:(BOOL)isTTSOn;
@end
