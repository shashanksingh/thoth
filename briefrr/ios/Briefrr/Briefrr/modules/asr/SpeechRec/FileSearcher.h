//
//  FileSearcher.h
//  SpeechRec
//
//  Created by Shashank Singh on 11/3/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileSearcher : NSObject

+(NSDictionary*)getLinesStartingWith:(NSArray*)prefixes inFileAtPath:(NSString*)filePath andMatchAllLinesWithSameRoot:(BOOL)matchAllLinesWithSameRoot;

@end
