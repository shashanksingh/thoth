//
//  BaseVCR.m
//  Briefrr
//
//  Created by Shashank Singh on 4/5/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "BaseVCR.h"
#import "Util.h"
#import "EventLogger.h"

@implementation BaseVCR
-(void)setCommandsToRecognize:(NSArray*)commands withWakeUpPhrase:(NSString*)wakeUpPhrase {
    self.wakeUpPhrase = wakeUpPhrase;
    NSMutableArray *effectiveCommandList = commands.mutableCopy;
    if (self.wakeUpPhrase) {
        for (NSString *command in commands) {
            NSString *prefixedCommand = [[self.wakeUpPhrase stringByAppendingString:@" "] stringByAppendingString:command];
            [effectiveCommandList addObject:prefixedCommand];
        }
    }
    self.currentCommands = effectiveCommandList;
}

- (void)onVoiceActivityBeginWithId:(int)voiceActivityId {
    self.currentVoiceActivityId = voiceActivityId;
}

- (void)onVoiceActivityEndWithSamples:(NSData*)data {
    
}

- (void)onVoiceActivityAudioSamples:(NSData*)data {
    
}

- (NSString*)postProcessRecognizedHypothesis:(NSString*)hyp forRecognizer:(NSString*)recognizer {
    if (!hyp) {
        return nil;
    }
    
    if ([hyp hasPrefix:self.wakeUpPhrase]) {
        hyp = [hyp substringFromIndex:self.wakeUpPhrase.length];
    }
    hyp = [Util trimString:hyp];
    if (hyp.length == 0) {
        return nil;
    }
    
    // if multiple commands were recognized we take the last one
    NSRange lastCommandRange = NSMakeRange(NSNotFound, 0);
    for (NSString *command in self.currentCommands) {
        NSRange range = [hyp rangeOfString:command];
        if (range.location == NSNotFound) {
            continue;
        }
        // make sure that this command is not in the hypothesis because
        // it is a substring of another command that was recognized
        if (range.location > 0) {
            unichar charBefore = [hyp characterAtIndex:range.location - 1];
            if (![[NSCharacterSet whitespaceCharacterSet] characterIsMember:charBefore]) {
                continue;
            }
        }
        if (range.location + range.length < hyp.length) {
            unichar charAfter = [hyp characterAtIndex:range.location - 1];
            if (![[NSCharacterSet whitespaceCharacterSet] characterIsMember:charAfter]) {
                continue;
            }
        }
        
        if (lastCommandRange.location != NSNotFound) {
            [[EventLogger getInstance] logEvent:@"warning_multiple_commands_recognized"
                                     properties:@{@"recognizer": recognizer,
                                                  @"hypothesis": hyp,
                                                  @"command": command}];
        }
        if (lastCommandRange.location == NSNotFound || lastCommandRange.location < range.location) {
            lastCommandRange = range;
        }
    }
    
    if (lastCommandRange.location == NSNotFound) {
        [[EventLogger getInstance] logEvent:@"warning_no_current_command_in_hypothesis"
                                 properties:@{@"recognizer": recognizer,
                                              @"hypothesis": hyp,
                                              @"current_commands": [self.currentCommands componentsJoinedByString:@"|"]}];
        return nil;
    }
    
    return [hyp substringWithRange:lastCommandRange];
}
@end
