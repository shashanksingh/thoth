//
//  NuanceVCR.m
//  Briefrr
//
//  Created by Shashank Singh on 4/5/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "NuanceVCR.h"
#import "Util.h"
#import "UserPreferenceService.h"
#import <AFNetworking/AFNetworking.h>
#import "EventLogger.h"
#import "RemoteConfigService.h"

static NSString* const URL_FORMAT = @"%{end_point}?appId=%{app_id}&appKey=%{app_key}&id=%{uid}";

static const NSTimeInterval NETWORK_TIMEOUT = 5;
static const NSUInteger STREAMING_BUFFER_SIZE = 1 << 16;


@interface NuanceVCR()
@property (nonatomic) int networkRequestId;
@property (nonatomic, strong) NSOutputStream *audioOutputStream;
@end

@implementation NuanceVCR

- (instancetype)init {
    self = [super init];
    if (self) {
        self.name = @"nuance";
        self.audioOutputStream = nil;
        self.networkRequestId = 0;
    }
    return self;
}

- (void)onVoiceActivityBeginWithId:(int)voiceActivityId {
    [super onVoiceActivityBeginWithId:voiceActivityId];
    
    if (self.audioOutputStream) {
        return;
    }
    
    NSString *appId = [[RemoteConfigService getInstance] getNuanceAppId];
    NSString *appKey = [[RemoteConfigService getInstance] getNuanceAppKey];
    NSString *endPoint = [[RemoteConfigService getInstance] getNuanceASREndPoint];
    NSString *userId = [[UserPreferenceService getInstance] getUserId];
    
    NSString *formattedURL = [Util formatString:URL_FORMAT withParameters:@{@"end_point": endPoint,
                                                                            @"app_id": appId,
                                                                            @"app_key": appKey,
                                                                            @"uid": userId}];
    
    NSURL *url = [Util urlFromString:formattedURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:NETWORK_TIMEOUT];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"curl/7.24.0" forHTTPHeaderField:@"User-Agent"];
    [request setValue:@"audio/x-wav;codec=pcm;bit=16;rate=16000" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"text/plain" forHTTPHeaderField:@"Accept"];
    [request setValue: @"eng-USA" forHTTPHeaderField:@"Accept-Language"];
    [request setValue:@"chunked" forHTTPHeaderField:@"Transfer-Encoding"];
    [request setValue:@"1" forHTTPHeaderField:@"X-Dictation-NBestListSize"];
    
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    [self createBoundInputStream:&inputStream outputStream:&outputStream bufferSize:STREAMING_BUFFER_SIZE];
    self.audioOutputStream = outputStream;
    
    [request setHTTPBodyStream:inputStream];
    
    __weak typeof(self) bself = self;
    int requestId = ++self.networkRequestId;
    [[EventLogger getInstance] timeEventStart:@"network_vcr"];
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (bself) {
            [bself onRecognitionResponseForRequestId:requestId usingOperation:operation withResponse:responseObject];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[EventLogger getInstance] timeEventEnd:@"network_vcr" properties:@{@"recognizer": @"nuance", @"error": error.localizedDescription}];
        
        __weak typeof(self) bself = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (bself && bself.delegate) {
                [bself.delegate commandRecognitionEndedByRecognizer:bself forVoiceActivityId:bself.currentVoiceActivityId];
                [bself.delegate commandRecognized:nil withScore:-1 byRecognizer:bself forVoiceActivityId:bself.currentVoiceActivityId];
            }
        });
    }];
    
    [op start];
}

- (void)stopUploadSamples {
    if (!self.audioOutputStream) {
        return;
    }
    [self.audioOutputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.audioOutputStream close];
    self.audioOutputStream = nil;
}

- (void)onVoiceActivityAudioSamples:(NSData *)data {
    [super onVoiceActivityEndWithSamples:data];
    
    if (!self.audioOutputStream) {
        return;
    }
    if (data.length == 0) {
        return;
    }
    
    NSInteger bytesWritten = [self.audioOutputStream write:data.bytes maxLength:data.length];
    if (bytesWritten <= 0) {
        [[EventLogger getInstance] logEvent:@"error_network_vcr_upload_audio" properties:@{@"error": @"network error: couldn't write all samples", @"total_bytes": @(data.length), @"written_bytes": @(bytesWritten)}];
        [self stopUploadSamples];
    }
}

- (void)onVoiceActivityEndWithSamples:(NSData *)data {
    [super onVoiceActivityEndWithSamples:data];
    
    __weak typeof(self) bself = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (bself && bself.delegate) {
            [bself.delegate commandRecognitionBeganByRecognizer:bself forVoiceActivityId:bself.currentVoiceActivityId];
        }
    });
    [self stopUploadSamples];
}

- (void)onRecognitionResponseForRequestId:(int)requestId
                           usingOperation:(AFHTTPRequestOperation*)operation
                             withResponse:(id)responseObject {
    
    BOOL tooLate = self.networkRequestId != requestId;
    NSString *response = operation.responseString;
    response = [response uppercaseString];
    
    NSArray *hypotheses = [response componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    [[EventLogger getInstance] timeEventEnd:@"network_vcr"
                                 properties:@{@"recognizer": self.name,
                                              @"hypotheses": hypotheses,
                                              @"too_late": @(tooLate)}];
    
    // another request has been issued after this one, ignore this
    if (tooLate) {
        return;
    }
    
    NSString *command = nil;
    NSString *hypothesis = hypotheses.count > 0 ? hypotheses[0] : nil;
    if (hypothesis) {
        command = [self postProcessRecognizedHypothesis:hypothesis forRecognizer:self.name];
    }
    
    [[EventLogger getInstance] logEvent:@"network_vcr_recognized_command" properties:@{@"recognizer": @"nuance", @"command": command ? command : @""}];
    
    __weak typeof(self) bself = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (bself && bself.delegate) {
            [bself.delegate commandRecognitionEndedByRecognizer:bself forVoiceActivityId:bself.currentVoiceActivityId];
            if (command) {
                [bself.delegate commandRecognized:command withScore:-1 byRecognizer:bself forVoiceActivityId:bself.currentVoiceActivityId];
            } else {
                [bself.delegate commandRecognized:nil withScore:-1 byRecognizer:bself forVoiceActivityId:bself.currentVoiceActivityId];
            }
        }
    });
}

- (void)createBoundInputStream:(NSInputStream**)inputStreamPtr
                  outputStream:(NSOutputStream**)outputStreamPtr
                    bufferSize:(NSUInteger)bufferSize
{
    CFReadStreamRef     readStream;
    CFWriteStreamRef    writeStream;
    
    assert( (inputStreamPtr != NULL) || (outputStreamPtr != NULL) );
    
    readStream = NULL;
    writeStream = NULL;
    
    CFStreamCreateBoundPair(
                            NULL,
                            ((inputStreamPtr  != nil) ? &readStream : NULL),
                            ((outputStreamPtr != nil) ? &writeStream : NULL),
                            (CFIndex) bufferSize);
    
    if (inputStreamPtr != NULL) {
        *inputStreamPtr  = CFBridgingRelease(readStream);
    }
    if (outputStreamPtr != NULL) {
        *outputStreamPtr = CFBridgingRelease(writeStream);
    }
    
    [*outputStreamPtr scheduleInRunLoop:[NSRunLoop currentRunLoop]
                       forMode:NSDefaultRunLoopMode];
    [*outputStreamPtr open];
}
@end
