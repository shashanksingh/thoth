//
//  PocketSphinxVCRService.m
//  Briefrr
//
//  Created by Shashank Singh on 4/5/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "PocketSphinxVCR.h"
#import "RecognitionModelManager.h"
#import <pocketsphinx/pocketsphinx.h>
#import "Logging.h"
#import "EventLogger.h"
#import "Macros.h"

static const char* BACKGROUND_Q_NAME = "com.briefrr.backgroundQueue.ps_vcr";

@interface PocketSphinxVCR()
@property (nonatomic) ps_decoder_t *decoder;
@property (nonatomic, strong) NSString *acousticModelPath;
@property (nonatomic, strong) NSString *gramFilePath;
@property (nonatomic, strong) NSString *dicFilePath;
@property (nonatomic, strong) NSString *wakeUpPhrase;
@property (nonatomic) BOOL isVoiceActivityOn;
@end

@implementation PocketSphinxVCR
- (instancetype)init {
    self = [super init];
    if (self) {
        self.name = @"pocketsphinx";
        self.acousticModelPath = [[RecognitionModelManager sharedInstance] getAcousticModelPath];
        [self initializeDecoder];
    }
    return self;
}

- (void)dealloc {
    if (self.decoder) {
        ps_free(self.decoder);
    }
}

-(dispatch_queue_t)getBackgroundQueue {
    static dispatch_once_t queueCreationGuard;
    static dispatch_queue_t queue;
    dispatch_once(&queueCreationGuard, ^{
        queue = dispatch_queue_create(BACKGROUND_Q_NAME, DISPATCH_QUEUE_SERIAL);
    });
    return queue;
}

- (void)initializeDecoder {
    if (!self.gramFilePath) {
        DDLogInfo(@"grammar file not available, skipping initing ps decoder");
        return;
    }
    if (!self.dicFilePath) {
        DDLogInfo(@"dictionary file not available, skipping initing ps decoder");
        return;
    }
    
    NSString *timingEventName = @"init_asr_decoder";
    [[EventLogger getInstance] timeEventStart:timingEventName];
    
    DDLogVerbose(@"grammar: %@", [NSString stringWithContentsOfFile:self.gramFilePath encoding:NSUTF8StringEncoding error:nil]);
    //DDLogVerbose(@"dictionary: %@", [NSString stringWithContentsOfFile:self.dicFilePath encoding:NSUTF8StringEncoding error:nil]);
    
    cmd_ln_t *config = cmd_ln_init(NULL, ps_args(), TRUE,
                                   "-hmm", [self.acousticModelPath cStringUsingEncoding:NSUTF8StringEncoding],
                                   "-dict", [self.dicFilePath cStringUsingEncoding:NSUTF8StringEncoding],
                                   "-kws", [self.gramFilePath cStringUsingEncoding:NSUTF8StringEncoding],
                                   "-kws_plp", "1e-10",
                                   "-kws_threshold", "1e10",
                                   "-vad_prespeech", "2",
                                   "-vad_postspeech", "100",
                                   "-cmn", "none",
                                   NULL);
    
    if (config == NULL) {
        DDLogError(@"Error: unable to create config for pocketsphinx decoder");
        [[EventLogger getInstance] timeEventEnd:timingEventName properties:@{@"success": @(NO), @"error": @"cmd_ln_init_failed"}];
        return;
    }
    
    if (self.decoder) {
        ps_free(self.decoder);
        self.decoder = NULL;
    }
    
    ps_default_search_args(config);
    self.decoder = ps_init(config);
    if (self.decoder == NULL) {
        [[EventLogger getInstance] timeEventEnd:timingEventName properties:@{@"success": @(NO), @"error": @"ps_init_failed"}];
        DDLogError(@"Error: unable to create pocketsphinx decoder");
        return;
    }
    
    [[EventLogger getInstance] timeEventEnd:timingEventName properties:@{@"success": @(YES)}];
}

- (void)setCommandsToRecognize:(NSArray*)commands withWakeUpPhrase:(NSString*)wakeUpPhrase {
    __weak typeof(self) bself = self;
    
    dispatch_async([self getBackgroundQueue], ^{
        if (!bself) {
            return;
        }
        
        [super setCommandsToRecognize:commands withWakeUpPhrase:wakeUpPhrase];
        
        RecognitionModelManager *rmm = [RecognitionModelManager sharedInstance];
        bself.gramFilePath = [rmm getGrammarFilePathForCommands:bself.currentCommands];
        bself.dicFilePath = [rmm getDictionaryFilePathForCommands:bself.currentCommands];
        
        if (!bself.decoder) {
            [bself initializeDecoder];
        } else {
            int ret;
            
            ret = ps_set_kws(bself.decoder, "keywords", [bself.gramFilePath cStringUsingEncoding:NSUTF8StringEncoding]);
            if (ret != 0) {
                [[EventLogger getInstance] logEvent:@"error_vcr_set_kws_failed" properties:@{@"error_code": @(ret)}];
                DDLogError(@"error in changing keywords, ps_set_kws failed, error: %d", ret);
                return;
            }
            ret = ps_set_search(bself.decoder, "keywords");
            if (ret != 0) {
                [[EventLogger getInstance] logEvent:@"error_vcr_set_search_failed" properties:@{@"error_code": @(ret)}];
                DDLogError(@"error in changing keywords, ps_set_search failed error: %d", ret);
                return;
            }
        }
    });
}

- (void)onVoiceActivityBeginWithId:(int)voiceActivityId {
    [super onVoiceActivityBeginWithId:voiceActivityId];
    
    __weak typeof(self) bself = self;
    dispatch_async([self getBackgroundQueue], ^{
        if (!bself) {
            return;
        }
        bself.isVoiceActivityOn = YES;
    });
}

- (void)onVoiceActivityEndWithSamples:(NSData*)data {
    [super onVoiceActivityEndWithSamples:data];
    
    __weak typeof(self) bself = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (bself && bself.delegate) {
            [bself.delegate commandRecognitionBeganByRecognizer:bself forVoiceActivityId:bself.currentVoiceActivityId];
        }
    });
    
    dispatch_async([self getBackgroundQueue], ^{
        if (!bself) {
            return;
        }
        
        NSString *command;
        int score = -1;
        
        @try {
            bself.isVoiceActivityOn = NO;
            
            if (!bself.decoder) {
                DDLogWarn(@"Warn: onVoiceActivityEndInAudioSamples called but decoder is null");
                return;
            }
            
            NSTimeInterval startTime = [NSDate timeIntervalSinceReferenceDate];
            
            int rv = ps_start_utt(bself.decoder, NULL);
            if (rv < 0) {
                DDLogError(@"Error: ps_start_utt returned error: %d", rv);
                return;
            }
            
            
            rv = ps_process_raw(bself.decoder, data.bytes, data.length/NUM_BYTES_PER_SAMPLE, FALSE, FALSE);
            if (rv < 0) {
                DDLogError(@"Error: ps_process_raw returned error: %d", rv);
                return;
            }
            
            
            rv = ps_end_utt(bself.decoder);
            if (rv < 0) {
                DDLogError(@"Error: ps_end_utt returned error: %d", rv);
                return;
            }
            
            char const *hypothesis, *uttid;
            
            hypothesis = ps_get_hyp(bself.decoder, &score, &uttid);
            NSTimeInterval endTime = [NSDate timeIntervalSinceReferenceDate];
            NSTimeInterval processingTime = endTime - startTime;
            
            DDLogVerbose(@"Pocketshpinx time taken to process: %.3f, samples: %lu, score: %d",
                         processingTime, data.length/NUM_BYTES_PER_SAMPLE, score);
            
            if (hypothesis == NULL) {
                DDLogVerbose(@"pocketsphinx returned NULL hypothesis");
                return;
            }
            
            DDLogVerbose(@"pocketsphinx returned hypothesis: %@", [NSString stringWithCString:hypothesis encoding:NSUTF8StringEncoding]);
            
            command = [NSString stringWithCString:hypothesis encoding:NSUTF8StringEncoding];
            command = [bself postProcessRecognizedHypothesis:command forRecognizer:bself.name];
            
        } @finally {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bself && bself.delegate) {
                    [bself.delegate commandRecognitionEndedByRecognizer:bself forVoiceActivityId:bself.currentVoiceActivityId];
                    
                    
                    if (command) {
                        [bself.delegate commandRecognized:command withScore:score byRecognizer:bself forVoiceActivityId:bself.currentVoiceActivityId];
                    } else {
                        [bself.delegate commandRecognized:nil withScore:-1 byRecognizer:bself forVoiceActivityId:bself.currentVoiceActivityId];
                    }
                }
            });
        }
    });
}
@end
