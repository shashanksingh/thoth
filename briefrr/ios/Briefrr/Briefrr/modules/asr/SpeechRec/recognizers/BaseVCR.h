//
//  BaseVCR.h
//  Briefrr
//
//  Created by Shashank Singh on 4/5/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BaseVCR;

@protocol VCRDelegate <NSObject>
- (void)commandRecognitionBeganByRecognizer:(BaseVCR*)recognizer forVoiceActivityId:(int)voiceActivityId;
- (void)commandRecognitionEndedByRecognizer:(BaseVCR*)recognizer forVoiceActivityId:(int)voiceActivityId;
- (void)commandRecognized:(NSString*)command withScore:(SInt32)score byRecognizer:(BaseVCR*)recognizer forVoiceActivityId:(int)voiceActivityId;
@end

@interface BaseVCR : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *wakeUpPhrase;
@property (nonatomic, strong) NSArray *currentCommands;
@property (nonatomic) int currentVoiceActivityId;
@property (nonatomic, weak) id<VCRDelegate> delegate;

- (void)setCommandsToRecognize:(NSArray*)commands withWakeUpPhrase:(NSString*)wakeUpPhrase;
- (void)onVoiceActivityBeginWithId:(int)voiceActivityId;
- (void)onVoiceActivityEndWithSamples:(NSData*)data;
- (void)onVoiceActivityAudioSamples:(NSData*)data;
- (NSString*)postProcessRecognizedHypothesis:(NSString*)hyp forRecognizer:(NSString*)recognizer;
@end
