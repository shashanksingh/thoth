//
//  RecognitionModelManager.h
//  SpeechRec
//
//  Created by Shashank Singh on 11/3/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecognitionModelManager : NSObject
+(RecognitionModelManager*)sharedInstance;

-(NSString*)getAcousticModelPath;
-(NSString*)getGrammarFilePathForCommands:(NSArray*)commands;
-(NSString*)getDictionaryFilePathForCommands:(NSArray*)commands;
@end
