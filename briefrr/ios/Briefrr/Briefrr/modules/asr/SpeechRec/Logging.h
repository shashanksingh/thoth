//
//  Logging.h
//  SpeechRec
//
//  Created by Shashank Singh on 11/5/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#ifndef SpeechRec_Logging_h
#define SpeechRec_Logging_h

#import <CocoaLumberjack/CocoaLumberjack.h>
#import <CocoaLumberjack/DDLog.h>
#import <CocoaLumberjack/DDTTYLogger.h>


static const NSUInteger ddLogLevel = DDLogLevelAll;

#endif
