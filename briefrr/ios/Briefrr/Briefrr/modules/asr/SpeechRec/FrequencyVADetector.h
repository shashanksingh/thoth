//
//  FrequencyVADetector.h
//  SpeechRec
//
//  Created by Shashank Singh on 12/18/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FrequencyVADetector : NSObject
-(BOOL)samplesHaveVoiceActivity:(SInt16*)samples numSamples:(UInt32)numSamples
            withOutputMuted:(BOOL)isOutputMuted;
@end
