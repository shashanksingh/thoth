//
//  AudioSubsystem.m
//  SpeechRec
//
//  Created by Shashank Singh on 11/2/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import "AudioSubsystem.h"

#import <AudioToolbox/AudioToolbox.h>
#import <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVFoundation.h>
#import "TTS.h"
#import "VADetector.h"
#import "AudioRecorder.h"
#import "Logging.h"
#import "EventLogger.h"
#import "EventManager.h"

#define kOutputBus 0
#define kInputBus 1

#define kSamplesPerSecond 16000
#define kBytesPerSample 2
#define kPreferredHardwareBufferSize 0.064 // in seconds

#define kOutputVolumeAttenuationFactor 0.0
#define kOutputVolumeAttenuationMinInterval 4


#define kLowPassFilterTimeSlice .001 // For calculating decibel levels
#define kDBOffset -120.0 // This is the negative offset for calculating decibel levels

@interface AudioSubsystem()
@property (nonatomic) AudioUnit audioUnit;
@property (nonatomic) BOOL isPaused;
@property (nonatomic, strong) VADetector *vaDetector;
@property (nonatomic) SInt16 *processedAudioSamples;
@property (nonatomic) double inputDecibels;
@property (nonatomic) BOOL isTTSOn;
@property (nonatomic, strong) NSTimer *attenuationCancellationTimer;
@end

@implementation AudioSubsystem
@synthesize audioUnit;

static OSStatus RenderNotification(void *inRefCon, AudioUnitRenderActionFlags *ioActionFlags, const AudioTimeStamp *inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList *ioData ) {
    if (inBusNumber == kOutputBus && *ioActionFlags | kAudioUnitRenderAction_PostRender) {
        [[SpeechSynthesizer sharedInstance] onAudioSamplesRenderd:inNumberFrames];
    }
    
    return noErr;
}

static OSStatus PlayoutProcess (void *inRefCon,
                                AudioUnitRenderActionFlags *ioActionFlags,
                                const AudioTimeStamp *inTimeStamp,
                                UInt32 inBusNumber,
                                UInt32 inNumberFrames,
                                AudioBufferList *ioData) {
    
    AudioSubsystem *this = (__bridge AudioSubsystem*)inRefCon;
    
    AudioBuffer buffer = ioData->mBuffers[0];
    SInt16* dataBuffer = buffer.mData;
    // TODO (shashank): find a better way to pause that uses
    // less CPU, may be by destroying and re-creating the
    // audiounit
    if (this.isPaused) {
        memset(dataBuffer, 0, buffer.mDataByteSize);
        return noErr;
    }
    
    SInt16* speechData = NULL;
    UInt32 numSpeechSamples = 0;
    SpeechSynthesizer *tts = [SpeechSynthesizer sharedInstance];
    [tts getAudioSamples:inNumberFrames inArray:&speechData withLength:&numSpeechSamples];
    
    this.isTTSOn = numSpeechSamples > 0;
    
    if (numSpeechSamples == 0) {
        memset(dataBuffer, 0, buffer.mDataByteSize);
        return noErr;
    }
    
    memset(dataBuffer, 0, buffer.mDataByteSize);
    memcpy(dataBuffer, speechData, numSpeechSamples * kBytesPerSample);
    
    return noErr;
    
}

static OSStatus	AudioUnitRenderCallback (void *inRefCon,
                                         AudioUnitRenderActionFlags *ioActionFlags,
                                         const AudioTimeStamp *inTimeStamp,
                                         UInt32 inBusNumber,
                                         UInt32 inNumberFrames,
                                         AudioBufferList *ioData) {
    
    AudioSubsystem *this = (__bridge AudioSubsystem*)inRefCon;
    if (this.isPaused) {
        return noErr;
    }
    
    AudioBuffer buffer;
    buffer.mDataByteSize = inNumberFrames * kBytesPerSample;
    buffer.mNumberChannels = 1;
    buffer.mData = NULL;
    
    AudioBufferList bufferList;
    bufferList.mNumberBuffers = 1;
    bufferList.mBuffers[0] = buffer;
    
    if (inNumberFrames > 0) {
        OSStatus renderStatus = AudioUnitRender(this.audioUnit, ioActionFlags,
                                                inTimeStamp, inBusNumber,
                                                inNumberFrames, &bufferList);
        
        if(renderStatus != noErr) {
            DDLogError(@"Error in AudioUnitRender: %d", (int)renderStatus);
            return renderStatus;
            
        } else {
            [[AudioRecorder sharedInstance] recordFrames:&bufferList :inNumberFrames];
            [this.vaDetector addAudioSamples:bufferList.mBuffers[0].mData OfSize:inNumberFrames * kBytesPerSample withTTSOn:this.isTTSOn];
            [this updateInputDecibelsForInputSamples:bufferList.mBuffers[0].mData withCount:inNumberFrames];
        }
    }
    
    return noErr;
}

- (void)setUpEvents {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAudioSessionEvent:) name:AVAudioSessionInterruptionNotification object:nil];
    
    __weak typeof(self) bself = self;
    [[EventManager getInstance] addEventListener:VOICE_ACTIVITY_BEGIN withCallback:^(va_list args) {
        if (!bself) {
            return;
        }
        [bself onVoiceActivityBegin];
    }];
    
    [[EventManager getInstance] addEventListener:VOICE_ACTIVITY_END withCallback:^(va_list args) {
        if (!bself) {
            return;
        }
        
        NSData *data = va_arg(args, NSData*);
        [bself onVoiceActivityEndWithData:data];
    }];
}

-(AudioSubsystem*)init {
    self = [super init];
    if (self) {
        self.audioUnit = NULL;
        self.isPaused = NO;
        self.isTTSOn = NO;
        self.attenuationCancellationTimer = NULL;
        
        [self setUpEvents];
        self.vaDetector = [[VADetector alloc] init];
    }
    return self;
}

-(void)dealloc {
    if (self.processedAudioSamples) {
        free(self.processedAudioSamples);
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(double)getInputGainLevel {
    return self.inputDecibels;
}

- (void)pause {
    self.isPaused = YES;
    if (self.audioUnit) {
        AudioOutputUnitStop(self.audioUnit);
        AudioUnitUninitialize(self.audioUnit);
        AudioComponentInstanceDispose(self.audioUnit);
        
        self.audioUnit = NULL;
    }
}

- (void)resume {
    self.isPaused = NO;
    [self initAudioUnit];
}

- (void)cancelOutputVolumeAttenuationTimer {
    if (self.attenuationCancellationTimer) {
        [self.attenuationCancellationTimer invalidate];
        self.attenuationCancellationTimer = nil;
    }
}

- (void)onVoiceActivityBegin {
    DDLogVerbose(@"output volume attenuation started");
    [self cancelOutputVolumeAttenuationTimer];
}

- (void)onVoiceActivityEndWithoutData {
    [self onVoiceActivityEndWithData:nil];
}

- (void)onVoiceActivityEndWithData:(NSData*)data {
    DDLogVerbose(@"output volume attenuation ended");
    // if the ASR system does not find any valid commands in the VADed samples we
    // automatically stop attenuation after a timeout
    [self cancelOutputVolumeAttenuationTimer];
    if (!data) {
        return;
    }
    
    self.attenuationCancellationTimer = [NSTimer scheduledTimerWithTimeInterval:kOutputVolumeAttenuationMinInterval
                                                                         target:self
                                                                       selector:@selector(onVoiceActivityEndWithoutData)
                                                                       userInfo:nil
                                                                        repeats:NO];
    
}

- (void)onAudioSessionEvent:(NSNotification*)notification {
    if (![notification.name isEqualToString:AVAudioSessionInterruptionNotification]) {
        return;
    }
    if ([[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] isEqualToNumber:@(AVAudioSessionInterruptionTypeBegan)]) {
        [[EventLogger getInstance] logEvent:@"audio_session_interruption_began"];
        
    } else if([[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] isEqualToNumber:@(AVAudioSessionInterruptionTypeEnded)]){
        [[EventLogger getInstance] logEvent:@"audio_session_interruption_ended"];
        [self initAudioUnit];
    }
}


-(void)updateInputDecibelsForInputSamples:(SInt16*)samples withCount:(UInt32)numSamples {
    if (numSamples == 0) {
        self.inputDecibels = 0;
        return;
    }
    
    Float32 decibels = kDBOffset; // When we have no signal we'll leave this on the lowest setting
    Float32 currentFilteredValueOfSampleAmplitude;
    Float32 previousFilteredValueOfSampleAmplitude = 0.0; // We'll need these in the low-pass filter
    Float32 peakValue = kDBOffset; // We'll end up storing the peak value here
    
    for (int i=0; i <numSamples; i=i+5) { // We're incrementing this by 5 because there's actually too much info here for us for a conventional UI timeslice and it's a cheap way to save CPU
        //NSLog(@"sample: %d", samples[i]);
        
        Float32 absoluteValueOfSampleAmplitude = abs(samples[i]); //Step 2: for each sample, get its amplitude's absolute value.
        
        // Step 3: for each sample's absolute value, run it through a simple low-pass filter
        // Begin low-pass filter
        currentFilteredValueOfSampleAmplitude = kLowPassFilterTimeSlice * absoluteValueOfSampleAmplitude + (1.0 - kLowPassFilterTimeSlice) * previousFilteredValueOfSampleAmplitude;
        previousFilteredValueOfSampleAmplitude = currentFilteredValueOfSampleAmplitude;
        Float32 amplitudeToConvertToDB = currentFilteredValueOfSampleAmplitude;
        // End low-pass filter
        
        Float32 sampleDB = 20.0*log10(amplitudeToConvertToDB) + kDBOffset;
        // Step 4: for each sample's filtered absolute value, convert it into decibels
        // Step 5: for each sample's filtered absolute value in decibels, add an offset value that normalizes the clipping point of the device to zero.
        
        if((sampleDB == sampleDB) && (sampleDB <= DBL_MAX && sampleDB >= -DBL_MAX)) { // if it's a rational number and isn't infinite
            
            if(sampleDB > peakValue) peakValue = sampleDB; // Step 6: keep the highest value you find.
            decibels = peakValue; // final value
        }
    }
    
    self.inputDecibels = (decibels + 160)/160;
}


-(void) initAudioUnit {
    
    AudioComponentDescription audioUnitDescription;
    
    audioUnitDescription.componentType = kAudioUnitType_Output;
    audioUnitDescription.componentSubType = kAudioUnitSubType_VoiceProcessingIO;
    audioUnitDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
    audioUnitDescription.componentFlags = 0;
    audioUnitDescription.componentFlagsMask = 0;
    
    AudioComponent audioComponent = AudioComponentFindNext(NULL, &audioUnitDescription);
    
    OSStatus status;
    NSError *error;
    
    status = AudioComponentInstanceNew(audioComponent, &self->audioUnit);
    if(status != noErr) {
        DDLogError(@"Error: Couldn't get new audio unit component instance: %d", (int)status);
        return;
    }
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    if (![session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionDefaultToSpeaker error:&error]) {
        DDLogError(@"Error: Couldn't set audio session category: %@", error);
    }
    
    // TODO (shashank): try video chat mode as well
    if (![session setMode:AVAudioSessionModeGameChat error:&error]) {
        DDLogError(@"Error: Couldn't set audio session mode: %@", error);
        return;
    }
    
    if (![session setPreferredIOBufferDuration:kPreferredHardwareBufferSize error:&error]) {
        DDLogError(@"Error: Couldn't set preferred IO buffer duration: %@", error);
        return;
    }
    
    if (session.IOBufferDuration != kPreferredHardwareBufferSize) {
        DDLogWarn(@"Warn: preferredIOBufferDuration not at the desired value, set: %.5f, get:%.5f", kPreferredHardwareBufferSize, session.preferredIOBufferDuration);
    }
    
    [session setActive:YES error:&error];
    if (error) {
       DDLogWarn(@"Warn: error in making audio session active: %@", error);
    }
    
    int one = 1;
    status = AudioUnitSetProperty(self.audioUnit, kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Input, kInputBus, &one, sizeof(one));
    if(status != noErr) {
        DDLogError(@"Error: Couldn't enable input on audio unit: %d", (int)status);
        return;
    }
    
    AudioStreamBasicDescription audioUnitRecordFormat;
    audioUnitRecordFormat.mChannelsPerFrame = 1;
    audioUnitRecordFormat.mSampleRate = kSamplesPerSecond;
    audioUnitRecordFormat.mFormatID = kAudioFormatLinearPCM;
    audioUnitRecordFormat.mBytesPerPacket = audioUnitRecordFormat.mChannelsPerFrame * kBytesPerSample;
    audioUnitRecordFormat.mFramesPerPacket = 1;
    audioUnitRecordFormat.mBytesPerFrame = audioUnitRecordFormat.mBytesPerPacket;
    audioUnitRecordFormat.mBitsPerChannel = kBytesPerSample * 8;
    audioUnitRecordFormat.mFormatFlags = kAudioFormatFlagsNativeEndian | kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
    
    status = AudioUnitSetProperty(self.audioUnit, kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Input, kOutputBus,
                                  &audioUnitRecordFormat, sizeof(audioUnitRecordFormat));
    if(status != noErr) {
        DDLogError(@"Error: unable to set input scope output format: %d", (int)status);
        return;
    }
    
    status = AudioUnitSetProperty(self.audioUnit, kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Output, kInputBus,
                                  &audioUnitRecordFormat, sizeof(audioUnitRecordFormat));
    if(status != noErr) {
        DDLogError(@"Error: unable to set output scope input format: %d", (int)status);
        return;
    }
    
    AURenderCallbackStruct auCbS;
    memset(&auCbS, 0, sizeof(auCbS));
    auCbS.inputProc = AudioUnitRenderCallback;
    auCbS.inputProcRefCon = (__bridge void *)(self);
    
    status = AudioUnitSetProperty(self.audioUnit, kAudioOutputUnitProperty_SetInputCallback,
                                  kAudioUnitScope_Global, kInputBus, &auCbS, sizeof(auCbS));
    if(status != noErr) {
        DDLogError(@"Error: unable to set output callback on input scope: %d", (int)status);
        return;
    }
    
    memset(&auCbS, 0, sizeof(auCbS));
    auCbS.inputProc = PlayoutProcess;
    auCbS.inputProcRefCon = (__bridge void *)(self);
    status = AudioUnitSetProperty(self.audioUnit, kAudioUnitProperty_SetRenderCallback,
                                  kAudioUnitScope_Input, kOutputBus, &auCbS, sizeof(auCbS));
    if(status != noErr) {
        DDLogError(@"Error: unable to set input callback on output scope: %d", (int)status);
        return;
    }
    
    status = AudioUnitAddRenderNotify(self.audioUnit, RenderNotification, NULL);
    if (status != noErr) {
        DDLogError(@"Error: unable to set render notification callback: %d", (int)status);
        return;
    }
    
    status = AudioUnitInitialize(self.audioUnit);
    if(status != noErr) {
        DDLogError(@"Error: unable to initialize audio unit: %d", (int)status);
        return;
    }
    
    status = AudioOutputUnitStart(self.audioUnit);
    if (status != noErr) {
        DDLogError(@"Error: unable to start audio unit: %d", (int)status);
        return;
    }
}
@end
