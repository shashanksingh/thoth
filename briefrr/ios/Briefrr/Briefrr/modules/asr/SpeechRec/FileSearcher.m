//
//  FileSearcher.m
//  SpeechRec
//
//  Created by Shashank Singh on 11/3/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import "FileSearcher.h"

@implementation FileSearcher

+(NSData*)readFile:(NSFileHandle*)fileHandle untilData:(NSData*)searchKey readChunkSize:(UInt32)chunkSize uptoOffset:(unsigned long long)endOffset {
    
    NSMutableData *readData = [NSMutableData data];
    
    while (1) {
        unsigned long long bytesUntilEnd = endOffset - fileHandle.offsetInFile;
        //NSLog(@"bytes until end: %llu", bytesUntilEnd);
        UInt32 nextChunkSize = bytesUntilEnd > chunkSize ? chunkSize : (UInt32)bytesUntilEnd;
        if (nextChunkSize <= 0) {
            return NULL;
        }
        
        NSData *chunk = [fileHandle readDataOfLength:nextChunkSize];
        // reached the end of file without being able to find key
        if (chunk.length == 0) {
            return NULL;
        }
        
        NSRange keyPosition =
        [chunk rangeOfData:searchKey options:0 range:NSMakeRange(0, chunk.length)];
        if (keyPosition.location != NSNotFound) {
            [readData appendData:[chunk subdataWithRange:NSMakeRange(0, keyPosition.location)]];
            return readData;
        }
    }
    return NULL;
}

+(NSDictionary*)getLinesStartingWith:(NSArray*)prefixes inFileAtPath:(NSString*)filePath andMatchAllLinesWithSameRoot:(BOOL)matchAllLinesWithSameRoot {
    
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingAtPath:filePath];
    if (fileHandle == NULL) {
        return NULL;
    }
    
    NSMutableDictionary *prefixToLine = [NSMutableDictionary dictionary];
    
    unsigned long long startOffset = 0ULL;
    [fileHandle seekToEndOfFile];
    unsigned long long fileEndOffset = [fileHandle offsetInFile];
    unsigned long long endOffset = fileEndOffset;
    
    
    NSData *newLineData = [@"\n" dataUsingEncoding:NSUTF8StringEncoding];
    NSData *tabData = [@"\t" dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error = NULL;
    NSRegularExpression *duplicationSuffixRegex = [NSRegularExpression regularExpressionWithPattern:@"\\(\\d+\\)$" options:NSRegularExpressionCaseInsensitive error:&error];
    if (error != NULL) {
        NSLog(@"bad regular expression for duplicationSuffixRegex: %@", error);
        return NULL;
    }
    
    NSArray *sortedPrefixes = [prefixes sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    for (NSString *prefix in sortedPrefixes) {
        
        while (startOffset + 1 < endOffset) {
            
            NSLog(@"start, end: %lld, %lld", startOffset, endOffset);
            
            unsigned long long midOffset = (startOffset/2) + (endOffset/2);
            [fileHandle seekToFileOffset:midOffset];
            
            NSData *dataUntilNextNewLine = [self readFile:fileHandle untilData:newLineData readChunkSize:128 uptoOffset:endOffset];
            
            NSLog(@"dataUntilNextNewLine: %@, %lu", [[NSString alloc] initWithData:dataUntilNextNewLine encoding:NSUTF8StringEncoding], (unsigned long)dataUntilNextNewLine.length);
            
            if (!dataUntilNextNewLine) {
                // this means the current segment is so small the mid to end of it
                // does not have any new lines (i.e. the second half is completely
                // contained within one line of data
                // TODO (shashank): replace the brute forcing with better edge case handling
                NSLog(@"reached end of segment, brute force search in whole segment: %@, %llu, %llu, %llu", prefix, startOffset, endOffset, endOffset - startOffset);
                
                [fileHandle seekToFileOffset:startOffset];
                BOOL found = NO;
                NSData *segmentData = [fileHandle readDataOfLength:endOffset - startOffset];
                NSString *segment = [[NSString alloc] initWithData:segmentData encoding:NSUTF8StringEncoding];
                NSArray *segmentLines = [segment componentsSeparatedByString:@"\n"];
                for (NSString *line in segmentLines) {
                    NSArray *lineParts = [line componentsSeparatedByString:@"\t"];
                    NSString *word = lineParts[0];
                    if ([word isEqualToString:prefix]) {
                        NSLog(@"match found by brute force in small segment: |%@|", word);
                        found = YES;
                        [prefixToLine setObject:@[line] forKey:prefix];
                        break;
                    }
                }
                
                if (!found) {
                    NSLog(@"word %@ not found", prefix);
                }
                // we let the next search start with the start of this segment
                // TODO (shashank): increment the startOffset too if we the
                // word we are looking for was found
                endOffset = fileEndOffset;
                break;
                
                
            } else {
                midOffset += dataUntilNextNewLine.length;
                [fileHandle seekToFileOffset:midOffset < fileEndOffset ? midOffset + 1 : midOffset];
            }
            
            NSData *dataUntilNextTabFound = [self readFile:fileHandle untilData:tabData readChunkSize:64 uptoOffset:endOffset];
            NSLog(@"dataUntilNextTabFound: %@", [[NSString alloc] initWithData:dataUntilNextTabFound encoding:NSUTF8StringEncoding]);
            if (!dataUntilNextTabFound) {
                NSLog(@"reached end of segment, prefix not found: %@", prefix);
                break;
            }
            
            [fileHandle seekToFileOffset:midOffset];
            
            NSString *word = [[NSString alloc] initWithData:dataUntilNextTabFound encoding:NSUTF8StringEncoding];
            NSString *rootWord = word;
            if (matchAllLinesWithSameRoot && [rootWord hasSuffix:@")"]) {
                NSRange range = NSMakeRange(0, [rootWord length]);
                rootWord = [duplicationSuffixRegex stringByReplacingMatchesInString:rootWord options:0 range:range withTemplate:@""];
            }
            
            NSLog(@"word found: |%@|, looking for: |%@|", rootWord, prefix);
            
            NSComparisonResult comparisonResult = [rootWord compare:prefix];
            if (comparisonResult == NSOrderedSame) {
                // found a matching root word
                if (matchAllLinesWithSameRoot) {
                    // TODO: look around in both directions for words with the
                    // same root (in case of multiple pronunciations for the
                    // same word
                }
                
                NSLog(@"match found at: %lld", [fileHandle offsetInFile]);
                
                // get the line starting at the current word
                [fileHandle seekToFileOffset:midOffset < fileEndOffset ? midOffset + 1 : midOffset];
                NSData *lineData = [self readFile:fileHandle untilData:newLineData readChunkSize:128 uptoOffset:fileEndOffset];
                NSString *line = [[NSString alloc] initWithData:lineData encoding:NSUTF8StringEncoding];
                [prefixToLine setObject:@[line] forKey:prefix];
                
                // since we have sorted the prefix list, the next prefix can only be found
                // after the current one
                startOffset = midOffset;
                endOffset = fileEndOffset;
                
                break;
                
            } else if (comparisonResult == NSOrderedAscending) {
                // the found word is "smaller" than the one we are looking for
                // search in the next half
                NSLog(@"ASC");
                if (midOffset == endOffset) {
                    NSLog(@"word not found");
                    break;
                }
                startOffset = midOffset;
            } else {
                // the found word is "greater" than the one we are looking for
                // search in the previous half
                NSLog(@"DESC");
                if (endOffset == midOffset) {
                    NSLog(@"word not found");
                    break;
                }
                endOffset = midOffset;
            }
        }
    }
    
    return prefixToLine;
    
}
@end

