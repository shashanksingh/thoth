//
//  brfrCircularBuffer.c
//  SpeechRec
//
//  Source: http://en.wikipedia.org/wiki/Circular_buffer
//
#include "brfrCircularBuffer.h"

#include <stdio.h>
#include <stdlib.h>

void brfrCbInit(BrfrCircularBuffer *cb, int size) {
    cb->size  = size + 1; /* include empty elem */
    cb->start = 0;
    cb->end   = 0;
    cb->elems = calloc(cb->size, sizeof(long));
}

void brfrCbFree(BrfrCircularBuffer *cb) {
    free(cb->elems); /* OK if null */
}

int brfrCbIsFull(BrfrCircularBuffer *cb) {
    return (cb->end + 1) % cb->size == cb->start;
}

int brfrCbIsEmpty(BrfrCircularBuffer *cb) {
    return cb->end == cb->start;
}

/* Write an element, overwriting oldest element if buffer is full. App can
 choose to avoid the overwrite by checking cbIsFull(). */
void brfrCbWrite(BrfrCircularBuffer *cb, long value) {
    cb->elems[cb->end] = value;
    cb->end = (cb->end + 1) % cb->size;
    if (cb->end == cb->start)
        cb->start = (cb->start + 1) % cb->size; /* full, overwrite */
}

/* Read oldest element. App must ensure !cbIsEmpty() first. */
long brfrCbRead(BrfrCircularBuffer *cb) {
    long rv = cb->elems[cb->start];
    cb->start = (cb->start + 1) % cb->size;
    return rv;
}

/* Read oldest element. App must ensure !cbIsEmpty() first. */
long brfrCbPeek(BrfrCircularBuffer *cb) {
    return cb->elems[cb->start];
}

