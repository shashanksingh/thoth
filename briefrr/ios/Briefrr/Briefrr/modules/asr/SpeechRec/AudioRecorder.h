//
//  AudioRecorder.h
//  SpeechRec
//
//  Created by Shashank Singh on 11/2/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/ExtendedAudioFile.h>

@interface AudioRecorder : NSObject {
@private
    ExtAudioFileRef outExtAudioFile;
}

+(AudioRecorder*)sharedInstance;
-(void)addData:(char*)data dataLength:(UInt32)length;
-(void)recordFrames:(AudioBufferList*)bufferList :(UInt32)numFrames;
-(void)recordFrames2:(NSData*)data;
-(void)recordFrames3:(NSData*)data :(BOOL)isSpeech;
-(void)closeFiles;
@end
