//
//  RootViewController.m
//  Briefrr
//
//  Created by Shashank Singh on 2/8/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "RootViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <pop/POP.h>
#import "LoadingViewController.h"
#import "EventManager.h"
#import "UserPreferenceService.h"
#import "SourcesService.h"
#import "ViewSlideTransitionAnimator.h"
#import "ViewSlideTransitionContext.h"
#import "SourcesIntroViewController.h"
#import "TwoSectionView.h"
#import "SourcesViewController.h"
#import "MicPermissionIntroViewController.h"
#import "NotificationPermissionIntroViewController.h"
#import "FeedViewController.h"
#import "Util.h"
#import "NotificationManager.h"
#import "RemoteControlEventHandler.h"
#import "IdlingManagementService.h"
#import "EventLogger.h"
#import "AboutView.h"

static const NSTimeInterval NOTIFICATION_PERMISSION_RE_ASK_MIN_INTERVAL = 7 * 86400;

@interface RootViewController ()<UIViewControllerTransitioningDelegate>
@end

@implementation RootViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        [[NotificationManager getInstance] updateOnInit];
        [[RemoteControlEventHandler getInstance] start];
    }
    return self;
}

- (void)viewDidLoad {
    [[EventLogger getInstance] logEvent:@"root_ui_loaded"];
    
    [super viewDidLoad];
    
    // having an empty TwoSectionView as background makes the sliding animation look better
    self.view = [[TwoSectionView alloc] initWithFrame:[[UIScreen mainScreen] bounds] hideLogo:YES];
    
    LoadingViewController *loadingViewController;
    loadingViewController = [[LoadingViewController alloc] init];
    
    
    // Note (shashank): since loading view controller always takes time to finish off
    // its animation we'll never be too late by setting the event listener here
    __weak typeof(self) bself = self;
    
    NSString *loadingEventListenerId =
        [[EventManager getInstance] addEventListener:LOADING_FINISHED withCallback:^(va_list args) {
            NSNumber *successObject = va_arg(args, NSNumber*);
            BOOL success = successObject.boolValue;
            // don't remove listener until the loading event has succeeded
            if (success) {
                [[EventManager getInstance] removeEventListener:loadingEventListenerId];
            }
            [bself onAppLoadingFinishedWithSuccess:success withLoadingVC:loadingViewController];
        }];
    
    [[EventManager getInstance] addEventListener:SHOW_FEED_PAGE withCallback:^(va_list args) {
        if (!bself) {
            return;
        }
        [bself showFeedPageWithPreviousVC:nil feedType:NEWS_FEED];
    }];
    
    [[EventManager getInstance] addEventListener:SHOW_SAVED_STORIES_PAGE withCallback:^(va_list args) {
        if (!bself) {
            return;
        }
        [bself showFeedPageWithPreviousVC:nil feedType:SAVED_STORIES];
    }];
    
    [[EventManager getInstance] addEventListener:SHOW_SOURCE_SELECTOR_PAGE withCallback:^(va_list args) {
        if (!bself) {
            return;
        }
        [bself showSourceSelectorPageWithPreviousVC:nil];
    }];
    
    [[EventManager getInstance] addEventListener:SHOW_ABOUT_PAGE withCallback:^(va_list args) {
        if (!bself) {
            return;
        }
        [bself showAboutPage];
    }];
    
    [[EventManager getInstance] addEventListener:FEED_READING_MODE_CHANGED withCallback:^(va_list args) {
        FeedReadingMode readingMode = ((NSNumber*)va_arg(args, NSNumber*)).intValue;
        if (readingMode == AUDIO) {
            [[IdlingManagementService getInstance] start];
        } else {
            [[IdlingManagementService getInstance] stop];
        }
    }];
    
    
    [self addViewController:loadingViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL)prefersStatusBarHidden {
    return YES;
}

- (UIViewController*)childViewControllerForStatusBarHidden {
    UIViewController *currentVC = [self getCurrentViewController];
    
    if ([currentVC isKindOfClass:[FeedViewController class]]) {
        return currentVC;
    }
    return nil;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [[IdlingManagementService getInstance] userInputReceived];
}

-(void)addViewController:(UIViewController*)viewController {
    viewController.transitioningDelegate = self;
    [self addChildViewController:viewController];
    viewController.view.frame = [[UIScreen mainScreen] bounds];
    [viewController didMoveToParentViewController:self];
    [self.view addSubview:viewController.view];
}

- (BOOL)shouldAskForNotificationPermission {
    if ([Util isNotificationPermissionGranted]) {
        return NO;
    }
    if (![[UserPreferenceService getInstance] hasUserSeenIntro]) {
        return NO;
    }
    if (![[UserPreferenceService getInstance] hasUserDeniedNotificationPermission]) {
        return YES;
    }
    NSTimeInterval timeSinceLastDenial = [[UserPreferenceService getInstance] timeSinceLastNotificationPermissionDenial];
    if (timeSinceLastDenial > NOTIFICATION_PERMISSION_RE_ASK_MIN_INTERVAL) {
        return YES;
    }
    return NO;
}

-(void)onAppLoadingFinishedWithSuccess:(BOOL)success withLoadingVC:(LoadingViewController*)loadingVC {
    BOOL introDone = [[UserPreferenceService getInstance] hasUserSeenIntro];
    BOOL isAnySourceSelected = [[SourcesService getInstance] isAnySourceSelected];
    if (!success) {
        if (!introDone) {
            // intro is not done yet and loading (of sources) failed
            // nothing we can do here, loading view will show an error
            // message, we just stop there
            return;
        } else if (!isAnySourceSelected) {
            // intro is done but no sources are selected and loading
            // of sources has failed, nothing we can do here, stop
            return;
        } else {
            // intro is done, at least one source is selected but
            // loading of news feed failed, take the user to an
            // empty feed page where we'll show an error message
            // TODO
        }
    } else {
        if (!introDone) {
            // intro is not done yet and we have successfully loaded
            // sources, take the user to sources intro page
            [self loadSourceIntroViewAfterPreviousViewController:loadingVC];
            return;
        } else if (!isAnySourceSelected) {
            // intro is done but no sources are selected and we have
            // successfully loaded sources, take the user to sources
            // intro page (instead of directly taking them to sources
            // page just to provide a bit of context) and they can
            // select sources
            [self loadSourceIntroViewAfterPreviousViewController:loadingVC];
        } else if ([self shouldAskForNotificationPermission]) {
            [self showNotificationPermissionPage];
        } else {
            // intro is done, at least one source is selected
            // and we have been able to load feed, take the user
            // to the feed page
            [self showFeedPageWithPreviousVC:nil feedType:NEWS_FEED];
        }
    }
}

- (void)transitionFromVC:(UIViewController *)fromVC toVC:(UIViewController *)toVC withCallback:(VoidCallback)callback  {
    if (!fromVC) {
        fromVC = [self getCurrentViewController];
    }
    
    [self addViewController:toVC];
    
    ViewSlideTransitionAnimator *animator = [[ViewSlideTransitionAnimator alloc] init];
    ViewSlideTransitionContext *context = [[ViewSlideTransitionContext alloc] initWithFromViewController:fromVC toViewController:toVC goingRight:YES];
    
    __weak typeof(self) bself = self;
    context.completionBlock = ^(BOOL didComplete) {
        if (!bself) {
            return;
        }
        
        [fromVC.view removeFromSuperview];
        [fromVC removeFromParentViewController];
        [toVC didMoveToParentViewController:self];
        
        if ([animator respondsToSelector:@selector (animationEnded:)]) {
            [animator animationEnded:didComplete];
        }
        
        if (callback) {
            callback();
        }
        [[EventManager getInstance] fireEvent:CURRENT_PAGE_CHANGED];
    };
    
    [bself setNeedsStatusBarAppearanceUpdate];
    [animator animateTransition:context];
    
}

-(void)loadSourceIntroViewAfterPreviousViewController:(UIViewController*)previousViewController {
    [[EventLogger getInstance] logEvent:@"show_source_intro"];
    SourcesIntroViewController *sourcesIntroVC = [[SourcesIntroViewController alloc] init];
    [self transitionFromVC:previousViewController toVC:sourcesIntroVC withCallback:nil];
}

- (void)showSourceSelectorPageWithPreviousVC:(UIViewController*)previousViewController {
    [[EventLogger getInstance] logEvent:@"show_source_selector"];
    SourcesViewController *sourcesVC = [[SourcesViewController alloc] init];
    if (!previousViewController) {
        previousViewController = [self getCurrentViewController];
    }
    __weak typeof(self) bself = self;
    [self transitionFromVC:previousViewController toVC:sourcesVC withCallback:^{
        [[EventManager getInstance] addEventListener:SOURCE_SELECTION_DONE oneShot:YES withCallback:^(va_list args) {
            [bself handleSourceSelectionDoneAfterPreviousViewController:sourcesVC];
        }];
    }];
}

- (void)handleSourceSelectionDoneAfterPreviousViewController:(UIViewController*)previousViewController {
    AVAudioSessionRecordPermission currentAudioPermission = [AVAudioSession sharedInstance].recordPermission;
    switch (currentAudioPermission) {
        case AVAudioSessionRecordPermissionUndetermined:
        case AVAudioSessionRecordPermissionDenied:
            [self showMicPermissionPageWithPreviousVC:previousViewController];
            break;
        case AVAudioSessionRecordPermissionGranted:
            [self showFeedPageWithPreviousVC:previousViewController feedType:NEWS_FEED];
        default:
            // TODO (shashank): log error
            break;
    }
}

- (void)showMicPermissionPageWithPreviousVC:(UIViewController*)previousViewController {
    [[EventLogger getInstance] logEvent:@"show_mic_permission_page"];
    MicPermissionIntroViewController *micVC = [[MicPermissionIntroViewController alloc] init];
    __weak typeof(self) bself = self;
    [self transitionFromVC:previousViewController toVC:micVC withCallback:^{
        [[EventManager getInstance] addEventListener:MIC_PERMISSION_GRANTED oneShot:YES withCallback:^(va_list args) {
            [bself showFeedPageWithPreviousVC:micVC feedType:NEWS_FEED];
        }];
    }];
}

- (void)showFeedPageWithPreviousVC:(UIViewController*)previousViewController feedType:(FeedType)feedType {
    if ([self isCurrentViewFeedOfType:feedType]) {
        return;
    }
    
    [[EventLogger getInstance] logEvent:@"show_feed_page"];
    if (!previousViewController) {
        previousViewController = [self getCurrentViewController];
    }
    
    [[UserPreferenceService getInstance] setUserHasSeenIntro];
    
    FeedViewController *feedVC = [[FeedViewController alloc] initWithFeedType:feedType];
    [self transitionFromVC:previousViewController toVC:feedVC withCallback:nil];
}

- (void)showNotificationPermissionPage {
    [[EventLogger getInstance] logEvent:@"show_notification_permission_page"];
    NotificationPermissionIntroViewController *notifVC = [[NotificationPermissionIntroViewController alloc] init];
    __weak typeof(self) bself = self;
    [self transitionFromVC:nil toVC:notifVC withCallback:^{
        [[EventManager getInstance] addEventListener:NOTIFICATION_PERMISSION_REQUEST_FINISHED oneShot:YES withCallback:^(va_list args) {
            [bself showFeedPageWithPreviousVC:notifVC feedType:NEWS_FEED];
        }];
    }];
}

- (void)showAboutPage {
    [[EventLogger getInstance] logEvent:@"show_about_page"];
    [AboutView show];
}

- (UIViewController*)getCurrentViewController {
    if (self.childViewControllers.count <= 0) {
        return nil;
    }
    return self.childViewControllers[self.childViewControllers.count - 1];
}

- (BOOL)isCurrentViewFeedOfType:(FeedType)feedType {
    UIViewController *currentVC = [self getCurrentViewController];
    if (!currentVC || ![currentVC isKindOfClass:[FeedViewController class]]) {
        return NO;
    }
    
    FeedViewController *feedVC = (FeedViewController*)currentVC;
    return feedVC.feedType == feedType;
}

- (BOOL)isCurrentViewSourceSelector {
    UIViewController *currentVC = [self getCurrentViewController];
    if (!currentVC) {
        return NO;
    }
    return [currentVC isKindOfClass:[SourcesViewController class]];
}

@end
