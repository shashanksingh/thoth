//
//  LoadingViewController.m
//  Briefrr
//
//  Created by Shashank Singh on 2/8/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "LoadingViewController.h"
#import "TwoSectionView.h"
#import "Util.h"
#import "Macros.h"
#import <FBShimmeringView.h>
#import "ProgressBar.h"
#import "ASImageNode.h"
#import "SourcesService.h"
#import "EventManager.h"
#import "ASTextNode.h"
#import "TTSService.h"
#import "FeedService.h"
#import "FeedReaderService.h"
#import "RemoteConfigService.h"

static const CGFloat PROGRESS_BAR_WIDTH_FRACTION = 0.5;
static const CGFloat PROGRESS_BAR_HEIGHT = 3;
static const CGFloat PROGRESS_BAR_BOTTOM_FRACTION = 0.5;
static const CGFloat APP_NAME_HEIGHT_FRACTION = 0.55;
static const CGFloat SLOGAN_HEIGHT_FRACTION = 0.45;
static const CGFloat X_MARGIN = 20;

static const CGFloat APP_NAME_FONT_SIZE = 50.0f;
static const CGFloat SLOGAN_FONT_SIZE = 24.0f;
static const CGFloat ERROR_MESSAGE_FONT_SIZE = 13.0f;
static const CGFloat ERROR_MESSAGE_RETRY_LINK_GAP = 10;

static const NSTimeInterval PROGRESS_BAR_START_DELAY = 0.1;
static const NSTimeInterval RETRY_MIN_DELAY = 2;


@interface LoadingViewController ()<ProgressBarDelegate>
@property (nonatomic, strong) TwoSectionView *sectionView;
@property (nonatomic, strong) ProgressBar *progressBar;
@property (nonatomic, strong) ASTextNode *errorMessage;
@property (nonatomic, strong) ASTextNode *retryLink;
@property (nonatomic) BOOL loadingSucceeded;
@end

@implementation LoadingViewController
@synthesize progressBar, sectionView, loadingSucceeded;

-(LoadingViewController*)init {
    self =[super init];
    if (self) {
        self.loadingSucceeded = NO;
    }
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    // Note (shashank): we are forced to keep the default view of the controller around
    // because we want to add this controller to a parent controller which would want
    // to add this controller's view to its own view and can't be made to wait for
    // this controller's view to load
    self.sectionView = [[TwoSectionView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.view addSubview:self.sectionView];
    
    [self populateTopSection];
    [self populateBottomSection];
    [self startLoadingWithMinInterval:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)progressBarAnimationFinished {
    [[EventManager getInstance] fireEvent:LOADING_FINISHED, @(self.loadingSucceeded)];
}

- (void)startLoadingWithMinInterval:(NSTimeInterval)delay {
    [self.errorMessage.view removeFromSuperview];
    [self.errorMessage removeFromSupernode];
    if (self.retryLink) {
        [self.retryLink.view removeFromSuperview];
        [self.retryLink removeFromSupernode];
        self.retryLink = nil;
    }
    [self.progressBar setHidden:NO];
    [self.progressBar startWithDelay:PROGRESS_BAR_START_DELAY];
    
    dispatch_time_t finishTime = dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC);
    
    __weak typeof(self) bself = self;
    ArrayCallback callback = ^(NSArray *data) {
        if (!bself) {
            return;
        }
        
        dispatch_after(finishTime, dispatch_get_main_queue(), ^{
            if (!bself) {
                return;
            }
            bself.loadingSucceeded = YES;
            [bself.progressBar stop:NO];
        });
    };
    GenericErrorback errorback = ^(NSError *error) {
        // TODO (shashank): log error
        if (!bself) {
            return;
        }
        dispatch_after(finishTime, dispatch_get_main_queue(), ^{
            if (!bself) {
                return;
            }
            [bself.progressBar stop:YES];
            [bself showErrorMessage:MESSAGE_APP_LOADING_ERROR];
        });
    };
    
    
    [[RemoteConfigService getInstance] refreshWithCallback:^{
        BOOL isAnySourceSelected = [[SourcesService getInstance] isAnySourceSelected];
        if (isAnySourceSelected) {
            [[SourcesService getInstance] updateSourcesWithCallback:^{
                NSArray *selectedSourceIds = [[SourcesService getInstance] getSelectedSourceIds];
                [[FeedService getInstance] getFeedForSources:selectedSourceIds offset:0 limit:FEED_PAGE_SIZE callback:^(NSArray *data) {
                    // this will start prefetching of audio for TTS
                    [[FeedReaderService getInstance] setFeed:data ofType:NEWS_FEED];
                    callback(data);
                } errorback:errorback];
            } withErrorback:errorback];
        } else {
            [[SourcesService getInstance] updateSourcesWithCallback:callback withErrorback:errorback];
        }
    } andErrorback:errorback];
    
    
}

-(void)populateTopSection {
    CGFloat sectionW = self.sectionView.topSectionContent.frame.size.width;
    CGFloat sectionH = self.sectionView.topSectionContent.frame.size.height;
    
    // add the app name
    CGFloat w = sectionW;
    CGFloat h = sectionH * APP_NAME_HEIGHT_FRACTION;
    CGFloat y = 0;
    CGRect frame = CGRectMake(0, y, w, h);
    UILabel *appNameLabel = [[UILabel alloc] initWithFrame:frame];
    appNameLabel.textAlignment = NSTextAlignmentCenter;
    appNameLabel.text = APP_NAME;
    appNameLabel.font = [UIFont fontWithName:LOGO_FONT_NAME size:APP_NAME_FONT_SIZE];
    appNameLabel.textColor = UIColorFromRGB(COLOR_CODE_MESSAGE);
    [self.sectionView.topSectionContent addSubview:appNameLabel];
    
    
    // add the slogan
    w = sectionW;
    h = sectionH * SLOGAN_HEIGHT_FRACTION;
    y = sectionH * APP_NAME_HEIGHT_FRACTION;
    frame = CGRectMake(0, y, w, h);
    FBShimmeringView *shimmeringView = [[FBShimmeringView alloc] initWithFrame:frame];
    shimmeringView.shimmering = YES;
    
    UILabel *sloganLabel = [[UILabel alloc] initWithFrame:frame];
    sloganLabel.textAlignment = NSTextAlignmentCenter;
    sloganLabel.text = NSLocalizedString(MESSAGE_APP_SLOGAN, nil);
    sloganLabel.font = [UIFont fontWithName:FONT_NAME_LIGHT size:SLOGAN_FONT_SIZE];
    sloganLabel.textColor = UIColorFromRGB(COLOR_CODE_MESSAGE);
    shimmeringView.contentView = sloganLabel;
    
    [self.sectionView.topSectionContent addSubview:shimmeringView];
}

-(void)populateBottomSection {
    // add the progress bar
    CGRect frame = self.sectionView.bottomSectionContent.frame;
    CGFloat w = frame.size.width * PROGRESS_BAR_WIDTH_FRACTION;
    CGFloat x = (frame.size.width - w)/2;
    CGFloat y = frame.size.height * (1 - PROGRESS_BAR_BOTTOM_FRACTION);
    CGRect progressBarFrame = CGRectMake(x, y, w, PROGRESS_BAR_HEIGHT);
    
    self.progressBar = [[ProgressBar alloc] initWithFrame:progressBarFrame];
    self.progressBar.delegate = self;
    [self.sectionView.bottomSectionContent addSubview:self.progressBar];
}

-(void)showErrorMessage:(NSString*)messageText {
    [self.progressBar setHidden:YES];
    
    CGFloat sectionW = self.sectionView.bottomSectionContent.frame.size.width;
    CGFloat sectionH = self.sectionView.bottomSectionContent.frame.size.height;
    __weak typeof(self) bself = self;
    
    // error message
    dispatch_async([Util getAsyncUIBackgroundQueue], ^{
        if (!bself) {
            return;
        }
        
        CGFloat w = sectionW;
        CGFloat h = sectionH;
        CGFloat x = 0;
        CGFloat y = 0;
        
        UIFont *font = [UIFont fontWithName:FONT_NAME_REGULAR_2 size:ERROR_MESSAGE_FONT_SIZE];
        UIColor *color = UIColorFromRGB(COLOR_CODE_ERROR_MESSAGE);
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setAlignment:NSTextAlignmentCenter];
        
        NSDictionary *attrs = @{ NSFontAttributeName: font, NSForegroundColorAttributeName: color, NSParagraphStyleAttributeName: paragraphStyle};
        NSAttributedString *message = [[NSAttributedString alloc] initWithString:messageText attributes:attrs];
        
        
        self.errorMessage = [[ASTextNode alloc] init];
        self.errorMessage.attributedString = message;
        CGSize size = [self.errorMessage measure:CGSizeMake(w - 2 * X_MARGIN, FLT_MAX)];
        CGPoint origin = CGPointMake((w - size.width) * 0.5, y + (h - size.height) * 0.5 - ERROR_MESSAGE_RETRY_LINK_GAP);
        self.errorMessage.frame = (CGRect){origin, size};
        
        color = UIColorFromRGB(COLOR_CODE_GREY);
        attrs = @{ NSFontAttributeName: font, NSForegroundColorAttributeName: color};
        message = [[NSAttributedString alloc] initWithString:MESSAGE_TRY_AGAIN attributes:attrs];
        
        bself.retryLink = [[ASTextNode alloc] init];
        bself.retryLink.attributedString = message;
        size = [bself.retryLink measure:CGSizeMake(w - 2 * X_MARGIN, FLT_MAX)];
        x = (w - size.width) * 0.5;
        y += CGRectGetMaxY(self.errorMessage.frame) + ERROR_MESSAGE_RETRY_LINK_GAP;
        origin = CGPointMake(x, y);
        bself.retryLink.frame = (CGRect){origin, size};
        bself.retryLink.userInteractionEnabled = YES;
        [bself.retryLink addTarget:self
                         action:@selector(retryLinkTapped)
               forControlEvents:ASControlNodeEventTouchUpInside];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!bself) {
                return;
            }
            [bself.sectionView.bottomSectionContent addSubview:bself.errorMessage.view];
            [bself.sectionView.bottomSectionContent addSubview:bself.retryLink.view];
            
            UIColor *borderColor = UIColorFromRGBA(COLOR_CODE_GREY, 0.5);
            [Util addBorderToUIView:bself.retryLink.view onSides:BOX_BOTTOM ofColor:borderColor andThickness:1];
        });
    });
    
}

- (void)retryLinkTapped {
    [self startLoadingWithMinInterval:RETRY_MIN_DELAY];
}

@end
