//
//  FeedView.m
//  Briefrr
//
//  Created by Shashank Singh on 2/17/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "StoryView.h"
#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ProgressBar.h"
#import "Util.h"
#import "Source.h"
#import "SourcesService.h"
#import "DropCapView.h"
#import "ActionButton.h"
#import <pop/POP.h>
#import "EventManager.h"
#import "UIImageEffects.h"
#import "UserPreferenceService.h"
#import "EventLogger.h"


static const CGFloat PROGRESS_BAR_WIDTH_FRACTION = 0.6;
static const CGFloat PROGRESS_BAR_HEIGHT = 3;
static const NSUInteger IMAGE_MASK_COLOR = COLOR_CODE_BLACK;
static const CGFloat IMAGE_MASK_OPACITY = 0.6;
static const CGFloat TITLE_FONT_SIZE = 15.0f;
static const CGFloat TITLE_INSET = 15;
static NSString* const TITLE_FONT_NAME = FONT_NAME_REGULAR;
static const CGFloat SUMMARY_FONT_SIZE = 12.0f;
static const CGFloat SUMMARY_INSET = 15;
static NSString* const SUMMARY_FONT_NAME = FONT_NAME_REGULAR;
static const CGFloat TITLE_SUMMARY_GAP = 20;
static const CGFloat TITLE_ANCHOR_WIDTH = 5;
static const CGFloat SUMMARY_DROP_CAP_FONT_SIZE_FACTOR = 3;

static const NSTimeInterval READING_MODE_CHANGE_ANIMATION_DURATION = 1.5;
static const NSTimeInterval IMAGE_LOAD_MAX_WAIT = 2.0;

static NSString* const DEFAULT_IMAGE = @"assets/images/source-logos/nyt.png";

@interface StoryView()<ProgressBarDelegate, ASNetworkImageNodeDelegate>
@property (nonatomic, strong) UIView *topSection;
@property (nonatomic, strong) UIView *bottomSection;
@property (nonatomic, strong) UIView *bottomContent;
@property (nonatomic, strong) UIView *imageMask;
@property (nonatomic, strong) ASDisplayNode *imageNodeContainer;
@property (nonatomic, strong) ASImageNode *imageNode;
@property (nonatomic, strong) ASTextNode *titleNode;
@property (nonatomic, strong) UIView *titleSummarySeparator;
@property (nonatomic, strong) ProgressBar *progressBar;
@property (nonatomic, strong) UIScrollView *summaryContainer;
@property (nonatomic, strong) DropCapView *summary;
@property (nonatomic, strong) ActionButton *saveButton;
@property (nonatomic, strong) ActionButton *viewFullButton;
@property (nonatomic, strong) ActionButton *shareButton;
@property (nonatomic, strong) NSTimer *imageLoadTimeoutTimer;
@end

@implementation StoryView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.story = nil;
        
        CGRect frame;
        CGFloat x, y, w, h;
        
        self.imageNodeContainer = [[ASDisplayNode alloc] init];
        self.imageNodeContainer.frame = self.topSection.bounds;
        [self.topSection addSubview:self.imageNodeContainer.view];
        
        frame = self.topSection.bounds;
        frame.size.width += 2;
        frame.origin.x--;
        self.imageMask = [[UIView alloc] initWithFrame:frame];
        self.imageMask.backgroundColor = UIColorFromRGBA(IMAGE_MASK_COLOR, IMAGE_MASK_OPACITY);
        [self.topSection addSubview:self.imageMask];
        
        y = self.imageMask.bounds.size.height - ACTION_BUTTON_SIZE * 1.5;
        w = ACTION_BUTTON_SIZE;
        h = ACTION_BUTTON_SIZE;
        frame = CGRectMake(0, y, w, h);
        
        UIColor *actionButtonColor = UIColorFromRGB(COLOR_CODE_WHITE);
        
        self.viewFullButton = [[ActionButton alloc] initWithFrame:frame label:FONT_ICON_CODE_EYE color:actionButtonColor];
        [self.viewFullButton addTarget:self action:@selector(onViewFullTap) forControlEvents:UIControlEventTouchUpInside];
        
        self.shareButton = [[ActionButton alloc] initWithFrame:frame label:FONT_ICON_CODE_FACEBOOK color:actionButtonColor];
        [self.shareButton addTarget:self action:@selector(onShareTap) forControlEvents:UIControlEventTouchUpInside];
        
        self.saveButton = [[ActionButton alloc] initWithFrame:frame label:FONT_ICON_CODE_BOOKMARK color:actionButtonColor];
        [self.saveButton addTarget:self action:@selector(onSaveTap) forControlEvents:UIControlEventTouchUpInside];
        
        NSArray *actionButtons = [self getActionButtons];
        CGFloat actionButtonGap = self.imageMask.bounds.size.width/actionButtons.count;
        for (NSUInteger i=0; i<actionButtons.count; ++i) {
            ActionButton *button = actionButtons[i];
            CGRect frame = button.frame;
            frame.origin.x = actionButtonGap * (i + 0.5) - ACTION_BUTTON_SIZE/2;
            button.frame = frame;
            button.alpha = 0;
            [self.imageMask addSubview:button];
        }
        
        self.bottomContent = [[UIView alloc] initWithFrame:self.bottomSection.bounds];
        self.bottomContent.userInteractionEnabled = YES;
        self.bottomContent.hidden = YES;
        [self.bottomSection addSubview:self.bottomContent];
        
        w = self.bottomSection.bounds.size.width * PROGRESS_BAR_WIDTH_FRACTION;
        h = PROGRESS_BAR_HEIGHT;
        x = CGRectGetMidX(self.bottomSection.bounds) - w/2;
        y = CGRectGetMidY(self.bottomSection.bounds) - h/2;
        CGRect progressBarFrame = CGRectMake(x, y, w, h);
        self.progressBar = [[ProgressBar alloc] initWithFrame:progressBarFrame];
        self.progressBar.delegate = self;
        [self.bottomSection addSubview:self.progressBar];
        
        self.summaryContainer = [[UIScrollView alloc] initWithFrame:self.bottomSection.bounds];
        [self.bottomContent addSubview:self.summaryContainer];
        
        x = 0;
        y = 0;
        w = TITLE_ANCHOR_WIDTH;
        h = 0;
        frame = CGRectMake(x, y, w, h);
        self.titleSummarySeparator = [[UIView alloc] initWithFrame:frame];
        self.titleSummarySeparator.backgroundColor = UIColorFromRGBA(COLOR_CODE_RED, 0.5);
        [self.bottomContent addSubview:self.titleSummarySeparator];
        
        [self.progressBar startWithDelay:0];
        
        __weak typeof(self) bself = self;
        [[EventManager getInstance] addEventListener:FEED_READING_MODE_CHANGED withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            FeedReadingMode readingMode = ((NSNumber*)va_arg(args, NSNumber*)).intValue;
            [bself onReadingModeChanged:readingMode];
        }];
    }
    return self;
}

- (NSArray*)getActionButtons {
    return @[self.viewFullButton, self.shareButton, self.saveButton];
}

- (void)didBecomeCurrentStory {
    for (ActionButton *button in [self getActionButtons]) {
        CGFloat toX = button.center.x;
        button.alpha = 1.0;
        
        CGFloat fromCenterX = button.superview.bounds.size.width + button.bounds.size.width/2;
        [Util animatedSlideIn:button fromCenterX:fromCenterX toCenterX:toX];
    }
}

- (void)didBecomeSecondaryStory {
    [self fadeActionButtons:YES];
}

- (void)updateOnReadingModeChange:(FeedReadingMode)readingMode {
    [self fadeActionButtons:readingMode == AUDIO];
}

- (void)fadeActionButtons:(BOOL)fadeOut {
    for (ActionButton *button in [self getActionButtons]) {
        POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
        anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        anim.fromValue = @(button.alpha);
        anim.toValue = @(fadeOut ? 0.0 : 1.0);
        [button pop_addAnimation:anim forKey:@"fade"];
    }
}

- (void)setStory:(Story*)story {
    if (self.story) {
        [self.progressBar stop:NO];
        [self.progressBar startWithDelay:0];
    }
    
    _story = story;
    self.progressBar.hidden = NO;
    self.bottomContent.hidden = YES;
    
    if (self.story) {
        [self updateImage];
        [self updateTitleAndSummary];
        [self updateActionButtons];
        
        if (self.imageLoadTimeoutTimer) {
            [self.imageLoadTimeoutTimer invalidate];
        }
        self.imageLoadTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:IMAGE_LOAD_MAX_WAIT target:self selector:@selector(onImageLoadTimeout) userInfo:nil repeats:NO];
    }
}

- (void)updateImage {
    [Util emptyDisplayNode:self.imageNodeContainer];
    
    if (![Util isRemoteURL:self.story.thumbnail]) {
        self.imageNode = [[ASImageNode alloc] init];
        self.imageNode.image = [UIImage imageNamed:self.story.thumbnail];
        self.imageNode.frame = self.imageNodeContainer.bounds;
        [self.imageNodeContainer addSubnode:self.imageNode];
        [self onImageLoaded:self.imageNode.image];
    } else {
        ASNetworkImageNode *imageNode = [[ASNetworkImageNode alloc] init];
        imageNode.placeholderEnabled = NO;
        imageNode.frame = self.topSection.bounds;
        imageNode.URL = [Util urlFromString:self.story.thumbnail];
        self.imageNode = imageNode;
        imageNode.delegate = self;
        [self.imageNodeContainer addSubnode:imageNode];
    }
}

- (void)updateTitleAndSummary {
    if (!self.story) {
        return;
    }
    if (self.titleNode) {
        [self.titleNode.view removeFromSuperview];
        [self.titleNode removeFromSupernode];
    }
    
    __weak typeof(self) bself = self;
    dispatch_async([Util getAsyncUIBackgroundQueue], ^{
        if (!bself) {
            return;
        }
        
        UIFont *font = [UIFont fontWithName:TITLE_FONT_NAME size:TITLE_FONT_SIZE];
        UIColor *color = UIColorFromRGB(COLOR_CODE_BLACK);
        UIColor *strokeColor = UIColorFromRGB(COLOR_CODE_WHITE);
        
        NSDictionary *titleAttributes = @{ NSFontAttributeName: font, NSForegroundColorAttributeName: color, NSStrokeWidthAttributeName: @(-1.0f), NSStrokeColorAttributeName: strokeColor};
        
        bself.titleNode = [[ASTextNode alloc] init];
        bself.titleNode.attributedString = [[NSAttributedString alloc] initWithString:bself.story.title
                                                                attributes:titleAttributes];
        
        CGFloat w = bself.topSection.bounds.size.width - 2 * TITLE_INSET;
        CGFloat h = bself.topSection.bounds.size.height - 2 * TITLE_INSET;
        CGSize bounds = CGSizeMake(w, h);
        [bself.titleNode measure:bounds];
        CGPoint origin = CGPointMake(TITLE_INSET, TITLE_INSET);
        bself.titleNode.frame = (CGRect){ origin, bself.titleNode.calculatedSize };
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [bself.bottomContent addSubview:bself.titleNode.view];
            // sumary update should only happen after title update so that summary y-position
            // can be updated to deal with possible title height change
            [bself updateSummary];
        });
    });
}

- (void)updateSummary {
    
    __weak typeof(self) bself = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!bself) {
            return;
        }
        
        // the title height might have changed, adjust position accordingly
        CGFloat y = CGRectGetMaxY(bself.titleNode.frame) + TITLE_SUMMARY_GAP;
        CGFloat h = bself.bottomContent.bounds.size.height - y;
        
        if (!bself.summary) {
            CGFloat w = bself.bottomContent.bounds.size.width - 2 * SUMMARY_INSET;
            CGFloat x = SUMMARY_INSET;
            CGRect frame = CGRectMake(x, 0, w, h);
            
            DropCapView *summaryView = [[DropCapView alloc] initWithFrame:frame];
            summaryView.backgroundColor = TRANSPARENT_COLOR;
            summaryView.fontName = SUMMARY_FONT_NAME;
            summaryView.textColor = UIColorFromRGB(COLOR_CODE_PARAGRAPH);
            summaryView.dropCapFontSize = SUMMARY_FONT_SIZE * SUMMARY_DROP_CAP_FONT_SIZE_FACTOR;
            summaryView.textFontSize = SUMMARY_FONT_SIZE;
            
            bself.summary = summaryView;
            [bself.summaryContainer addSubview:bself.summary];
        }
        
        bself.summary.text = self.story.summary;
        
        CGRect frame = bself.summary.frame;
        frame.size.height = bself.summary.computedTextHeight;
        bself.summary.frame = frame;
        
        frame = bself.summaryContainer.frame;
        frame.origin.y = y;
        frame.size.height = h;
        bself.summaryContainer.frame = frame;
        bself.summaryContainer.contentSize = bself.summary.bounds.size;
        [bself.summaryContainer setContentOffset:CGPointZero animated:NO];
        
        h = bself.titleNode.bounds.size.height;
        y = bself.titleNode.frame.origin.y;
        frame = bself.titleSummarySeparator.frame;
        frame.size.height = h;
        frame.origin.y = y;
        bself.titleSummarySeparator.frame = frame;
    });
}

- (void)updateActionButtons {
    BOOL isStorySaved = self.story && [[UserPreferenceService getInstance] isStorySaved:self.story];
    if (isStorySaved) {
        [self.saveButton drawBorderWithAnimation:YES];
    } else {
        [self.saveButton eraseBorderWithAnimation:YES];
    }
}

- (void)onViewFullTap {
    if (!self.story) {
        // TODO (shashank): log error
        return;
    }
    [[EventManager getInstance] fireEvent:SHOW_FULL_STORY, self.story];
}

- (void)onShareTap {
    if (!self.story) {
        // TODO (shashank): log error
        return;
    }
    
    NSMutableArray *sharingItems = [NSMutableArray new];
    if (self.story.title) {
        [sharingItems addObject:self.story.title];
    }
    if (self.story.thumbnail) {
        [sharingItems addObject:self.imageNode.image];
    }
    if (self.story.link) {
        [sharingItems addObject:self.story.link];
    }
    
    [[EventManager getInstance] fireEvent:SHOW_SHARE_DIALOG, sharingItems];
}

- (void)onSaveTap {
    if (!self.story) {
        return;
    }
    // toggle saved state
    if ([[UserPreferenceService getInstance] isStorySaved:self.story]) {
        [[UserPreferenceService getInstance] deleteSavedStory:self.story];
    } else {
        [[UserPreferenceService getInstance] saveStory:self.story];
    }
    [self updateOnSavedStateChange];
}

- (void)updateOnSavedStateChange {
    [self updateActionButtons];
}

- (void)imageNodeDidFinishDecoding:(ASNetworkImageNode *)imageNode {
    [self onImageLoaded:imageNode.image];
}

- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image {
}

- (void)onStoryLoaded {
    if (!self.progressBar.isInProgress) {
        return;
    }
    
    if ([NSThread isMainThread]) {
        [self.progressBar stop:NO];
    } else {
        __weak typeof(self) bself = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!bself) {
                return;
            }
            [bself.progressBar stop:NO];
        });
    }
}

- (void)onImageLoadTimeout {
    self.imageLoadTimeoutTimer = nil;
    [self onStoryLoaded];
}

- (void)onImageLoaded:(UIImage*)image {
    if (self.imageLoadTimeoutTimer) {
        [self.imageLoadTimeoutTimer invalidate];
        self.imageLoadTimeoutTimer = nil;
    }
    [self onStoryLoaded];
}

- (void)progressBarAnimationFinished {
    self.progressBar.hidden = YES;
    self.bottomContent.hidden = NO;
    
    if (self.delegate) {
        [self.delegate storyViewLoaded:self];
    }
}

- (void)onReadingModeChanged:(FeedReadingMode)readingMode {
    NSString *animationKey = @"imageMaskOpacityAnimation";
    CGFloat finalOpacity = readingMode == AUDIO ? 0.0 : IMAGE_MASK_OPACITY;
    UIColor *finalImageMaskColor = UIColorFromRGBA(IMAGE_MASK_COLOR, finalOpacity);
    [self.imageMask pop_removeAnimationForKey:animationKey];
    
    POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewBackgroundColor];
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    anim.fromValue = (id)self.imageMask.backgroundColor.CGColor;
    anim.toValue = (id)finalImageMaskColor.CGColor;
    anim.duration = READING_MODE_CHANGE_ANIMATION_DURATION;
    
    [self.imageMask pop_addAnimation:anim forKey:animationKey];
}

@end
