//
//  NotificationPermissionIntroViewController.m
//  Briefrr
//
//  Created by Shashank Singh on 3/15/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "NotificationPermissionIntroViewController.h"
#import "Macros.h"
#import "EventManager.h"
#import "UserPreferenceService.h"
#import "Util.h"

static const CGFloat MAJOR_MESSAGE_FONT_SIZE = 30.0f;
static const CGFloat MINOR_MESSAGE_FONT_SIZE = 15.0f;

@implementation NotificationPermissionIntroViewController
- (NSString*)majorText {
    return MESSAGE_NOTIFICATION_PERMISSION_MAJOR;
}

- (NSString*)minorText {
    return MESSAGE_NOTIFICATION_PERMISSION_MINOR;
}

- (NSString*)actionText {
    return MESSAGE_NOTIFICATION_PERMISSION_ACTION;
}

- (NSString*)skipLinkText {
    return MESSAGE_NOTIFICATION_PERMISSION_SKIP;
}

- (CGFloat)majorTextFontSize {
    return MAJOR_MESSAGE_FONT_SIZE;
}

- (CGFloat)minorTextFontSize {
    return MINOR_MESSAGE_FONT_SIZE;
}

- (void)onActionButtonTapped {
    if ([Util isNotificationPermissionGranted]) {
        [[EventManager getInstance] fireEvent:NOTIFICATION_PERMISSION_REQUEST_FINISHED, @(YES), @(NO)];
        return;
    }
    
    [[EventManager getInstance] addEventListener:USER_NOTIFICATION_SETTING_CHANGED oneShot:YES withCallback:^(va_list args) {
        if (![Util isNotificationPermissionGranted]) {
            [[UserPreferenceService getInstance] updateNotificationPermissionDenialTimeToCurrent];
            [[EventManager getInstance] fireEvent:NOTIFICATION_PERMISSION_REQUEST_FINISHED, @(NO), @(NO)];
        } else {
            [[UserPreferenceService getInstance] clearNotificationPermissionDenialTime];
            [[EventManager getInstance] fireEvent:NOTIFICATION_PERMISSION_REQUEST_FINISHED, @(YES), @(NO)];
        }
    }];
    
    // Note (shashank): iOS8
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
}

- (void)onSkipLinkTapped {
    [[EventManager getInstance] fireEvent:NOTIFICATION_PERMISSION_REQUEST_FINISHED, @(NO), @(YES)];
}
@end
