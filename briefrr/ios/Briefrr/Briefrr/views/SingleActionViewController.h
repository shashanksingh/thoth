//
//  SingleActionViewController.h
//  Briefrr
//
//  Created by Shashank Singh on 2/16/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleActionViewController : UIViewController
@property (nonatomic, strong) NSString *majorText;
@property (nonatomic, strong) NSString *minorText;
@property (nonatomic, strong) NSString *actionText;
@property (nonatomic, strong) NSString *skipLinkText;
@property (nonatomic) CGFloat majorTextFontSize;
@property (nonatomic) CGFloat minorTextFontSize;

- (void)onActionButtonTapped;
- (void)onSkipLinkTapped;
- (void)setMinorTextColor:(UIColor*)color;
@end
