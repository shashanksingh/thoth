//
//  AboutView.m
//  Briefrr
//
//  Created by Shashank Singh on 4/14/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "AboutView.h"
#import "Macros.h"
#import "KLCPopup.h"
#import <QuartzCore/QuartzCore.h>
#import "Util.h"

static const CGFloat CONTENT_BOX_WIDTH = 263;
static const CGFloat CONTENT_BOX_HEIGHT = 334;
static const CGFloat TOP_SECTION_HEIGHT_FRACTION = 0.66;
static const CGFloat TOP_INSET = 10;
static const CGFloat LOGO_SIZE = 180;
static const CGFloat LOGO_FONT_SIZE = 180;
static const CGFloat CONTACT_FONT_SIZE = 16;
static const CGFloat ATTRIBUTION_TEXT_FONT_SIZE = 11;
static const CGFloat ATTRIBUTION_LOGO_SIZE = 100;
static const CGFloat BORDER_HEIGHT = 1;
static const CGFloat BORDER_WIDTH_FRACTION = 0.67;

static NSString* const NUANCE_ATTRIBUTION_TEXT = @"Speech recognition provided by Nuance Communications, Inc.";


@interface AboutView()
@end

@implementation AboutView
- (instancetype)init {
    CGFloat w = CONTENT_BOX_WIDTH;
    CGFloat h = CONTENT_BOX_HEIGHT;
    CGRect frame = CGRectMake(0, 0, w, h);
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorFromRGB(COLOR_CODE_LIGHT_GREY);
        self.layer.masksToBounds = NO;
        self.layer.shadowOffset = CGSizeMake(-2, 2);
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        
        h = self.bounds.size.height * TOP_SECTION_HEIGHT_FRACTION;
        frame = CGRectMake(0, TOP_INSET, self.bounds.size.width, h);
        UIView *topSection = [[UIView alloc] initWithFrame:frame];
        [self addSubview:topSection];
        
        CGFloat y = h;
        h = self.bounds.size.height * (1 - TOP_SECTION_HEIGHT_FRACTION);
        frame = CGRectMake(0, y, self.bounds.size.width, h);
        UIView *bottomSection = [[UIView alloc] initWithFrame:frame];
        [self addSubview:bottomSection];
        
        frame = CGRectMake(0, 0, self.bounds.size.width, LOGO_SIZE);
        UILabel *logoLabel = [[UILabel alloc] initWithFrame:frame];
        logoLabel.textAlignment = NSTextAlignmentCenter;
        logoLabel.text = @"b";
        logoLabel.font = [UIFont fontWithName:LOGO_FONT_NAME size:LOGO_FONT_SIZE];
        logoLabel.textColor = UIColorFromRGB(COLOR_CODE_BLACK);
        [topSection addSubview:logoLabel];
        
        frame = CGRectMake(0, CGRectGetMaxY(frame), self.bounds.size.width, CONTACT_FONT_SIZE);
        UILabel *contactLabel = [[UILabel alloc] initWithFrame:frame];
        contactLabel.textAlignment = NSTextAlignmentCenter;
        contactLabel.text = SUPPORT_EMAIL;
        contactLabel.font = [UIFont fontWithName:FONT_NAME_ITALIC size:CONTACT_FONT_SIZE];
        contactLabel.textColor = UIColorFromRGB(COLOR_CODE_BLACK);
        [topSection addSubview:contactLabel];
        
        y = topSection.bounds.size.height - BORDER_HEIGHT;
        w = topSection.bounds.size.width * BORDER_WIDTH_FRACTION;
        CGFloat x = topSection.center.x - w/2;
        UIView *border = [[UIView alloc] initWithFrame:CGRectMake(x, y, w, BORDER_HEIGHT)];
        border.backgroundColor = UIColorFromRGBA(COLOR_CODE_GREY, 0.5);
        [topSection addSubview:border];
        
        UIImage *image = [UIImage imageNamed:@"assets/images/nuance_logo.png"];
        CGFloat aspectRatio = image.size.width/image.size.height;
        frame = CGRectMake(0, TOP_INSET * 2, ATTRIBUTION_LOGO_SIZE, ATTRIBUTION_LOGO_SIZE/aspectRatio);
        //center
        frame.origin.x = (self.bounds.size.width - frame.size.width)/2;
        UIImageView *attributionLogo = [[UIImageView alloc] initWithFrame:frame];
        attributionLogo.image = image;
        attributionLogo.contentMode = UIViewContentModeScaleAspectFit;
        [bottomSection addSubview:attributionLogo];
        
        frame = CGRectMake(0, CGRectGetMaxY(frame), self.bounds.size.width, ATTRIBUTION_TEXT_FONT_SIZE * 4);
        UILabel *attributionTextLabel = [[UILabel alloc] initWithFrame:frame];
        attributionTextLabel.textAlignment = NSTextAlignmentCenter;
        attributionTextLabel.text = NUANCE_ATTRIBUTION_TEXT;
        attributionTextLabel.textAlignment = NSTextAlignmentCenter;
        attributionTextLabel.numberOfLines = 0;
        attributionTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
        attributionTextLabel.font = [UIFont fontWithName:FONT_NAME_REGULAR size:ATTRIBUTION_TEXT_FONT_SIZE];
        attributionTextLabel.textColor = UIColorFromRGB(COLOR_CODE_GREY);
        [bottomSection addSubview:attributionTextLabel];
        
    }
    
    return self;
}

+ (void)show {
    AboutView *view = [[AboutView alloc] init];
    KLCPopup *popup = [KLCPopup popupWithContentView:view showType:KLCPopupShowTypeGrowIn dismissType:KLCPopupDismissTypeGrowOut maskType:KLCPopupMaskTypeDimmed dismissOnBackgroundTouch:YES dismissOnContentTouch:YES];
    [popup show];
}
@end
