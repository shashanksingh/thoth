//
//  FeedViewController.h
//  Briefrr
//
//  Created by Shashank Singh on 2/17/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "Macros.h"

@interface FeedViewController : UIViewController
@property (nonatomic) FeedType feedType;
- (instancetype)initWithFeedType:(FeedType)feedType;
@end
