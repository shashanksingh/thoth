//
//  EmptyFeedView.h
//  Briefrr
//
//  Created by Shashank Singh on 3/24/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "BaseCarouselView.h"
#import "Macros.h"

@interface EmptyFeedView : BaseCarouselView
- (instancetype)initWithFrame:(CGRect)frame feedType:(FeedType)feedType;
@end
