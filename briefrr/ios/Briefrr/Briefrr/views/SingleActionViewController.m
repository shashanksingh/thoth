//
//  SingleActionViewController.m
//  Briefrr
//
//  Created by Shashank Singh on 2/16/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "SingleActionViewController.h"

#import <pop/POP.h>
#import "TwoSectionView.h"
#import "Macros.h"
#import "ASTextNode.h"
#import "Util.h"
#import "ProceedButton.h"
#import "EventManager.h"
#import <FBShimmeringView.h>

static const CGFloat CONTENT_INSET = 20;
static const CGFloat MAJOR_MESSAGE_HEIGHT_FRACTION = 0.45;
static const CGFloat MINOR_MESSAGE_PADDING = 20;
static const CGFloat ACTION_BUTTON_WIDTH = 220;
static const CGFloat ACTION_BUTTON_HEIGHT = 50;
static const CGFloat SKIP_LINK_FONT_SIZE = 12;
static const CGFloat ACTION_BUTTON_SKIP_LINK_GAP = 10;


static const CGFloat ACTION_BUTTON_ON_SHOW_ANIMATION_DELAY = 0.3;

@interface SingleActionViewController ()
@property (nonatomic, strong) TwoSectionView *view;
@property (nonatomic, strong) UILabel *minorMessageLabel;
@property (nonatomic, strong) ProceedButton *actionButton;
@property (nonatomic, strong) UILabel *skipLink;
@end

@implementation SingleActionViewController

- (void)loadView {
    self.view = [[TwoSectionView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self addMajorMessage];
    [self addMinorMessage];
    [self addActionButton];
    [self addSkipLink];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)willMoveToParentViewController:(UIViewController *)parent {
    __weak typeof(self) bself = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, ACTION_BUTTON_ON_SHOW_ANIMATION_DELAY * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [bself animateActionButtonOnShow];
    });
}

- (void)setMinorTextColor:(UIColor*)color {
    if (!self.minorMessageLabel) {
        // TODO (shashank): log warning
        return;
    }
    self.minorMessageLabel.textColor = color;
}

- (void)addMajorMessage {
    CGFloat sectionW = self.view.topSectionContent.frame.size.width - 2 * CONTENT_INSET;
    CGFloat sectionH = self.view.topSectionContent.frame.size.height;
    
    CGFloat w = sectionW;
    CGFloat h = sectionH * MAJOR_MESSAGE_HEIGHT_FRACTION;
    CGFloat y = 0;
    CGRect frame = CGRectMake(CONTENT_INSET, y, w, h);
    
    FBShimmeringView *shimmeringView = [[FBShimmeringView alloc] initWithFrame:frame];
    shimmeringView.shimmering = YES;
    [self.view.topSectionContent addSubview:shimmeringView];
    
    UILabel *majorMessageLabel = [[UILabel alloc] initWithFrame:frame];
    majorMessageLabel.textAlignment = NSTextAlignmentCenter;
    majorMessageLabel.adjustsFontSizeToFitWidth = YES;
    majorMessageLabel.text = self.majorText;
    majorMessageLabel.font = [UIFont fontWithName:FONT_NAME_LIGHT size:self.majorTextFontSize];
    majorMessageLabel.textColor = UIColorFromRGB(COLOR_CODE_MESSAGE);
    shimmeringView.contentView = majorMessageLabel;
    
}

- (void)addMinorMessage {
    CGFloat sectionW = self.view.topSectionContent.frame.size.width;
    CGFloat sectionH = self.view.topSectionContent.frame.size.height;
    
    CGFloat w = sectionW - 2 * MINOR_MESSAGE_PADDING;
    CGFloat h = sectionH * (1 - MAJOR_MESSAGE_HEIGHT_FRACTION);
    CGFloat y = sectionH * MAJOR_MESSAGE_HEIGHT_FRACTION;
    CGRect frame = CGRectMake(MINOR_MESSAGE_PADDING, y, w, h);
    
    self.minorMessageLabel = [[UILabel alloc] initWithFrame:frame];
    self.minorMessageLabel.textAlignment = NSTextAlignmentCenter;
    self.minorMessageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.minorMessageLabel.numberOfLines = 0;
    
    self.minorMessageLabel.text = self.minorText;
    self.minorMessageLabel.font = [UIFont fontWithName:FONT_NAME_LIGHT size:self.minorTextFontSize];
    self.minorMessageLabel.textColor = UIColorFromRGB(COLOR_CODE_MESSAGE);
    [self.view.topSectionContent addSubview:self.minorMessageLabel];
}

- (void)addActionButton {
    CGFloat sectionH = self.view.bottomSectionContent.frame.size.height;
    
    // load the button too far to the right, the animation will kick in
    // on view being shown and move it to the correct place
    CGFloat x = 2 * self.view.bottomSectionContent.frame.size.width;
    CGFloat y = sectionH/2 - ACTION_BUTTON_HEIGHT/2;
    CGRect frame = CGRectMake(x, y, ACTION_BUTTON_WIDTH, ACTION_BUTTON_HEIGHT);
    
    UIColor *bgColor = UIColorFromRGBA(COLOR_CODE_ACTION_BUTTON, 0.75);
    self.actionButton = [[ProceedButton alloc] initWithFrame:frame label:self.actionText backgroundColor:bgColor];
    [self.view.bottomSectionContent addSubview:self.actionButton];
    
    [self.actionButton addTarget:self action:@selector(onActionButtonTapped) forControlEvents:UIControlEventTouchUpInside];
}

- (void)addSkipLink {
    self.skipLink = nil;
    if (!self.skipLinkText) {
        return;
    }
    
    CGFloat x = 2 * self.view.bottomSectionContent.frame.size.width;
    CGFloat y = CGRectGetMaxY(self.actionButton.frame) + ACTION_BUTTON_SKIP_LINK_GAP;
    CGFloat w = self.view.bottomSectionContent.frame.size.width - 2 * CONTENT_INSET;
    CGFloat h = SKIP_LINK_FONT_SIZE * 2;
    CGRect frame = CGRectMake(x, y, w, h);
    self.skipLink = [[UILabel alloc] initWithFrame:frame];
    self.skipLink.textColor = UIColorFromRGB(COLOR_CODE_GREY);
    self.skipLink.text = self.skipLinkText;
    self.skipLink.textAlignment = NSTextAlignmentCenter;
    [self.view.bottomSectionContent addSubview:self.skipLink];
    
    self.skipLink.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSkipLinkTapped)];
    [self.skipLink addGestureRecognizer:tapGesture];
}

- (void)animateActionButtonOnShow {
    CGFloat w = self.view.bottomSectionContent.bounds.size.width;
    CGFloat centerX = w + self.actionButton.bounds.size.width/2;
    [Util animatedSlideIn:self.actionButton fromCenterX:centerX toCenterX:w/2];
    if (self.skipLink) {
        [Util animatedSlideIn:self.skipLink fromCenterX:centerX toCenterX:w/2];
    }
}

- (void)onActionButtonTapped {
}

- (void)onSkipLinkTapped {
    
}
@end
