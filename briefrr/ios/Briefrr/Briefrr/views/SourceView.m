//
//  SourceView.m
//  Briefrr
//
//  Created by Shashank Singh on 2/13/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "SourceView.h"
#import <pop/POP.h>
#import "Source.h"
#import "Macros.h"
#import "Util.h"
#import "UIImageEffects.h"
#import "EventManager.h"
#import "ActionView.h"
#import "ActionViewAnimatorService.h"

static const CGFloat CELL_INSET = 4;
static const CGFloat LABEL_INSET = 8;
static const CGFloat SOURCE_NAME_FONT_SIZE = 14.0f;
static const CGFloat LOGO_MASK_OPACITY = 0.80;
static const CGFloat SELECTED_SOURCE_INDICATOR_SIZE = 30;
static const CGFloat SELECTED_SOURCE_INDICATOR_FONT_SIZE = 28.0;
static const CGFloat SOURCE_LOGO_ANIMATION_SHIFT_AMOUNT = 6;

@interface SourceView()
@property (nonatomic, retain) Source *source;
@property (nonatomic, strong) ASDisplayNode *sourceLogo;
@property (nonatomic, strong) UIView *sourceNameLabel;
@property (nonatomic, strong) UIView *selectionIndicator;
@end

@implementation SourceView
- (instancetype)initWithSource:(Source*)source andSize:(CGSize)size {
    self = [super init];
    if (self) {
        self.source = source;
        self.frame = CGRectMake(0, 0, size.width, size.height);
        self.userInteractionEnabled = YES;
        self.cornerRadius = 1;
        
        // TODO (shashank): this can cause a crash
        // (https://github.com/facebook/AsyncDisplayKit/issues/244).
        // update ADK and re-enable this
        //self.shouldRasterizeDescendants = YES;
        
        self.backgroundColor = UIColorFromRGB(0xFFFFFF);
        
        self.sourceLogo = [[ASImageNode alloc] init];
        self.sourceLogo.backgroundColor = ASDisplayNodeDefaultPlaceholderColor();
        
        asimagenode_modification_block_t blurFilter = ^UIImage* (UIImage *img) {
            UIColor *tint = [UIColor clearColor];
            return [UIImageEffects imageByApplyingBlurToImage:img withRadius:7 tintColor:tint saturationDeltaFactor:1 maskImage:nil];
        };
        
        if ([self.source isLogoLocallyAvailable]) {
            ASImageNode *imageNode = [[ASImageNode alloc] init];
            imageNode.imageModificationBlock = blurFilter;
            imageNode.image = [UIImage imageWithContentsOfFile:self.source.logoURL];
            self.sourceLogo = imageNode;
            
        } else {
            ASNetworkImageNode *imageNode = [[ASNetworkImageNode alloc] init];
            imageNode.imageModificationBlock = blurFilter;
            imageNode.URL = [Util urlFromString:self.source.logoURL];
            self.sourceLogo = imageNode;
        }
        
        CGFloat x = CELL_INSET;
        CGFloat y = CELL_INSET;
        CGFloat w = size.width - 2 * CELL_INSET;
        CGFloat h = size.height - 2 * CELL_INSET;
        CGRect logoFrame = CGRectMake(x, y, w, h);
        self.sourceLogo.frame = logoFrame;
        [self addSubnode:self.sourceLogo];
        
        // add a mask
        ASDisplayNode *logoMask = [[ASDisplayNode alloc] init];
        logoMask.backgroundColor = UIColorFromRGBA(0x000000, LOGO_MASK_OPACITY);
        logoMask.frame = self.sourceLogo.frame;
        [self addSubnode:logoMask];
        
        // add a label for the name of the source
        UIFont *font = [UIFont fontWithName:FONT_NAME_REGULAR size:SOURCE_NAME_FONT_SIZE];
        NSShadow *shadow = [[NSShadow alloc] init];
        shadow.shadowColor = UIColorFromRGB(0x000000);
        shadow.shadowBlurRadius = 0.0;
        shadow.shadowOffset = CGSizeMake(0.0, 1.0);
        
        NSDictionary *attrs = @{ NSFontAttributeName: font,
                                 NSForegroundColorAttributeName: UIColorFromRGB(COLOR_CODE_WHITE),
                                 NSShadowAttributeName: shadow};
        
        ASTextNode *nameNode = [[ASTextNode alloc] init];
        nameNode.attributedString = [[NSAttributedString alloc] initWithString:self.source.name attributes:attrs];
        [nameNode measure:CGSizeMake(logoMask.bounds.size.width, FLT_MAX)];
        CGPoint origin = CGPointMake(LABEL_INSET + SOURCE_LOGO_ANIMATION_SHIFT_AMOUNT, LABEL_INSET);
        nameNode.frame = (CGRect){ origin, nameNode.calculatedSize };
        
        self.sourceNameLabel = nameNode.view;
        [logoMask addSubnode:nameNode];
        
        // add a label for the selection indicator
        attrs = @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:SELECTED_SOURCE_INDICATOR_FONT_SIZE],
                   NSForegroundColorAttributeName: UIColorFromRGB(COLOR_CODE_SUCCESS),
                   NSShadowAttributeName: shadow
                   };
        
        ASTextNode *selectedIndicatorNode = [[ASTextNode alloc] init];
        selectedIndicatorNode.attributedString = [[NSAttributedString alloc] initWithString:UNICODE_CHECK_MARK attributes:attrs];
        [selectedIndicatorNode measure:CGSizeMake(logoMask.bounds.size.width, FLT_MAX)];
        
        x = logoMask.bounds.size.width - SELECTED_SOURCE_INDICATOR_SIZE - LABEL_INSET;
        y = logoMask.bounds.size.height - SELECTED_SOURCE_INDICATOR_SIZE - LABEL_INSET;
        origin = CGPointMake(x, y);
        
        selectedIndicatorNode.frame = (CGRect){ origin, selectedIndicatorNode.calculatedSize };
        [logoMask addSubnode:selectedIndicatorNode];
        
        self.selectionIndicator = selectedIndicatorNode.view;
        self.selectionIndicator.hidden = !self.source.selected;
        
        __weak typeof(self) bself = self;
        [[EventManager getInstance] addEventListener:SHOW_SOURCE_SELECTOR_PAGE_ANIMATIONS oneShot:YES withCallback:^(va_list args) {
            [bself animateOnShow];
        }];
    }
    return self;
}

- (CGSize)calculateSizeThatFits:(CGSize)constrainedSize {
    return self.frame.size;
}

- (void)didLoad {
    [[ActionViewAnimatorService getInstance] addAnimationsOnView:self.view];
    [Util addOrAppendTapHandlerToView:self.view target:self action:@selector(onTap:)];
}

- (void)onTap:(UITapGestureRecognizer*)recognizer {
    if (recognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    self.source.selected = !self.source.selected;
    self.selectionIndicator.hidden = !self.source.selected;
    
    [[EventManager getInstance] fireEvent:SOURCE_SELECTION_CHANGED, self.source];
}

- (void)animateOnShow {
    POPSpringAnimation *springAnimation = [POPSpringAnimation animation];
    springAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewCenter];
    
    CGRect frame = self.sourceNameLabel.frame;
    CGPoint fromValue = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
    frame.origin.x -= SOURCE_LOGO_ANIMATION_SHIFT_AMOUNT;
    CGPoint toValue = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
    
    springAnimation.fromValue = [NSValue valueWithCGPoint:fromValue];
    springAnimation.toValue = [NSValue valueWithCGPoint:toValue];
    
    springAnimation.velocity = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
    springAnimation.springBounciness = 15;
    springAnimation.springSpeed = 2;
    
    __weak typeof(self) bself = self;
    springAnimation.completionBlock = ^(POPAnimation *anim, BOOL finished){
        if (!bself) {
            return;
        }
        [Util addDeviceTiltEffectToView:bself.sourceNameLabel withMaximumMovement:15];
    };
    
    [self.sourceNameLabel pop_addAnimation:springAnimation forKey:@"center"];
}

@end
