//
//  FeedView.h
//  Briefrr
//
//  Created by Shashank Singh on 2/17/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Story.h"
#import "Macros.h"
#import "BaseCarouselView.h"

@class StoryView;

@protocol StoryViewDelegate<NSObject>
- (void)storyViewLoaded:(StoryView*)view;
@end

@interface StoryView : BaseCarouselView
@property (nonatomic, weak) id<StoryViewDelegate> delegate;
@property (nonatomic, strong) Story *story;
- (void)didBecomeCurrentStory;
- (void)didBecomeSecondaryStory;
- (void)updateOnReadingModeChange:(FeedReadingMode)readingMode;
- (void)updateOnSavedStateChange;
@end
