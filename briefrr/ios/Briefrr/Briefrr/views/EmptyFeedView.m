//
//  EmptyFeedView.m
//  Briefrr
//
//  Created by Shashank Singh on 3/24/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "EmptyFeedView.h"
#import <FontAwesomeKit/FontAwesomeKit.h>

static const CGFloat ICON_FONT_SIZE = 64.0f;
static const CGFloat MESSAGE_FONT_SIZE = 12.0f;
static const CGFloat X_INSET = 20;

@interface EmptyFeedView()
@property (nonatomic) FeedType feedType;
@end

@implementation EmptyFeedView
- (instancetype)initWithFrame:(CGRect)frame feedType:(FeedType)feedType {
    self = [super initWithFrame:frame];
    if (self) {
        CGRect rect;
        CGFloat x, y, w, h;
        
        x = 0;
        w = self.topSection.bounds.size.width;
        y = self.topSection.bounds.size.height/2 - ICON_FONT_SIZE/2;
        h = ICON_FONT_SIZE;
        
        rect = CGRectMake(x, y, w, h);
        UILabel *iconLabel = [[UILabel alloc] initWithFrame:rect];
        iconLabel.textAlignment = NSTextAlignmentCenter;
        [self.topSection addSubview:iconLabel];
        
        NSString *iconCode = feedType == NEWS_FEED ? FONT_ICON_CODE_NEWSFEED : FONT_ICON_CODE_BOOKMARK;
        FAKFontAwesome *icon = [FAKFontAwesome iconWithCode:iconCode size:ICON_FONT_SIZE];
        [icon addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(COLOR_CODE_GREY)];
        iconLabel.attributedText = icon.attributedString;
        
        x = X_INSET;
        y = 0;
        w = self.bottomSection.bounds.size.width - 2 * X_INSET;
        h = self.bottomSection.bounds.size.height - y;
        rect = CGRectMake(x, y, w, h);
        
        UILabel *message = [[UILabel alloc] initWithFrame:rect];
        message.textAlignment = NSTextAlignmentCenter;
        message.text = feedType == NEWS_FEED ? MESSAGE_EMPTY_NEWS_FEED : MESSAGE_EMPTY_SAVED_STORIES_FEED;
        message.font = [UIFont fontWithName:FONT_NAME_LIGHT size:MESSAGE_FONT_SIZE];
        message.textColor = UIColorFromRGB(COLOR_CODE_GREY);
        message.numberOfLines = 0;
        message.lineBreakMode = NSLineBreakByWordWrapping;
        [self.bottomSection addSubview:message];
    }
    return self;
}
@end
