//
//  SourceView.h
//  Briefrr
//
//  Created by Shashank Singh on 2/13/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "Source.h"

@interface SourceView : ASCellNode
- (instancetype)initWithSource:(Source*)source andSize:(CGSize)size;
@end
