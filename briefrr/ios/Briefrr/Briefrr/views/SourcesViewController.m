//
//  SourcesViewController.m
//  Briefrr
//
//  Created by Shashank Singh on 2/10/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "SourcesViewController.h"
#import "ASCollectionView.h"
#import "SourcesService.h"
#import "Source.h"
#import "SourceView.h"
#import "TabbedView.h"
#import "TopAppBanner.h"
#import "EventManager.h"
#import "Util.h"

static const CGFloat TOP_BANNER_HEIGHT = 60;
static const CGFloat ON_SHOW_ANIMATION_DELAY = 0.5;

@interface SourcesViewController ()<ASCollectionViewDataSource, ASCollectionViewDelegate>
@property (nonatomic, strong) TopAppBanner *topBanner;
@property (nonatomic, strong) TabbedView *tabbedView;
@property (nonatomic, strong) ASCollectionView *publishersGrid;
@property (nonatomic, strong) ASCollectionView *topicsGrid;
@end

@implementation SourcesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGB(0xFFFFFF);
    
    CGRect topBannerFrame = CGRectMake(0, 0, self.view.bounds.size.width, TOP_BANNER_HEIGHT);
    self.topBanner = [[TopAppBanner alloc] initWithFrame:topBannerFrame];
    [self.view addSubview:self.topBanner];
    
    self.publishersGrid = [self createGrid];
    self.topicsGrid = [self createGrid];
    
    NSArray *tabDescriptors = @[@{@"name": @"Topics", @"view": self.topicsGrid},
                                @{@"name": @"Publishers", @"view": self.publishersGrid}];
    
    CGFloat y = self.topBanner.bounds.size.height;
    CGFloat w = self.view.bounds.size.width;
    CGFloat h = self.view.bounds.size.height - y;
    CGRect tabbedViewFrame = CGRectMake(0, y, w, h);
    self.tabbedView = [[TabbedView alloc] initWithFrame:tabbedViewFrame andTabs:tabDescriptors];
    
    [self.view insertSubview:self.tabbedView belowSubview:self.topBanner];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (ASCollectionView*)createGrid {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 0;
    
    ASCollectionView *grid = [[ASCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    grid.asyncDataSource = self;
    grid.asyncDelegate = self;
    grid.backgroundColor = UIColorFromRGB(0xFFFFFF);
    
    return grid;
}

-(void)willMoveToParentViewController:(UIViewController *)parent {
    
}

- (void)didMoveToParentViewController:(UIViewController *)parent {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, ON_SHOW_ANIMATION_DELAY * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [[EventManager getInstance] fireEvent:SHOW_SOURCE_SELECTOR_PAGE_ANIMATIONS];
    });
}

#pragma mark -
#pragma mark ASCollectionView data source.

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *gridSources = [self getSourcesForGrid:collectionView];
    if (!gridSources) {
        return nil;
    }
    
    Source *source = gridSources[indexPath.item];
    CGFloat side = self.view.frame.size.width * 0.5;
    CGSize size = CGSizeMake(side, side * (3.0f/4.0f));
    SourceView *sourceCell = [[SourceView alloc] initWithSource:source andSize:size];
    return sourceCell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSArray *gridSources = [self getSourcesForGrid:collectionView];
    if (!gridSources) {
        return 0;
    }
    return gridSources.count;
}

- (NSArray*)getSourcesForGrid:(UICollectionView*)grid {
    if (grid == self.publishersGrid) {
        return [[SourcesService getInstance] getAllPublishers];
    } else if (grid == self.topicsGrid) {
        return [[SourcesService getInstance] getAllTopics];
    } else {
        // TODO (shashank): log error
        return nil;
    }
}

@end
