//
//  MicPermissionIntroViewController.m
//  Briefrr
//
//  Created by Shashank Singh on 2/16/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "MicPermissionIntroViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "Macros.h"
#import "EventManager.h"
#import "Util.h"

static NSString* const MAJOR_MESSAGE_TEXT = @"Briefrr needs your permission!";
static NSString* const MINOR_MESSAGE_TEXT = @"Briefrr needs to access the microphone to understand your voice commands.";
static NSString* const ACTION_BUTTON_TEXT = @"Grant Permission";

static const CGFloat MAJOR_MESSAGE_FONT_SIZE = 20.0f;
static const CGFloat MINOR_MESSAGE_FONT_SIZE = 14.0f;

@interface MicPermissionIntroViewController()
@property (nonatomic) BOOL permissionDeniedOnce;
@end

@implementation MicPermissionIntroViewController

- (NSString*)majorText {
    return MAJOR_MESSAGE_TEXT;
}

- (NSString*)minorText {
    return MINOR_MESSAGE_TEXT;
}

- (NSString*)actionText {
    return ACTION_BUTTON_TEXT;
}

- (CGFloat)majorTextFontSize {
    return MAJOR_MESSAGE_FONT_SIZE;
}

- (CGFloat)minorTextFontSize {
    return MINOR_MESSAGE_FONT_SIZE;
}

- (void)onActionButtonTapped {
    
    AVAudioSessionRecordPermission currentAudioPermission = [AVAudioSession sharedInstance].recordPermission;
    if (currentAudioPermission == AVAudioSessionRecordPermissionDenied) {
        // already denied, take the user directly to the settings page
        [Util openAppSystemSettings];
        return;
    }
    
    // permissions not denied or granted yet
    __weak typeof(self) bself = self;
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!granted) {
                [bself setMinorTextColor:UIColorFromRGB(COLOR_CODE_ERROR_MESSAGE)];
                return;
            }
            [[EventManager getInstance] fireEvent:MIC_PERMISSION_GRANTED];
        });
    }];
}

@end
