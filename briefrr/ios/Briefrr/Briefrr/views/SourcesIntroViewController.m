//
//  SourcesIntroViewController.m
//  Briefrr
//
//  Created by Shashank Singh on 2/8/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "SourcesIntroViewController.h"
#import "EventManager.h"
#import "Macros.h"

static const CGFloat MAJOR_MESSAGE_FONT_SIZE = 30.0f;
static const CGFloat MINOR_MESSAGE_FONT_SIZE = 16.0f;

@implementation SourcesIntroViewController
- (NSString*)majorText {
    return MESSAGE_SOURCE_SELECTION_INTRO_MAJOR;
}

- (NSString*)minorText {
    return MESSAGE_SOURCE_SELECTION_INTRO_MINOR;
}

- (NSString*)actionText {
    return MESSAGE_SOURCE_SELECTION_INTRO_ACTION;
}

- (CGFloat)majorTextFontSize {
    return MAJOR_MESSAGE_FONT_SIZE;
}

- (CGFloat)minorTextFontSize {
    return MINOR_MESSAGE_FONT_SIZE;
}

- (void)onActionButtonTapped {
    [[EventManager getInstance] fireEvent:SHOW_SOURCE_SELECTOR_PAGE];
}
@end
