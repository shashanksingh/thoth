//
//  BaseCarouselView.m
//  Briefrr
//
//  Created by Shashank Singh on 3/24/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "BaseCarouselView.h"
#import "Macros.h"
#import "Util.h"

static const CGFloat TOP_SECTION_HEIGHT_FRACTION = 0.45;

@implementation BaseCarouselView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = TRANSPARENT_COLOR;
        [self.layer setShadowColor:UIColorFromRGB(COLOR_CODE_BLACK).CGColor];
        [self.layer setShadowOpacity:0.75];
        [self.layer setShadowRadius:2.0];
        [self.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
        self.layer.shouldRasterize = YES;
        self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
        self.layer.rasterizationScale = UIScreen.mainScreen.scale;
        
        CGFloat y, w, h;
        
        w = self.bounds.size.width;
        h = self.bounds.size.height * TOP_SECTION_HEIGHT_FRACTION;
        CGRect topSectionFrame = CGRectMake(0, 0, w, h);
        self.topSection = [[UIView alloc] initWithFrame:topSectionFrame];
        [self addSubview:self.topSection];
        [Util addBorderToUIView:self.topSection onSides:BOX_BOTTOM ofColor:UIColorFromRGB(COLOR_CODE_LIGHT_GREY) andThickness:1.0f];
        
        y = self.topSection.bounds.size.height;
        w = self.bounds.size.width;
        h = self.bounds.size.height * (1 - TOP_SECTION_HEIGHT_FRACTION);
        CGRect bottomSectionFrame = CGRectMake(0, y, w, h);
        self.bottomSection = [[UIView alloc] initWithFrame:bottomSectionFrame];
        self.bottomSection.backgroundColor = UIColorFromRGB(COLOR_CODE_WHITE);
        [self addSubview:self.bottomSection];
    }
    return self;
}
@end
