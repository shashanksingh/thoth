//
//  TabSwitchMenu.h
//  Briefrr
//
//  Created by Shashank Singh on 3/5/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwingDropMenu.h"

@interface TabSwitchMenu : SwingDropMenu
+ (TabSwitchMenu*)getInstance;
@end
