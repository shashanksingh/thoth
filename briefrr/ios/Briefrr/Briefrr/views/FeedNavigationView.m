//
//  FeedNavigationView.m
//  Briefrr
//
//  Created by Shashank Singh on 2/24/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "FeedNavigationView.h"
#import "ActionButton.h"
#import "Macros.h"
#import <pop/POP.h>
#import "EventManager.h"
#import "Util.h"
#import <FontAwesomeKit/FontAwesomeKit.h>
#import "MicrophoneIcon.h"
#import "SpeakerIcon.h"
#import "TabSwitchMenu.h"
#import "FeedReaderControls.h"
#import "AppStateManager.h"
#import "AnimatedViewProtocol.h"
#import "EventLogger.h"

static const CGFloat ACTION_BUTTON_INSET_X = 15;
static const CGFloat ACTION_BUTTON_INSET_Y = 30;
static const CGFloat AUDIO_READING_MODE_OPACITY = 0.9;
static const CGFloat VISUAL_READING_MODE_OPACITY = 0.0;
static const CGFloat READING_MODE_CHANGE_ANIMATION_DURATION = 1.5;
static const CGFloat READER_STATE_CHANGE_ANIMATION_DURATION = 0.25;
static const NSUInteger BUTTON_COLOR = COLOR_CODE_WHITE;

static const CGFloat STATE_INDICATOR_ICON_SIZE = 100;
static const CGFloat STATE_INDICATOR_ICON_STATUS_MESSAGE_GAP = 50;
static const CGFloat STATUS_MESSAGE_FONT_SIZE = 20;
static const CGFloat STATUS_MESSAGE_INSET_X = 20;
static const NSTimeInterval STATUS_MESSAGE_ANIMATION_DURATION = 1.0;

static NSString* const READING_MODE_CHANGE_ANIMATION_KEY = @"readingModeChangeAnimation";

@interface FeedNavigationView()
@property (nonatomic, strong) SwingDropMenu *tabSwitchMenu;
@property (nonatomic, strong) ActionButton *modeSwitchButton;
@property (nonatomic, strong) SpeakerIcon *speakingStateIndicator;
@property (nonatomic, strong) MicrophoneIcon *listeningStateIndicator;
@property (nonatomic, strong) UILabel *statusMessage;
@property (nonatomic, strong) FeedReaderControls *readerControls;
@end

@implementation FeedNavigationView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        CGRect frame;
        CGFloat x, y, w, h;
        
        self.backgroundColor = TRANSPARENT_COLOR;
        self.tintColor = TRANSPARENT_COLOR;
        
        UIColor *iconColor = UIColorFromRGB(BUTTON_COLOR);
        
        x = ACTION_BUTTON_INSET_X;
        y = ACTION_BUTTON_INSET_Y;
        self.tabSwitchMenu = [TabSwitchMenu getInstance];
        frame = self.tabSwitchMenu.frame;
        frame.origin.x = ACTION_BUTTON_INSET_X;
        frame.origin.y = ACTION_BUTTON_INSET_Y;
        self.tabSwitchMenu.frame = frame;
        
        [self addSubview:self.tabSwitchMenu];
        
        x = self.bounds.size.width - ACTION_BUTTON_SIZE - ACTION_BUTTON_INSET_X;
        w = h = ACTION_BUTTON_SIZE;
        frame = CGRectMake(x, y, w, h);
        self.modeSwitchButton = [[ActionButton alloc] initWithFrame:frame label:FONT_ICON_CODE_BULLHORN color:iconColor];
        [self addSubview:self.modeSwitchButton];
        [self.modeSwitchButton addTarget:self action:@selector(onModeSwitchButtonTap) forControlEvents:UIControlEventTouchUpInside];
        
        self.speakingStateIndicator = [[SpeakerIcon alloc] initWithFrame:[self getStateIndicatorIconFrame]];
        [self addSubview:self.speakingStateIndicator];
        self.speakingStateIndicator.alpha = 0;
        
        self.listeningStateIndicator = [[MicrophoneIcon alloc] initWithFrame:[self getStateIndicatorIconFrame]];
        [self addSubview:self.listeningStateIndicator];
        self.listeningStateIndicator.alpha = 0;
        
        x = STATUS_MESSAGE_INSET_X;
        y = self.listeningStateIndicator.frame.origin.y + STATE_INDICATOR_ICON_SIZE +STATE_INDICATOR_ICON_STATUS_MESSAGE_GAP;
        w = self.bounds.size.width - 2 * STATUS_MESSAGE_INSET_X;
        h = STATUS_MESSAGE_FONT_SIZE * 2;
        frame = CGRectMake(x, y, w, h);
        self.statusMessage = [[UILabel alloc] initWithFrame:frame];
        self.statusMessage.alpha = 0;
        self.statusMessage.textAlignment = NSTextAlignmentCenter;
        self.statusMessage.textColor = UIColorFromRGB(BUTTON_COLOR);
        self.statusMessage.layer.shadowColor = UIColorFromRGB(COLOR_CODE_BLACK).CGColor;
        self.statusMessage.layer.shadowOffset = CGSizeMake(0.0, 0.0);
        self.statusMessage.layer.shadowRadius = 2.0;
        self.statusMessage.layer.shadowOpacity = 1;
        self.statusMessage.adjustsFontSizeToFitWidth = YES;
        [self setStatusMessageText:HELP_MESSAGE_START_COMMAND];
        
        [self addSubview:self.statusMessage];
        
        [self addControlIcons];
        
        self.audioReadingModeDisabled = YES;
        
        __weak typeof(self) bself = self;
        [[EventManager getInstance] addEventListener:FEED_READING_MODE_CHANGED withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            FeedReadingMode readingMode = ((NSNumber*)va_arg(args, NSNumber*)).intValue;
            [bself onReadingModeChanged:readingMode];
        }];
        
        [[EventManager getInstance] addEventListener:FEED_READER_INPUT_OUTPUT_STATE_CHANGED withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            FeedReaderState feedReaderState = ((NSNumber*)va_arg(args, NSNumber*)).intValue;
            [bself onFeedReaderStateChange:feedReaderState];
        }];
        
        [[EventManager getInstance] addEventListener:COMMAND_RECOGNIZED withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            NSString *command = ((NSString*)va_arg(args, NSString*));
            [bself onCommandRecognized:command];
        }];
        
        [[EventManager getInstance] addEventListener:VOICE_ACITIVITY_COMMAND_RECOGNITION_BEGIN withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            [bself onCommandRecognitionBegin];
        }];
        [[EventManager getInstance] addEventListener:VOICE_ACITIVITY_COMMAND_RECOGNITION_END withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            [bself onCommandRecognitionEnd];
        }];
        
    }
    return self;
}

- (void)setAudioReadingModeDisabled:(BOOL)audioReadingModeDisabled {
    _audioReadingModeDisabled = audioReadingModeDisabled;
    
    NSString *animationKey = @"fadeModeSwitchButton";
    CGFloat alphaTo = audioReadingModeDisabled ? 0 : 1;
    [self animateView:self.modeSwitchButton key:animationKey propName:kPOPViewAlpha from:@(self.modeSwitchButton.alpha) to:@(alphaTo) onCompletion:nil];
}


- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    for (UIView *view in self.subviews) {
        if (!view.hidden && view.alpha > 0 && view.userInteractionEnabled && [view pointInside:[self convertPoint:point toView:view] withEvent:event])
            return YES;
    }
    return NO;
}

- (CGRect)getStateIndicatorIconFrame {
    CGFloat x = self.bounds.size.width/2 - STATE_INDICATOR_ICON_SIZE/2;
    CGFloat y = self.bounds.size.height/2 - STATE_INDICATOR_ICON_SIZE;
    return CGRectMake(x, y, STATE_INDICATOR_ICON_SIZE, STATE_INDICATOR_ICON_SIZE);
}

- (void)addControlIcons {
    CGFloat x, y, w, h;
    CGRect frame;
    
    // TODO (shashank): this 3.5 factor should be inside the controls code
    w = 3.5 * ACTION_BUTTON_SIZE;
    h = ACTION_BUTTON_SIZE;
    x = self.bounds.size.width - ACTION_BUTTON_INSET_X - w;
    y = self.bounds.size.height - ACTION_BUTTON_INSET_Y - h;
    frame = CGRectMake(x, y, w, h);
    
    self.readerControls = [[FeedReaderControls alloc] initWithFrame:frame];
    self.readerControls.alpha = 0;
    [self addSubview:self.readerControls];
}

- (void)onModeSwitchButtonTap {
    FeedReadingMode readingMode = [AppStateManager getInstance].feedReadingMode;
    if (readingMode == AUDIO) {
        [AppStateManager getInstance].feedReadingMode = VISUAL;
    } else {
        [AppStateManager getInstance].feedReadingMode = AUDIO;
    }
}

- (void)setStatusMessageText:(NSString*)text {
    UIFont *statusMessageRegularFont = [UIFont fontWithName:FONT_NAME_LIGHT size:STATUS_MESSAGE_FONT_SIZE];
    UIFont *statusMessageBoldFont = [UIFont fontWithName:FONT_NAME_SEMI_BOLD size:STATUS_MESSAGE_FONT_SIZE];
    NSDictionary *attrs = @{NSFontAttributeName: statusMessageRegularFont};
    NSMutableAttributedString *statusMessageAttrString = [[NSMutableAttributedString alloc] initWithString:text attributes:attrs];
    [Util applyBoldToAttributedString:statusMessageAttrString usingBoldFont:statusMessageBoldFont];
    self.statusMessage.attributedText = statusMessageAttrString;
}

- (POPBasicAnimation*)getAnimationForKey:(NSString*)key propName:(NSString*)propName from:(id)from to:(id)to {
    POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:propName];
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    anim.fromValue = from;
    anim.toValue = to;
    anim.duration = READING_MODE_CHANGE_ANIMATION_DURATION;
    return anim;
}

- (void)animateLayer:(CALayer*)layer key:(NSString*)key propName:(NSString*)propName from:(id)from to:(id)to {
    [layer pop_removeAnimationForKey:key];
    POPBasicAnimation *anim = [self getAnimationForKey:key propName:propName from:from to:to];
    [layer pop_addAnimation:anim forKey:key];
}

- (void)animateView:(UIView*)view key:(NSString*)key propName:(NSString*)propName from:(id)from to:(id)to onCompletion:(void (^)(POPAnimation *anim, BOOL finished))onCompletion {
    
    [view pop_removeAnimationForKey:key];
    POPBasicAnimation *anim = [self getAnimationForKey:key propName:propName from:from to:to];
    if (onCompletion) {
        anim.completionBlock = onCompletion;
    }
    [view pop_addAnimation:anim forKey:key];
}

- (void)onReadingModeChanged:(FeedReadingMode)readingMode {
    // animate the bg color of the overlay
    CGFloat finalViewOpacity = readingMode == AUDIO ? AUDIO_READING_MODE_OPACITY : VISUAL_READING_MODE_OPACITY;
    [self animateLayer:self.layer key:@"colorAnimationView" propName:kPOPLayerBackgroundColor from:(id)self.backgroundColor.CGColor to:UIColorFromRGBA(COLOR_CODE_BLACK, finalViewOpacity)];
    
    [Util fadeAndSlide:self.tabSwitchMenu fadeIn:readingMode == VISUAL slideInDirection:TOP animationKey:@"fadeAnimationTabSwitchButton" duration:READING_MODE_CHANGE_ANIMATION_DURATION onCompletion:nil];
    
    [Util fadeAndSlide:self.readerControls fadeIn:readingMode == AUDIO slideInDirection:BOTTOM animationKey:@"fadeAnimationReaderControls" duration:READING_MODE_CHANGE_ANIMATION_DURATION onCompletion:nil];
    
    // animate the mode switch button as a feedback for the tap
    if (readingMode == AUDIO) {
        [self.modeSwitchButton drawBorderWithAnimation:YES];
    } else {
        [self.modeSwitchButton eraseBorderWithAnimation:YES];
    }
    
    // when changing mode to audio we always initialize in listening state
    // when changing mode to visual we hide both
    [Util fadeAndSlide:self.speakingStateIndicator fadeIn:NO slideInDirection:TOP animationKey:READING_MODE_CHANGE_ANIMATION_KEY duration:READING_MODE_CHANGE_ANIMATION_DURATION onCompletion:nil];
    
    [Util fadeAndSlide:self.listeningStateIndicator fadeIn:readingMode == AUDIO slideInDirection:TOP animationKey:READING_MODE_CHANGE_ANIMATION_KEY duration:READING_MODE_CHANGE_ANIMATION_DURATION onCompletion:nil];
    
    [Util fadeAndSlide:self.statusMessage fadeIn:readingMode == AUDIO slideInDirection:BOTTOM animationKey:READING_MODE_CHANGE_ANIMATION_KEY duration:READING_MODE_CHANGE_ANIMATION_DURATION onCompletion:nil];
    
    [self animateStatusTextTo:HELP_MESSAGE_START_COMMAND];
}

- (void)animateStatusTextTo:(NSString*)text {
    if (![self.statusMessage.text isEqualToString:text]) {
        __weak typeof(self) bself = self;
        [UIView transitionWithView:self.statusMessage duration:STATUS_MESSAGE_ANIMATION_DURATION options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionTransitionCrossDissolve animations:^{
            if (!bself) {
                return;
            }
            [bself setStatusMessageText:text];
        } completion:nil];
    }
}

- (void)onFeedReaderStateChange:(FeedReaderState)feedReaderState {
    
    UIView *toShow = feedReaderState == LISTENING ? self.listeningStateIndicator : self.speakingStateIndicator;
    UIView *toHide = feedReaderState == LISTENING ? self.speakingStateIndicator : self.listeningStateIndicator;
    
    [Util fadeAndSlide:toHide fadeIn:NO slideInDirection:NONE animationKey:READING_MODE_CHANGE_ANIMATION_KEY duration:READER_STATE_CHANGE_ANIMATION_DURATION onCompletion:nil];
    
    [Util fadeAndSlide:toShow fadeIn:YES slideInDirection:NONE animationKey:READING_MODE_CHANGE_ANIMATION_KEY duration:READER_STATE_CHANGE_ANIMATION_DURATION onCompletion:^(BOOL finished) {
        if (finished) {
            [(NSObject<AnimatedView>*)toShow startAnimation];
            [(NSObject<AnimatedView>*)toHide stopAnimation];
        }
    }];
}

- (void)onCommandRecognized:(NSString*)command {
    NSString *statusText = nil;
    if ([command isEqualToString:COMMAND_START_READER]) {
        statusText = HELP_MESSAGE_PAUSE_COMMAND;
    } else if ([command isEqualToString:COMMAND_PAUSE_READER]) {
        statusText = HELP_MESSAGE_CONTINUE_COMMAND;
    } else if ([command isEqualToString:COMMAND_CONTINUE_READER]) {
        statusText = HELP_MESSAGE_PAUSE_COMMAND;
    } else if ([command isEqualToString:COMMAND_NEXT_STORY]) {
        statusText = HELP_MESSAGE_PAUSE_COMMAND;
    } else if ([command isEqualToString:COMMAND_PREVIOUS_STORY]) {
        statusText = HELP_MESSAGE_PAUSE_COMMAND;
    } else if ([command isEqualToString:COMMAND_SAVE_STORY]) {
        statusText = nil;
    } else if ([command isEqualToString:COMMAND_STOP_READER]) {
        statusText = nil;
    } else {
        [[EventLogger getInstance] logEvent:@"error_unhandled_recognized_command" properties:@{@"command": command, @"location": @"feed_nav_view"}];
    }
    
    if (statusText) {
        [self animateStatusTextTo:statusText];
    }
}

- (void)onCommandRecognitionBegin {
    [self.listeningStateIndicator startBusyIndicator];
}

- (void)onCommandRecognitionEnd {
    [self.listeningStateIndicator stopBusyIndicator];
}

@end
