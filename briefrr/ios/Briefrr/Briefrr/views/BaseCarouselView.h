//
//  BaseCarouselView.h
//  Briefrr
//
//  Created by Shashank Singh on 3/24/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseCarouselView : UIView
@property (nonatomic, strong) UIView *topSection;
@property (nonatomic, strong) UIView *bottomSection;
@end
