//
//  AnimatedViewProtocol.h
//  Briefrr
//
//  Created by Shashank Singh on 3/25/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#ifndef Briefrr_AnimatedViewProtocol_h
#define Briefrr_AnimatedViewProtocol_h

@protocol AnimatedView<NSObject>
- (void)startAnimation;
- (void)stopAnimation;
@end

#endif
