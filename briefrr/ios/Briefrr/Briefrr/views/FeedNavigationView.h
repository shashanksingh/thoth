//
//  FeedNavigationView.h
//  Briefrr
//
//  Created by Shashank Singh on 2/24/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedNavigationView : UIView
@property (nonatomic) BOOL audioReadingModeDisabled;
@end
