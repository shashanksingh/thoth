//
//  FeedViewController.m
//  Briefrr
//
//  Created by Shashank Singh on 2/17/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "FeedViewController.h"
#import "StoryView.h"
#import "FeedService.h"
#import "SourcesService.h"
#import "TopAppBanner.h"
#import "EventManager.h"
#import "Story.h"
#import "TOWebViewController.h"
#import "FeedNavigationView.h"
#import "VoiceCommandService.h"
#import "FeedReaderService.h"
#import <Pop/POP.h>
#import "UserPreferenceService.h"
#import "EmptyFeedView.h"
#import "AppStateManager.h"
#import "Util.h"

static const NSUInteger STORY_COUNT_INFINITY = 10000;

static const CGFloat CAROUSEL_TOP_INSET = 30;
static const CGFloat FEED_VIEW_WIDTH_FRACTION = 0.85;
static const CGFloat FEED_VIEW_HEIGHT_FRACTION = 0.80;
static const CGFloat FEED_VIEW_SCALE_FACTOR_AUDIO_MODE = 1.05;
static const NSUInteger NUM_LOADED_CAROUSEL_ITEMS = 5;

static const NSTimeInterval RECOGNITION_START_DELAY = 0.4;

static NSString* const CAROUSEL_SCALE_ANIMATION_KEY = @"feedCarouselScaleAnimation";

@interface FeedViewController ()<iCarouselDataSource, iCarouselDelegate, StoryViewDelegate>
@property (nonatomic, strong) NSArray *feed;
@property (nonatomic, strong) iCarousel *carousel;
@property (nonatomic) NSInteger lastCurrentItemIndex;
@property (nonatomic, strong) FeedNavigationView *feedNavigationView;
@end

@implementation FeedViewController

- (instancetype)initWithFeedType:(FeedType)feedType {
    self = [super init];
    if (self) {
        self.lastCurrentItemIndex = -1;
        self.feed = nil;
        self.feedType = feedType;
        
        __weak typeof(self) bself = self;
        
        [[EventManager getInstance] addEventListener:COMMAND_RECOGNIZED withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            NSString *command = va_arg(args, NSString*);
            if ([command isEqualToString:COMMAND_SAVE_STORY]) {
                [bself saveCurrentStory];
            }
        }];
        
        [[EventManager getInstance] addEventListener:SHOW_FULL_STORY oneShot:NO withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            
            Story *storyToShow = va_arg(args, Story*);
            NSURL *linkURL = [Util urlFromString:storyToShow.link];
            
            TOWebViewController *webViewController = [[TOWebViewController alloc] initWithURL:linkURL];
            [bself presentViewController:[[UINavigationController alloc] initWithRootViewController:webViewController] animated:YES completion:nil];
        }];
        
        [[EventManager getInstance] addEventListener:SHOW_SHARE_DIALOG oneShot:NO withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            
            NSArray *sharingItems = va_arg(args, NSArray*);
            
            UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
            [bself presentViewController:activityController animated:YES completion:nil];
        }];
        
        [[EventManager getInstance] addEventListener:FEED_READING_MODE_CHANGED withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            
            FeedReadingMode readingMode = ((NSNumber*)va_arg(args, NSNumber*)).intValue;
            [bself onReadingModeChange:readingMode];
        }];
        
        [[EventManager getInstance] addEventListener:FEED_READER_STARTED_READING_STORY withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            if (!bself.feed) {
                return;
            }
            int currentStoryIndex = ((NSNumber*)va_arg(args, NSNumber*)).intValue;
            if (currentStoryIndex < 0 || currentStoryIndex >= bself.feed.count) {
                //TODO (shashank): log error
                return;
            }
            currentStoryIndex = (int)(MAX(0, MIN(bself.feed.count -1, currentStoryIndex)));
            [bself.carousel scrollToItemAtIndex:currentStoryIndex animated:YES];
        }];
        
        if (self.feedType == NEWS_FEED) {
            NSArray *selectedSourceIds = [[SourcesService getInstance] getSelectedSourceIds];
            [[FeedService getInstance] getFeedForSources:selectedSourceIds offset:0 limit:FEED_PAGE_SIZE callback:^(NSArray *data) {
                if (!bself) {
                    return;
                }
                [bself setFeed:data];
            } errorback:^(NSError *error) {
                // TODO (shashank): log error
                // TODO (shashank): handle error
            }];
        } else {
            NSArray *savedStories = [[UserPreferenceService getInstance] getSavedStories];
            self.feed = savedStories;
        }
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGBA(COLOR_CODE_BLACK, 0.0);
    
    CGFloat y = CAROUSEL_TOP_INSET;
    CGFloat w = self.view.bounds.size.width;
    CGFloat h = self.view.bounds.size.height - CAROUSEL_TOP_INSET;
    CGRect carouselRect = CGRectMake(0, y, w, h);
    
    self.carousel = [[iCarousel alloc] initWithFrame:carouselRect];
    self.carousel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.carousel.type = iCarouselTypeCustom;
    self.carousel.dataSource = self;
    self.carousel.delegate = self;
    [self.view addSubview:self.carousel];
    
    self.feedNavigationView = [[FeedNavigationView alloc] initWithFrame:self.view.bounds];
    self.feedNavigationView.audioReadingModeDisabled = [self isFeedEmpty];
    [self.view addSubview:self.feedNavigationView];
    
    [self onReadingModeChange:[AppStateManager getInstance].feedReadingMode];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL)prefersStatusBarHidden {
    return NO;
}

- (BOOL)isFeedEmpty {
    return self.feed && self.feed.count == 0;
}

- (UIView*)carousel:(iCarousel*)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    // if there is a mismatch in the type of view and the current emptiness state of
    // feed we can't reuse view
    if (view) {
        if ([view isKindOfClass:[EmptyFeedView class]] && ![self isFeedEmpty]) {
            view = nil;
        } else if ([view isKindOfClass:[StoryView class]] && [self isFeedEmpty]) {
            view = nil;
        }
    }
    
    if (!view) {
        CGFloat w = self.view.bounds.size.width * FEED_VIEW_WIDTH_FRACTION;
        CGFloat h = self.view.bounds.size.height * FEED_VIEW_HEIGHT_FRACTION;
        CGRect frame = CGRectMake(0, 0, w, h);
        if ([self isFeedEmpty]) {
            view = [[EmptyFeedView alloc] initWithFrame:frame feedType:self.feedType];
        } else {
            StoryView *storyView = [[StoryView alloc] initWithFrame:frame];
            storyView.delegate = self;
            view = storyView;
        }
    }
    
    if ([view isKindOfClass:[StoryView class]]) {
        StoryView *storyView = (StoryView*)view;
        if (self.feed && index < self.feed.count) {
            [storyView setStory:self.feed[index]];
        }
    }
    return view;
}

- (NSInteger)numberOfItemsInCarousel:(iCarousel*)carousel {
    if (!self.feed) {
        return STORY_COUNT_INFINITY;
    }
    if (self.feed.count == 0) {
        return 1;
    }
    return self.feed.count;
}

- (UIView*)carousel:(iCarousel *)carousel placeholderViewAtIndex:(NSInteger)index reusingView:(UIView *)view {
    return nil;
}

- (NSInteger)numberOfPlaceholdersInCarousel:(iCarousel *)carousel {
    return 0;
}

- (CATransform3D)carousel:(iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform {
    
    CGFloat tilt = 0.9f;
    CGFloat clampedOffset = MAX(-1.0, MIN(1.0, offset));
    
    CGFloat distance = 500.0f; //number of pixels to move the items away from camera
    CGFloat z = - fminf(1.0f, fabs(offset)) * distance;
    
    transform = CATransform3DTranslate(transform, offset * carousel.itemWidth * 1.2, 22.0f, z);
    transform = CATransform3DRotate(transform, -clampedOffset * M_PI_2 * tilt, 0.0, 1.0, 0.0);
    return transform;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value {
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap: {
            return NO;
        }
        case iCarouselOptionSpacing: {
            return value;
        }
        case iCarouselOptionVisibleItems: {
            return NUM_LOADED_CAROUSEL_ITEMS;
        }
        default: {
            return value;
        }
    }
}

- (void)setFeed:(NSArray *)feed {
    _feed = feed;
    [[FeedReaderService getInstance] setFeed:feed ofType:self.feedType];
    
    NSUInteger numFeedItems = self.feed ? self.feed.count : 0;
    if (numFeedItems == 0) {
        [self.carousel reloadData];
    }
    
    [AppStateManager getInstance].currentStoryIndex = 0;
    
    if (self.carousel) {
        for (NSUInteger i=0; i<MIN(numFeedItems, self.carousel.indexesForVisibleItems.count); ++i) {
            NSInteger storyIndex = ((NSNumber*)self.carousel.indexesForVisibleItems[i]).integerValue;
            StoryView *view = (StoryView*)[self.carousel itemViewAtIndex:storyIndex];
            [view setStory:self.feed[storyIndex]];
        }
    }
    
    if (!feed || feed.count == 0 || [AppStateManager getInstance].initalFeedLoadDone) {
        [self updateReadingMode];
    } else {
        // update will happen after first story is loaded
    }
    
}

- (void)updateReadingMode {
    BOOL audioModeNotSupported = !self.feed || [self isFeedEmpty];
    self.feedNavigationView.audioReadingModeDisabled = audioModeNotSupported;
    FeedReadingMode currentReadingMode = [AppStateManager getInstance].feedReadingMode;
    
    if (audioModeNotSupported) {
        if (currentReadingMode == AUDIO) {
            [AppStateManager getInstance].feedReadingMode = VISUAL;
        }
    } else {
        // after first run if there is a valid feed automatically move to audio mode
        // on first load of the default feed.
        if (![AppStateManager getInstance].initalFeedLoadDone) {
            [AppStateManager getInstance].feedReadingMode = AUDIO;
        }
    }
}

- (void)onCurrentItemStableChange {
    if (self.lastCurrentItemIndex == self.carousel.currentItemIndex) {
        return;
    }
    self.lastCurrentItemIndex = self.carousel.currentItemIndex;
    [AppStateManager getInstance].currentStoryIndex = self.carousel.currentItemIndex;
    
    
    NSInteger currentItemIndex = self.carousel.currentItemIndex;
    UIView *currentView = self.carousel.currentItemView;
    if (![currentView isKindOfClass:[StoryView class]]) {
        return;
    }
    StoryView *currentStoryView = (StoryView*)currentView;
    [currentStoryView didBecomeCurrentStory];
    
    // previous
    if (currentItemIndex > 0) {
        UIView *previousView = [self.carousel itemViewAtIndex:currentItemIndex - 1];
        if (![currentView isKindOfClass:[StoryView class]]) {
            // TODO (shashank): log error
            return;
        }
        StoryView *previousStoryView = (StoryView*)previousView;
        [previousStoryView didBecomeSecondaryStory];
    }
    
    if (currentItemIndex < self.carousel.numberOfItems - 1) {
        UIView *nextView = [self.carousel itemViewAtIndex:currentItemIndex + 1];
        if (![nextView isKindOfClass:[StoryView class]]) {
            // TODO (shashank): log error
            return;
        }
        StoryView *nextStoryView = (StoryView*)nextView;
        [nextStoryView didBecomeSecondaryStory];
    }
}

- (void)onReadingModeChange:(FeedReadingMode)readingMode {
    if ([self isFeedEmpty]) {
        return;
    }
    
    for (NSUInteger i=0; i<self.carousel.indexesForVisibleItems.count; ++i) {
        NSInteger storyIndex = ((NSNumber*)self.carousel.indexesForVisibleItems[i]).integerValue;
        StoryView *view = (StoryView*)[self.carousel itemViewAtIndex:storyIndex];
        [view updateOnReadingModeChange:readingMode];
    }
    
    CGFloat scale = readingMode == VISUAL ? 1 : FEED_VIEW_SCALE_FACTOR_AUDIO_MODE;
    POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    anim.toValue = [NSValue valueWithCGSize:CGSizeMake(scale, scale)];
    anim.duration = 1.5;
    [self.carousel pop_removeAnimationForKey:CAROUSEL_SCALE_ANIMATION_KEY];
    [self.carousel pop_addAnimation:anim forKey:CAROUSEL_SCALE_ANIMATION_KEY];
    
    if (readingMode == VISUAL) {
        [[VoiceCommandService getInstance] pauseRecognition];
        [[FeedReaderService getInstance] stopReading];
    } else {
        // startReco is an expensive step when initing the whole ASR stack
        // executing it immediately causes the UI to be unresponsive for a bit
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, RECOGNITION_START_DELAY * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [[VoiceCommandService getInstance] startRecognition];
        });
    }
}

- (void)carouselDidEndDecelerating:(iCarousel *)carousel {
    [self onCurrentItemStableChange];
}

- (void)carouselDidEndDragging:(iCarousel *)carousel willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self onCurrentItemStableChange];
    }
}

- (void)storyViewLoaded:(StoryView *)view {
    if (view == self.carousel.currentItemView) {
        [view didBecomeCurrentStory];
    }
    if (self.feed && self.feed.count > 0) {
        [self updateReadingMode];
        [AppStateManager getInstance].initalFeedLoadDone = YES;
    }
}

- (void)saveCurrentStory {
    if (!self.carousel) {
        return;
    }
    
    UIView *currentView = self.carousel.currentItemView;
    if (!currentView || ![currentView isKindOfClass:[StoryView class]]) {
        // e.g. in case of empty feed
        return;
    }
    
    
    StoryView *currentStoryView = (StoryView*)currentView;
    [[UserPreferenceService getInstance] saveStory:currentStoryView.story];
    [currentStoryView updateOnSavedStateChange];
}

@end
