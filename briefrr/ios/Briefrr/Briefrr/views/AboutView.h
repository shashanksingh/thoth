//
//  AboutView.h
//  Briefrr
//
//  Created by Shashank Singh on 4/14/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutView : UIView
+ (void)show;
@end
