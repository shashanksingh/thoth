//
//  TabSwitchMenu.m
//  Briefrr
//
//  Created by Shashank Singh on 3/5/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "TabSwitchMenu.h"
#import "Macros.h"
#import "ActionButton.h"
#import "EventManager.h"
#import "RootViewController.h"
#import "Util.h"

static const NSUInteger MENU_ITEM_ICON_COLOR = COLOR_CODE_WHITE;
static const NSTimeInterval SWITCH_DELAY = 0.25;

@interface TabSwitchMenu()
@property (nonatomic, strong) ActionButton *feedTabButton;
@property (nonatomic, strong) ActionButton *savedFeedTabButton;
@property (nonatomic, strong) ActionButton *sourceSelectorTabButton;
@property (nonatomic, strong) ActionButton *aboutPagePopupButton;
@end

@implementation TabSwitchMenu

+ (TabSwitchMenu*)getInstance {
    static TabSwitchMenu *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[TabSwitchMenu alloc] initWithSize:ACTION_BUTTON_SIZE];
    });
    return sharedInstance;
}

- (instancetype)initWithSize:(CGFloat)size {
    CGRect frame = CGRectMake(0, 0, size, size);
    self = [super initWithFrame:frame withMenuButtonLabel:FONT_ICON_CODE_NAV];
    if (self) {
        UIColor *buttonColor = UIColorFromRGB(MENU_ITEM_ICON_COLOR);
        CGRect buttonFrame = CGRectMake(0, 0, self.bounds.size.width * 1.25, self.bounds.size.height * 1.25);
        self.feedTabButton = [[ActionButton alloc] initWithFrame:buttonFrame label:FONT_ICON_CODE_NEWSFEED color:buttonColor showBorder:NO];
        [self addMenuItem:self.feedTabButton];
        
        self.savedFeedTabButton = [[ActionButton alloc] initWithFrame:buttonFrame label:FONT_ICON_CODE_BOOKMARK color:buttonColor showBorder:NO];
        [self addMenuItem:self.savedFeedTabButton];
        
        self.sourceSelectorTabButton = [[ActionButton alloc] initWithFrame:buttonFrame label:FONT_ICON_CODE_SETTINGS color:buttonColor showBorder:NO];
        [self addMenuItem:self.sourceSelectorTabButton];
        
        self.aboutPagePopupButton = [[ActionButton alloc] initWithFrame:buttonFrame label:FONT_ICON_CODE_INFO color:buttonColor showBorder:NO];
        [self addMenuItem:self.aboutPagePopupButton];
        
        [self.feedTabButton addTarget:self action:@selector(onFeedMenuButtonTap) forControlEvents:UIControlEventTouchUpInside];
        [self.savedFeedTabButton addTarget:self action:@selector(onSavedFeedMenuButtonTap) forControlEvents:UIControlEventTouchUpInside];
        [self.sourceSelectorTabButton addTarget:self action:@selector(onSourceMenuButtonTap) forControlEvents:UIControlEventTouchUpInside];
        [self.aboutPagePopupButton addTarget:self action:@selector(onAboutPageButtonTap) forControlEvents:UIControlEventTouchUpInside];
        
        [self clearSelectedState];
        [self setSelectedState];
        
        __weak typeof(self) bself = self;
        [[EventManager getInstance] addEventListener:CURRENT_PAGE_CHANGED withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            [bself clearSelectedState];
            [bself setSelectedState];
        }];
        
    }
    return self;
}

- (RootViewController*)getRootViewController {
    return (RootViewController*)[Util getRootViewController];
}

- (void)handleMenuButtonTapAndFireEvent:(EventType)eventType {
    [self clearSelectedState];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, SWITCH_DELAY * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [[EventManager getInstance] fireEvent:eventType];
    });
    [self closeMenu];
}

- (void)onFeedMenuButtonTap {
    if ([[self getRootViewController] isCurrentViewFeedOfType:NEWS_FEED]) {
        return;
    }
    [self handleMenuButtonTapAndFireEvent:SHOW_FEED_PAGE];
}

- (void)onSavedFeedMenuButtonTap {
    if ([[self getRootViewController] isCurrentViewFeedOfType:SAVED_STORIES]) {
        return;
    }
    [self handleMenuButtonTapAndFireEvent:SHOW_SAVED_STORIES_PAGE];
}

- (void)onSourceMenuButtonTap {
    if ([[self getRootViewController] isCurrentViewSourceSelector]) {
        return;
    }
    [self handleMenuButtonTapAndFireEvent:SHOW_SOURCE_SELECTOR_PAGE];
}

- (void)onAboutPageButtonTap {
    [self handleMenuButtonTapAndFireEvent:SHOW_ABOUT_PAGE];
}

- (void)clearSelectedState {
    [self.feedTabButton eraseBorderWithAnimation:NO];
    [self.savedFeedTabButton eraseBorderWithAnimation:NO];
    [self.sourceSelectorTabButton eraseBorderWithAnimation:NO];
}

- (void)setSelectedState {
    if ([[self getRootViewController] isCurrentViewFeedOfType:NEWS_FEED]) {
        [self.feedTabButton drawBorderWithAnimation:YES];
    } else if ([[self getRootViewController] isCurrentViewFeedOfType:SAVED_STORIES]) {
        [self.savedFeedTabButton drawBorderWithAnimation:YES];
    } else if ([[self getRootViewController] isCurrentViewSourceSelector]) {
        [self.sourceSelectorTabButton drawBorderWithAnimation:YES];
    }
}
@end
