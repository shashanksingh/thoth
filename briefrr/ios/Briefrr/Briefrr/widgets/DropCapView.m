//
//  ShapedTextLabel.m
//  DropCap
//
//  Created by Travis Kirton on 13-01-31.
//

#import "DropCapView.h"
#import <CoreText/CoreText.h>

@implementation DropCapView

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self setup];
    }
    return self;
}

-(void)setup {
    //set up default variables
    _text = @"";
    _fontName = @"Avenir-Roman";
    _dropCapFontSize = 60;
    _dropCapKernValue = 20;
    _textFontSize = 12;
    _textColor = [UIColor clearColor];
    _textAlignment = NSTextAlignmentJustified;
    _computedTextHeight = -1;
}

- (void)drawRect:(CGRect)rect {
    NSAssert(self.text != nil, @"text is nil");
    NSAssert(self.textColor != nil, @"textColor is nil");
    NSAssert(self.fontName != nil, @"fontName is nil");
    NSAssert(self.dropCapFontSize > 0, @"dropCapFontSize is <= 0");
    NSAssert(self.textFontSize > 0, @"textFontSize is <=0");
    
    if (self.text.length == 0) {
        return;
    }
    
    //convert the text aligment from NSTextAligment to CTTextAlignment
    CTTextAlignment ctTextAlignment = NSTextAlignmentToCTTextAlignment(self.textAlignment);
    
    //create a paragraph style
    CTParagraphStyleSetting paragraphStyleSettings[] = { {
            .spec = kCTParagraphStyleSpecifierAlignment,
            .valueSize = sizeof ctTextAlignment,
            .value = &ctTextAlignment
        }
    };
    
    CFIndex settingCount = sizeof paragraphStyleSettings / sizeof *paragraphStyleSettings;
    CTParagraphStyleRef style = CTParagraphStyleCreate(paragraphStyleSettings, settingCount);
    
    //create two fonts, with the same name but differing font sizes
    CTFontRef dropCapFontRef = CTFontCreateWithName((__bridge CFStringRef)self.fontName, self.dropCapFontSize, NULL);
    CTFontRef textFontRef = CTFontCreateWithName((__bridge CFStringRef)self.fontName, self.textFontSize, NULL);

    //create a dictionary of style elements for the drop cap letter
    NSDictionary *dropCapDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                (__bridge id)dropCapFontRef, kCTFontAttributeName,
                                self.textColor.CGColor, kCTForegroundColorAttributeName,
                                style, kCTParagraphStyleAttributeName,
                                @(self.dropCapKernValue) , kCTKernAttributeName,
                                nil];
    //convert it to a CFDictionaryRef
    CFDictionaryRef dropCapAttributes = (__bridge CFDictionaryRef)dropCapDict;

    //create a dictionary of style elements for the main text body
    NSDictionary *textDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                 (__bridge id)textFontRef, kCTFontAttributeName,
                                 self.textColor.CGColor, kCTForegroundColorAttributeName,
                                 style, kCTParagraphStyleAttributeName,
                                 nil];
    //convert it to a CFDictionaryRef
    CFDictionaryRef textAttributes = (__bridge CFDictionaryRef)textDict;

    //clean up, because the dictionaries now have copies
    CFRelease(dropCapFontRef);
    CFRelease(textFontRef);
    CFRelease(style);
    
    //create an attributed string for the dropcap
    CFAttributedStringRef dropCapString = CFAttributedStringCreate(kCFAllocatorDefault,
                                                                   (__bridge CFStringRef)[self.text substringToIndex:1],
                                                                   dropCapAttributes);
    
    //create an attributed string for the text body
    CFAttributedStringRef textString = CFAttributedStringCreate(kCFAllocatorDefault,
                                                                (__bridge CFStringRef)[self.text substringFromIndex:1],
                                                                   textAttributes);
    
    //create an frame setter for the dropcap
    CTFramesetterRef dropCapSetter = CTFramesetterCreateWithAttributedString(dropCapString);

    //create an frame setter for the dropcap
    CTFramesetterRef textSetter = CTFramesetterCreateWithAttributedString(textString);
    
    //clean up
    CFRelease(dropCapString);
    CFRelease(textString);
    
    //get the size of the drop cap letter
    CFRange range;
    CGSize maxSizeConstraint = CGSizeMake(200.0f, 200.0f);
    CGSize dropCapSize = CTFramesetterSuggestFrameSizeWithConstraints(dropCapSetter,
                                                                      CFRangeMake(0, 1),
                                                                      dropCapAttributes,
                                                                      maxSizeConstraint,
                                                                      &range);
    
    CGSize textSize = CTFramesetterSuggestFrameSizeWithConstraints(textSetter,
                                                                   CFRangeMake(0, 0),
                                                                   textAttributes,
                                                                   CGSizeMake(self.bounds.size.width, CGFLOAT_MAX),
                                                                   NULL);

    // because of the styling the size computation above is for a different
    // layout than the actual one (it uses the space that will in reality
    // be carved out for drop cap). We compensate for the conservatively.
    _computedTextHeight = textSize.height + self.dropCapFontSize;
    
    
    //create the path that the main body of text will be drawn into
    //i create the path based on the dropCapSize
    //adjusting to tighten things up (e.g. the *0.8,done by eye)
    //to get some padding around the edges of the screen
    //you could go to +5 (x) and self.frame.size.width -5 (same for height)
    CGMutablePathRef textBox = CGPathCreateMutable();
    CGPathMoveToPoint(textBox, nil, dropCapSize.width, 0);
    CGPathAddLineToPoint(textBox, nil, dropCapSize.width, dropCapSize.height * 0.8); 
    CGPathAddLineToPoint(textBox, nil, 0, dropCapSize.height * 0.8);
    CGPathAddLineToPoint(textBox, nil, 0, self.frame.size.height);
    CGPathAddLineToPoint(textBox, nil, self.frame.size.width, self.frame.size.height);
    CGPathAddLineToPoint(textBox, nil, self.frame.size.width, 0);
    CGPathCloseSubpath(textBox);
    
    //create a transform which will flip the CGContext into the same orientation as the UIView
    CGAffineTransform flipTransform = CGAffineTransformIdentity;
    flipTransform = CGAffineTransformTranslate(flipTransform,
                                               0,
                                               self.bounds.size.height);
    flipTransform = CGAffineTransformScale(flipTransform, 1, -1);
    
    //invert the path for the text box
    CGPathRef invertedTextBox = CGPathCreateCopyByTransformingPath(textBox,
                                                                   &flipTransform);
    CFRelease(textBox);
    
    //create the CTFrame that will hold the main body of text
    CTFrameRef textFrame = CTFramesetterCreateFrame(textSetter,
                                                    CFRangeMake(0, 0),
                                                    invertedTextBox,
                                                    NULL);
    CFRelease(invertedTextBox);
    CFRelease(textSetter);
    
    //create the drop cap text box
    //it is inverted already because we don't have to create an independent cgpathref (like above)
    CGPathRef dropCapTextBox = CGPathCreateWithRect(CGRectMake(self.dropCapKernValue/2.0f,
                                                               0,
                                                               dropCapSize.width,
                                                               dropCapSize.height),
                                                    &flipTransform);
    CTFrameRef dropCapFrame = CTFramesetterCreateFrame(dropCapSetter,
                                                       CFRangeMake(0, 0),
                                                       dropCapTextBox,
                                                       NULL);
    CFRelease(dropCapTextBox);
    CFRelease(dropCapSetter);
    
    //draw the frames into our graphic context
    CGContextRef gc = UIGraphicsGetCurrentContext();
    if (!gc) {
        return;
    }
    
    CGContextSaveGState(gc); {
        CGContextConcatCTM(gc, flipTransform);
        CTFrameDraw(dropCapFrame, gc);
        CTFrameDraw(textFrame, gc);
    } CGContextRestoreGState(gc);
    CFRelease(dropCapFrame);
    CFRelease(textFrame);
}

- (void)redraw {
    // need to call draw rect right away so that any changes
    // in the computed height are reflected to the caller
    // at the end of this synchronous call
    [self drawRect:self.bounds];
    [self setNeedsDisplay];
}

#pragma mark SETTERS
//All of these have [self setNeedsDisplay] to force redrawing when they render
-(void)setText:(NSString *)text {
    _text = [text copy];
    [self redraw];
}

-(void)setTextAlignment:(NSTextAlignment)textAlignment {
    _textAlignment = textAlignment;
    [self redraw];
}

-(void)setDropCapFontSize:(CGFloat)dropCapFontSize {
    _dropCapFontSize = dropCapFontSize;
    [self redraw];
}

-(void)setTextColor:(UIColor *)textColor {
    _textColor = textColor;
    [self redraw];
}

-(void)setTextFontSize:(CGFloat)textFontSize {
    _textFontSize = textFontSize;
    [self redraw];
}

-(void)setFontName:(NSString *)fontName {
    _fontName = fontName;
    [self redraw];
}

-(void)setPath:(UIBezierPath *)path {
    _path = path;
    [self redraw];
}
@end
