//
//  SpeakerIcon.m
//  Briefrr
//
//  Created by Shashank Singh on 3/3/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "SpeakerIcon.h"
#import "Macros.h"
#import <FontAwesomeKit/FontAwesomeKit.h>
#import "AudioWaveView.h"

static const NSUInteger DEFAULT_COLOR = COLOR_CODE_WHITE;

@interface SpeakerIcon()
@property (nonatomic, strong) AudioWaveView *leftWaveView;
@property (nonatomic, strong) AudioWaveView *rightWaveView;
@end

@implementation SpeakerIcon

- (instancetype)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame color:UIColorFromRGB(DEFAULT_COLOR)];
}

- (instancetype)initWithFrame:(CGRect)frame color:(UIColor*)color {
    self = [super initWithFrame:frame];
    if (self) {
        UILabel *label = [[UILabel alloc] initWithFrame:self.bounds];
        label.textAlignment = NSTextAlignmentCenter;
        
        CGFloat fontSize = label.bounds.size.height/3;
        NSString *text = FONT_ICON_CODE_SPEAKER;
        FAKFontAwesome *icon = [FAKFontAwesome iconWithCode:text size:fontSize];
        [icon addAttribute:NSForegroundColorAttributeName value:color];
        label.attributedText = icon.attributedString;
        
        [self addSubview:label];
        
        
        self.leftWaveView = [[AudioWaveView alloc] initWithFrame:self.bounds waveDirection:WAVE_LEFT];
        [self addSubview:self.leftWaveView];
        
        self.rightWaveView = [[AudioWaveView alloc] initWithFrame:self.bounds waveDirection:WAVE_RIGHT];
        [self addSubview:self.rightWaveView];
    }
    return self;
}

- (void)startAnimation {
    [self.leftWaveView startAnimation];
    [self.rightWaveView startAnimation];
}

- (void)stopAnimation {
    [self.leftWaveView stopAnimation];
    [self.rightWaveView stopAnimation];
}
@end