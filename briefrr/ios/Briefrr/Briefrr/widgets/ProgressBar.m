//
//  ProgressBar.m
//  Briefrr
//
//  Created by Shashank Singh on 2/7/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "ProgressBar.h"
#import "Macros.h"

static const UInt32 ANIMATION_NUM_POINTS = 10000;
static const NSTimeInterval MAX_ANIMATION_TIME = 100.0;
static const NSTimeInterval ANIMATION_FINISH_INTERVAL = 1;

static const NSTimeInterval WARNING_COLOR_AFTER_INTERVAL = 5;
static const NSTimeInterval ERROR_COLOR_AFTER_INTERVAL = 10;

static NSString *animationKey = @"progressBarAnimation";

@interface ProgressBar()
@property (nonatomic, strong) UIView *pb;
@property (nonatomic, strong) NSTimer *showDelayTimer;
@property (nonatomic, strong) NSTimer *colorChangeTimer;
@end

@implementation ProgressBar
@synthesize delegate, pb, showDelayTimer, colorChangeTimer;

-(ProgressBar*)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.isInProgress = NO;
        self.showDelayTimer = nil;
        self.colorChangeTimer = nil;
        
        CGRect pbFrame = CGRectMake(0, 0, 0, frame.size.height);
        self.pb = [[UIView alloc] initWithFrame:pbFrame];
        [self addSubview:self.pb];
    }
    return self;
}

-(void)startWithDelay:(NSTimeInterval)initDelay {
    [self resetTimers];
    self.isInProgress = YES;
    self.pb.backgroundColor = UIColorFromRGB(COLOR_CODE_GREEN);
    
    CGRect frame = self.pb.frame;
    frame.size.width = 0;
    self.pb.frame = frame;
    
    self.showDelayTimer = [NSTimer scheduledTimerWithTimeInterval:initDelay
                                                             target:self
                                                           selector:@selector(startAnimation)
                                                           userInfo:nil
                                                            repeats:NO];
    
    NSTimeInterval warningColorAfter = initDelay + WARNING_COLOR_AFTER_INTERVAL;
    self.colorChangeTimer = [NSTimer scheduledTimerWithTimeInterval:warningColorAfter
                                                             target:self
                                                           selector:@selector(changeColorToWarning)
                                                           userInfo:nil
                                                            repeats:NO];
}

-(void)stop:(BOOL)errorOccurred {
    [self resetTimers];
    self.isInProgress = NO;
    
    if (errorOccurred) {
        [self changeColorToError];
    }
    
    // copy animation state into the layer from the presentation layer
    // to avoid snapping back of animation
    CALayer *presentationLayer = (CALayer*)self.pb.layer.presentationLayer;
    CGFloat currentWidth = presentationLayer.bounds.size.width;
    CGFloat currentPositionX = presentationLayer.position.x;
    
    CGRect frame = self.pb.layer.frame;
    frame.size.width = currentWidth;
    self.pb.layer.frame = frame;
    
    CGPoint position = self.pb.layer.position;
    position.x = currentPositionX;
    self.pb.layer.position = position;
    
    [self.pb.layer removeAnimationForKey:animationKey];
}

-(void)resetTimers {
    if (self.showDelayTimer) {
        [self.showDelayTimer invalidate];
        self.showDelayTimer = nil;
    }
    if (self.colorChangeTimer) {
        [self.colorChangeTimer invalidate];
        self.colorChangeTimer = nil;
    }
}

-(void)startAnimation {
    self.showDelayTimer = nil;
    
    CAAnimationGroup *animations = [self createAnimations];
    animations.delegate = self;
    [self.pb.layer addAnimation:animations forKey:animationKey];
}

- (void)animationDidStop:(CAAnimation *)animation finished:(BOOL)finished {
    // quickly rush to the end
    __weak typeof(self) bself = self;
    
    UIViewAnimationOptions options = UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseIn;
    [UIView animateWithDuration:ANIMATION_FINISH_INTERVAL delay:0 options:options
        animations:^{
        if (!bself) {
            return;
        }
        CGRect bounds = bself.pb.layer.bounds;
        bounds.size.width = bself.frame.size.width;
        bself.pb.layer.bounds = bounds;
        
        CGPoint position = bself.pb.layer.position;
        position.x = bounds.size.width/2;
        bself.pb.layer.position = position;
    } completion:^(BOOL finished) {
        if (bself && bself.delegate) {
            [bself.delegate progressBarAnimationFinished];
        }
    }];
}

-(CAAnimationGroup*) createAnimations {
    CAKeyframeAnimation *widthAnimation = [CAKeyframeAnimation animationWithKeyPath:@"bounds.size.width"];
    widthAnimation.removedOnCompletion = NO;
    widthAnimation.fillMode = kCAFillModeForwards;
    
    CAKeyframeAnimation *positionAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position.x"];
    positionAnimation.removedOnCompletion = NO;
    positionAnimation.fillMode = kCAFillModeForwards;
    
    
    CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
    animationGroup.animations = @[widthAnimation, positionAnimation];
    animationGroup.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    animationGroup.duration = MAX_ANIMATION_TIME;
    animationGroup.removedOnCompletion = NO;
    animationGroup.fillMode = kCAFillModeForwards;
    
    NSMutableArray *widthAnimationValues = [NSMutableArray arrayWithCapacity:ANIMATION_NUM_POINTS];
    NSMutableArray *positionAnimationValues = [NSMutableArray arrayWithCapacity:ANIMATION_NUM_POINTS];
    
    CGRect bounds = self.pb.bounds;
    double finalWidth = self.frame.size.width;
    double speed = 0;
    double width = 0;
    double originX = bounds.origin.x;
    
    for (UInt32 i=0; i<ANIMATION_NUM_POINTS; ++i) {
        speed = pow((finalWidth - width), 1.5)/ANIMATION_NUM_POINTS;
        if (i != 0) {
            width += speed;
            originX += speed/2;
        }
        
        bounds.size.width = width;
        bounds.origin.x = originX;
        
        [widthAnimationValues addObject:@(width)];
        [positionAnimationValues addObject:@(originX)];
    }
    
    widthAnimation.values = widthAnimationValues;
    positionAnimation.values = positionAnimationValues;
    
    return animationGroup;
}

-(void)changeColorToWarning {
    self.pb.backgroundColor = UIColorFromRGB(COLOR_CODE_YELLOW);
    
    NSTimeInterval errorColorAfter = ERROR_COLOR_AFTER_INTERVAL - WARNING_COLOR_AFTER_INTERVAL;
    self.colorChangeTimer = [NSTimer scheduledTimerWithTimeInterval:errorColorAfter
                                                             target:self
                                                           selector:@selector(changeColorToError)
                                                           userInfo:nil
                                                            repeats:NO];
}

-(void)changeColorToError {
    self.pb.backgroundColor = UIColorFromRGB(COLOR_CODE_RED);
    self.colorChangeTimer = nil;
}

@end
