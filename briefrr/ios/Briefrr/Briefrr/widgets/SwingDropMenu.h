//
//  SwingDropMenu.h
//  Briefrr
//
//  Created by Shashank Singh on 3/5/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwingDropMenu : UIView
- (instancetype)initWithFrame:(CGRect)frame withMenuButtonLabel:(NSString*)menuButtonLabel;
- (void)addMenuItem:(UIView*)menuItem;
- (void)openMenu;
- (void)closeMenu;
@end
