//
//  ActionView.m
//  Briefrr
//
//  Created by Shashank Singh on 2/15/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "ActionView.h"
#import <pop/POP.h>
#import "ActionViewAnimatorService.h"

@implementation ActionView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [[ActionViewAnimatorService getInstance] addAnimationsOnView:self];
    }
    return self;
}

@end
