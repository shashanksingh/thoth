//
//  FeedReaderControls.m
//  Briefrr
//
//  Created by Shashank Singh on 3/16/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "FeedReaderControls.h"
#import "ActionButton.h"
#import "Macros.h"
#import "FeedReaderService.h"
#import "EventManager.h"
#import "VoiceCommandService.h"

static const CGFloat BUTTON_GAP_WIDTH_FRACTION = 0.4;

@interface FeedReaderControls()
@property (nonatomic, strong) ActionButton *backButton;
@property (nonatomic, strong) ActionButton *playButton;
@property (nonatomic, strong) ActionButton *pauseButton;
@property (nonatomic, strong) ActionButton *nextButton;
@end

@implementation FeedReaderControls
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UIColor *color = UIColorFromRGBA(COLOR_CODE_WHITE, 0.75);
        
        CGFloat size = self.bounds.size.height;
        CGFloat buttonGap = size * BUTTON_GAP_WIDTH_FRACTION;
        
        CGFloat x = 0;
        CGFloat y = 0;
        CGFloat w = size;
        CGFloat h = size;
        
        CGRect frame = CGRectMake(x, y, w, h);
        self.backButton = [[ActionButton alloc] initWithFrame:frame label:FONT_ICON_CODE_MEDIA_BACK color:color];
        [self addSubview:self.backButton];
        
        x += size + buttonGap;
        frame = CGRectMake(x, y, w, h);
        self.playButton = [[ActionButton alloc] initWithFrame:frame label:FONT_ICON_CODE_MEDIA_PLAY color:color];
        [self addSubview:self.playButton];
        self.pauseButton = [[ActionButton alloc] initWithFrame:frame label:FONT_ICON_CODE_MEDIA_PAUSE color:color];
        [self addSubview:self.pauseButton];
        
        x += size + buttonGap;
        frame = CGRectMake(x, y, w, h);
        self.nextButton = [[ActionButton alloc] initWithFrame:frame label:FONT_ICON_CODE_MEDIA_NEXT color:color];
        [self addSubview:self.nextButton];
        
        w = CGRectGetMaxX(frame);
        frame = self.frame;
        frame.size.width = w;
        self.frame = frame;
        
        [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
        [self.playButton addTarget:self action:@selector(onPlayButtonTap) forControlEvents:UIControlEventTouchUpInside];
        [self.pauseButton addTarget:self action:@selector(onPauseButtonTap) forControlEvents:UIControlEventTouchUpInside];
        [self.nextButton addTarget:self action:@selector(onNextButtonTap) forControlEvents:UIControlEventTouchUpInside];
        
        [self updatePlayPauseButtons];
        __weak typeof(self) bself = self;
        [[EventManager getInstance] addEventListener:FEED_READER_READING_STATE_CHANGED withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            [bself updatePlayPauseButtons];
        }];
        [[EventManager getInstance] addEventListener:FEED_READING_MODE_CHANGED withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            [bself updatePlayPauseButtons];
        }];
    }
    return self;
}

- (void)updatePlayPauseButtons {
    BOOL isReading = [[FeedReaderService getInstance] isReading] || [VoiceCommandService getInstance].pausedToListenForCommand;
    UIView *shown = isReading ? self.pauseButton : self.playButton;
    UIView *hidden = isReading ? self.playButton : self.pauseButton;
    
    NSLog(@"isReading: %d", isReading);
    [UIView transitionFromView:hidden toView:shown duration:1 options:UIViewAnimationOptionTransitionCrossDissolve completion:nil];
}

- (void)onBackButtonTap {
    [[EventManager getInstance] fireEvent:NON_VOICE_COMMAND_PREVIOUS_STORY];
}

- (void)onPlayButtonTap {
    [[EventManager getInstance] fireEvent:NON_VOICE_COMMAND_PLAY_FEED];
}

- (void)onPauseButtonTap {
    [[EventManager getInstance] fireEvent:NON_VOICE_COMMAND_PAUSE_FEED];
}

- (void)onNextButtonTap {
    [[EventManager getInstance] fireEvent:NON_VOICE_COMMAND_NEXT_STORY];
}

@end
