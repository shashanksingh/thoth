//
//  AudioWaveView.h
//  Briefrr
//
//  Created by Shashank Singh on 3/1/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, AudioWaveDirection) {
    WAVE_UP = 0,
    WAVE_RIGHT = 1 << 0,
    WAVE_BOTTOM = 1 << 1,
    WAVE_LEFT = 1 << 2
};

@interface AudioWaveView : UIView
- (instancetype)initWithFrame:(CGRect)frame waveDirection:(AudioWaveDirection)direction;
- (void)startAnimation;
- (void)stopAnimation;
@end
