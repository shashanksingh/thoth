//
//  ActionButton.m
//  Briefrr
//
//  Created by Shashank Singh on 2/22/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "ActionButton.h"
#import "Macros.h"
#import <FontAwesomeKit/FontAwesomeKit.h>
#import "THLabel.h"

static const CGFloat FONT_SIZE_BUTTON_SIZE_FRACTION_NO_BORDER = 0.55;
static const CGFloat FONT_SIZE_BUTTON_SIZE_FRACTION_WITH_BORDER = 0.55;
static const NSUInteger BORDER_COLOR = COLOR_CODE_WHITE;
static const CGFloat BORDER_OPACITY = 0.4;
static const NSTimeInterval BORDER_ANIMATION_DURATION = 0.75;
static NSString* const ANIMATION_ID_KEY = @"animationIdKey";
static NSString* const BORDER_DRAW_ANIMATION_ID = @"broderDrawAnimationKey";
static NSString* const BORDER_ERASE_ANIMATION_ID = @"broderEraseAnimationKey";

@interface ActionButton()
@property (nonatomic) BOOL showBorder;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) CAShapeLayer *borderLayer;
@end

@implementation ActionButton

- (instancetype)initWithFrame:(CGRect)frame label:(NSString*)text color:(UIColor*)color showBorder:(BOOL)showBorder {
    self = [super initWithFrame:frame];
    if (self) {
        self.showBorder = showBorder;
        
        self.layer.borderWidth = showBorder ? [UIScreen mainScreen].scale/2 : 0;
        if (showBorder) {
            self.layer.cornerRadius = frame.size.width/2;
            self.layer.borderColor = UIColorFromRGBA(BORDER_COLOR, BORDER_OPACITY).CGColor;
        }
        self.backgroundColor = TRANSPARENT_COLOR;
        
        self.text = text;
        self.textColor = color;
        
        CGRect textFrame = self.bounds;
        THLabel *textLabel = [[THLabel alloc] initWithFrame:textFrame];
        textLabel.strokeColor = UIColorFromRGBA(COLOR_CODE_BLACK, 0.33);
        textLabel.strokeSize = 1.5;
        textLabel.textAlignment = NSTextAlignmentCenter;
        textLabel.attributedText = [[NSAttributedString alloc] initWithString:self.text];
        textLabel.center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
        [self addSubview:textLabel];
        self.textLabel = textLabel;
        
        [self setTextColor:color];
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame label:(NSString*)text color:(UIColor*)color {
    return [self initWithFrame:frame label:text color:color showBorder:NO];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    if (CGRectContainsPoint(self.bounds, point)) {
        return self;
    }
    return [super hitTest:point withEvent:event];
}

- (void)setTextColor:(UIColor*)color {
    _textColor = color;
    self.layer.borderColor = [color colorWithAlphaComponent:BORDER_OPACITY].CGColor;
    
    CGFloat fraction = self.showBorder ? FONT_SIZE_BUTTON_SIZE_FRACTION_WITH_BORDER:FONT_SIZE_BUTTON_SIZE_FRACTION_NO_BORDER;
    CGFloat fontSize = self.bounds.size.height * fraction;
    FAKFontAwesome *icon = [FAKFontAwesome iconWithCode:self.text size:fontSize];
    [icon addAttribute:NSForegroundColorAttributeName value:color];
    self.textLabel.attributedText = icon.attributedString;
}

- (CABasicAnimation*)getStrokeStartAnimation:(NSString*)animationId {
    CABasicAnimation *strokeStartAnim = [CABasicAnimation animationWithKeyPath:@"strokeStart"];
    strokeStartAnim.duration = BORDER_ANIMATION_DURATION;
    strokeStartAnim.autoreverses = NO;
    strokeStartAnim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    strokeStartAnim.delegate = self;
    [strokeStartAnim setValue:animationId forKey:ANIMATION_ID_KEY];
    return strokeStartAnim;
}

- (void)drawBorderWithAnimation:(BOOL)animated {
    if (!self.borderLayer) {
        self.borderLayer = [[CAShapeLayer alloc] init];
        self.borderLayer.lineWidth = 1.0f;
        self.borderLayer.fillColor = TRANSPARENT_COLOR.CGColor;
        self.borderLayer.strokeColor = UIColorFromRGBA(COLOR_CODE_WHITE, 0.5).CGColor;
        [self.layer addSublayer:self.borderLayer];
        
        CGMutablePathRef path = CGPathCreateMutable();
        
        CGPathAddArc(path, NULL, self.bounds.size.width/2, self.bounds.size.height/2, self.bounds.size.width * 0.6, M_PI * 0.0, M_PI *2, true);
        self.borderLayer.path = path;
        CGPathRelease(path);
    }
    
    if (animated) {
        CABasicAnimation *strokeStartAnim = [self getStrokeStartAnimation:BORDER_DRAW_ANIMATION_ID];
        self.borderLayer.strokeStart = 0.0f;
        strokeStartAnim.fromValue = @(1.0f);
        strokeStartAnim.toValue = @(0.0f);
        [self.borderLayer addAnimation:strokeStartAnim forKey:@"borderStrokeStartAnimation"];
    }
}

- (void)eraseBorderWithAnimation:(BOOL)animated {
    if (!self.borderLayer) {
        return;
    }
    
    if (animated) {
        CABasicAnimation *strokeStartAnim = [self getStrokeStartAnimation:BORDER_ERASE_ANIMATION_ID];
        self.borderLayer.strokeStart = 1.0f;
        strokeStartAnim.fromValue = @(0.0f);
        strokeStartAnim.toValue = @(1.0f);
        [self.borderLayer addAnimation:strokeStartAnim forKey:@"borderStrokeStartAnimation"];
    } else {
        self.borderLayer.strokeStart = 1.0f;
        [self setTextColor:UIColorFromRGB(COLOR_CODE_WHITE)];
    }
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    NSString *animationID = [anim valueForKey:ANIMATION_ID_KEY];
    if (!animationID) {
        return;
    }
    /*
    if ([animationID isEqualToString:BORDER_DRAW_ANIMATION_ID]) {
        [self setTextColor:UIColorFromRGBA(COLOR_CODE_YELLOW, 0.45)];
    } else if ([animationID isEqualToString:BORDER_ERASE_ANIMATION_ID]) {
        [self setTextColor:UIColorFromRGB(COLOR_CODE_WHITE)];
    }*/
}

@end
