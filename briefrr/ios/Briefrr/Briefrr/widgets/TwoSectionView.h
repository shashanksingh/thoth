//
//  TwoSectionView.h
//  Briefrr
//
//  Created by Shashank Singh on 2/7/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TwoSectionView : UIView
@property (nonatomic, strong) UIView *topSectionContent;
@property (nonatomic, strong) UIView *bottomSectionContent;

-(TwoSectionView*)initWithFrame:(CGRect)frame hideLogo:(BOOL)hideLogo;
@end
