//
//  MicrophoneIcon.h
//  Briefrr
//
//  Created by Shashank Singh on 3/2/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnimatedViewProtocol.h"

@interface MicrophoneIcon : UIView<AnimatedView>
- (void)startBusyIndicator;
- (void)stopBusyIndicator;
@end
