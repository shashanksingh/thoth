//
//  AudioWaveView.m
//  Briefrr
//
//  Created by Shashank Singh on 3/1/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "AudioWaveView.h"
#import "Macros.h"
#import "EventManager.h"

static const NSTimeInterval WAVE_ANIMATION_INTERVAL = 3.5;
static const int NUM_WAVES = 3;

@interface AudioWaveLayer : CAShapeLayer
- (void)startAnimationWithDelay:(NSTimeInterval)delay;
@end

@interface AudioWaveLayer()
@property (nonatomic) CGFloat size;
@property (nonatomic) AudioWaveDirection waveDirection;
@property (nonatomic, strong) NSTimer *animationDelayTimer;
@end

@implementation AudioWaveLayer
- (instancetype)initWithSize:(CGFloat)size waveDirection:(AudioWaveDirection)direction {
    self = [super init];
    if (self) {
        self.size = size;
        self.waveDirection = direction;
        
        self.lineWidth = 3.0f;
        self.fillColor = TRANSPARENT_COLOR.CGColor;
        self.strokeColor = UIColorFromRGB(COLOR_CODE_WHITE).CGColor;
        self.strokeStart = 0;
        self.strokeEnd = 0;
    }
    return self;
}

- (void)startAnimationWithDelay:(NSTimeInterval)delay {
    if (self.animationDelayTimer) {
        [self.animationDelayTimer invalidate];
        self.animationDelayTimer = nil;
    }
    self.animationDelayTimer = [NSTimer scheduledTimerWithTimeInterval:delay
                                                                target:self
                                                              selector:@selector(startAnimation)
                                                              userInfo:nil
                                                               repeats:NO];
}

- (void)startAnimation {
    CGMutablePathRef path = CGPathCreateMutable();
    CGFloat minAngle = 0;
    CGFloat maxAngle = 0;
    
    switch (self.waveDirection) {
        case WAVE_UP:
            minAngle = 0;
            maxAngle = M_PI;
            break;
        case WAVE_RIGHT:
            minAngle = 0.5 * M_PI;
            maxAngle = 1.5 * M_PI;
            break;
        case WAVE_BOTTOM:
            minAngle = M_PI;
            maxAngle = 2 * M_PI;
            break;
        case WAVE_LEFT:
            minAngle = 1.5 * M_PI;
            maxAngle = 2.5 * M_PI;
            break;
        default:
            break;
    }
    
    CGPathAddArc(path, NULL, 0, 0, self.size/2, minAngle, maxAngle, true);
    
    self.path = path;
    CGPathRelease(path);
    
    CABasicAnimation *strokeStartAnim = [CABasicAnimation animationWithKeyPath:@"strokeStart"];
    strokeStartAnim.fromValue = @(0.45f);
    strokeStartAnim.toValue = @(0.30f);
    
    CABasicAnimation *strokeEndAnim = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    strokeEndAnim.fromValue = @(0.55f);
    strokeEndAnim.toValue = @(0.70f);
    
    CABasicAnimation *moveAnim = [CABasicAnimation animationWithKeyPath:@"position"];
    CGPoint fromPoint = self.position;
    CGPoint toPoint = self.position;
    switch (self.waveDirection) {
        case WAVE_UP:
            fromPoint.y = -self.size * 0.25;
            toPoint.y = -self.size;
            break;
        case WAVE_RIGHT:
            fromPoint.x = self.size * 0.25;
            toPoint.x = self.size;
            break;
        case WAVE_BOTTOM:
            fromPoint.y = -self.size * 0.25;
            toPoint.y = -self.size;
            break;
        case WAVE_LEFT:
            fromPoint.x = self.size * 0.75;
            toPoint.x = 0;
            break;
        default:
            break;
    }
    moveAnim.fromValue = [NSValue valueWithCGPoint:fromPoint];
    moveAnim.toValue = [NSValue valueWithCGPoint:toPoint];
    
    CABasicAnimation *lineWidthAnim = [CABasicAnimation animationWithKeyPath:@"lineWidth"];
    lineWidthAnim.fromValue = [self valueForKey:@"lineWidth"];
    lineWidthAnim.toValue = @(2 * self.lineWidth);
    
    CABasicAnimation *opacityAnim = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacityAnim.fromValue = [NSNumber numberWithFloat:1.0];
    opacityAnim.toValue = [NSNumber numberWithFloat:0.0];
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.duration = WAVE_ANIMATION_INTERVAL;
    group.repeatCount = HUGE_VALF;
    group.autoreverses = NO;
    group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    group.animations = [NSArray arrayWithObjects:strokeStartAnim, strokeEndAnim, moveAnim, lineWidthAnim, opacityAnim, nil];
    [self addAnimation:group forKey:@"waveAnimations"];
}

- (void)stopAnimation {
    [self removeAllAnimations];
}
@end


@interface AudioWaveView()
@property (nonatomic) AudioWaveDirection waveDirection;
@property (nonatomic, strong) NSArray *audioWaveLayers;
@end

@implementation AudioWaveView
- (instancetype)initWithFrame:(CGRect)frame waveDirection:(AudioWaveDirection)direction {
    self = [super initWithFrame:frame];
    if (self) {
        self.waveDirection = direction;
        
        CGFloat waveSize = MIN(self.bounds.size.width, self.bounds.size.height);
        
        NSMutableArray *waveLayers = [NSMutableArray arrayWithCapacity:NUM_WAVES];
        for (NSUInteger i=0; i<NUM_WAVES; ++i) {
            AudioWaveLayer *layer = [[AudioWaveLayer alloc] initWithSize:waveSize waveDirection:direction];
            [self.layer addSublayer:layer];
            // TODO (shashank): these coordinates are arbitrary, need to be relative to
            // the bounds
            layer.position = CGPointMake(60, 50);
            [waveLayers addObject:layer];
        }
        self.audioWaveLayers = waveLayers;
        [self startAnimation];
        
        __weak typeof(self) bself = self;
        [[EventManager getInstance] addEventListener:APP_BECAME_ACTIVE withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            [bself startAnimation];
        }];
        
    }
    return self;
}

- (void)startAnimation {
    for (int i=0; i<self.audioWaveLayers.count; ++i) {
        AudioWaveLayer *layer = self.audioWaveLayers[i];
        [layer startAnimationWithDelay:(WAVE_ANIMATION_INTERVAL/NUM_WAVES) * i];
    }
}

- (void)stopAnimation {
    for (int i=0; i<self.audioWaveLayers.count; ++i) {
        AudioWaveLayer *layer = self.audioWaveLayers[i];
        [layer stopAnimation];
    }
}

@end
