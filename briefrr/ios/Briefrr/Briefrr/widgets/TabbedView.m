//
//  TabbedView.m
//  Briefrr
//
//  Created by Shashank Singh on 2/15/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "TabbedView.h"
#import "Macros.h"
#import "Util.h"

static const CGFloat TAB_BAR_HEIGHT = 50;
static const CGFloat TAB_BAR_TAB_CONTENT_Y_GAP = 16;
static const CGFloat TAB_NAME_FONT_SIZE = 14.0f;
static const NSUInteger UNSELECTED_TAB_COLOR = COLOR_CODE_GREY;
static const CGFloat UNSELECTED_TAB_COLOR_OPACITY = 0.2;
static const NSUInteger TAB_LABEL_COLOR = COLOR_CODE_BLACK;
static const CGFloat UNSELECTED_TAB_LABEL_OPACITY = 0.55;
static const CGFloat TAB_LABEL_UNDERLINE_WIDTH_FRACTION = 1.2;
static const CGFloat TAB_LABEL_UNDERLINE_HEIGHT = 1.5;
static const CGFloat TAB_LABEL_UNDERLINE_GAP = 4;
static const NSUInteger TAB_LABEL_UNDERLINE_COLOR = COLOR_CODE_RED;
static const CGFloat TAB_LABEL_UNDERLINE_OPACITY = 0.6;

@interface TabbedView()
@property (nonatomic, strong) NSMutableArray *tabs;
@property (nonatomic, strong) UIView *tabBar;
@property (nonatomic, strong) UIView *tabContainer;
@property (nonatomic) NSUInteger selectedTabIndex;
@end

@implementation TabbedView
- (instancetype)initWithFrame:(CGRect)frame andTabs:(NSArray*)tabDescriptors {
    self = [super initWithFrame:frame];
    if (self) {
        self.tabs = [NSMutableArray array];
        CGRect tabBarFrame = CGRectMake(0, 0, self.bounds.size.width, TAB_BAR_HEIGHT);
        self.tabBar = [[UIView alloc] initWithFrame:tabBarFrame];
        [self addSubview:self.tabBar];
        
        UITapGestureRecognizer *tapRecognizer =
            [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTabHeaderTap:)];
        [self.tabBar addGestureRecognizer:tapRecognizer];
        
        CGFloat w = self.bounds.size.width;
        CGFloat y = self.tabBar.bounds.size.height + TAB_BAR_TAB_CONTENT_Y_GAP;
        CGFloat h = self.bounds.size.height - y;
        CGRect tabContainerFrame = CGRectMake(0, y, w, h);
        self.tabContainer = [[UIView alloc] initWithFrame:tabContainerFrame];
        [self addSubview:self.tabContainer];
        
        self.tabs = [NSMutableArray arrayWithArray:tabDescriptors];
        [self drawTabs];
    }
    return self;
}

- (NSDictionary*)getSelectedTabDescriptor {
    return self.tabs[self.selectedTabIndex];
}

- (void)addTab:(UIView*)tab withName:(NSString*)name {
    [self.tabs addObject:@{@"name": name, @"view": tab}];
}

- (void)drawTabs {
    NSUInteger numTabs = self.tabs.count;
    if (numTabs == 0) {
        return;
    }
    
    CGFloat tabHeaderWidth = self.tabBar.bounds.size.width/numTabs;
    for (NSUInteger i=0; i<numTabs; ++i) {
        NSDictionary *tabDescriptor = self.tabs[i];
        
        CGRect frame = CGRectMake(tabHeaderWidth * i, 0, tabHeaderWidth, self.tabBar.bounds.size.height);
        UIView *tabHeader = [[UIView alloc] initWithFrame:frame];
        [self.tabBar addSubview:tabHeader];
        
        UILabel *tabNameLabel = [[UILabel alloc] initWithFrame:tabHeader.bounds];
        tabNameLabel.textAlignment = NSTextAlignmentCenter;
        tabNameLabel.text = tabDescriptor[@"name"];
        tabNameLabel.font = [UIFont fontWithName:FONT_NAME_REGULAR size:TAB_NAME_FONT_SIZE];
        tabNameLabel.textColor = UIColorFromRGB(TAB_LABEL_COLOR);
        tabNameLabel.tag = TAG_TAB_HEADER_LABEL;
        [tabHeader addSubview:tabNameLabel];
        
        CGFloat w = tabNameLabel.intrinsicContentSize.width * TAB_LABEL_UNDERLINE_WIDTH_FRACTION;
        CGFloat h = TAB_LABEL_UNDERLINE_HEIGHT;
        CGFloat x = CGRectGetMidX(tabNameLabel.frame) - w/2;
        CGFloat y = CGRectGetMidY(tabNameLabel.frame) + tabNameLabel.intrinsicContentSize.height/2 + TAB_LABEL_UNDERLINE_GAP;
        frame = CGRectMake(x, y, w, h);
        UIView *tabNameUnderline = [[UIView alloc] initWithFrame:frame];
        tabNameUnderline.tag = TAG_TAB_HEADER_LABEL_UNDERLINE;
        tabNameUnderline.hidden = YES;
        tabNameUnderline.backgroundColor = UIColorFromRGBA(TAB_LABEL_UNDERLINE_COLOR, TAB_LABEL_UNDERLINE_OPACITY);
        [tabHeader addSubview:tabNameUnderline];
        
    }
    
    self.selectedTabIndex = 0;
    [self updateSelectedTab];
}

- (void)handleTabHeaderTap:(UITapGestureRecognizer *)recognizer {
    CGPoint loc = [recognizer locationInView:self.tabBar];
    UIView* tabHeader = [self.tabBar hitTest:loc withEvent:nil];
    NSUInteger tabIndex = [self.tabBar.subviews indexOfObject:tabHeader];
    self.selectedTabIndex = tabIndex;
    [self updateSelectedTab];
}

- (void)updateSelectedTab {
    NSDictionary *selectedTabDescriptor = self.tabs[self.selectedTabIndex];
    
    [Util emptyUIView:self.tabContainer];
    UIView *tab = selectedTabDescriptor[@"view"];
    tab.frame = self.tabContainer.bounds;
    [self.tabContainer addSubview:tab];
    
    UIColor *borderColor = UIColorFromRGB(0xDDDDDD);
    for (NSUInteger i=0; i<self.tabBar.subviews.count; ++i) {
        UIView *tabHeader = self.tabBar.subviews[i];
        UIView *labelView = [tabHeader viewWithTag:TAG_TAB_HEADER_LABEL];
        if (![labelView isKindOfClass:[UILabel class]]) {
            // TODO (shashank): log error
            //continue;
        }
        UILabel *label = (UILabel*)labelView;
        
        if (i == self.selectedTabIndex) {
            tabHeader.backgroundColor = self.backgroundColor;
            [Util addBorderToUIView:tabHeader onSides:BOX_LEFT | BOX_RIGHT ofColor:borderColor andThickness:1];
            label.textColor = UIColorFromRGB(TAB_LABEL_COLOR);
            [tabHeader viewWithTag:TAG_TAB_HEADER_LABEL_UNDERLINE].hidden = NO;
        } else {
            tabHeader.backgroundColor = UIColorFromRGBA(UNSELECTED_TAB_COLOR, UNSELECTED_TAB_COLOR_OPACITY);
            [Util addBorderToUIView:tabHeader onSides:BOX_BOTTOM ofColor:borderColor andThickness:1];
            label.textColor = UIColorFromRGBA(TAB_LABEL_COLOR, UNSELECTED_TAB_LABEL_OPACITY);
            [tabHeader viewWithTag:TAG_TAB_HEADER_LABEL_UNDERLINE].hidden = YES;
        }
    }
}
@end
