//
//  TopAppBanner.m
//  Briefrr
//
//  Created by Shashank Singh on 2/15/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "TopAppBanner.h"
#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "Macros.h"
#import "ProceedButton.h"
#import "Util.h"
#import "EventManager.h"
#import "UserPreferenceService.h"
#import "TabSwitchMenu.h"
#import "SourcesService.h"

static NSString* const TOP_BANNER_BACKGROUND_IMG = @"assets/images/banner_bg.jpg";
static const CGFloat APP_LOGO_Y_OFFSET = 5;
static const CGFloat TOP_BANNER_BACKGROUND_IMG_CROP_Y = 200;
static const CGFloat APP_LOGO_FONT_SIZE = 52.0f;
static const CGFloat ACTION_BUTTON_WIDTH = 250;
static const CGFloat ACTION_BUTTON_HEIGHT_FRACTION = 0.65;

static const NSTimeInterval VIEW_SWITCH_ANIMATION_DURATION = 0.3f;

@interface TopAppBanner()
@property (nonatomic, strong) UILabel *appLogoLabel;
@property (nonatomic, strong) ProceedButton *actionButton;
@end

@implementation TopAppBanner
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        ASImageNode *backgroundImage = [[ASImageNode alloc] init];
        
        UIImage *originalImage = [UIImage imageNamed:TOP_BANNER_BACKGROUND_IMG];
        CGRect croppedFrame = (CGRect) {CGPointMake(0, TOP_BANNER_BACKGROUND_IMG_CROP_Y), self.bounds.size};
        CGImageRef imageRef = CGImageCreateWithImageInRect(originalImage.CGImage, croppedFrame);
        
        backgroundImage.image = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        backgroundImage.frame = self.bounds;
        [self addSubview:backgroundImage.view];
        
        CGRect appLogoFrame = self.bounds;
        appLogoFrame.origin.y = APP_LOGO_Y_OFFSET;
        
        self.appLogoLabel = [[UILabel alloc] initWithFrame:appLogoFrame];
        self.appLogoLabel.textAlignment = NSTextAlignmentCenter;
        self.appLogoLabel.text = @"b";
        self.appLogoLabel.font = [UIFont fontWithName:LOGO_FONT_NAME size:APP_LOGO_FONT_SIZE];
        self.appLogoLabel.textColor = UIColorFromRGB(COLOR_CODE_MESSAGE);
        [self addSubview:self.appLogoLabel];
        
        __weak typeof(self) bself = self;
        [[EventManager getInstance] addEventListener:SOURCE_SELECTION_CHANGED withCallback:^(va_list args) {
            [bself updateOnSourceSelectionChange];
        }];
        [self updateOnSourceSelectionChange];
    }
    return self;
}

- (void)switchToActionView:(NSString*)actionText {
    if (!self.actionButton) {
        CGFloat w = ACTION_BUTTON_WIDTH;
        CGFloat h = self.bounds.size.height * ACTION_BUTTON_HEIGHT_FRACTION;
        CGRect actionButtonFrame = CGRectMake(0, 0, w, h);
        UIColor *backgroundColor = UIColorFromRGBA(COLOR_CODE_ACTION_BUTTON, 0.5);
        self.actionButton = [[ProceedButton alloc] initWithFrame:actionButtonFrame label:actionText backgroundColor:backgroundColor];
        self.actionButton.center = self.center;
        
        [Util addOrAppendTapHandlerToView:self.actionButton target:self action:@selector(onActionButtonTap:)];
    }
    
    [UIView transitionFromView:self.appLogoLabel
                        toView:self.actionButton duration:VIEW_SWITCH_ANIMATION_DURATION
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished) {
                    }];
}

- (void)switchToLogoView {
    [UIView transitionFromView:self.actionButton
                        toView:self.appLogoLabel duration:VIEW_SWITCH_ANIMATION_DURATION
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished) {
                    }];
}

- (void)onActionButtonTap:(UIGestureRecognizer*)recognizer {
    [[EventManager getInstance] fireEvent:SOURCE_SELECTION_DONE];
}

- (void)updateOnSourceSelectionChange {
    if ([[SourcesService getInstance] anySourceOrTopicSelected]) {
        [self switchToActionView:@"I'm done selecting sources"];
    } else {
        [self switchToLogoView];
    }
}
@end
