//
//  ActionButton.m
//  Briefrr
//
//  Created by Shashank Singh on 2/9/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "ProceedButton.h"
#import "Macros.h"

static const CGFloat ARROW_LABEL_WIDTH = 20;
static const CGFloat ARROW_LABEL_HEIGHT = 20;


@implementation ProceedButton
- (instancetype)initWithFrame:(CGRect)frame label:(NSString*)text backgroundColor:(UIColor*)backgroundColor {
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = frame.size.height * 0.5;
        self.layer.masksToBounds = YES;
        self.backgroundColor = backgroundColor;
        
        CGRect textFrame = self.bounds;
        UILabel *textLabel = [[UILabel alloc] initWithFrame:textFrame];
        textLabel.textColor = UIColorFromRGB(COLOR_CODE_WHITE);
        textLabel.font = [UIFont fontWithName:FONT_NAME_SEMI_BOLD size:15];
        textLabel.textAlignment = NSTextAlignmentCenter;
        textLabel.text = text;
        [self addSubview:textLabel];

        
        CGFloat x = self.bounds.size.width - ARROW_LABEL_WIDTH - self.layer.cornerRadius/2 + 5;
        CGFloat y = self.bounds.size.height/2 - ARROW_LABEL_HEIGHT/2;
        CGRect frame = CGRectMake(x, y, ARROW_LABEL_WIDTH, ARROW_LABEL_HEIGHT);
        UILabel *arrowLabel = [[UILabel alloc] initWithFrame:frame];
        arrowLabel.textColor = UIColorFromRGB(COLOR_CODE_WHITE);
        arrowLabel.text = @"\u276D";
        [self addSubview:arrowLabel];
    }
    return self;
}
@end
