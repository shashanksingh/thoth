//
//  SpeakerIcon.h
//  Briefrr
//
//  Created by Shashank Singh on 3/3/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpeakerIcon : UIView
- (void)startAnimation;
- (void)stopAnimation;
@end
