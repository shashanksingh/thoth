//
//  MicrophoneIcon.m
//  Briefrr
//
//  Created by Shashank Singh on 3/2/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "MicrophoneIcon.h"
#import "Macros.h"
#import <FontAwesomeKit/FontAwesomeKit.h>
#import "Util.h"
#import "VCRService.h"

static const NSUInteger DEFAULT_COLOR = COLOR_CODE_WHITE;
static const CGFloat AUDIO_LEVEL_VIEW_MIN_SCALE_DELTA = 0.01;
static const CGFloat AUDIO_LEVEL_VIEW_MIN_RANDOM_SCALE_DELTA_FACTOR = 0.5;
static const NSTimeInterval AUDIO_LEVEL_VIEW_MIN_UPDATE_INTERVAL = 0.4;
static const CGFloat AUDIO_LEVEL_VIEW_MIN_SCALE = 1.45;
static const CGFloat AUDIO_LEVEL_VIEW_MAX_SCALE = 3;

static const CGFloat BUSY_INDICATOR_ANGLE = M_PI/4;
static const NSTimeInterval BUSY_ANIMATION_INTERVAL = 1.0f;

@interface BusyIndicatorLayer : CAShapeLayer
@property (nonatomic) CGFloat radius;
- (instancetype)initWithRadius:(CGFloat)radius;
- (void)startAnimation;
- (void)stopAnimation;
@end

@implementation BusyIndicatorLayer
- (instancetype)initWithRadius:(CGFloat)radius {
    self = [super init];
    if (self) {
        self.radius = radius;
        
        self.lineWidth = 2.0f;
        self.fillColor = TRANSPARENT_COLOR.CGColor;
        self.strokeColor = UIColorFromRGBA(COLOR_CODE_YELLOW, 0.85).CGColor;
    }
    return self;
}

- (void)startAnimation {
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddArc(path, NULL, self.radius, self.radius, self.radius, 0, BUSY_INDICATOR_ANGLE, false);
    self.path = path;
    CGPathRelease(path);
    
    // Note (shashank): I don't understand these settings at this time
    // and they might not be optimal but since this works, leaving it
    // like this.
    self.bounds = CGRectMake(0, 0, self.radius * 2, self.radius * 2);
    self.position = CGPointMake(self.radius, self.radius);
    self.anchorPoint = CGPointMake(0.5, 0.5);
    
    
    CABasicAnimation *rotationAnim = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnim.fromValue = @(0);
    rotationAnim.toValue = @(2 * M_PI);
    rotationAnim.duration = BUSY_ANIMATION_INTERVAL;
    rotationAnim.repeatCount = HUGE_VALF;
    rotationAnim.autoreverses = NO;
    rotationAnim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    [self addAnimation:rotationAnim forKey:@"busyAnimations"];
}

- (void)stopAnimation {
    [self removeAllAnimations];
}

@end


@interface AudioInputLevelView : UIView
@property (nonatomic, strong) BusyIndicatorLayer *busyIndicator;
@property (nonatomic, strong) CADisplayLink *timer;
@property (nonatomic, assign) NSTimeInterval lastUpdateTimestamp;
@property (nonatomic) double lastKnownAudioLevel;

- (void)startBusyIndicator;
- (void)stopBusyIndicator;
@end


@implementation AudioInputLevelView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.busyIndicator = [[BusyIndicatorLayer alloc] initWithRadius:self.frame.size.width/2];
        [self.layer addSublayer:self.busyIndicator];
    }
    return self;
}

- (void)tick:(CADisplayLink *)sender {
    CGFloat elapsedTime = sender.timestamp  - self.lastUpdateTimestamp;
    if (elapsedTime < AUDIO_LEVEL_VIEW_MIN_UPDATE_INTERVAL) {
        return;
    }
    self.lastUpdateTimestamp = sender ? sender.timestamp : 0;
    self.lastKnownAudioLevel = [[VCRService sharedInstance] getInputGainLevel];
    
    CGFloat currentScale = self.transform.a;
    CGFloat nextScale = AUDIO_LEVEL_VIEW_MIN_SCALE + self.lastKnownAudioLevel;
    if (fabs(currentScale - nextScale) < AUDIO_LEVEL_VIEW_MIN_SCALE_DELTA) {
        double rangeMin = MAX(AUDIO_LEVEL_VIEW_MIN_SCALE, currentScale - AUDIO_LEVEL_VIEW_MIN_SCALE_DELTA);
        double rangeMax = MIN(AUDIO_LEVEL_VIEW_MAX_SCALE, currentScale + AUDIO_LEVEL_VIEW_MIN_SCALE_DELTA);
        nextScale = [Util generateRandomDoubleInRangeWithMin:rangeMin andMax:rangeMax];
        if (fabs(currentScale - nextScale) < AUDIO_LEVEL_VIEW_MIN_SCALE_DELTA * AUDIO_LEVEL_VIEW_MIN_RANDOM_SCALE_DELTA_FACTOR) {
            return;
        }
    }
    
    [self.layer removeAllAnimations];
    __weak typeof(self) bself = self;
    [UIView animateWithDuration:AUDIO_LEVEL_VIEW_MIN_UPDATE_INTERVAL
                          delay:0
                          options:UIViewAnimationOptionBeginFromCurrentState
                          animations:(void (^)(void)) ^{
                              if (!bself) {
                                  return;
                              }
                              bself.transform = CGAffineTransformMakeScale(nextScale, nextScale);
                          }
                          completion:nil];
}

- (void)startAnimation {
    self.timer = [CADisplayLink displayLinkWithTarget:self selector:@selector(tick:)];
    [self.timer addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
}

- (void)stopAnimation {
    if (!self.timer) {
        return;
    }
    [self.timer removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    self.timer = nil;
}

- (void)startBusyIndicator {
    self.busyIndicator.hidden = NO;
    [self.busyIndicator startAnimation];
}

- (void)stopBusyIndicator {
    [self.busyIndicator stopAnimation];
    self.busyIndicator.hidden = YES;
}

@end

@interface MicrophoneIcon()
@property (nonatomic, strong) AudioInputLevelView *micLevelView;
@end

@implementation MicrophoneIcon

- (instancetype)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame color:UIColorFromRGB(DEFAULT_COLOR)];
}

- (instancetype)initWithFrame:(CGRect)frame color:(UIColor*)color {
    self = [super initWithFrame:frame];
    if (self) {
        UILabel *label = [[UILabel alloc] initWithFrame:self.bounds];
        label.textAlignment = NSTextAlignmentCenter;
        
        CGFloat fontSize = label.bounds.size.height;
        NSString *text = FONT_ICON_CODE_MIC;
        FAKIonIcons *icon = [FAKIonIcons iconWithCode:text size:fontSize];
        [icon addAttribute:NSForegroundColorAttributeName value:color];
        label.attributedText = icon.attributedString;
        
        [self addSubview:label];
        
        self.micLevelView = [[AudioInputLevelView alloc] initWithFrame:self.bounds];
        self.micLevelView.layer.cornerRadius = self.micLevelView.bounds.size.width/2;
        self.micLevelView.backgroundColor = UIColorFromRGBA(COLOR_CODE_RED, 0.4);
        self.micLevelView.transform = CGAffineTransformMakeScale(AUDIO_LEVEL_VIEW_MIN_SCALE, AUDIO_LEVEL_VIEW_MIN_SCALE);
        [self insertSubview:self.micLevelView belowSubview:label];
    }
    return self;
}

- (void)startAnimation {
    [self.micLevelView startAnimation];
}

- (void)stopAnimation {
    [self.micLevelView stopAnimation];
}

- (void)startBusyIndicator {
    [self.micLevelView startBusyIndicator];
}

- (void)stopBusyIndicator {
    [self.micLevelView stopBusyIndicator];
}

@end
