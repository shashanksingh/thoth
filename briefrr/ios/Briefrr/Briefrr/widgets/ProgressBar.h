//
//  ProgressBar.h
//  Briefrr
//
//  Created by Shashank Singh on 2/7/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProgressBarDelegate <NSObject>
-(void)progressBarAnimationFinished;
@end

@interface ProgressBar : UIView
@property (nonatomic, weak) id<ProgressBarDelegate> delegate;
@property (nonatomic) BOOL isInProgress;

-(void)startWithDelay:(NSTimeInterval)initDelay;
-(void)stop:(BOOL)errorOccurred;
@end
