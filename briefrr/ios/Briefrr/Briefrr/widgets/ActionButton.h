//
//  ActionButton.h
//  Briefrr
//
//  Created by Shashank Singh on 2/22/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "ActionView.h"

@interface ActionButton : ActionView
@property (nonatomic, strong) UIColor *textColor;
- (instancetype)initWithFrame:(CGRect)frame label:(NSString*)text color:(UIColor*)color;
- (instancetype)initWithFrame:(CGRect)frame label:(NSString*)text color:(UIColor*)color showBorder:(BOOL)showBorder;
- (void)drawBorderWithAnimation:(BOOL)animated;
- (void)eraseBorderWithAnimation:(BOOL)animated;
@end
