//
//  SwingDropMenu.m
//  Briefrr
//
//  Created by Shashank Singh on 3/5/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "SwingDropMenu.h"
#import "ActionButton.h"
#import "Macros.h"
#import <pop/POP.h>
#import "Util.h"
#import "EventManager.h"

static const NSUInteger MENU_BUTTON_COLOR = COLOR_CODE_WHITE;
static const CGFloat ROPE_HEIGHT = 34;
static const CGFloat ROPE_WIDTH = 1;

@interface SwingDropMenu()
@property (nonatomic) BOOL isOpen;
@property (nonatomic) BOOL isSwinging;
@property (nonatomic, strong) ActionButton *menuButton;
@property (nonatomic, strong) NSMutableArray *menuItems;
@property (nonatomic, strong) UIView *menuDropDown;
@end

@implementation SwingDropMenu

- (instancetype)initWithFrame:(CGRect)frame withMenuButtonLabel:(NSString*)menuButtonLabel {
    self = [super initWithFrame:frame];
    if (self) {
        self.isOpen = NO;
        self.menuItems = [NSMutableArray array];
        
        UIColor *buttonColor = UIColorFromRGB(MENU_BUTTON_COLOR);
        self.menuButton = [[ActionButton alloc] initWithFrame:self.bounds label:menuButtonLabel color:buttonColor];
        [self addSubview:self.menuButton];
        [self.menuButton addTarget:self action:@selector(toggleMenu) forControlEvents:UIControlEventTouchUpInside];
        
        CGRect frame = CGRectMake(0, self.menuButton.frame.size.height, 0, 0);
        self.menuDropDown = [[UIView alloc] initWithFrame:frame];
        [self addSubview:self.menuDropDown];
        
        __weak typeof(self) bself = self;
        [[EventManager getInstance] addEventListener:DEVICE_MOVED_SIGNIFICANTLY withCallback:^(va_list args) {
            if (!bself || !bself.isOpen) {
                return;
            }
            int yawChangeDirection __attribute__((unused)) = ((NSNumber*)va_arg(args, NSNumber*)).intValue;
            int pitchChangeDirection __attribute__((unused)) = ((NSNumber*)va_arg(args, NSNumber*)).intValue;
            int rollChangeDirection = ((NSNumber*)va_arg(args, NSNumber*)).intValue;
            
            [bself swingDropDown:rollChangeDirection > 0 withSubtleSwing:YES];
        }];
    }
    return self;
}

- (void)addMenuItem:(UIView*)menuItem {
    [self.menuItems addObject:menuItem];
}

- (void)toggleMenu {
    self.isOpen = !self.isOpen;
    self.isSwinging = NO;
    if (self.isOpen) {
        [self openMenu];
    } else {
        [self closeMenu];
    }
}

- (void)openMenu {
    CGFloat currentY = 0;
    CGFloat maxWidth = 0.0;
    
    for (int i=0; i<self.menuItems.count; ++i) {
        CGRect ropeFrame = CGRectMake(self.bounds.size.width/2 - ROPE_WIDTH/2, currentY, ROPE_WIDTH, 1);
        UIView *rope = [[UIView alloc] initWithFrame:ropeFrame];
        rope.layer.borderWidth = 3;
        rope.layer.borderColor = TRANSPARENT_COLOR.CGColor;
        rope.layer.shouldRasterize = YES;
        rope.backgroundColor = UIColorFromRGB(COLOR_CODE_WHITE);
        [self.menuDropDown addSubview:rope];
        
        POPSpringAnimation *ropeDropAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
        CGRect finalFrame = CGRectMake(ropeFrame.origin.x, ropeFrame.origin.y, ROPE_WIDTH, ROPE_HEIGHT);
        ropeDropAnimation.toValue = [NSValue valueWithCGRect:finalFrame];
        ropeDropAnimation.springBounciness = 18.0f;
        [rope pop_addAnimation:ropeDropAnimation forKey:@"ropeDropAnimation"];
        
        UIView *menuItem = self.menuItems[i];
        menuItem.alpha = 1.0;
        menuItem.frame = CGRectMake(0, currentY, menuItem.frame.size.width, menuItem.frame.size.height);
        [self.menuDropDown addSubview:menuItem];
        
        CGFloat menuItemFinalY = currentY + ROPE_HEIGHT;
        POPSpringAnimation *menuItemDropAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
        finalFrame = CGRectMake(self.bounds.size.width/2 - menuItem.frame.size.width/2, menuItemFinalY, menuItem.frame.size.width, menuItem.frame.size.height);
        menuItemDropAnimation.toValue = [NSValue valueWithCGRect:finalFrame];
        menuItemDropAnimation.springBounciness = 18.0f;
        [menuItem pop_addAnimation:menuItemDropAnimation forKey:@"menuItemDropAnimation"];
        
        currentY += ROPE_HEIGHT + menuItem.frame.size.height;
        maxWidth = MAX(menuItem.bounds.size.width, MAX(rope.bounds.size.width, maxWidth));
    }
    
    // the subview frames are still being animated, can't use resizeViewToFitSubviews
    CGRect menuDropDownFrame = CGRectMake(0, self.menuButton.frame.size.height, maxWidth, currentY);
    self.menuDropDown.frame = menuDropDownFrame;
    
    // resizeViewToFitSubviews is not recursive for performance
    // we need to call this one explicitly
    [Util resizeViewToFitSubviews:self];
    
    [self swingDropDown:YES withSubtleSwing:NO];
}

- (void)closeMenu {
    for (UIView *subView in self.menuDropDown.subviews) {
        POPBasicAnimation *dropDownRollUpAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionY];
        dropDownRollUpAnimation.toValue = @(self.menuButton.bounds.size.height);
        dropDownRollUpAnimation.completionBlock = ^(POPAnimation *anim, BOOL finished) {
            [subView removeFromSuperview];
        };
        [subView pop_addAnimation:dropDownRollUpAnimation forKey:@"dropDownRollUpAnimation"];
        
        POPBasicAnimation *fadeOutAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
        fadeOutAnimation.toValue = @(0);
        [subView pop_addAnimation:fadeOutAnimation forKey:@"dropDownFadeOutAnimation"];
    }
    
    self.menuDropDown.frame = CGRectZero;
    [Util resizeViewToFitSubviews:self];
}

- (void)swingDropDown:(BOOL)swingRight withSubtleSwing:(BOOL)subtleSwing {
    if (self.isSwinging) {
        return;
    }
    self.isSwinging = YES;
    self.layer.shouldRasterize = YES;
    
    // setting the anchor point for rotation to middle-top
    // re-store frame to make sure that doesn't cause a
    // translation
    CGRect menuDropDownFrame = self.menuDropDown.frame;
    self.menuDropDown.layer.anchorPoint = CGPointMake(0.5, 0.0);
    self.menuDropDown.frame = menuDropDownFrame;
    __weak typeof(self) bself = self;
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        CGFloat rotation = M_PI * (subtleSwing ? 0.02 : 0.03);
        if (swingRight) {
            rotation *= -1;
        }
        bself.menuDropDown.layer.transform = CATransform3DRotate(CATransform3DIdentity, rotation, 0, 0, 1);
    } completion:^(BOOL finished) {
        POPSpringAnimation *rotationAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerRotation];
        
        rotationAnimation.toValue = @(0);
        rotationAnimation.springSpeed = (subtleSwing ? 1: 2);
        rotationAnimation.springBounciness = (subtleSwing ? 10 : 20);
        rotationAnimation.dynamicsMass = (subtleSwing ? 1 : 2);
        
        rotationAnimation.completionBlock = ^(POPAnimation *anim, BOOL finished) {
            bself.isSwinging = NO;
            bself.layer.shouldRasterize = NO;
        };
        [bself.menuDropDown.layer pop_addAnimation:rotationAnimation forKey:@"menuDropDownRotationAnimation"];
    }];
    
    
}

@end
