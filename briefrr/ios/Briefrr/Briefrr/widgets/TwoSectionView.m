//
//  TwoSectionView.m
//  Briefrr
//
//  Created by Shashank Singh on 2/7/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "TwoSectionView.h"
#import "ASImageNode.h"
#import "Macros.h"
#import "UIImageEffects.h"
#import "Util.h"

static const CGFloat TOP_SECTION_FRACTION = 0.7;
static const CGFloat LOGO_HEIGHT_FRACTION = 0.45;
static const CGFloat LOGO_FONT_SIZE = 140.0f;

@interface TwoSectionView()
@property (nonatomic, strong) UIView *topSection;
@property (nonatomic, strong) UIView *bottomSection;
@end

@implementation TwoSectionView
@synthesize topSectionContent, bottomSectionContent, topSection, bottomSection;

-(TwoSectionView*)initWithFrame:(CGRect)frame hideLogo:(BOOL)hideLogo {
    self = [super initWithFrame:frame];
    if (self) {
        [self addTopSectionAndSkipLogo:hideLogo];
        [self addBottomSection];
    }
    return self;
}

-(TwoSectionView*)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame hideLogo:NO];
}

-(void)addTopSectionAndSkipLogo:(BOOL)skipLogo {
    CGRect frame = self.frame;
    frame.size.height *= TOP_SECTION_FRACTION;
    self.topSection = [[UIView alloc] initWithFrame:frame];
    [self addSubview:self.topSection];
    
    
    CGFloat sectionW = self.topSection.frame.size.width;
    CGFloat sectionH = self.topSection.frame.size.height;
    
    // add the background image
    UIImageView *backgroundImage = [[UIImageView alloc] init];
    backgroundImage.image = [UIImage imageNamed:@"assets/images/banner_bg.jpg"];
    backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImage.frame = CGRectMake(0, 0, sectionW, sectionH);
    [self.topSection addSubview:backgroundImage];
    [Util addDeviceTiltEffectToView:backgroundImage withMaximumMovement:40];
    
    CGFloat w, h, y;
    if (!skipLogo) {
        // add the logo
        w = sectionW;
        h = sectionH * LOGO_HEIGHT_FRACTION;
        y = h/2 - LOGO_FONT_SIZE/2;
        frame = CGRectMake(0, y, w, h);
        UILabel *logoLabel = [[UILabel alloc] initWithFrame:frame];
        logoLabel.textAlignment = NSTextAlignmentCenter;
        logoLabel.text = @"b";
        logoLabel.font = [UIFont fontWithName:LOGO_FONT_NAME size:LOGO_FONT_SIZE];
        logoLabel.textColor = UIColorFromRGB(COLOR_CODE_LOGO);
        [self.topSection addSubview:logoLabel];
    }
    
    w = sectionW;
    h = sectionH * (1 - LOGO_HEIGHT_FRACTION);
    y = sectionH * LOGO_HEIGHT_FRACTION;
    frame = CGRectMake(0, y, w, h);
    self.topSectionContent = [[UIView alloc] initWithFrame:frame];
    [self.topSection addSubview:self.topSectionContent];
    
}

-(void)addBottomSection {
    CGRect frame = self.frame;
    frame.origin.y = frame.size.height * TOP_SECTION_FRACTION;
    frame.size.height = frame.size.height * (1 - TOP_SECTION_FRACTION);
    self.bottomSection = [[UIView alloc] initWithFrame:frame];
    [self addSubview:self.bottomSection];
    
    self.bottomSectionContent = [[UIView alloc] initWithFrame:self.bottomSection.bounds];
    [self.bottomSection addSubview:self.bottomSectionContent];
}
@end
