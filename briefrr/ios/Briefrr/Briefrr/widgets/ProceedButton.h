//
//  ActionButton.h
//  Briefrr
//
//  Created by Shashank Singh on 2/9/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "ActionView.h"

@interface ProceedButton : ActionView
- (instancetype)initWithFrame:(CGRect)frame label:(NSString*)text backgroundColor:(UIColor*)backgroundColor;
@end
