//
//  RootViewController.h
//  Briefrr
//
//  Created by Shashank Singh on 2/8/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"

@interface RootViewController : UIViewController
- (BOOL)isCurrentViewFeedOfType:(FeedType)feedType;
- (BOOL)isCurrentViewSourceSelector;
@end
