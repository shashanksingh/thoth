//
//  ViewSlideTransitionAnimator.h
//  Briefrr
//
//  Created by Shashank Singh on 2/8/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ViewSlideTransitionAnimator : NSObject<UIViewControllerAnimatedTransitioning>

@end
