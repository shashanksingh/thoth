//
//  EventManager.m
//  Briefrr
//
//  Created by Shashank Singh on 2/8/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "EventManager.h"
#import "Util.h"
#import "EventLogger.h"

@interface EventManager()
@property (strong) NSMutableDictionary *eventTypeToListenerIDs;
@property (strong) NSMutableDictionary *listenerIDToCallback;
@end

@implementation EventManager
@synthesize eventTypeToListenerIDs;
@synthesize listenerIDToCallback;

- (id)init {
    self = [super init];
    if(self) {
        self.eventTypeToListenerIDs = [NSMutableDictionary dictionary];
        self.listenerIDToCallback = [NSMutableDictionary dictionary];
    }
    return self;
}

+ (EventManager*)getInstance {
    static EventManager *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[EventManager alloc] init];
    });
    return sharedInstance;
}

- (NSMutableSet*)getListenerIDs:(EventType)eventType {
    NSNumber *eventKey = [NSNumber numberWithInt:eventType];
    
    NSMutableSet *listenerIDs = [self.eventTypeToListenerIDs objectForKey:eventKey];
    if(!listenerIDs) {
        listenerIDs = [NSMutableSet set];
        [self.eventTypeToListenerIDs setObject:listenerIDs forKey:eventKey];
    }
    return listenerIDs;
}


- (NSString*)addEventListener:(EventType)eventType withCallback:(VarargsCallback)callback {
    return [self addEventListener:eventType oneShot:NO withCallback:callback];
}

- (NSString*)addEventListener:(EventType)eventType oneShot:(BOOL)oneShot withCallback:(VarargsCallback)callback {
    NSMutableSet *listenerIDs = [self getListenerIDs:eventType];
    
    NSString *listenerID = [Util createUUID];
    
    __weak typeof(self) bself = self;
    __block VarargsCallback wrappedCallback = ^(va_list args) {
        if(oneShot) {
            [bself removeEventListener:listenerID];
        }
        callback(args);
    };
    [wrappedCallback copy];
    
    [listenerIDs addObject:listenerID];
    [self.listenerIDToCallback setObject:wrappedCallback forKey:listenerID];
    
    return listenerID;
}

- (void)removeEventListener:(NSString*)listenerID {
    if(!listenerID) {
        return;
    }
    
    [self.listenerIDToCallback removeObjectForKey:listenerID];
    
    for(NSNumber *eventKey in self.eventTypeToListenerIDs) {
        NSMutableSet *listenerIDs = [self.eventTypeToListenerIDs objectForKey:eventKey];
        [listenerIDs removeObject:listenerID];
    }
}

- (void)fireEvent:(EventType)eventType,... {
    if (![NSThread isMainThread]) {
        [[EventLogger getInstance] logEvent:@"error_event_fired_off_main_thread" properties:@{@"event": @(eventType)}];
    }
    
    NSSet *listenerIDs = [self getListenerIDs:eventType];
    
    // copy callbacks to avoid mutated while iterating error
    // fire once callbacks mutate the callback registry
    NSMutableArray *callbacksToCall = [NSMutableArray array];
    for(NSString *listenerID in listenerIDs) {
        VarargsCallback callback = [self.listenerIDToCallback objectForKey:listenerID];
        [callbacksToCall addObject:callback];
    }
    
    for(VarargsCallback callback in callbacksToCall) {
        va_list vl;
        va_start(vl, eventType);
        va_end(vl);
        callback(vl);
    }
}

@end
