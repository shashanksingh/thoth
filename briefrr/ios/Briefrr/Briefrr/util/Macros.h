//
//  Macros.h
//  Briefrr
//
//  Created by Shashank Singh on 2/1/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#ifndef Briefrr_Macros_h
#define Briefrr_Macros_h

#define APP_NAME @"Briefrr"
#define APP_UNIQUE_ID @"com.briefrr.Briefrr"
#define ERROR_DOMAIN @"com.briefrr"
#define SERVER_BASE_URL @"http://briefrr.com:8080/Backend/"
#define SUPPORT_EMAIL @"support@briefrr.com"


#define LOGO_FONT_NAME @"Airstream"
#define FONT_NAME_LIGHT @"OpenSans-Light"
#define FONT_NAME_SEMI_BOLD @"Opensans-SemiBold"
#define FONT_NAME_REGULAR @"OpenSans"
#define FONT_NAME_LIGHT_2 @"OpenSans-Light"
#define FONT_NAME_REGULAR_2 @"Avenir-Roman"
#define FONT_NAME_ITALIC @"Avenir-MediumOblique"

#define COLOR_CODE_BLACK 0x000000
#define COLOR_CODE_WHITE 0xFFFFFF
#define COLOR_CODE_LOGO 0xF7F7F7
#define COLOR_CODE_GREEN 0x2E8B57
#define COLOR_CODE_YELLOW 0xF4C430
#define COLOR_CODE_RED 0xCF1020
#define COLOR_CODE_LIGHT_RED 0xB7410E
#define COLOR_CODE_GREY 0x666666
#define COLOR_CODE_LIGHT_GREY 0xCECECE
#define COLOR_CODE_MESSAGE 0xCECECE
#define COLOR_CODE_ERROR_MESSAGE 0xE34234
#define COLOR_CODE_SUCCESS 0x228B22
#define COLOR_CODE_ACTION_BUTTON 0xE34234
#define COLOR_CODE_PARAGRAPH 0x464646

#define UIColorFromRGB(rgbValue) \
    [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
    green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
    blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
    alpha:1.0]

#define UIColorFromRGBA(rgbValue, a) \
    [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
    green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
    blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

#define TRANSPARENT_COLOR [UIColor clearColor]

#define FONT_ICON_CODE_EYE @"\uf06e"
#define FONT_ICON_CODE_FACEBOOK @"\uf09a"
#define FONT_ICON_CODE_BOOKMARK @"\uf097"
#define FONT_ICON_CODE_BULLHORN @"\uf0a1"
#define FONT_ICON_CODE_NAV @"\uf0c9"
#define FONT_ICON_CODE_SPEAKER @"\uf111"
#define FONT_ICON_CODE_MIC @"\uf460"
#define FONT_ICON_CODE_NEWSFEED @"\uf1ea"
#define FONT_ICON_CODE_SETTINGS @"\uf013"
#define FONT_ICON_CODE_INFO @"\uf05a"
#define FONT_ICON_CODE_MEDIA_BACK @"\uf04a"
#define FONT_ICON_CODE_MEDIA_PLAY @"\uf04b"
#define FONT_ICON_CODE_MEDIA_PAUSE @"\uf04c"
#define FONT_ICON_CODE_MEDIA_NEXT @"\uf04e"

#define UNICODE_CHECK_MARK @"\u2713"

#define ACTION_BUTTON_SIZE 36

#define FEED_PAGE_SIZE 300

#define HOUR_MAX_MORNING 12
#define HOUR_MAX_AFTERNOON 17

#define HELP_MESSAGE_COMMAND_INTRO @"You can say Briefer<vtml_pause time=\"500\"/> pause <vtml_pause time=\"400\"/> to pause at any time. You can also say Briefer<vtml_pause time=\"500\"/> save!<vtml_pause time=\"400\"/> to save a story to read it later."

#define HELP_MESSAGE_START_COMMAND @"say <b>briefer start</b> to begin"
#define HELP_MESSAGE_PAUSE_COMMAND @"say <b>briefer pause</b> to pause"
#define HELP_MESSAGE_CONTINUE_COMMAND @"say <b>briefer continue</b> to resume"

#define COMMAND_FEEDBACK_NEXT_STORY @"Skipping to the next story."
#define COMMAND_FEEDBACK_PREVIOUS_STORY @"Going back to the previous story."
#define COMMAND_FEEDBACK_SAVE_STORY @"Story saved for later reading."
#define COMMAND_FEEDBACK_FIRST_SAVE_STORY @"Story saved for later reading. You can find the stories you save in the saved stories section of Briefer."

#define MESSAGE_GREETING_MORNING @"Good Morning %{first_name}!"
#define MESSAGE_GREETING_AFTERNOON @"Good Afternoon %{first_name}!"
#define MESSAGE_GREETING_EVENING @"Good Evening %{first_name}!"

#define MESSAGE_APP_SLOGAN @"Your news, on the go!"
#define MESSAGE_APP_LOADING_ERROR @"Uh oh! Something went wrong. We are working on fixing it. Please check back in a bit."
#define MESSAGE_TRY_AGAIN @"try again"
#define MESSAGE_EMPTY_NEWS_FEED @"Sorry! We don't have any stories for the news sources you have selected. Would you mind selecting some more sources?"
#define MESSAGE_EMPTY_SAVED_STORIES_FEED @"You don't have any saved stories. Save interesting stories from your news feed so that you can read them here later."

#define MESSAGE_SOURCE_SELECTION_INTRO_MAJOR @"What do you like?"
#define MESSAGE_SOURCE_SELECTION_INTRO_MINOR @"Tell Briefrr about the topics and news publishers you like."
#define MESSAGE_SOURCE_SELECTION_INTRO_ACTION @"Select Sources"

#define MESSAGE_NOTIFICATION_PERMISSION_MAJOR @"Never miss your news again!"
#define MESSAGE_NOTIFICATION_PERMISSION_MINOR @"Briefrr can remind you to get your news on time. Would you like that?"
#define MESSAGE_NOTIFICATION_PERMISSION_ACTION @"Notify me"
#define MESSAGE_NOTIFICATION_PERMISSION_SKIP @"not now"

#define MESSAGE_NOTIFICATION_FEED_READY @"Your personalized news bulletin is ready."

#define MESSAGE_PREAMBLE_NEWS_FEED_START @"Here is your latest news on Briefer."

#define COMMAND_START_READER @"START"
#define COMMAND_PAUSE_READER @"PAUSE"
#define COMMAND_STOP_READER @"STOP"
#define COMMAND_CONTINUE_READER @"CONTINUE"
#define COMMAND_NEXT_STORY @"NEXT"
#define COMMAND_PREVIOUS_STORY @"BACK"
#define COMMAND_SAVE_STORY @"SAVE"
#define COMMAND_SKIP_STORY @"SKIP"

#define radiansToDegrees(x) (180/M_PI)*x
#define ARC4RANDOM_MAX 0x100000000

typedef void(^VarargsCallback)(va_list);
typedef void(^GenericCallback)(id data);
typedef void(^GenericErrorback)(NSError *error);
typedef void(^ArrayCallback)(NSArray *data);
typedef void(^BooleanCallback)(BOOL booleanValue);
typedef void(^VoidCallback)();

typedef NS_OPTIONS(NSUInteger, BoxSide) {
    BOX_TOP = 0,
    BOX_RIGHT   = 1 << 0,
    BOX_BOTTOM  = 1 << 1,
    BOX_LEFT = 1 << 2
};

typedef NS_OPTIONS(NSUInteger, ViewTag) {
    // random large number to avoid conflicts with any other lib
    TAG_TAB_HEADER_LABEL_BASE = 454106772,
    TAG_TAB_HEADER_LABEL = TAG_TAB_HEADER_LABEL_BASE + 1,
    TAG_TAB_HEADER_LABEL_UNDERLINE = TAG_TAB_HEADER_LABEL_BASE + 2
};

typedef NS_OPTIONS(NSUInteger, FeedReadingMode) {
    AUDIO = 0,
    VISUAL = 1
};

typedef NS_OPTIONS(NSUInteger, FeedReaderState) {
    SPEAKING = 0,
    LISTENING = 1
};

typedef NS_OPTIONS(NSUInteger, FeedType) {
    NEWS_FEED = 0,
    SAVED_STORIES = 1 << 0
};

typedef NS_OPTIONS(NSUInteger, Direction) {
    NONE = 0,
    TOP = 1 << 0,
    RIGHT = 1 << 1,
    BOTTOM = 1 << 2,
    LEFT = 1 << 3
};

#define NUM_BYTES_PER_SAMPLE 2

#endif
