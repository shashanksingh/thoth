//
//  Util.h
//  Briefrr
//
//  Created by Shashank Singh on 2/1/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import <pop/POP.h>
#import "Macros.h"

@interface Util : NSObject
+ (dispatch_queue_t)getAsyncUIBackgroundQueue;
+ (NSString *)createUUID;
+ (NSArray *)map:(NSArray *) array WithLambda:(id(^)(id))lambda;
+ (BOOL)anyInArray:(NSArray*)array satisfyingPredicate:(NSNumber*(^)(id))lambda;
+ (void)emptyUIView:(UIView*)view;
+ (void)emptyDisplayNode:(ASDisplayNode*)node;
+ (void)addBorderToUIView:(UIView*)view onSides:(BoxSide)sides ofColor:(UIColor*)color andThickness:(CGFloat)thickness;
+ (void)addOrAppendTapHandlerToView:(UIView*)view target:(id)target action:(SEL)action;
+ (void)addDeviceTiltEffectToView:(UIView*)view withMaximumMovement:(CGFloat)maxMovement;
+ (void)openAppSystemSettings;
+ (NSArray*)safeSubArray:(NSArray*)array offset:(NSUInteger)offset limit:(NSUInteger)limit;
+ (BOOL)isRemoteURL:(NSString*)url;
+ (void)animatedSlideIn:(UIView*)view fromCenterX:(CGFloat)fromX toCenterX:(CGFloat)toX;
+ (UIViewController*)getRootViewController;
+ (NSString*)capitalizeFirstLetter:(NSString*)string;
+ (void)applyBoldToAttributedString:(NSMutableAttributedString*)attributedString usingBoldFont:(UIFont*)boldFont;
+ (double)generateRandomDoubleInRangeWithMin:(double)rangeMin andMax:(double)rangeMax;
+ (id)nsNullToNil:(id)value;
+ (void)resizeViewToFitSubviews:(UIView*)view;
+ (NSString*)trimString:(NSString*)string;
+ (NSString*)stripHTMLTagsFromString:(NSString*)string;
+ (NSString*)getGreetingForCurrentTime;
+ (BOOL)isNotificationPermissionGranted;
+ (UIImage*)addShadowToUIImage:(UIImage*)image;
+ (void)fadeAndSlide:(UIView*)view fadeIn:(BOOL)fadeIn slideInDirection:(Direction)slideInDirection animationKey:(NSString*)animationKey duration:(NSTimeInterval)duration onCompletion:(BooleanCallback)onCompletion;
+ (NSString*)arryaToJSONString:(NSArray*)array;
+ (NSString*)formatString:(NSString*)string withParameters:(NSDictionary*)params;
+ (NSURL*)urlFromString:(NSString*)string;
@end
