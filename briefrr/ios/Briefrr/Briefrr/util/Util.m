//
//  Util.m
//  Briefrr
//
//  Created by Shashank Singh on 2/1/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "Util.h"
#import <pop/POP.h>
#import <CoreGraphics/CGImage.h>

@interface UITiltMotionEffectGroup : UIMotionEffectGroup
@end
@implementation UITiltMotionEffectGroup
@end

@implementation Util
+ (dispatch_queue_t)getAsyncUIBackgroundQueue {
    static dispatch_once_t queueCreationGuard;
    static dispatch_queue_t queue;
    dispatch_once(&queueCreationGuard, ^{
        queue = dispatch_queue_create("com.briefrr.async_ui_background_queue", DISPATCH_QUEUE_SERIAL);
    });
    return queue;
}

+ (NSString *)createUUID {
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidStr = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
    CFRelease(uuid);
    
    return uuidStr;
}

+ (NSArray *)map:(NSArray *) array WithLambda:(id(^)(id))lambda {
    NSMutableArray *rv = [[NSMutableArray alloc] initWithCapacity: [array count]];
    for(id element in array) {
        [rv addObject:lambda(element)];
    }
    return rv;
}

+ (BOOL)anyInArray:(NSArray*)array satisfyingPredicate:(NSNumber*(^)(id))lambda {
    for(id element in array) {
        if (lambda(element).boolValue) {
            return YES;
        }
    }
    return NO;
}

+ (void)emptyUIView:(UIView*)view {
    NSArray *viewsToRemove = [NSArray arrayWithArray:view.subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
}

+ (void)emptyDisplayNode:(ASDisplayNode*)node {
    NSArray *nodesToRemove = [NSArray arrayWithArray:node.subnodes];
    for (ASDisplayNode *n in nodesToRemove) {
        [n removeFromSupernode];
    }
}


+ (void)addBorderToUIView:(UIView*)view onSides:(BoxSide)sides ofColor:(UIColor*)color andThickness:(CGFloat)thickness {
    NSArray *sideNames = @[@(BOX_TOP), @(BOX_RIGHT), @(BOX_BOTTOM), @(BOX_LEFT)];
    NSString *borderLayerName = [APP_NAME stringByAppendingString:@"_box__border"];
    
    NSArray *sublayersToRemove = [NSArray arrayWithArray:view.layer.sublayers];
    for (CALayer *sublayer in sublayersToRemove) {
        if ([sublayer.name isEqualToString:borderLayerName]) {
            [sublayer removeFromSuperlayer];
        }
    }
    
    for (NSNumber *sideNameNum in sideNames) {
        BoxSide sideName = sideNameNum.intValue;
        if (!(sides & sideName)) {
            continue;
        }
        
        CGFloat w = CGRectGetWidth(view.bounds);
        CGFloat h = CGRectGetHeight(view.bounds);
        
        CALayer *border = [CALayer layer];
        border.backgroundColor = color.CGColor;
        if (sideName == BOX_TOP) {
            border.frame = CGRectMake(0, 0, w, thickness);
        } else if (sideName == BOX_RIGHT) {
            border.frame = CGRectMake(w - thickness, 0, thickness, h);
        } else if (sideName == BOX_BOTTOM) {
            border.frame = CGRectMake(0, h - thickness, w, thickness);
        } else {
            border.frame = CGRectMake(0, 0, thickness, h);
        }
        border.name = borderLayerName;
        [view.layer addSublayer:border];
    }
}

+ (void)addDeviceTiltEffectToView:(UIView*)view withMaximumMovement:(CGFloat)maxMovement {
    if (maxMovement < 0) {
        maxMovement = INFINITY;
    }
    // remove any existing instance of the motion effect
    for (UIMotionEffect *effect in view.motionEffects) {
        if ([effect isKindOfClass:[UITiltMotionEffectGroup class]]) {
            [view removeMotionEffect:effect];
            break;
        }
    }
    
    CGFloat size = MIN(maxMovement, view.bounds.size.width);
    
    UIInterpolatingMotionEffect *horizontalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect.minimumRelativeValue = @(-size/2);
    horizontalMotionEffect.maximumRelativeValue = @(size/2);
    
    UITiltMotionEffectGroup *group = [UITiltMotionEffectGroup new];
    group.motionEffects = @[horizontalMotionEffect];
    [view addMotionEffect:group];
}

+ (void)addOrAppendTapHandlerToView:(UIView*)view target:(id)target action:(SEL)action {
    // Hack (shashank): uiview can't have two gesture recognizers of the
    // same type. since ActionViewAnimatorService adds its own tap gesture
    // recognizer we are forced to re-use that as we can't add our own here
    for (UIGestureRecognizer *recognizer in view.gestureRecognizers) {
        if ([recognizer isKindOfClass:[UITapGestureRecognizer class]]) {
            [recognizer addTarget:target action:action];
            return;
        }
    }
    
    UITapGestureRecognizer *tapRecognizer =
        [[UITapGestureRecognizer alloc] initWithTarget:target action:action];
    [view addGestureRecognizer:tapRecognizer];
}

+ (void)openAppSystemSettings {
    // Note (shashank): iOS8
    if (&UIApplicationOpenSettingsURLString != NULL) {
        NSURL *appSettings = [Util urlFromString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:appSettings];
    } else {
        // TODO (shashank): log warning
    }
}

+ (NSArray*)safeSubArray:(NSArray*)array offset:(NSUInteger)offset limit:(NSUInteger)limit {
    if (!array) {
        return @[];
    }
    offset = MAX(0, offset);
    offset = MIN(array.count - 1, offset);
    limit = MAX(0, limit);
    limit = MIN(array.count - offset, limit);
    return [array subarrayWithRange:NSMakeRange(offset, limit)];
}

+ (BOOL)isRemoteURL:(NSString*)url {
    return [url hasPrefix:@"http://"] || [url hasPrefix:@"https://"];
}

+ (void)animatedSlideIn:(UIView*)view fromCenterX:(CGFloat)fromX toCenterX:(CGFloat)toX {
    view.center = CGPointMake(fromX, view.center.y);
    
    POPSpringAnimation *springAnimation = [POPSpringAnimation animation];
    springAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewCenter];
    
    CGPoint fromValue = CGPointMake(fromX, view.center.y);
    CGPoint toValue = CGPointMake(toX, view.center.y);
    
    springAnimation.fromValue = [NSValue valueWithCGPoint:fromValue];
    springAnimation.toValue = [NSValue valueWithCGPoint:toValue];
    
    springAnimation.velocity = [NSValue valueWithCGPoint:CGPointMake(10, 10)];
    springAnimation.springBounciness = 10;
    springAnimation.springSpeed = 5;
    
    __weak typeof(self) bself = self;
    springAnimation.completionBlock = ^(POPAnimation *anim, BOOL finished) {
        if (!finished) {
            return;
        }
        if (!bself) {
            return;
        }
        [Util addDeviceTiltEffectToView:view withMaximumMovement:20];
    };
    
    [view pop_addAnimation:springAnimation forKey:@"center"];
}

+ (UIViewController*)getRootViewController {
    return [UIApplication sharedApplication].delegate.window.rootViewController;
}

+ (NSString*)capitalizeFirstLetter:(NSString*)string {
    if (string.length <= 0) {
        return string;
    }
    return [string stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[string substringToIndex:1] capitalizedString]];
}

+ (void)applyBoldToAttributedString:(NSMutableAttributedString*)attributedString usingBoldFont:(UIFont*)boldFont {
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:@"<b>(.*?)</b>"
                                  options:NSRegularExpressionCaseInsensitive
                                  error:&error];
    if (error) {
        // TODO (shashank): log error
        return;
    }

    NSRange fullRange = NSMakeRange(0, attributedString.string.length);
    NSArray *matches = [regex matchesInString:attributedString.string options:0 range:fullRange];
    
    for (NSTextCheckingResult *match in [matches reverseObjectEnumerator]) {
        NSRange rangeToReplace = [match rangeAtIndex:0];
        NSString *replacement = [attributedString.string substringWithRange:[match rangeAtIndex:1]];
        NSRange rangeInReplacedString = NSMakeRange(rangeToReplace.location, replacement.length);
        
        [attributedString replaceCharactersInRange:rangeToReplace withString:replacement];
        [attributedString addAttribute:NSFontAttributeName
                      value:boldFont
                      range:rangeInReplacedString];
    }
}

+ (double)generateRandomDoubleInRangeWithMin:(double)rangeMin andMax:(double)rangeMax {
    return ((double)arc4random() / ARC4RANDOM_MAX) * (rangeMax - rangeMin) + rangeMin;
}

+ (id)nsNullToNil:(id)value {
    if (value == (id)[NSNull null]) {
        return nil;
    }
    return value;
}

+ (void)resizeViewToFitSubviews:(UIView*)view {
    float w = 0;
    float h = 0;
    
    for (UIView *v in [view subviews]) {
        float fw = v.frame.origin.x + v.frame.size.width;
        float fh = v.frame.origin.y + v.frame.size.height;
        w = MAX(fw, w);
        h = MAX(fh, h);
    }
    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, w, h)];
}

+ (NSString*)trimString:(NSString*)string {
    return [string stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
}

+ (NSString*)stripHTMLTagsFromString:(NSString*)string {
    NSRange r;
    NSString *rv = [string copy];
    while ((r = [rv rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound) {
        rv = [rv stringByReplacingCharactersInRange:r withString:@""];
    }
    return rv;
}

+ (NSString*)getGreetingForCurrentTime {
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitHour fromDate:now];
    
    NSInteger hour = [components hour];
    if (hour < HOUR_MAX_MORNING) {
        return MESSAGE_GREETING_MORNING;
    }
    if (hour < HOUR_MAX_AFTERNOON) {
        return MESSAGE_GREETING_AFTERNOON;
    }
    return MESSAGE_GREETING_EVENING;
}

+ (BOOL)isNotificationPermissionGranted {
    UIUserNotificationSettings *currentSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
    return currentSettings.types != UIUserNotificationTypeNone;
}

+ (UIImage*)addShadowToUIImage:(UIImage*)image {
    CGSize size = [image size];
    UIGraphicsBeginImageContext(size);
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    [image drawInRect:rect blendMode:kCGBlendModeNormal alpha:1.0];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(context, 1, 1, 1, 1);
    CGContextSetLineWidth(context, 6);
    CGContextStrokeRect(context, rect);
    UIImage *rv =  UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return rv;
}

+ (Direction)getOppositeDirection:(Direction)direction {
    switch (direction) {
        case TOP:
            return BOTTOM;
        case RIGHT:
            return LEFT;
        case BOTTOM:
            return TOP;
        case LEFT:
            return RIGHT;
        default:
            break;
    }
    return direction;
}

+ (void)fadeAndSlide:(UIView*)view fadeIn:(BOOL)fadeIn slideInDirection:(Direction)slideInDirection animationKey:(NSString*)animationKey duration:(NSTimeInterval)duration onCompletion:(BooleanCallback)onCompletion {
    
    NSString *fadeAnimationKey = [animationKey stringByAppendingString:@"_fade"];
    NSString *slideAnimationKey = [animationKey stringByAppendingString:@"_slide"];
    
    POPBasicAnimation *fadeAnim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    fadeAnim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    fadeAnim.fromValue = @(view.alpha);
    fadeAnim.toValue = @(fadeIn ? 1 : 0);
    fadeAnim.duration = duration;
    
    [view pop_removeAnimationForKey:fadeAnimationKey];
    if (onCompletion) {
        fadeAnim.completionBlock = ^(POPAnimation *anim, BOOL finished) {
            onCompletion(finished);
        };
    }
    [view pop_addAnimation:fadeAnim forKey:fadeAnimationKey];
    
    if (slideInDirection == NONE) {
        return;
    }
    
    // this makes sure that if an animation is removed while still in progress
    // we finish its effect
    POPBasicAnimation *currentAnimation = [view pop_animationForKey:slideAnimationKey];
    if (currentAnimation) {
        view.center = ((NSValue*)currentAnimation.toValue).CGPointValue;
    }
    [view pop_removeAnimationForKey:slideAnimationKey];
    
    CGFloat slideAmount = 20;
    CGPoint currentCenter = view.center;
    CGPoint finalCenter  = view.center;
    CGPoint restoreCenter = view.center;
    
    switch (slideInDirection) {
        case TOP:
            if (fadeIn) {
                currentCenter.y -= slideAmount;
            } else {
                finalCenter.y -= slideAmount;
            }
            break;
        case RIGHT:
            if (fadeIn) {
                currentCenter.x += slideAmount;
            } else {
                finalCenter.x += slideAmount;
            }
            break;
        case BOTTOM:
            if (fadeIn) {
                currentCenter.y += slideAmount;
            } else {
                finalCenter.y += slideAmount;
            }
            break;
        case LEFT:
            if (fadeIn) {
                currentCenter.x -= slideAmount;
            } else {
                finalCenter.x -= slideAmount;
            }
            break;
        default:
            break;
    }
    
    POPBasicAnimation *slideAnim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewCenter];
    slideAnim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    slideAnim.fromValue = [NSValue valueWithCGPoint:currentCenter];
    slideAnim.toValue = [NSValue valueWithCGPoint:finalCenter];
    slideAnim.duration = duration;
    
    // both animations have the same duration so calling the callback
    // from one of the animations is good enough as both will always
    // finish or get cancelled together
    slideAnim.completionBlock = ^(POPAnimation *anim, BOOL finished) {
        view.userInteractionEnabled = YES;
        view.center = restoreCenter;
    };
    view.userInteractionEnabled = NO;
    [view pop_addAnimation:slideAnim forKey:slideAnimationKey];
}

+ (NSString*)arryaToJSONString:(NSArray*)array {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:nil error:&error];
    if (error) {
        return nil;
    }
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

+ (NSString*)formatString:(NSString*)string withParameters:(NSDictionary*)params {
    for (NSString *key in params) {
        NSString *pattern = [NSString stringWithFormat:@"%%{%@}", key];
        string = [string stringByReplacingOccurrencesOfString:pattern withString:params[key]];
    }
    return string;
}

+ (NSURL*)urlFromString:(NSString*)string {
    return [NSURL URLWithString:[string stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
}

@end
