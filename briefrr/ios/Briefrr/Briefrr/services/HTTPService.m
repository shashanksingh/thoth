//
//  HTTPService.m
//  Briefrr
//
//  Created by Shashank Singh on 2/8/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "HTTPService.h"
#import "Source.h"
#import "Util.h"
#import "Macros.h"
#import "UserPreferenceService.h"
#import "EventLogger.h"

static NSString * const PATH_SOURCES = @"Sources.jsp";
static NSString * const PATH_FEED = @"UserFeed.jsp";
static NSString * const PATH_MARK_READ = @"SetSeen.jsp";

@implementation HTTPService

- (HTTPService*)init {
    self = [super initWithBaseURL:[Util urlFromString:SERVER_BASE_URL]];
    if (self) {
        
    }
    return self;
}

+ (HTTPService*)getInstance {
    static HTTPService *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[HTTPService alloc] init];
    });
    return sharedInstance;
}

- (void)handleResponseValidationError:(NSString*)message withErrorback:(GenericErrorback)errorback {
    // TODO (shashank) log error
    NSError *error = [NSError errorWithDomain:ERROR_DOMAIN code:-1 userInfo:@{@"message": message}];
    errorback(error);
}

- (NSString*)getUserId {
    // TODO (shashank): user correct userid once backend starts supporting it.
    NSString *userId = [[UserPreferenceService getInstance] getUserId];
    return userId;
}

- (void)getAllSourcesWithCallback:(ArrayCallback)callback withErrorback:(GenericErrorback)errorback {
    [self GET:PATH_SOURCES parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        if (![responseObject isKindOfClass:[NSArray class]]) {
            NSString *message = [NSString stringWithFormat:@"response for sources API expects and array as response got: %@", responseObject];
            [self handleResponseValidationError:message withErrorback:errorback];
            return;
        }
        
        NSArray *sources = [Util map:(NSArray*)responseObject WithLambda:^id(NSDictionary *sourceDescriptor) {
            return [[Source alloc] initWithJSONDescriptor:sourceDescriptor];
        }];
        callback(sources);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        errorback(error);
    }];
}

- (void)getFeedForSources:(NSArray*)sourceIds offset:(NSUInteger)offset limit:(NSUInteger)limit callback:(ArrayCallback)callback errorback:(GenericErrorback)errorback {
    
    NSDictionary *params = @{@"user": [self getUserId], @"sources": [Util arryaToJSONString:sourceIds], @"from": @(offset), @"max": @(limit)};
    
    [self GET:PATH_FEED parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if (![responseObject isKindOfClass:[NSArray class]]) {
            NSString *message = [NSString stringWithFormat:@"response for feed API expects and array as response got: %@",responseObject];
            [self handleResponseValidationError:message withErrorback:errorback];
            return;
        }
        
        NSArray *stories = [Util map:(NSArray*)responseObject WithLambda:^id(NSDictionary *storyDescriptor) {
            return [[Story alloc] initWithJSONDescriptor:storyDescriptor];
        }];
        callback(stories);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        errorback(error);
    }];
}

- (void)markStoryRead:(Story*)story {
    NSDictionary *params = @{@"user": [self getUserId], @"guids":[Util arryaToJSONString:@[story.guid]]};
    [self POST:PATH_MARK_READ parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [[EventLogger getInstance] logEvent:@"mark_story_read_success" properties:@{@"story_guid": story.guid}];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        // TODO (shashank): log
        [[EventLogger getInstance] logEvent:@"mark_story_read_failed" properties:@{@"story_guid": story.guid, @"error": error}];
    }];
}
@end
