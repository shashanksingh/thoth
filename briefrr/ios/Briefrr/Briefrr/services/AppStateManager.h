//
//  AppStateManager.h
//  Briefrr
//
//  Created by Shashank Singh on 3/25/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"

@interface AppStateManager : NSObject
@property (nonatomic) FeedReadingMode feedReadingMode;
@property (nonatomic) BOOL initalFeedLoadDone;
@property (nonatomic) NSInteger currentStoryIndex;
+ (AppStateManager*)getInstance;
@end
