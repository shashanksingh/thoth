//
//  VoiceCommandService.h
//  Briefrr
//
//  Created by Shashank Singh on 3/11/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VoiceCommandService : NSObject
@property (nonatomic) BOOL pausedToListenForCommand;
+ (VoiceCommandService*)getInstance;
- (void)startRecognition;
- (void)pauseRecognition;
@end
