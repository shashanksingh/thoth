//
//  SourcesService.m
//  Briefrr
//
//  Created by Shashank Singh on 2/8/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "SourcesService.h"
#import "UserPreferenceService.h"
#import "HTTPService.h"
#import "EventManager.h"
#import "Util.h"

@interface SourcesService()
@property (nonatomic, strong) NSArray *publishers;
@property (nonatomic, strong) NSArray *topics;
@property (nonatomic, strong) NSDictionary *sourceIdToSource;
@end

@implementation SourcesService

-(SourcesService*)init {
    self = [super init];
    if (self) {
        self.publishers = nil;
        self.topics = nil;
        self.sourceIdToSource = nil;
        
        __weak typeof(self) bself = self;
        [[EventManager getInstance] addEventListener:SOURCE_SELECTION_CHANGED withCallback:^(va_list args) {
            Source *changedSource = va_arg(args, Source*);
            [bself onSourceSelectionChange:changedSource];
        }];
    }
    return self;
}

+ (SourcesService*)getInstance {
    static SourcesService *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SourcesService alloc] init];
    });
    return sharedInstance;
}

- (Source*)getSourceById:(NSString*)sourceId {
    return self.sourceIdToSource ? self.sourceIdToSource[sourceId] : nil;
}

- (NSArray*)getSelectedPublishers {
    return [[UserPreferenceService getInstance] getSelectedPublishers];
}

- (NSArray*)getSelectedTopics {
    return [[UserPreferenceService getInstance] getSelectedTopics];
}

- (BOOL)isAnySourceSelected {
    return [self getSelectedPublishers].count > 0 || [self getSelectedTopics].count > 0;
}

- (NSArray*)getSelectedSourceIds {
    return [[self getSelectedPublishers] arrayByAddingObjectsFromArray:[self getSelectedTopics]];
}

- (NSArray*)getAllPublishers {
    if (!self.publishers) {
        // TODO (shashank): log error
        return @[];
    }
    return self.publishers;
}

- (NSArray*)getAllTopics {
    if (!self.topics) {
        // TODO (shashank): log error
        return @[];
    }
    return self.topics;
}

-(void) updateSourcesWithCallback:(VoidCallback)callback withErrorback:(GenericErrorback)errorback {
    // TODO (shashank): guard against multiple || calls
    __weak typeof(self) bself = self;
    [[HTTPService getInstance] getAllSourcesWithCallback:^(NSArray *sourceList) {
        NSMutableArray *pubs = [NSMutableArray array];
        NSMutableArray *tops = [NSMutableArray array];
        
        NSSet *selectedPublishers = [NSSet setWithArray:[self getSelectedPublishers]];
        NSSet *selectedTopics = [NSSet setWithArray:[self getSelectedTopics]];
        NSMutableDictionary *sourceIdToSource = [NSMutableDictionary dictionary];
        
        for (Source *source in sourceList) {
            [sourceIdToSource setObject:source forKey:source.id];
            
            if ([source isPublisher]) {
                [pubs addObject:source];
                source.selected = [selectedPublishers containsObject:source.id];
            } else if ([source isTopic]) {
                [tops addObject:source];
                source.selected = [selectedTopics containsObject:source.id];
            } else {
                // TODO(shashank): log error
            }
            
            bself.publishers = pubs;
            bself.topics = tops;
        }
        
        bself.sourceIdToSource = sourceIdToSource;
        
        callback();
        
    } withErrorback:errorback];
}

- (void)onSourceSelectionChange:(Source*)changedSource {
    NSMutableArray *sourceIdList;
    if ([changedSource isPublisher]) {
        sourceIdList = [NSMutableArray arrayWithArray:[self getSelectedPublishers]];
    } else if ([changedSource isTopic]) {
        sourceIdList = [NSMutableArray arrayWithArray:[self getSelectedTopics]];
    } else {
        // TODO (shashank): log error
        return;
    }
    
    if (changedSource.selected) {
        if (![sourceIdList containsObject:changedSource.id]) {
            [sourceIdList addObject:changedSource.id];
        }
    } else {
        [sourceIdList removeObject:changedSource.id];
    }
    
    if ([changedSource isPublisher]) {
        [[UserPreferenceService getInstance] setSelectedPublishers:sourceIdList];
    } else if ([changedSource isTopic]) {
        [[UserPreferenceService getInstance] setSelectedTopics:sourceIdList];
    } else {
        // TODO (shashank): log error
        return;
    }
}

- (BOOL)anySourceOrTopicSelected {
    NSNumber* (^predicate)(id) = ^NSNumber*(Source *source) {
        return @(source.selected);
    };
    
    BOOL anyPublisherSelected = NO;
    BOOL anyTopicSelected = NO;
    
    NSArray *publishers = [self getAllPublishers];
    anyPublisherSelected = [Util anyInArray:publishers satisfyingPredicate:predicate];
    if (!anyPublisherSelected) {
        NSArray *topics = [self getAllTopics];
        anyTopicSelected = [Util anyInArray:topics satisfyingPredicate:predicate];
    }
    
    return anyPublisherSelected || anyTopicSelected;
}

@end
