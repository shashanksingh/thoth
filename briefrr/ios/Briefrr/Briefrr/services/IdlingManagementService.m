//
//  IdlingManagementService.m
//  Briefrr
//
//  Created by Shashank Singh on 3/24/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "IdlingManagementService.h"
#import <UIKit/UIKit.h>

static const NSTimeInterval IDLE_INTERVAL = 20;
static const float MIN_BRIGHT_BRIGHTNESS = 0.2;
static const float DIM_BRIGHTNESS = 0.1;

@interface IdlingManagementService()
@property (nonatomic, strong) NSTimer *idleTimer;
@property (nonatomic) float userPreferredBrightness;
@end

@implementation IdlingManagementService

- (instancetype)init {
    self = [super init];
    if (self) {
        self.userPreferredBrightness = MAX(MIN_BRIGHT_BRIGHTNESS, [UIScreen mainScreen].brightness);
    }
    return self;
}

+ (IdlingManagementService*)getInstance {
    static IdlingManagementService *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[IdlingManagementService alloc] init];
    });
    return sharedInstance;
}

- (void)clearIdleTimer {
    if (self.idleTimer) {
        [self.idleTimer invalidate];
        self.idleTimer = nil;
    }
}

- (void)resetIdleTimer {
    [self clearIdleTimer];
    self.idleTimer = [NSTimer scheduledTimerWithTimeInterval:IDLE_INTERVAL target:self selector:@selector(onIdleTimerFired) userInfo:nil repeats:NO];
}

- (void)userInputReceived {
    // ignore if the timer is not running
    if (!self.idleTimer) {
        return;
    }
    [UIScreen mainScreen].brightness = self.userPreferredBrightness;
    [self resetIdleTimer];
}

- (void)start {
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    [self resetIdleTimer];
}

- (void)stop {
    [UIScreen mainScreen].brightness = self.userPreferredBrightness;
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    [self clearIdleTimer];
}

- (void)onIdleTimerFired {
    [UIScreen mainScreen].brightness = DIM_BRIGHTNESS;
}

@end
