//
//  NotificationManager.h
//  Briefrr
//
//  Created by Shashank Singh on 3/15/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationManager : NSObject
+ (NotificationManager*)getInstance;
- (void)updateOnInit;
@end
