//
//  FeedService.m
//  Briefrr
//
//  Created by Shashank Singh on 2/17/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "FeedService.h"
#import "HTTPService.h"
#import "Util.h"

static const NSTimeInterval FEED_CACHE_EXPIRATION = 120.0; // 2 minutes

@interface FeedService()
@property (nonatomic, strong) NSArray *feed;
@property (nonatomic, strong) NSSet *cachedFeedSources;
@property (nonatomic) NSTimeInterval cachedFeedTimestamp;
@end

@implementation FeedService

- (instancetype)init {
    self = [super init];
    if (self) {
        self.feed = nil;
        self.cachedFeedSources = nil;
        self.cachedFeedTimestamp = 0;
    }
    return self;
}

+ (instancetype)getInstance {
    static FeedService *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[FeedService alloc] init];
    });
    return sharedInstance;
}

- (void)getFeedForSources:(NSArray*)sourceIds offset:(NSUInteger)offset limit:(NSUInteger)limit callback:(ArrayCallback)callback errorback:(GenericErrorback)errorback {

    if (self.feed && [self isCachedFeedFresh]) {
        callback([Util safeSubArray:self.feed offset:offset limit:limit]);
        return;
    }
    
    __weak typeof(self) bself = self;
    [[HTTPService getInstance] getFeedForSources:sourceIds offset:offset limit:offset + limit callback:^(NSArray *data) {
        if (!bself) {
            return;
        }
        bself.feed = data;
        bself.cachedFeedSources = [NSSet setWithArray:sourceIds];
        bself.cachedFeedTimestamp = [[NSDate date] timeIntervalSince1970];
        callback([Util safeSubArray:bself.feed offset:offset limit:limit]);
        
    } errorback:^(NSError *error) {
        errorback(error);
    }];
    
}

- (BOOL)isCachedFeedFresh {
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    return currentTime - self.cachedFeedTimestamp < FEED_CACHE_EXPIRATION;
}

- (BOOL)isCachedFeedForSameSourcesAs:(NSArray*)sourceIds {
    if (!self.cachedFeedSources) {
        return NO;
    }
    return [[NSSet setWithArray:sourceIds] isEqualToSet:self.cachedFeedSources];
}
@end
