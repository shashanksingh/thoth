//
//  FeedService.h
//  Briefrr
//
//  Created by Shashank Singh on 2/17/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"

@interface FeedService : NSObject
+ (instancetype)getInstance;
- (void)getFeedForSources:(NSArray*)sourceIds offset:(NSUInteger)offset limit:(NSUInteger)limit callback:(ArrayCallback)callback errorback:(GenericErrorback)errorback;
@end
