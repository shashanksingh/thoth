//
//  IdlingManagementService.h
//  Briefrr
//
//  Created by Shashank Singh on 3/24/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IdlingManagementService : NSObject
+ (IdlingManagementService*)getInstance;
- (void)userInputReceived;
- (void)start;
- (void)stop;
@end
