//
//  EventLogger.h
//  Briefrr
//
//  Created by Shashank Singh on 3/24/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventLogger : NSObject
+ (EventLogger*)getInstance;
- (void)logEvent:(NSString*)eventName;
- (void)logEvent:(NSString*)eventName properties:(NSDictionary*)properties;
- (void)timeEventStart:(NSString*)event;
- (void)timeEventEnd:(NSString*)event;
- (void)timeEventEnd:(NSString*)event properties:(NSDictionary*)properties;
@end
