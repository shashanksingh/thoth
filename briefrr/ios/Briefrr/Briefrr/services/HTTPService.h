//
//  HTTPService.h
//  Briefrr
//
//  Created by Shashank Singh on 2/8/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPSessionManager.h"
#import "Macros.h"
#import "Story.h"

@interface HTTPService : AFHTTPSessionManager
+ (HTTPService*)getInstance;

- (void)getAllSourcesWithCallback:(ArrayCallback)callback withErrorback:(GenericErrorback)errorback;
- (void)getFeedForSources:(NSArray*)sourceIds offset:(NSUInteger)offset limit:(NSUInteger)limit callback:(ArrayCallback)callback errorback:(GenericErrorback)errorback;
- (void)markStoryRead:(Story*)story;
@end
