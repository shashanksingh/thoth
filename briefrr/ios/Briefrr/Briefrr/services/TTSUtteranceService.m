//
//  TTSUtteranceService.m
//  Briefrr
//
//  Created by Shashank Singh on 3/27/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "TTSUtteranceService.h"
#import "Util.h"

static const double UTTERANCE_VOLUME_HEADLINE = -1.0;
static const double UTTERANCE_VOLUME_SUMMARY = -1.0;
static const double UTTERANCE_PITCH_HEADLINE = -1;
static const double UTTERANCE_PITCH_SUMMARY = -1;
static const double UTTERANCE_SPEED_HEADLINE = 0.11;
static const double UTTERANCE_SPEED_SUMMARY = 0.13;
static const double UTTERNACE_PREDELAY_HEADLINE = 0.0;
static const double UTTERNACE_POSTDELAY_HEADLINE = 0.0;
static const double UTTERNACE_PREDELAY_SUMMARY = 1.0;
static const double UTTERNACE_POSTDELAY_SUMMARY = 3.0;
static const double UTTERANCE_SPEED_USER_MESSAGE = -1;
static const double UTTERNACE_PREDELAY_USER_MESSAGE = 0.5;
static const double UTTERNACE_POSTDELAY_USER_MESSAGE = 1.0;
static NSString* const UTTERANCE_VOICE_TO_USE_HEADLINE = @"@male";
static NSString* const UTTERANCE_VOICE_TO_USE_SUMMARY = @"@male";

@interface TTSUtteranceService()
@property (nonatomic, strong) NSMutableDictionary *storyGuidToVoice;
@end

@implementation TTSUtteranceService

- (instancetype)init {
    self = [super init];
    if (self) {
        self.storyGuidToVoice = [NSMutableDictionary dictionary];
    }
    return self;
}

+ (TTSUtteranceService*)getInstance {
    static TTSUtteranceService *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[TTSUtteranceService alloc] init];
    });
    return sharedInstance;
}

- (NSString*)getRandomVoice {
    double rand = [Util generateRandomDoubleInRangeWithMin:0 andMax:1];
    if (rand < 0.5) {
        return @"@male";
    }
    return @"@female";
}

- (NSString*)getVoiceForStory:(Story*)story {
    NSString *voice = self.storyGuidToVoice[story.guid];
    if (!voice) {
        voice = [self getRandomVoice];
        self.storyGuidToVoice[story.guid] = voice;
    }
    return voice;
    
}

- (SpeechUtterance*)getSpeechUtteranceForStoryHeadline:(Story*)story {
    SpeechUtterance *utterance = [[SpeechUtterance alloc] init];
    utterance.speechText = story.title;
    utterance.volume = UTTERANCE_VOLUME_HEADLINE;
    utterance.pitch = UTTERANCE_PITCH_HEADLINE;
    utterance.speed = UTTERANCE_SPEED_HEADLINE;
    utterance.preDelay = UTTERNACE_PREDELAY_HEADLINE;
    utterance.postDelay = UTTERNACE_POSTDELAY_HEADLINE;
    utterance.voiceToUse = [self getVoiceForStory:story];
    return utterance;
}

- (SpeechUtterance*)getSpeechUtteranceForStorySummary:(Story*)story {
    SpeechUtterance *utterance = [[SpeechUtterance alloc] init];
    utterance.speechText = story.summary;
    utterance.volume = UTTERANCE_VOLUME_SUMMARY;
    utterance.pitch = UTTERANCE_PITCH_SUMMARY;
    utterance.speed = UTTERANCE_SPEED_SUMMARY;
    utterance.preDelay = UTTERNACE_PREDELAY_SUMMARY;
    utterance.postDelay = UTTERNACE_POSTDELAY_SUMMARY;
    utterance.voiceToUse = [self getVoiceForStory:story];
    return utterance;
}

- (NSArray*)getSpeechUtterancesForStory:(Story*)story {
    SpeechUtterance *headlineUtterance = [self getSpeechUtteranceForStoryHeadline:story];
    SpeechUtterance *summaryUtterance = [self getSpeechUtteranceForStorySummary:story];
    return @[headlineUtterance, summaryUtterance];
}

- (SpeechUtterance*)getUserMessageUtterance:(NSString*)text {
    SpeechUtterance *utterance = [[SpeechUtterance alloc] init];
    utterance.speechText = text;
    utterance.volume = UTTERANCE_VOLUME_HEADLINE;
    utterance.pitch = UTTERANCE_PITCH_HEADLINE;
    utterance.speed = UTTERANCE_SPEED_USER_MESSAGE;
    utterance.preDelay = UTTERNACE_PREDELAY_USER_MESSAGE;
    utterance.postDelay = UTTERNACE_POSTDELAY_USER_MESSAGE;
    utterance.voiceToUse = [self getRandomVoice];//UTTERANCE_VOICE_TO_USE_HEADLINE;
    return utterance;
}

@end
