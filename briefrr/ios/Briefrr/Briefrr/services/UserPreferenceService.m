//
//  UserPreferenceService.m
//  Briefrr
//
//  Created by Shashank Singh on 2/8/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "UserPreferenceService.h"
#import "Util.h"
#import "SSKeychain.h"
#import <Security/Security.h>

static NSString* const KEYCHAIN_ACCOUNT_USER_ID = @"keychanin_account_user_id";

static NSString* const KEY_APP_FIRST_OPEN_TIME = @"app_first_open_time";
static NSString* const KEY_NUM_APP_OPENS = @"app_number";
static NSString* const KEY_SELECTED_TOPICS = @"selected_topics";
static NSString* const KEY_SELECTED_PUBLISHERS = @"selected_publishers";
static NSString* const KEY_INTRO_DONE = @"intro_done";
static NSString* const KEY_APP_OPEN_TIME_OF_DAY = @"app_open_time_of_day";
static NSString* const KEY_NOTIFICATION_PERMISSION_DENIAL_TIME = @"notification_permission_denial_time";
static NSString* const KEY_SAVED_STORIES = @"saved_stories";

static const NSUInteger MAX_APP_OPEN_TIME_OF_DAY_HISTORY = 100;
static const NSTimeInterval MIN_INTERVAL_FOR_NEW_APP_OPEN = 20 * 60.0;

@interface UserPreferenceService()
@property (nonatomic, strong) NSString *userFirstName;
@end

@implementation UserPreferenceService

- (UserPreferenceService*)init {
    self = [super init];
    if (self) {
        self.userFirstName = nil;
    }
    return self;
}

+ (UserPreferenceService*)getInstance {
    static UserPreferenceService *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[UserPreferenceService alloc] init];
    });
    return sharedInstance;
}

- (NSUserDefaults*)getDefaults {
    return [NSUserDefaults standardUserDefaults];
}

- (id)getValueForKey:(NSString*)key {
    NSData *serialized = [[self getDefaults] dataForKey:key];
    if (!serialized) {
        return nil;
    }
    return [NSKeyedUnarchiver unarchiveObjectWithData:serialized];
}

- (NSArray*)getArrayForKey:(NSString*)key {
    NSArray *rv = [self getValueForKey:key];
    if (!rv) {
        return @[];
    }
    return rv;
}

- (void)setValue:(id)value forKey:(NSString*)key {
    NSData *serializedTopics = [NSKeyedArchiver archivedDataWithRootObject:value];
    
    NSUserDefaults *defaults = [self getDefaults];
    [defaults setObject:serializedTopics forKey:key];
    [defaults synchronize];
}

- (void)clearKey:(NSString*)key {
    NSUserDefaults *defaults = [self getDefaults];
    [defaults removeObjectForKey:key];
    [defaults synchronize];
}

- (NSString*)getUserId {
    NSString *userId = [SSKeychain passwordForService:APP_UNIQUE_ID account:KEYCHAIN_ACCOUNT_USER_ID];
    if (!userId) {
        userId = [Util createUUID];
        [SSKeychain setPassword:userId forService:APP_UNIQUE_ID account:KEYCHAIN_ACCOUNT_USER_ID];
    }
    return userId;
}

- (NSTimeInterval)getAppFirstOpenTime {
    NSNumber *firstOpenTime = [self getValueForKey:KEY_APP_FIRST_OPEN_TIME];
    if (!firstOpenTime) {
        return -1;
    }
    return firstOpenTime.doubleValue;
}

- (NSTimeInterval)getTimeSinceAppFirstOpened {
    NSTimeInterval appFirstOpenTime = [self getAppFirstOpenTime];
    if (appFirstOpenTime < 0) {
        return 0;
    }
    return [NSDate date].timeIntervalSince1970 - appFirstOpenTime;
}

- (NSUInteger)getNumberOfTimesAppHasBeenOpened {
    NSNumber *numAppOpens = [self getValueForKey:KEY_NUM_APP_OPENS];
    if (!numAppOpens) {
        return 0;
    }
    return numAppOpens.integerValue;
}

- (BOOL)isFirstRun {
    return [self getNumberOfTimesAppHasBeenOpened] <= 1;
}

- (void)incrementNumAppOpens {
    NSUInteger numAppOpens = [self getNumberOfTimesAppHasBeenOpened];
    [self setValue:@(numAppOpens + 1) forKey:KEY_NUM_APP_OPENS];
}

- (NSArray*)getSelectedTopics {
    return [self getArrayForKey:KEY_SELECTED_TOPICS];
}

- (void)setSelectedTopics:(NSArray*)selectedTopics {
    [self setValue:selectedTopics forKey:KEY_SELECTED_TOPICS];
}

- (NSArray*)getSelectedPublishers {
    return [self getArrayForKey:KEY_SELECTED_PUBLISHERS];
}

- (void)setSelectedPublishers:(NSArray*)sources {
    [self setValue:sources forKey:KEY_SELECTED_PUBLISHERS];
}

- (BOOL)hasUserSeenIntro {
    return !![[self getDefaults] boolForKey:KEY_INTRO_DONE];
}

- (void)setUserHasSeenIntro {
    NSUserDefaults *defaults = [self getDefaults];
    [defaults setBool:YES forKey:KEY_INTRO_DONE];
    [defaults synchronize];
}

- (void)updateOnOpen {
    [self addCurrentTimeAsAppOpenTime];
}

- (void)addCurrentTimeAsAppOpenTime {
    NSDate *date = [NSDate date];
    if ([self getAppFirstOpenTime] < 0) {
        [self setValue:@([date timeIntervalSince1970]) forKey:KEY_APP_FIRST_OPEN_TIME];
    }
    [self incrementNumAppOpens];
    
    NSTimeInterval time = date.timeIntervalSince1970;
    NSTimeInterval lastOpenTime = [self getLastAppOpenAbsoluteTime];
    // quick successive opens are not considered new opens
    if (fabs(time - lastOpenTime) < MIN_INTERVAL_FOR_NEW_APP_OPEN) {
        // TODO (shashank): log event, ideally the user should not have to
        // reopen the app so soon
        return;
    }
    
    
    NSMutableArray *savedTimes = [self getAppOpenAbsoluteTimes].mutableCopy;
    while (savedTimes.count > MAX_APP_OPEN_TIME_OF_DAY_HISTORY) {
        [savedTimes removeObjectAtIndex:0];
    }
    [savedTimes addObject:@(time)];
    [self setValue:savedTimes forKey:KEY_APP_OPEN_TIME_OF_DAY];
}

- (NSArray*)getAppOpenAbsoluteTimes {
    NSArray *savedTimes = [self getArrayForKey:KEY_APP_OPEN_TIME_OF_DAY];
    if (!savedTimes) {
        savedTimes = @[];
    }
    return savedTimes;
}

- (NSArray*)getAppOpenTimesOfDay {
    NSArray *savedTimes = [self getAppOpenAbsoluteTimes];
    // Note (shashank): iOS8
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    return [Util map:savedTimes WithLambda:^NSNumber*(NSNumber *epoch) {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:epoch.doubleValue];
        NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: date];
        return @(components.hour * 60 + components.minute);
    }];
}

- (NSTimeInterval)getLastAppOpenAbsoluteTime {
    NSArray *savedTimes = [self getAppOpenAbsoluteTimes];
    if (savedTimes.count == 0) {
        return 0;
    }
    return ((NSNumber*)savedTimes[savedTimes.count - 1]).doubleValue;
}

- (BOOL)hasUserDeniedNotificationPermission {
    return !![self getValueForKey:KEY_NOTIFICATION_PERMISSION_DENIAL_TIME];
}

- (NSTimeInterval)timeSinceLastNotificationPermissionDenial {
    NSNumber *lastDenialTime = [self getValueForKey:KEY_NOTIFICATION_PERMISSION_DENIAL_TIME];
    if (!lastDenialTime) {
        return HUGE_VALF;
    }
    return [[NSDate date] timeIntervalSince1970] - lastDenialTime.doubleValue;
}

- (void)updateNotificationPermissionDenialTimeToCurrent {
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    [self setValue:@(currentTime) forKey:KEY_NOTIFICATION_PERMISSION_DENIAL_TIME];
}

- (void)clearNotificationPermissionDenialTime {
    [self clearKey:KEY_NOTIFICATION_PERMISSION_DENIAL_TIME];
}

- (NSDictionary*)getSavedStoryGuidToDescriptorMap {
    NSDictionary *savedStoryGuidToDescriptor = [self getValueForKey:KEY_SAVED_STORIES];
    if (!savedStoryGuidToDescriptor) {
        savedStoryGuidToDescriptor = @{};
    }
    return savedStoryGuidToDescriptor;
}

- (NSArray*)getSavedStories {
    NSArray *savedStories = [self getSavedStoryGuidToDescriptorMap].allValues;
    return [Util map:savedStories WithLambda:^Story*(NSDictionary *storyDescriptor) {
        return [[Story alloc] initWithJSONDescriptor:storyDescriptor];
    }];
}

- (void)saveStory:(Story*)story {
    NSMutableDictionary *savedStoryGuidToDescriptor = ((NSArray*)[self getSavedStoryGuidToDescriptorMap]).mutableCopy;
    [savedStoryGuidToDescriptor setObject:story.descriptor forKey:story.guid];
    
    // TODO (shashank): cap on number of stories saved
    [self setValue:savedStoryGuidToDescriptor forKey:KEY_SAVED_STORIES];
}

- (void)deleteSavedStory:(Story*)story {
    NSMutableDictionary *savedStoryGuidToDescriptor = ((NSArray*)[self getSavedStoryGuidToDescriptorMap]).mutableCopy;
    [savedStoryGuidToDescriptor removeObjectForKey:story.guid];
    [self setValue:savedStoryGuidToDescriptor forKey:KEY_SAVED_STORIES];
}

- (BOOL)isStorySaved:(Story*)story {
    NSDictionary *savedStoryGuidToDescriptor = [self getValueForKey:KEY_SAVED_STORIES];
    return !![savedStoryGuidToDescriptor objectForKey:story.guid];
}

- (NSString*)getUserFirstName {
    if (!self.userFirstName) {
        NSString *deviceName = [[UIDevice currentDevice] name];
        
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:@"(.+?)(\'|’)s (iPhone|iPad)"
                                      options:NSRegularExpressionCaseInsensitive
                                      error:&error];
        if (error) {
            // TODO (shashank): log error
            return nil;
        }
        
        NSRange fullRange = NSMakeRange(0, deviceName.length);
        NSArray *matches = [regex matchesInString:deviceName options:0 range:fullRange];
        if (matches.count == 0) {
            self.userFirstName = nil;
        } else {
            NSRange fullNameRange = [matches[0] rangeAtIndex:1];
            NSString *fullName = [deviceName substringWithRange:fullNameRange];
            NSArray *nameParts = [fullName componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if (nameParts.count == 0) {
                self.userFirstName = nil;
            } else {
                self.userFirstName = nameParts[0];
            }
        }
    }
    
    return self.userFirstName;
}
@end
