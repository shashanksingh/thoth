//
//  RemoteConfigService.h
//  Briefrr
//
//  Created by Shashank Singh on 4/17/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"

@interface RemoteConfigService : NSObject
+ (RemoteConfigService*)getInstance;
- (void)refreshWithCallback:(VoidCallback)callback andErrorback:(GenericErrorback)errorback;

- (NSString*)getNeospeechEmbeddedTTSLicense;
- (NSString*)getNuanceAppId;
- (NSString*)getNuanceAppKey;
- (NSString*)getNuanceASREndPoint;
- (NSDictionary*)getMixpaneLoggingRatios;
@end
