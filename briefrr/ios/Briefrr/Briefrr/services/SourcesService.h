//
//  SourcesService.h
//  Briefrr
//
//  Created by Shashank Singh on 2/8/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"
#import "Source.h"

@interface SourcesService : NSObject
+ (SourcesService*)getInstance;

- (Source*)getSourceById:(NSString*)sourceId;
- (NSArray*)getSelectedPublishers;
- (NSArray*)getSelectedTopics;
- (BOOL)isAnySourceSelected;
- (NSArray*)getSelectedSourceIds;

- (void)updateSourcesWithCallback:(VoidCallback)callback withErrorback:(GenericErrorback)errorback;
- (NSArray*)getAllPublishers;
- (NSArray*)getAllTopics;

- (BOOL)anySourceOrTopicSelected;

@end
