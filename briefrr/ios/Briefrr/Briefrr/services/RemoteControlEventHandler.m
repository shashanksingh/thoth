//
//  RemoteControlEventHandler.m
//  Briefrr
//
//  Created by Shashank Singh on 3/18/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "RemoteControlEventHandler.h"
#import <MediaPlayer/MediaPlayer.h>
#import "EventManager.h"
#import "Macros.h"
#import "Util.h"
#import "AppStateManager.h"

@implementation RemoteControlEventHandler

+ (RemoteControlEventHandler*)getInstance {
    static RemoteControlEventHandler *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[RemoteControlEventHandler alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)start {
    [self becomeFirstResponder];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    MPRemoteCommandCenter *rcc = [MPRemoteCommandCenter sharedCommandCenter];
    __weak typeof(self) bself = self;
    
    // Note (shashank): It seems that the pause button in the media control UI will
    // not change to a play button on tapping it if there is any audio unit that
    // running in the app (even if it input/record only audio unit). Since we
    // need to keep recording audio to be able to listen for voice commands
    // this means we can't have the play/pause buttons appearing in the UI.
    // Hence we disabled them command. This is better than having a button
    // in there that works but does not change state thereby confusing the
    // user. Also, it seems this issue is only for the media player control
    // UI and does not affect external hardware generated events (so earphone
    // controls or connected car controls should work alright).
    rcc.playCommand.enabled = NO;
    [rcc.playCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent *event) {
        if (!bself) {
            return MPRemoteCommandHandlerStatusSuccess;
        }
        [bself fireEventIfApplicable:NON_VOICE_COMMAND_PLAY_FEED];
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    
    rcc.pauseCommand.enabled = NO;
    [rcc.pauseCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent *event) {
        if (!bself) {
            return MPRemoteCommandHandlerStatusSuccess;
        }
        [bself fireEventIfApplicable:NON_VOICE_COMMAND_PAUSE_FEED];
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    
    rcc.togglePlayPauseCommand.enabled = NO;
    
    rcc.nextTrackCommand.enabled = YES;
    [rcc.nextTrackCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent *event) {
        if (!bself) {
            return MPRemoteCommandHandlerStatusSuccess;
        }
        [bself fireEventIfApplicable:NON_VOICE_COMMAND_NEXT_STORY];
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    
    rcc.previousTrackCommand.enabled = YES;
    [rcc.previousTrackCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent *event) {
        if (!bself) {
            return MPRemoteCommandHandlerStatusSuccess;
        }
        [bself fireEventIfApplicable:NON_VOICE_COMMAND_PREVIOUS_STORY];
        return MPRemoteCommandHandlerStatusSuccess;
    }];
}

- (void)handleRemoteControlEvent:(UIEvent*)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlPause:
            [self fireEventIfApplicable:NON_VOICE_COMMAND_PAUSE_FEED];
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self fireEventIfApplicable:NON_VOICE_COMMAND_PLAY_FEED];
            break;
        case UIEventSubtypeRemoteControlTogglePlayPause:
            [self fireEventIfApplicable:NON_VOICE_COMMAND_TOGGLE_FEED_PLAY];
            break;
        case UIEventSubtypeRemoteControlNextTrack:
            [self fireEventIfApplicable:NON_VOICE_COMMAND_NEXT_STORY];
            break;
        case UIEventSubtypeRemoteControlPreviousTrack:
            [self fireEventIfApplicable:NON_VOICE_COMMAND_PREVIOUS_STORY];
            break;
        default:
            break;
    }
}

- (BOOL)shouldRespondToCommands {
    return [AppStateManager getInstance].feedReadingMode == AUDIO;
}

- (void)fireEventIfApplicable:(EventType)event {
    if (![self shouldRespondToCommands]) {
        return;
    }
    [[EventManager getInstance] fireEvent:event];
}
@end
