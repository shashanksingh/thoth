//
//  FeedReaderService.h
//  Briefrr
//
//  Created by Shashank Singh on 2/25/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"

@interface FeedReaderService : NSObject
+ (FeedReaderService*)getInstance;
- (void)setFeed:(NSArray*)feed ofType:(FeedType)feedType;
- (void)startReadingFromCurrentStory;
- (void)startReadingFromIndex:(NSUInteger)index;
- (void)readNextStory;
- (void)readPreviousStory;
- (void)pauseReading;
- (void)resumeReading;
- (void)stopReading;
- (BOOL)isReading;
- (BOOL)hasStarted;
- (void)speakImmediately:(NSString*)text;
@end
