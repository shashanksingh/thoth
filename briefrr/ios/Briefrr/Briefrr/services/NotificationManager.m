//
//  NotificationManager.m
//  Briefrr
//
//  Created by Shashank Singh on 3/15/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "NotificationManager.h"
#import <UIKit/UIKit.h>
#import "UserPreferenceService.h"
#import "Macros.h"
#import "Util.h"

static const NSUInteger APP_START_TIME_CLUSTER_MAX_SPAN_MINUTES = 30;
static const NSUInteger MIN_NOTIFICATION_DELTA_MINUTES = 300;
static const NSUInteger MIN_NOTIFICATION_MINUTE_OF_DAY = 5 * 60;
static const NSUInteger MAX_NOTIFICATION_MINUTE_OF_DAY = 21 * 60;
static const NSUInteger DEFAULT_FIRST_NOTIFICATION_MINUTE_OF_DAY = 8 * 60;
static const NSUInteger DEFAULT_SECOND_NOTIFICATION_MINUTE_OF_DAY = 18 * 60;

@implementation NotificationManager

+ (NotificationManager*)getInstance {
    static NotificationManager *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[NotificationManager alloc] init];
    });
    return sharedInstance;
}

- (void)updateOnInit {
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    NSArray *pastAppOpenTimes = [[UserPreferenceService getInstance] getAppOpenTimesOfDay];
    NSMutableArray *notificationTimes = [self getNotificationMinutesOfDay:pastAppOpenTimes].mutableCopy;
    
    if (notificationTimes.count < 1) {
        [notificationTimes addObject:@(DEFAULT_FIRST_NOTIFICATION_MINUTE_OF_DAY)];
    }
    if (notificationTimes.count < 2) {
        [notificationTimes addObject:@(DEFAULT_SECOND_NOTIFICATION_MINUTE_OF_DAY)];
    }
    
    NSInteger firstNotificationMinute = ((NSNumber*)notificationTimes[0]).intValue;
    // Note(shashank): no notification in early hours until we are sure our clustering
    // algo works fine
    firstNotificationMinute = MIN(MAX(firstNotificationMinute, MIN_NOTIFICATION_MINUTE_OF_DAY), MAX_NOTIFICATION_MINUTE_OF_DAY);
    [self scheduleLocalNotificationAtMinuteOfDay:firstNotificationMinute withMessage:[self getMessageForMinuteOfDay:firstNotificationMinute]];
    
    NSInteger secondNotificationMinute = ((NSNumber*)notificationTimes[1]).intValue;
    secondNotificationMinute = MAX(secondNotificationMinute, firstNotificationMinute + MIN_NOTIFICATION_DELTA_MINUTES);
    if (secondNotificationMinute <= MAX_NOTIFICATION_MINUTE_OF_DAY) {
        [self scheduleLocalNotificationAtMinuteOfDay:secondNotificationMinute withMessage:[self getMessageForMinuteOfDay:secondNotificationMinute]];
    }
}

- (NSString*)getMessageForMinuteOfDay:(NSInteger)minute {
    NSString *greeting;
    if (minute < HOUR_MAX_MORNING * 60) {
        greeting = MESSAGE_GREETING_MORNING;
    } else if (minute < HOUR_MAX_AFTERNOON * 60) {
        greeting = MESSAGE_GREETING_AFTERNOON;
    } else {
        greeting = MESSAGE_GREETING_EVENING;
    }
    
    NSString *userFirstName = [[UserPreferenceService getInstance] getUserFirstName];
    greeting = [Util formatString:greeting withParameters:@{@"first_name": userFirstName ? userFirstName : @""}];
    
    return [[greeting stringByAppendingString:@" "] stringByAppendingString:MESSAGE_NOTIFICATION_FEED_READY];
}

- (NSArray*)getNotificationMinutesOfDay:(NSArray*)pastAppOpenTimes {
    if (pastAppOpenTimes.count <= 1) {
        return pastAppOpenTimes;
    }
    pastAppOpenTimes = [pastAppOpenTimes sortedArrayUsingSelector: @selector(compare:)];
    NSMutableArray *clusters = [NSMutableArray array];
    
    NSMutableArray *currentCluster = [NSMutableArray array];
    [clusters addObject:currentCluster];
    
    for (NSInteger i=0; i<pastAppOpenTimes.count; ++i) {
        if (i == 0) {
            [currentCluster addObject:pastAppOpenTimes[i]];
        } else {
            int thisMinute = ((NSNumber*)pastAppOpenTimes[i]).intValue;
            int currentClusterMinMinute = ((NSNumber*)currentCluster[0]).intValue;
            
            if (thisMinute - currentClusterMinMinute <= APP_START_TIME_CLUSTER_MAX_SPAN_MINUTES) {
                [currentCluster addObject:@(thisMinute)];
            } else {
                currentCluster = [NSMutableArray array];
                [clusters addObject:currentCluster];
                [currentCluster addObject:@(thisMinute)];
            }
        }
    }
    
    [clusters sortUsingComparator:^NSComparisonResult(NSArray *c1, NSArray *c2) {
        return c2.count - c1.count;
    }];
    
    NSMutableArray *notificationTimes = [NSMutableArray array];
    // we notify on the earliest time in the biggest clusters
    // it's better to notify too early than to notify too late
    NSArray *firstCluster = clusters[0];
    NSNumber *firstNotificationTime = firstCluster[0];
    [notificationTimes addObject:firstNotificationTime];
    
    for (NSInteger i=1; i<clusters.count && notificationTimes.count < 2; ++i) {
        NSArray *cluster = clusters[i];
        for (NSInteger j=0; j<cluster.count; ++j) {
            int minuteOfDay = ((NSNumber*)cluster[j]).intValue;
            if (minuteOfDay - firstNotificationTime.intValue > MIN_NOTIFICATION_DELTA_MINUTES) {
                [notificationTimes addObject:@(minuteOfDay)];
                break;
            }
        }
    }
    
    return notificationTimes;
}

- (void)scheduleLocalNotificationAtMinuteOfDay:(NSInteger)minute withMessage:(NSString*)message {
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    
    NSDate *date = [NSDate date];
    // Note (shashank): iOS8
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: date];
    [components setHour: floor(minute/60)];
    [components setMinute: minute%60];
    [components setSecond: 0];
    
    NSDate *fireTime = [gregorian dateFromComponents: components];
    localNotif.fireDate = fireTime;
    localNotif.alertBody = message;
    localNotif.repeatInterval = kCFCalendarUnitDay;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
}
@end
