//
//  ActionViewAnimatorService.h
//  Briefrr
//
//  Created by Shashank Singh on 2/16/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ActionViewAnimatorService : NSObject
+ (instancetype)getInstance;
- (void)addAnimationsOnView:(UIView*)view;
@end
