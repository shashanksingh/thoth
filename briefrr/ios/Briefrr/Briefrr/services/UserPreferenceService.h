//
//  UserPreferenceService.h
//  Briefrr
//
//  Created by Shashank Singh on 2/8/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Story.h"

@interface UserPreferenceService : NSObject
+ (UserPreferenceService*) getInstance;

- (NSString*)getUserId;

- (NSTimeInterval)getTimeSinceAppFirstOpened;
- (BOOL)isFirstRun;
- (NSUInteger)getNumberOfTimesAppHasBeenOpened;

- (NSArray*)getSelectedTopics;
- (void)setSelectedTopics:(NSArray*)selectedTopics;

- (NSArray*)getSelectedPublishers;
- (void)setSelectedPublishers:(NSArray*)sources;

- (BOOL)hasUserSeenIntro;
- (void)setUserHasSeenIntro;

- (void)updateOnOpen;
- (NSArray*)getAppOpenTimesOfDay;

- (BOOL)hasUserDeniedNotificationPermission;
- (NSTimeInterval)timeSinceLastNotificationPermissionDenial;
- (void)updateNotificationPermissionDenialTimeToCurrent;
- (void)clearNotificationPermissionDenialTime;

- (NSArray*)getSavedStories;
- (void)saveStory:(Story*)story;
- (void)deleteSavedStory:(Story*)story;
- (BOOL)isStorySaved:(Story*)story;

- (NSString*)getUserFirstName;

@end
