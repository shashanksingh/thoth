//
//  AppStateManager.m
//  Briefrr
//
//  Created by Shashank Singh on 3/25/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "AppStateManager.h"
#import "EventManager.h"
#import "UserPreferenceService.h"

@implementation AppStateManager

- (instancetype)init {
    self = [super init];
    if (self) {
        self.feedReadingMode = VISUAL;
        self.initalFeedLoadDone = NO;
    }
    return self;
}

+ (AppStateManager*)getInstance {
    static AppStateManager *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[AppStateManager alloc] init];
    });
    return sharedInstance;
}

- (void)setFeedReadingMode:(FeedReadingMode)feedReadingMode {
    if (self.feedReadingMode == feedReadingMode) {
        return;
    }
    _feedReadingMode = feedReadingMode;
    [[EventManager getInstance] fireEvent:FEED_READING_MODE_CHANGED, @(self.feedReadingMode)];
}

@end
