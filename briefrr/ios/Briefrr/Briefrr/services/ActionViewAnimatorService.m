//
//  ActionViewAnimatorService.m
//  Briefrr
//
//  Created by Shashank Singh on 2/16/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "ActionViewAnimatorService.h"
#import <pop/POP.h>

@interface ActionViewAnimatorService()<UIGestureRecognizerDelegate>

@end

@implementation ActionViewAnimatorService

+ (instancetype)getInstance {
    static ActionViewAnimatorService *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ActionViewAnimatorService alloc] init];
    });
    return sharedInstance;
}

- (void)addAnimationsOnView:(UIView*)view {
    if ([view isKindOfClass:[UIControl class]]) {
        UIControl *control = (UIControl*)view;
        [control addTarget:self action:@selector(scaleToSmall:)
          forControlEvents:UIControlEventTouchDown | UIControlEventTouchDragEnter];
        [control addTarget:self action:@selector(scaleAnimation:)
          forControlEvents:UIControlEventTouchUpInside];
        [control addTarget:self action:@selector(scaleToDefault:)
          forControlEvents:UIControlEventTouchDragExit];
    } else {
        UITapGestureRecognizer *tapRecognizer =
            [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
        [view addGestureRecognizer:tapRecognizer];
        
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(onPress:)];
        lpgr.minimumPressDuration = 0.2; //seconds
        lpgr.delegate = self;
        [view addGestureRecognizer:lpgr];
    }
}

- (void)scaleToSmall:(UIView*)source {
    POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.95f, 0.95f)];
    [source.layer pop_addAnimation:scaleAnimation forKey:@"layerScaleSmallAnimation"];
}

- (void)scaleAnimation:(UIView*)source {
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.velocity = [NSValue valueWithCGSize:CGSizeMake(3.f, 3.f)];
    scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.f, 1.f)];
    scaleAnimation.springBounciness = 18.0f;
    [source.layer pop_addAnimation:scaleAnimation forKey:@"layerScaleSpringAnimation"];
}

- (void)scaleToDefault:(UIView*)source {
    POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.f, 1.f)];
    [source.layer pop_addAnimation:scaleAnimation forKey:@"layerScaleDefaultAnimation"];
}

- (void)onTap:(UITapGestureRecognizer*)recognizer {
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
        case UIGestureRecognizerStateEnded:
            [self scaleAnimation:recognizer.view];
            break;
        default:
            break;
    }
}

- (void)onPress:(UILongPressGestureRecognizer*)recognizer {
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            [self scaleToSmall:recognizer.view];
            break;
        case UIGestureRecognizerStateFailed:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateEnded:
            [self scaleToDefault:recognizer.view];
            break;
        default:
            break;
    }
}

@end
