//
//  MotionDetectionService.h
//  Briefrr
//
//  Created by Shashank Singh on 4/22/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MotionDetectionService : NSObject
+ (MotionDetectionService*)getInstance;
@end
