//
//  EventLogger.m
//  Briefrr
//
//  Created by Shashank Singh on 3/24/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "EventLogger.h"
#import "Mixpanel.h"
#import "UserPreferenceService.h"
#import "SourcesService.h"
#import "RemoteConfigService.h"

@interface EventLogger()
@property (nonatomic, strong) NSDate *appInitTime;
@end

@implementation EventLogger

- (instancetype)init {
    self = [super init];
    if (self) {
        self.appInitTime = [NSDate date];
        [[Mixpanel sharedInstance] identify:[[UserPreferenceService getInstance] getUserId]];
        NSString *firstName = [[UserPreferenceService getInstance] getUserFirstName];
        if (firstName) {
            [[Mixpanel sharedInstance].people set:@{@"first_name": firstName}];
        }
    }
    return self;
}

+ (EventLogger*)getInstance {
    static EventLogger *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[EventLogger alloc] init];
    });
    return sharedInstance;
}

- (void)logEvent:(NSString*)eventName {
    [self logEvent:eventName properties:nil];
}

- (float)getLoggingRatioForEvent:(NSString*)eventName {
    NSDictionary *loggingRatios = [[RemoteConfigService getInstance] getMixpaneLoggingRatios];
    if (!loggingRatios) {
        return 1.0;
    }
    NSNumber *loggingRatio = loggingRatios[eventName];
    if (!loggingRatio) {
        return 1.0;
    }
    return loggingRatio.floatValue;
}

- (void)logEvent:(NSString*)eventName properties:(NSDictionary*)properties {
    float loggingRatio = [self getLoggingRatioForEvent:eventName];
    float loggingPercent = loggingRatio * 100;
    int rand = arc4random()%100;
    if (rand > loggingPercent) {
        return;
    }
    
    if (!properties) {
        properties = @{};
    }
    
    NSMutableDictionary *props = [NSMutableDictionary dictionaryWithCapacity:properties.count];
    for (NSString *key in properties) {
        NSObject *value = properties[key];
        if (![value isKindOfClass:[NSString class]]) {
            value = value.description;
        }
        [props setObject:value forKey:key];
    }
    
    NSString *userId = [[UserPreferenceService getInstance] getUserId];
    props[@"user_id"] = userId;
    
    NSString *deviceName = [[UIDevice currentDevice] name];
    props[@"device_name"] = deviceName;
    
    NSString *userFirstName = [[UserPreferenceService getInstance] getUserFirstName];
    if (userFirstName) {
        props[@"user_first_name"] = userFirstName;
    }
    
    long timestamp = floorf([[NSDate date] timeIntervalSince1970] * 1000);
    props[@"timestamp"] = @(timestamp);
    
    NSTimeInterval delta = [[NSDate date] timeIntervalSinceDate:self.appInitTime];
    delta = round(delta * 1000);
    props[@"time_since_init"] = @(delta);
    
    BOOL introDone = [[UserPreferenceService getInstance] hasUserSeenIntro];
    props[@"intro_done"] = @(introDone);
    
    NSArray* selectedSources = [[SourcesService getInstance] getSelectedSourceIds];
    props[@"num_selected_sources"] = @(selectedSources.count);
    props[@"selected_sources"] = selectedSources;
    
    [[Mixpanel sharedInstance] track:eventName properties:props];
}

- (void)timeEventStart:(NSString *)event {
    [[Mixpanel sharedInstance] timeEvent:event];
}

- (void)timeEventEnd:(NSString*)event {
    [self timeEventEnd:event properties:nil];
}

- (void)timeEventEnd:(NSString*)event properties:(NSDictionary*)properties {
    [self logEvent:event properties:properties];
}

@end
