//
//  MotionDetectionService.m
//  Briefrr
//
//  Created by Shashank Singh on 4/22/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "MotionDetectionService.h"
#import <CoreMotion/CoreMotion.h>

static const double MIN_ACCELERATION_FOR_MOTION = 1.2;

@interface MotionDetectionService()
@property (nonatomic, strong) CMMotionManager *motionManager;
@property (nonatomic, strong) NSFileHandle *logFile;
@end

@implementation MotionDetectionService
- (instancetype)init {
    self = [super init];
    if (self) {
        self.motionManager = [[CMMotionManager alloc] init];
        self.motionManager.accelerometerUpdateInterval = 0.1;
        
        if ([self.motionManager isAccelerometerAvailable]) {
            NSDate *currDate = [NSDate date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"dd.MM.YY HH:mm:ss"];
            
            NSString *dateString = [dateFormatter stringFromDate:currDate];
            NSString *fileName = [NSString stringWithFormat:@"%@.acc.txt", dateString];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *onDiskPath = [documentsDirectory stringByAppendingPathComponent:fileName];
            
            [[NSFileManager defaultManager] createFileAtPath:onDiskPath contents:nil attributes:nil];
            
            self.logFile = [NSFileHandle fileHandleForUpdatingAtPath:onDiskPath];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            __weak typeof(self) bself = self;
            [self.motionManager startAccelerometerUpdatesToQueue:queue withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (!bself) {
                        return;
                    }
                    
                    NSTimeInterval timestamp = [NSDate date].timeIntervalSince1970;
                    double accX = accelerometerData.acceleration.x;
                    double accY = accelerometerData.acceleration.y;
                    double accZ = accelerometerData.acceleration.z;
                    double acc = sqrt(accX * accX + accY * accY + accZ * accZ);
                    if (acc >= MIN_ACCELERATION_FOR_MOTION) {
                        NSString *logLine = [NSString stringWithFormat:@"%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", timestamp, accX, accY, accZ, acc];
                        [bself.logFile writeData:[logLine dataUsingEncoding:NSUTF8StringEncoding]];
                    }
                });
            }]; 
        }
    }
    return self;
}

+ (MotionDetectionService*)getInstance {
    static MotionDetectionService *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MotionDetectionService alloc] init];
    });
    return sharedInstance;
}

- (void)dealloc {
    if (self.motionManager) {
        [self.motionManager stopAccelerometerUpdates];
    }
    if (self.logFile) {
        [self.logFile closeFile];
    }
}
@end
