//
//  RemoteConfigService.m
//  Briefrr
//
//  Created by Shashank Singh on 4/17/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "RemoteConfigService.h"
#import <AFNetworking/AFNetworking.h>
#import "EventLogger.h"
#import "UserPreferenceService.h"

static NSString* const CONFIG_ENDPOINT = @"http://briefrr.com:8080/Backend/Config.jsp";;

static NSString* const CONFIG_KEYPATH_NEOSPEECH_EMBEDDED_LICENSE = @"neospeech.embedded.license.body";
static NSString* const CONFIG_KEYPATH_NUANCE_APP_ID = @"nuance.app.id";
static NSString* const CONFIG_KEYPATH_NUANCE_APP_KEY = @"nuance.app.key";
static NSString* const CONFIG_KEYPATH_NUANCE_ASR_END_POINT = @"nuance.asr.endPoint";
static NSString* const CONFIG_KEYPATH_MIXPANEL_LOGGING_RATIOS = @"mixpanel.loggingRatio";

@interface RemoteConfigService()
@property (nonatomic, strong) NSDictionary *config;
@end

@implementation RemoteConfigService

- (instancetype)init {
    self = [super init];
    if (self) {
        self.config = nil;
    }
    return self;
}

+ (RemoteConfigService*)getInstance {
    static RemoteConfigService *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[RemoteConfigService alloc] init];
    });
    return sharedInstance;
}

- (void)logConfigRefreshError:(NSString*)error {
    [[EventLogger getInstance] logEvent:@"remote_config_refresh_failed" properties:@{@"error": error}];
}

- (void)refreshWithCallback:(VoidCallback)callback andErrorback:(GenericErrorback)errorback {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"uid": [[UserPreferenceService getInstance] getUserId]};
    
    __weak typeof(self) bself = self;
    [manager GET:CONFIG_ENDPOINT parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (!bself) {
            return;
        }
        
        if (![responseObject isKindOfClass:[NSDictionary class]]) {
            NSString *message = @"/config did not return a valid json object";
            [bself logConfigRefreshError:message];
            NSError *error = [NSError errorWithDomain:ERROR_DOMAIN code:-1 userInfo:@{@"message": message}];
            errorback(error);
            return;
        }
        
        [[EventLogger getInstance] logEvent:@"remote_config_refresh_succeeded"
                                 properties:@{@"config": operation.responseString}];
        bself.config = responseObject;
        callback();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [bself logConfigRefreshError:error.localizedDescription];
        errorback(error);
    }];
}

- (id)getConfigValueForKey:(NSString*)key {
    // necessary to not log in case of missing logging related params to avoid infinite
    // recursion.
    if (!self.config && ![key isEqualToString:CONFIG_KEYPATH_MIXPANEL_LOGGING_RATIOS]) {
        [[EventLogger getInstance] logEvent:@"error_remote_config_unavailable"];
        return nil;
    }
    
    @try {
        return [self.config valueForKeyPath:key];
    } @catch(NSException *e) {
        if ([[e name] isEqualToString:NSUndefinedKeyException]) {
            [[EventLogger getInstance] logEvent:@"error_remote_config_unavailable_keypath" properties:@{@"keypath": key}];
            return nil;
        } else {
            @throw;
        }
    }
    
}

- (NSString*)getNeospeechEmbeddedTTSLicense {
    return [self getConfigValueForKey:CONFIG_KEYPATH_NEOSPEECH_EMBEDDED_LICENSE];
}

- (NSString*)getNuanceAppId {
    return [self getConfigValueForKey:CONFIG_KEYPATH_NUANCE_APP_ID];
}

- (NSString*)getNuanceAppKey {
    return [self getConfigValueForKey:CONFIG_KEYPATH_NUANCE_APP_KEY];
}

- (NSString*)getNuanceASREndPoint {
    return [self getConfigValueForKey:CONFIG_KEYPATH_NUANCE_ASR_END_POINT];
}

- (NSDictionary*)getMixpaneLoggingRatios {
    return [self getConfigValueForKey:CONFIG_KEYPATH_MIXPANEL_LOGGING_RATIOS];
}

@end
