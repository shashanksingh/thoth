//
//  FeedReaderService.m
//  Briefrr
//
//  Created by Shashank Singh on 2/25/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "FeedReaderService.h"
#import "TTS.h"
#import "Story.h"
#import "EventManager.h"
#import "Util.h"
#import <MediaPlayer/MediaPlayer.h>
#import "SourcesService.h"
#import "Source.h"
#import "UserPreferenceService.h"
#import "TTSUtteranceService.h"
#import "HTTPService.h"
#import "EventLogger.h"
#import "AppStateManager.h"

static const char* BACKGROUND_QUEUE_NAME = "com.briefrr.feed_reader_background_q";
static const NSUInteger TTS_AUDIO_PREFETCH_MAX = 3;

@interface FeedReaderService()<SpeechSynthesizerDelegate>
@property (nonatomic) FeedType feedType;
@property (nonatomic) BOOL reading;
@property (nonatomic, retain) NSArray *feed;
@property (nonatomic) int currentStoryIndex;
@property (nonatomic) BOOL hasReadCommandIntro;
@property (nonatomic) BOOL hasGreetedAtBeginFeed;
@property (nonatomic) BOOL utteranceAudioPrefetchPending;
@property (nonatomic, strong) NSMutableSet *readStories;
@end

@implementation FeedReaderService
- (instancetype)init {
    self = [super init];
    if (self) {
        self.utteranceAudioPrefetchPending = NO;
        self.hasReadCommandIntro = NO;
        self.hasGreetedAtBeginFeed = NO;
        self.feed = nil;
        self.readStories = [NSMutableSet set];
        [SpeechSynthesizer sharedInstance].delegate = self;
    }
    return self;
}

+ (FeedReaderService*)getInstance {
    static FeedReaderService *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[FeedReaderService alloc] init];
    });
    return sharedInstance;
}

-(dispatch_queue_t)getBackgroundQueue {
    static dispatch_once_t queueCreationGuard;
    static dispatch_queue_t queue;
    dispatch_once(&queueCreationGuard, ^{
        queue = dispatch_queue_create(BACKGROUND_QUEUE_NAME, DISPATCH_QUEUE_SERIAL);
    });
    return queue;
}

- (void)setFeed:(NSArray*)feed ofType:(FeedType)feedType {
    self.feed = feed;
    self.feedType = feedType;
    self.currentStoryIndex = -1;
    if (self.feed) {
        [self prefetchTTSAudio];
    }
}

- (void)setCurrentStoryIndex:(int)currentStoryIndex {
    int oldValue = _currentStoryIndex;
    _currentStoryIndex = currentStoryIndex;
    if (oldValue != currentStoryIndex) {
        [[EventManager getInstance] fireEvent:FEED_READER_STARTED_READING_STORY, @(currentStoryIndex)];
    }
}

- (void)prefetchTTSAudio {
    if (self.utteranceAudioPrefetchPending) {
        // TODO (shashank): log warning
        return;
    }
    
    NSMutableArray *utterancesToPrefetch = [NSMutableArray array];
    
    // Note (shashank): we include pre-fetching of user message
    // utterances in all utterance pre-fetches for simplictly and
    // robustness (just in case they are not already pre-fetched).
    // With caching in the pre-fetching code the only extra cost
    // because of this is the hash computation cost which we are
    // fine with.
    NSArray *userMessageUtterances = [self getUserMessageUtterances];
    [utterancesToPrefetch addObjectsFromArray:userMessageUtterances];
    
    int startIndex = self.currentStoryIndex >= 0 ? self.currentStoryIndex : 0;
    for (int i=startIndex; i<MIN(self.feed.count, startIndex + TTS_AUDIO_PREFETCH_MAX); ++i) {
        Story *story = self.feed[i];
        NSArray *utterances = [[TTSUtteranceService getInstance] getSpeechUtterancesForStory:story];
        [utterancesToPrefetch addObjectsFromArray:utterances];
    }
    
    self.utteranceAudioPrefetchPending = YES;
    __weak typeof(self) bself = self;
    [[SpeechSynthesizer sharedInstance] prefetchAudioSamplesForUtterances:utterancesToPrefetch withCallback:^(BOOL success) {
        // TODO (shashank): log
        if (!bself) {
            return;
        }
        bself.utteranceAudioPrefetchPending = NO;
    }];
}

- (NSArray*)getUserMessageUtterances {
    NSString *preamble = [self getPreamble];
    SpeechUtterance *preambleUtterance = [[TTSUtteranceService getInstance] getUserMessageUtterance:preamble];
    return @[preambleUtterance];
}

- (void)notifyReadingStateChange {
    // this delay ensures that SpeechSynthesizer's queue has had a chance to run
    // and the listeners of this event read the updated "isSpeaking" state from
    // it
    dispatch_async(dispatch_get_main_queue(), ^{
        [[EventManager getInstance] fireEvent:FEED_READER_READING_STATE_CHANGED];
    });
}

- (void)startReadingFromCurrentStory {
    NSInteger currentIndex = [AppStateManager getInstance].currentStoryIndex;
    [self startReadingFromIndex:currentIndex];
}

- (void)startReadingFromIndex:(NSUInteger)index {
    [[SpeechSynthesizer sharedInstance] clearUtteranceQueue];
    
    if (!self.feed) {
        // TODO (shashank): log error
        return;
    }
    if (index >= self.feed.count) {
        // TODO (shashank): log error
        return;
    }
    
    while (index < self.feed.count) {
        Story *story = self.feed[index];
        if ([self.readStories containsObject:story.guid]) {
            index++;
        } else {
            break;
        }
    }
    if (index >= self.feed.count) {
        // TODO (shashank): log error
        return;
    }
    
    if (index == 0) {
        NSString *preamble = [self getPreamble];
        if (!self.hasGreetedAtBeginFeed) {
            self.hasGreetedAtBeginFeed = YES;
        }
        if (!self.hasReadCommandIntro) {
            self.hasReadCommandIntro = YES;
        }
        if (preamble && preamble.length > 0) {
            [[FeedReaderService getInstance] speakImmediately:preamble];
        }
    }
    
    self.currentStoryIndex = (int)index;
    [self readCurrentStory];
    
    [self notifyReadingStateChange];
}

- (NSString*)getPreamble {
    NSString *preamble = @"";
    // the user has to manually go to saved feed, she knows what
    // she is expecting, no preamble
    if (self.feedType == SAVED_STORIES) {
        return preamble;
    }
    
    if (!self.hasGreetedAtBeginFeed) {
        preamble = [Util getGreetingForCurrentTime];
        NSString *userFirstName = [[UserPreferenceService getInstance] getUserFirstName];
        preamble = [Util formatString:preamble withParameters:@{@"first_name": userFirstName ? userFirstName : @""}];
        
        preamble = [preamble stringByAppendingString:@" "];
        
        preamble = [preamble stringByAppendingString:MESSAGE_PREAMBLE_NEWS_FEED_START];
        if ([self shouldReadCommandIntro]) {
            preamble = [preamble stringByAppendingString:@" "];
            preamble = [preamble stringByAppendingString:HELP_MESSAGE_COMMAND_INTRO];
        }
    }
    return preamble;
}

- (void)pauseReading {
    self.reading = NO;
    [[SpeechSynthesizer sharedInstance] pause];
    [self notifyReadingStateChange];
    [self updateNowPlayingInfoPlaybackRate];
}

- (void)resumeReading {
    self.reading = YES;
    [[SpeechSynthesizer sharedInstance] resume];
    [self notifyReadingStateChange];
    [self updateNowPlayingInfoPlaybackRate];
}

- (void)stopReading {
    self.reading = NO;
    self.currentStoryIndex = 0;
    [[SpeechSynthesizer sharedInstance] clearUtteranceQueue];
}

- (BOOL)isReading {
    return _reading;
}

- (BOOL)hasStarted {
    return self.currentStoryIndex >= 0;
}

- (void)readNextStory {
    [self markCurrentStoryRead];
    
    self.currentStoryIndex = MIN((int)(self.feed.count - 1), self.currentStoryIndex + 1);
    [[SpeechSynthesizer sharedInstance] clearUtteranceQueue];
    [self speakImmediately:COMMAND_FEEDBACK_NEXT_STORY];
    [self readCurrentStory];
}

- (void)readPreviousStory {
    self.currentStoryIndex = MAX(0, self.currentStoryIndex - 1);
    [[SpeechSynthesizer sharedInstance] clearUtteranceQueue];
    [self speakImmediately:COMMAND_FEEDBACK_PREVIOUS_STORY];
    [self readCurrentStory];
}

- (void)speechSyntheSizerStartedUtterance:(SpeechUtterance*)utterance {
    self.reading = YES;
    [self prefetchTTSAudio];
    
    // avoid updating the UI for summary as it will be the same as that for the headline.
    // this is important as loading the image asynchronously for the info for the summary
    // can cause the placeholder to unnecessarily appear temporarily
    NSDictionary *currentInfo = [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo;
    if (currentInfo) {
        int currentInfoTrackNumber = ((NSNumber*)currentInfo[MPMediaItemPropertyAlbumTrackNumber]).intValue;
        if (currentInfoTrackNumber == self.currentStoryIndex) {
            return;
        }
    }
    
    Story *currentStory = self.feed[self.currentStoryIndex];
    Source *currentStorySource = [[SourcesService getInstance] getSourceById:currentStory.sourceId];
    
    NSString *localImageURL = nil;
    if (![Util isRemoteURL:currentStory.thumbnail]) {
        localImageURL = currentStory.thumbnail;
    } else {
        localImageURL = currentStorySource.logoURL;
    }
    UIImage *image = [UIImage imageNamed:localImageURL];
    MPMediaItemArtwork *albumArt = nil;
    if (!image) {
        // TODO (shashank): log
    } else {
        image = [Util addShadowToUIImage:image];
        if (image) {
            albumArt = [[MPMediaItemArtwork alloc] initWithImage:image];
        } else {
            // TODO (shashank): log
        }
        
    }
    
    
    NSDictionary *info = @{ MPMediaItemPropertyMediaType: @(MPMediaTypePodcast),
                            MPMediaItemPropertyTitle: APP_NAME,
                            MPMediaItemPropertyArtist: currentStorySource.name,
                            MPMediaItemPropertyAlbumTitle: currentStory.title,
                            MPMediaItemPropertyAlbumTrackNumber: @(self.currentStoryIndex),
                            MPMediaItemPropertyAlbumTrackCount: @(self.feed.count)};
    
    NSMutableDictionary *mutableInfo = info.mutableCopy;
    if (albumArt) {
        mutableInfo[MPMediaItemPropertyArtwork] = albumArt;
    }
    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = mutableInfo;
    
    if (currentStory.thumbnail && ![localImageURL isEqualToString:currentStory.thumbnail]) {
        [self fetchAndSetNowPlayingInfoArtwork];
    }
}

- (void)fetchAndSetNowPlayingInfoArtwork {
    int storyIndex = self.currentStoryIndex;
    Story *currentStory = self.feed[self.currentStoryIndex];
    __weak typeof(self) bself = self;
    
    dispatch_async([self getBackgroundQueue], ^{
        if (!bself) {
            return;
        }
        // current story has changed already
        if (bself.currentStoryIndex != storyIndex) {
            return;
        }
        
        NSURL *url = [Util urlFromString:currentStory.thumbnail];
        NSError *error;
        NSData *imageData = [NSData dataWithContentsOfURL:url options:NSDataReadingMappedIfSafe error:&error];
        if (error) {
            NSDictionary *props = @{@"thumbnail_url": currentStory.thumbnail, @"story_guid": currentStory.guid, @"story_url": currentStory.link};
            [[EventLogger getInstance] logEvent:@"story_thumbnail_fetch_failed" properties:props];
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!bself) {
                return;
            }
            // current story has changed already
            if (bself.currentStoryIndex != storyIndex) {
                return;
            }
            
            if (!imageData) {
                return;
            }
            
            NSMutableDictionary *info = [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo.mutableCopy;
            UIImage *image = [UIImage imageWithData:imageData];
            image = [Util addShadowToUIImage:image];
            MPMediaItemArtwork *albumArt = [[MPMediaItemArtwork alloc] initWithImage:image];
            info[MPMediaItemPropertyArtwork] = albumArt;
            
            [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = info;
        });
    });
}

- (void)updateNowPlayingInfoPlaybackRate {
    NSMutableDictionary *info = [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo.mutableCopy;
    info[MPNowPlayingInfoPropertyPlaybackRate] = @([self isReading] ? 1 : 0.0);
    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = info;
}

- (void)speechSyntheSizerFinishedUtterance:(SpeechUtterance *)utterance wasPartiallyUttered:(BOOL)partial wasSkipped:(BOOL)skipped queuedUtteranceCount:(NSUInteger)queuedUtteranceCount {
    
    if (queuedUtteranceCount == 0) {
        self.reading = NO;
        [self markCurrentStoryRead];
        
        self.currentStoryIndex++;
        [self readCurrentStory];
    }
}

- (void)readCurrentStory {
    if (self.currentStoryIndex >= self.feed.count) {
        // TODO (shashank): handle end of feed
        return;
    }
    if (self.currentStoryIndex < 0) {
        self.currentStoryIndex = 0;
    }
    
    BOOL wasReading = self.reading;
    self.reading = YES;
    if (!wasReading) {
        [self notifyReadingStateChange];
    }
    
    Story *story = self.feed[self.currentStoryIndex];
    NSArray *utterances = [[TTSUtteranceService getInstance] getSpeechUtterancesForStory:story];
    for (SpeechUtterance *utterance in utterances) {
        [[SpeechSynthesizer sharedInstance] queueUtterance:utterance];
    }
}

- (void)speakImmediately:(NSString*)text {
    SpeechUtterance *utterance = [[TTSUtteranceService getInstance] getUserMessageUtterance:text];
    [[SpeechSynthesizer sharedInstance] speakImmediately:utterance];
}

- (BOOL)shouldReadCommandIntro {
    if (self.hasReadCommandIntro) {
        return NO;
    }
    
    NSUInteger numAppOpens = [[UserPreferenceService getInstance] getNumberOfTimesAppHasBeenOpened];
    double progressionValue = 1.0;
    // we'll never reach enough app opens for this while
    // loop to be a perf problem
    while (floorf(progressionValue) <= numAppOpens) {
        if (floorf(progressionValue) == numAppOpens) {
            return YES;
        }
        progressionValue *= 1.5;
    }
    return NO;
}

- (void)markCurrentStoryRead {
    if (self.currentStoryIndex >= self.feed.count) {
        return;
    }
    
    Story *currentStory = self.feed[self.currentStoryIndex];
    [self.readStories addObject:currentStory.guid];
    [[HTTPService getInstance] markStoryRead:currentStory];
}

@end
