//
//  TTSUtteranceService.h
//  Briefrr
//
//  Created by Shashank Singh on 3/27/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTS.h"
#import "Story.h"

@interface TTSUtteranceService : NSObject
+ (TTSUtteranceService*)getInstance;
- (SpeechUtterance*)getSpeechUtteranceForStoryHeadline:(Story*)story;
- (SpeechUtterance*)getSpeechUtteranceForStorySummary:(Story*)story;
- (NSArray*)getSpeechUtterancesForStory:(Story*)story;
- (SpeechUtterance*)getUserMessageUtterance:(NSString*)text;
@end
