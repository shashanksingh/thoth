//
//  BackgroundFetchService.m
//  Briefrr
//
//  Created by Shashank Singh on 3/27/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "BackgroundFetchService.h"
#import "SourcesService.h"
#import "FeedService.h"
#import "EventLogger.h"
#import "Story.h"
#import "TTSUtteranceService.h"
#import "TTSService.h"

static const int MAX_BACKGROUND_STORY_UTTERANCE_PREFETCH = 5;

@interface BackgroundFetchService()
@property (nonatomic) BOOL fetchInProgress;
@end

@implementation BackgroundFetchService
- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

+ (BackgroundFetchService*)getInstance {
    static BackgroundFetchService *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BackgroundFetchService alloc] init];
    });
    return sharedInstance;
}

- (void)fetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    [[EventLogger getInstance] logEvent:@"background_fetch_inited"];
    
    NSArray *selectedSourceIds = [[SourcesService getInstance] getSelectedSourceIds];
    [[FeedService getInstance] getFeedForSources:selectedSourceIds offset:0 limit:MAX_BACKGROUND_STORY_UTTERANCE_PREFETCH callback:^(NSArray *data) {
        
        NSMutableArray *utterances = [NSMutableArray array];
        for (Story *story in data) {
            NSArray *storyUtterances = [[TTSUtteranceService getInstance] getSpeechUtterancesForStory:story];
            [utterances addObjectsFromArray:storyUtterances];
        }
        
        [[TTSService sharedInstance] prefetchAudioSamplesForUtterances:utterances withCallback:^(BOOL success) {
            [[EventLogger getInstance] logEvent:@"background_fetch_finished" properties:@{@"success": @(success)}];
            completionHandler(UIBackgroundFetchResultNewData);
        }];
        
    } errorback:^(NSError *error) {
        // TODO (shashank): log error
        [[EventLogger getInstance] logEvent:@"background_fetch_failed" properties:@{@"reason": @"feed_fetch_failed", @"error": error}];
        // always tell iOS that new data was available. the hypothesis being
        // iOS will allow more chances to prefetch if it thinks each time there
        // is new data to be fetched.
        completionHandler(UIBackgroundFetchResultNewData);
    }];
}

@end
