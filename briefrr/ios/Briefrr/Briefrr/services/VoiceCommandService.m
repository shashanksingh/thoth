//
//  VoiceCommandService.m
//  Briefrr
//
//  Created by Shashank Singh on 3/11/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "VoiceCommandService.h"
#import "VCRService.h"
#import "Macros.h"
#import "FeedReaderService.h"
#import "Util.h"
#import "EventManager.h"
#import "IdlingManagementService.h"
#import "EventLogger.h"
#import "UserPreferenceService.h"
#import "AppStateManager.h"

static NSString* const COMMAND_WAKEUP_PHRASE = @"BRIEFER";

@implementation VoiceCommandService
- (instancetype)init {
    self = [super init];
    if (self) {
        self.pausedToListenForCommand = NO;
        [[VCRService sharedInstance] pauseRecognition];
        [[VCRService sharedInstance] setCommandsToRecognize:@[COMMAND_START_READER] withWakeUpPhrase:COMMAND_WAKEUP_PHRASE];
        [self setUpEventHandlers];
    }
    return self;
}

- (void)setUpEventHandlers {
    __weak typeof(self) bself = self;
    
    [[EventManager getInstance] addEventListener:FEED_READING_MODE_CHANGED withCallback:^(va_list args) {
        if (!bself) {
            return;
        }
        FeedReadingMode readingMode = ((NSNumber*)va_arg(args, NSNumber*)).intValue;
        if (readingMode == AUDIO) {
            [bself setCommands:@[COMMAND_START_READER]];
        }
    }];
    
    [[EventManager getInstance] addEventListener:VOICE_ACTIVITY_BEGIN withCallback:^(va_list args) {
        if (!bself) {
            return;
        }
        [bself onVoiceActivityBegin];
    }];
    [[EventManager getInstance] addEventListener:VOICE_ACTIVITY_NO_COMMAND_RECOGNIZED withCallback:^(va_list args) {
        if (!bself) {
            return;
        }
        [bself onVoiceActivityEndWithoutCommand];
    }];
    [[EventManager getInstance] addEventListener:COMMAND_RECOGNIZED withCallback:^(va_list args) {
        if (!bself) {
            return;
        }
        
        NSString *command = va_arg(args, NSString*);
        NSNumber *score = va_arg(args, NSNumber*);
        [bself onCommandRecognized:command withScore:score.intValue];
    }];
    
    NSDictionary *nonVoiceCommandToVoiceCommand = @{
                                                    @(NON_VOICE_COMMAND_PLAY_FEED): COMMAND_START_READER,
                                                    @(NON_VOICE_COMMAND_PAUSE_FEED): COMMAND_PAUSE_READER,
                                                    @(NON_VOICE_COMMAND_NEXT_STORY): COMMAND_NEXT_STORY,
                                                    @(NON_VOICE_COMMAND_PREVIOUS_STORY): COMMAND_PREVIOUS_STORY
                                                    };
    for (NSNumber *nonVoiceCommand in nonVoiceCommandToVoiceCommand) {
        EventType nonVoiceCommandEvent = nonVoiceCommand.intValue;
        NSString *voiceCommand = nonVoiceCommandToVoiceCommand[nonVoiceCommand];
        
        [[EventManager getInstance] addEventListener:nonVoiceCommandEvent withCallback:^(va_list args) {
            if (!bself) {
                return;
            }
            [bself onCommandRecognized:voiceCommand withScore:-1];
        }];
    }
    
    [[EventManager getInstance] addEventListener:NON_VOICE_COMMAND_TOGGLE_FEED_PLAY withCallback:^(va_list args) {
        if (!bself) {
            return;
        }
        if ([[FeedReaderService getInstance] isReading]) {
            [bself onCommandRecognized:COMMAND_PAUSE_READER withScore:-1];
        } else {
            [bself onCommandRecognized:COMMAND_CONTINUE_READER withScore:-1];
        }
    }];
}

+ (VoiceCommandService*)getInstance {
    static VoiceCommandService *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[VoiceCommandService alloc] init];
    });
    return sharedInstance;
}

- (void)startRecognition {
    [[VCRService sharedInstance] resumeRecognition];
}

- (void)pauseRecognition {
    [[VCRService sharedInstance] pauseRecognition];
}

- (void)onVoiceActivityBegin {
    [[EventLogger getInstance] logEvent:@"voice_activity_began"];
    [[IdlingManagementService getInstance] userInputReceived];
    
    [self notifyListeningStart];
    if ([[FeedReaderService getInstance] isReading]) {
        self.pausedToListenForCommand = YES;
    }
    [[FeedReaderService getInstance] pauseReading];
}

- (void)onVoiceActivityEndWithoutCommand {
    [[EventLogger getInstance] logEvent:@"voice_activity_ended_without_command"];
    if (self.pausedToListenForCommand) {
        self.pausedToListenForCommand = NO;
        [[FeedReaderService getInstance] resumeReading];
        [self notifySpeakingStart];
    }
}

- (void)onCommandRecognized:(NSString *)command withScore:(SInt32)score {
    command = [Util trimString:command];
    [[IdlingManagementService getInstance] userInputReceived];
    
    if ([command isEqualToString:COMMAND_START_READER]) {
        [self setCommands:@[COMMAND_PAUSE_READER, COMMAND_NEXT_STORY, COMMAND_PREVIOUS_STORY, COMMAND_SAVE_STORY]];
        [[FeedReaderService getInstance] startReadingFromCurrentStory];
        [self notifySpeakingStart];
    } else if ([command isEqualToString:COMMAND_PAUSE_READER]) {
        [self setCommands:@[COMMAND_CONTINUE_READER]];
        self.pausedToListenForCommand = NO;
        [[FeedReaderService getInstance] pauseReading];
        [self notifyListeningStart];
    } else if ([command isEqualToString:COMMAND_NEXT_STORY]) {
        [self setCommands:@[COMMAND_PAUSE_READER, COMMAND_NEXT_STORY, COMMAND_PREVIOUS_STORY, COMMAND_SAVE_STORY]];
        [[FeedReaderService getInstance] readNextStory];
        [self notifySpeakingStart];
    } else if ([command isEqualToString:COMMAND_PREVIOUS_STORY]) {
        [self setCommands:@[COMMAND_PAUSE_READER, COMMAND_NEXT_STORY, COMMAND_PREVIOUS_STORY, COMMAND_SAVE_STORY]];
        [[FeedReaderService getInstance] readPreviousStory];
        [self notifySpeakingStart];
    } else if ([command isEqualToString:COMMAND_CONTINUE_READER]) {
        [self setCommands:@[COMMAND_PAUSE_READER, COMMAND_NEXT_STORY, COMMAND_PREVIOUS_STORY, COMMAND_SAVE_STORY]];
        if (![[FeedReaderService getInstance] hasStarted]) {
            [[FeedReaderService getInstance] startReadingFromCurrentStory];
        } else {
            [[FeedReaderService getInstance] resumeReading];
        }
        [self notifySpeakingStart];
    } else if ([command isEqualToString:COMMAND_SAVE_STORY]) {
        NSArray *savedStories = [[UserPreferenceService getInstance] getSavedStories];
        if (!savedStories || savedStories.count == 0) {
            [[FeedReaderService getInstance] speakImmediately:COMMAND_FEEDBACK_FIRST_SAVE_STORY];
        } else {
            [[FeedReaderService getInstance] speakImmediately:COMMAND_FEEDBACK_SAVE_STORY];
        }
    } else if ([command isEqualToString:COMMAND_STOP_READER]) {
        [[AppStateManager getInstance] setFeedReadingMode:VISUAL];
    } else {
        [[EventLogger getInstance] logEvent:@"error_unhandled_recognized_command" properties:@{@"command": command, @"location": @"vcr_service_on_command_recognized"}];
    }
}

- (void)setCommands:(NSArray*)commands {
    [[VCRService sharedInstance] setCommandsToRecognize:commands withWakeUpPhrase:COMMAND_WAKEUP_PHRASE];
}

- (void)notifyListeningStart {
    [[EventManager getInstance] fireEvent:FEED_READER_INPUT_OUTPUT_STATE_CHANGED, @(LISTENING)];
}

- (void)notifySpeakingStart {
    [[EventManager getInstance] fireEvent:FEED_READER_INPUT_OUTPUT_STATE_CHANGED, @(SPEAKING)];
}

@end
