//
//  BackgroundFetchService.h
//  Briefrr
//
//  Created by Shashank Singh on 3/27/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BackgroundFetchService : NSObject
+ (BackgroundFetchService*)getInstance;
- (void)fetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;
@end
