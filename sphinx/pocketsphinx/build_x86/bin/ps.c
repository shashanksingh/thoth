#include "pocketsphinx.h"

static void
recognize_from_file(int argc, char*argv[])
{
    int16 adbuf[4096];
    const char *fname;
    const char *hyp;
    const char *uttid;
    int32 k;
    uint8 utt_started, in_speech;
    FILE *rawfd;
    ps_decoder_t *ps;

    fname = argv[1];
    if ((rawfd = fopen(fname, "rb")) == NULL) {
        printf("Failed to open file '%s' for reading",
                       fname);
        return;
    }
    
    cmd_ln_t *config = cmd_ln_init(NULL, ps_args(), TRUE,
                                   "-hmm", "/Users/shashank/projects/iris/speechrec/SpeechRecResources_EN_US/AcousticModel_semi",
                                   "-dict", "/Users/shashank/projects/iris/speechrec/SpeechRecResources_EN_US/LanguageModel.dic",
                                   "-kws", "/Users/shashank/temp/kws2.txt",
                                   "-kws_plp", "1e-10",
                                   "-kws_threshold", "1e10",
                                   "-vad_prespeech", "2",
                                   "-vad_postspeech", "100",
                                   "-cmn", "none",
                                   NULL);
    
    ps_default_search_args(config);
    ps = ps_init(config);
    
    /*ps_start_utt(ps, NULL);
    while ((k = fread(adbuf, sizeof(int16), 4096, rawfd)) > 0) {
        ps_process_raw(ps, adbuf, k, FALSE, FALSE);
    }
    ps_end_utt(ps);
    hyp = ps_get_hyp(ps, NULL, &uttid);
    printf("xx%s: %s\n", uttid, hyp);
    fclose(rawfd);
    return;*/
    
    utt_started = FALSE;
    ps_start_utt(ps, NULL);
    while ((k = fread(adbuf, sizeof(int16), 4096, rawfd)) > 0) {
        ps_process_raw(ps, adbuf, k, FALSE, FALSE);
        in_speech = ps_get_in_speech(ps);
        if (in_speech && !utt_started) {
            utt_started = TRUE;
        } 
        if (!in_speech && utt_started) {
            ps_end_utt(ps);
            hyp = ps_get_hyp(ps, NULL, &uttid);
            printf("%s: %s\n", uttid, hyp);
            ps_start_utt(ps, NULL);
            utt_started = FALSE;
        }
    }
    ps_end_utt(ps);
    if (utt_started) {
        hyp = ps_get_hyp(ps, NULL, &uttid);
        printf("%s: %s\n", uttid, hyp);
    }
    
    fclose(rawfd);
}

int main(int argc, char*argv[]) {
    recognize_from_file(argc, argv);
}
