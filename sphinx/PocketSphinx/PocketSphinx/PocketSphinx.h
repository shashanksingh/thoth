//
//  PocketSphinx.h
//  PocketSphinx
//
//  Created by Shashank Singh on 11/9/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PocketSphinx.
FOUNDATION_EXPORT double PocketSphinxVersionNumber;

//! Project version string for PocketSphinx.
FOUNDATION_EXPORT const unsigned char PocketSphinxVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PocketSphinx/PublicHeader.h>


