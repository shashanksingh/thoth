//
//  CarLocationService.h
//  CarLocator
//
//  Created by Shashank Singh on 1/31/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CarLocatorDelegate<NSObject>
-(void)onApproachingCar;
@end

@interface CarLocationService : NSObject
@property (nonatomic, strong) id<CarLocatorDelegate> delegate;

+(CarLocationService*)sharedInstance;
@end
