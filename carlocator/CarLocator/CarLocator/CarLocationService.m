//
//  CarLocationService.m
//  CarLocator
//
//  Created by Shashank Singh on 1/31/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import "CarLocationService.h"
#import <CoreMotion/CoreMotion.h>
#import <CoreLocation/CoreLocation.h>

@interface CarLocationService()<CLLocationManagerDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CMMotionManager *motionManager;
@end

@implementation CarLocationService
@synthesize delegate, motionManager;

+(CarLocationService*)sharedInstance {
    static CarLocationService *sharedInstance = nil;
    @synchronized(self) {
        if (sharedInstance == nil)
            sharedInstance = [[self alloc] init];
    }
    return sharedInstance;
}

-(CarLocationService*)init {
    self = [super init];
    if (self) {
        self.locationManager = [[CLLocationManager alloc] init];
        // TODO (shashank): use the non-deprecated version of this
        // setting
        //self.locationManager.purpose = @"imma track yo ass";
        self.locationManager.delegate = self;
        
        self.motionManager = [[CMMotionManager alloc] init];
        self.motionManager.
        
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        [self.locationManager startUpdatingLocation];
        
        [self.motionManager startAccelerometerUpdatesToQueue:NSOperationQueue() withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.xAxis.text = [NSString stringWithFormat:@"%.2f",accelerometerData.acceleration.x];
                self.yAxis.text = [NSString stringWithFormat:@"%.2f",accelerometerData.acceleration.y];
                self.zAxis.text = [NSString stringWithFormat:@"%.2f",accelerometerData.acceleration.z];
            });
            
        }];
        
        //self.motionManager.magnetometerUpdateInterval
    }
    return self;
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
}

-(void)onAccelerometerUpdate:(CMAccelerometerData*)accelerometerData error:(NSError*)error {
    
}


@end
