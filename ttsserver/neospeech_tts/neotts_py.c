#include <Python.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include "vt_eng.h"
#include "vt_bre.h"

typedef enum { false, true } bool;
typedef enum {US_ENGLISH, BRITISH_ENGLISH} TTSLanguage;

#define VOICE_ID_KATE 0
#define VOICE_ID_PAUL 1
#define VOICE_ID_JULIE 3
#define VOICE_ID_JAMES 4
#define VOICE_ID_ASHLEY 5
#define VOICE_ID_BRIDGET 0
#define VOICE_ID_HUGH 1

static int initializedSuccessfully = 0;
static PyObject *NeoTTSError;

static int usEngVoiceIds[] = {VOICE_ID_JAMES, VOICE_ID_KATE, VOICE_ID_PAUL, VOICE_ID_JULIE, VOICE_ID_ASHLEY};
static int brEngVoiceIds[] = {}; //{VOICE_ID_BRIDGET, VOICE_ID_HUGH};

static bool* voiceEngineLoadStateUSEng;
static bool* voiceEngineLoadStateBREng;

double clamp(double val) {
    if (val < 0) return -1;
    if (val > 1) return 1;
    return val;
}

bool isVoiceEngineLoaded(TTSLanguage lang, int voiceId) {
    bool *stateArray = lang == US_ENGLISH ? voiceEngineLoadStateUSEng : voiceEngineLoadStateBREng;
    if (!stateArray) {
        fprintf(stderr, "Error in checking if voice-engine is loaded, state array is null: %d, %d\n", lang, voiceId);
        return false;
    }
    
    int* voiceIdArray = lang == US_ENGLISH ? usEngVoiceIds : brEngVoiceIds;
    int numVoices = (lang == US_ENGLISH ? sizeof(usEngVoiceIds) : sizeof(brEngVoiceIds))/sizeof(int);
    int voiceIdIndex = -1;
    int i;
    for (i=0; i<numVoices; ++i) {
        if (voiceIdArray[i] == voiceId) {
            voiceIdIndex = i;
            break;
        }
    }
    
    if (voiceIdIndex == -1) {
        fprintf(stderr, "Error in checking if voice-engine is loaded, voiceId not found: voiceId: %d, lang: %d\n", voiceId, lang);
        return false;
    }
    
    return stateArray[voiceIdIndex];
}

int getRandomVoiceId(TTSLanguage lang) {
    
    bool *stateArray = lang == US_ENGLISH ? voiceEngineLoadStateUSEng : voiceEngineLoadStateBREng;
    if (!stateArray) {
        return -1;
    }
    
    int* voiceIdArray = lang == US_ENGLISH ? usEngVoiceIds : brEngVoiceIds;
    int numVoices = sizeof(lang == US_ENGLISH ? usEngVoiceIds : brEngVoiceIds)/sizeof(int);
    int numLoadedVoices = 0;
    int i;
    for (i=0; i<numVoices; ++i) {
        if (stateArray[i]) {
            numLoadedVoices++;
        }
    }
    if (numLoadedVoices == 0) {
        return -1;
    }
    
    int randomIndex = rand() % numLoadedVoices;
    int loadedVoiceIndex = -1;
    for (i=0; i<numVoices; ++i) {
        if (stateArray[i]) {
            loadedVoiceIndex++;
        }
        if (loadedVoiceIndex == randomIndex) {
            return voiceIdArray[i];
        }
    }
    return -1;
    
}

int initializeEngine(PyObject* args) {
    int result;
    int infoval;
    char *voiceDBDir;
    int i;
    
    PyArg_ParseTuple(args, "s", &voiceDBDir);
    
    // load us-english engine
    result = VT_GetTTSInfo_ENG(VT_LOAD_SUCCESS_CODE, NULL, &infoval, sizeof(int));
    if (result != VT_INFO_SUCCESS) {
        fprintf(stderr, "Error in getting english tts engine info: %d\n", result);
        return 1;
    }
    
    // load all available english voices
    int numUSEngVoices = sizeof(usEngVoiceIds)/sizeof(int);
    voiceEngineLoadStateUSEng = (bool*)malloc(numUSEngVoices * sizeof(bool));
    
    for (i=0; i<numUSEngVoices; ++i) {
        int voiceId = usEngVoiceIds[i];
        result = VT_LOADTTS_ENG(0, voiceId, voiceDBDir, NULL);
        
        bool success = result == infoval;
        voiceEngineLoadStateUSEng[i] = success;
        
        if (!success) {
            fprintf(stderr, "Error in loading US english voice(%d): %d\n", voiceId, result);
            PyErr_SetString(NeoTTSError, "Error in loaing US english voice");
            return 1;
        } else {
            printf("Successfully loaded US english voice: %d\n", voiceId);
        }
        
    }
    
    // load br-english engine
    result = VT_GetTTSInfo_BRE(VT_LOAD_SUCCESS_CODE, NULL, &infoval, sizeof(int));
    if (result != VT_INFO_SUCCESS) {
        fprintf(stderr, "Error in getting british english tts engine info: %d\n", result);
        return 1;
    }
    
    // load all available british english voices
    int numBREngVoices = sizeof(brEngVoiceIds)/sizeof(int);
    voiceEngineLoadStateBREng = (bool*)malloc(numBREngVoices * sizeof(bool));
    
    for (i=0; i<numBREngVoices; ++i) {
        int voiceId = brEngVoiceIds[i];
        result = VT_LOADTTS_BRE((int)0, voiceId, voiceDBDir, NULL);
        
        bool success = result == infoval;
        voiceEngineLoadStateBREng[i] = success;
        
        if (!success) {
            fprintf(stderr, "Error in loading british english voice(%d): %d\n", voiceId, result);
            PyErr_SetString(NeoTTSError, "Error in loaing British english voice");
        }
    }
    
    initializedSuccessfully = 1;
    return 0;
}

static PyObject* py_ttsToFile(PyObject* self, PyObject* args)
{
    char *text;
    char *outputFilePath;
    char *voice;
    double pitch;
    double speed;
    double volume;
    
    int voiceId;
    int result;
    
    if (!initializedSuccessfully) {
        PyErr_SetString(NeoTTSError, "Engine not initialized yet");
        return NULL;
    }
    
    
    PyArg_ParseTuple(args, "sssddd", &text, &outputFilePath, &voice, &pitch, &speed, &volume);
    printf("received arguments: %s, %s, %s, %.2f, %.2f, %.2f\n", text, outputFilePath, voice, pitch, speed, volume);
    
    pitch = (int)(50 + clamp(pitch) * 150);
    speed = (int)(50 + clamp(speed) * 350);
    volume = (int)(clamp(volume) * 500);
    
    TTSLanguage lang = US_ENGLISH;
    voiceId = -1;
    
    if (!voice) {
        voice = "default";
    }
    
    if (strcasecmp(voice, "james") == 0) {
        voiceId = VOICE_ID_JAMES;
    } else if (strcasecmp(voice, "kate") == 0) {
        voiceId = VOICE_ID_KATE;
    } else if (strcasecmp(voice, "paul") == 0) {
        voiceId = VOICE_ID_PAUL;
    } else if (strcasecmp(voice, "julie") == 0) {
        voiceId = VOICE_ID_JULIE;
    } else if (strcasecmp(voice, "ashley") == 0) {
        voiceId = VOICE_ID_ASHLEY;
    } else if (strcasecmp(voice, "bridget") == 0) {
        voiceId = VOICE_ID_BRIDGET;
        lang = BRITISH_ENGLISH;
    } else if (strcasecmp(voice, "hugh") == 0) {
        voiceId = VOICE_ID_HUGH;
        lang = BRITISH_ENGLISH;
    }
    
    
    if (voiceId != -1 && !isVoiceEngineLoaded(lang, voiceId)) {
        fprintf(stderr, "error in converting text file to wav file: voice-engine not loaded for voice: %s, using random\n", voice);
        voiceId = -1;
    }
    
    if (voiceId == -1) {
        voiceId = getRandomVoiceId(lang);
    }
    
    short (*textToFileFunction) (int fmt,
                                char * tts_text, char * filename, int nSpeakerID, int pitch,
                                int speed,
                                int volume, int pause,
                                int dictidx, int texttype);
    //void (*engineUnloadFunction) (int speakerID);
    
    
    if (lang == US_ENGLISH) {
        textToFileFunction = VT_TextToFile_ENG;
        //engineUnloadFunction = VT_UNLOADTTS_ENG;
    } else {
        textToFileFunction = VT_TextToFile_BRE;
        //engineUnloadFunction = VT_UNLOADTTS_BRE;
    }
    
    result = textToFileFunction(VT_FILE_API_FMT_S16PCM_WAVE, text,
                               outputFilePath, voiceId, pitch, speed, volume, -1, -1, -1);
    
    if (result != VT_FILE_API_SUCCESS) {
        fprintf(stderr, "error in converting text file to wav file: %d, %s", result, text);
        //engineUnloadFunction(voiceId);
        //initializedSuccessfully = 0;
        PyErr_SetString(NeoTTSError, "Error in converting text to file");
        return NULL;
    } else {
        result = 0;
    }

    return Py_BuildValue("i", result);
}

static PyObject* py_initEngine(PyObject* self, PyObject* args) {
    int result = initializeEngine(args);
    if (result != 0) {
        return NULL;
    }
    return Py_BuildValue("i", result);
}

static PyMethodDef moduleMethods[] = {
    {"initialize_engine", py_initEngine, METH_VARARGS},
    {"tts_to_file", py_ttsToFile, METH_VARARGS},
    {NULL, NULL}
};

PyMODINIT_FUNC
initneotts(void)
{
    PyObject *module;
    
    module = Py_InitModule3("neotts", moduleMethods, "NeoSpeech TTS Server Python bindings");
    if (module == NULL) {
        return;
    }
    
    NeoTTSError = PyErr_NewException("neotts.NeoTTSError", NULL, NULL);
    Py_INCREF(NeoTTSError);
    PyModule_AddObject(module, "NeoTTSError", NeoTTSError);
}
