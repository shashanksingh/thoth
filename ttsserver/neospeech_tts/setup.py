from distutils.core import setup, Extension

extension = Extension('neotts', ['neotts_py.c'], include_dirs=["inc"], library_dirs=["lib"], libraries=["vt_eng", "vt_bre"], runtime_library_dirs=["$ORIGIN/../../lib"])
setup(name='neotts', version='1.0',  ext_modules=[extension])
