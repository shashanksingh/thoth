import sys
import twisted.internet.defer as defer
from twisted.web.static import File
import klein
import futures
import hashlib
import os.path
import logging
import subprocess
import threading
import shlex
import neotts
import traceback

MAX_CONCURRENT_TTS_CALLS = 2
REQUEST_HASH_KEY_JOIN_KEY = 'd45b97ea-be6e-4123-aed3-ebbc3754d132'
RAW_AUDIO_FILE_EXTENSION = '.wav'
COMPRESSED_AUDIO_FILE_EXTENSION = '.m4a'
LOG_FILE_EXTENSION = '.log'
AUDIO_CONTENT_TYPE = 'audio/aacp'
FFMPEG_CONVERSION_COMMAND = 'ffmpeg/ffmpeg/build/bin/ffmpeg -y -i "%(input_file_path)s" -c:a libfdk_aac -b:a 64k "%(output_file_path)s"'
ENVIRONMENT_VARIABLES = {'LD_LIBRARY_PATH': 'ffmpeg/fdk-aac/build/lib'}

class TTSConverter(object):
    def __init__(self, neo_data_dir_path, audio_data_cache_dir_path):
        if neotts.initialize_engine(neo_data_dir_path):
            raise Exception('failed to initialize tts engine')

        self.audio_data_cache_dir_path = audio_data_cache_dir_path
        self.executor = futures.ThreadPoolExecutor(max_workers=MAX_CONCURRENT_TTS_CALLS)

    @staticmethod
    def get_tts_to_file_request_hash(text, voice, pitch, speed, volume):
        key = REQUEST_HASH_KEY_JOIN_KEY.join([text, voice, str(pitch), str(speed), str(volume)])
        md5 = hashlib.md5()
        md5.update(key)
        request_hash = md5.hexdigest()
        return request_hash

    @staticmethod
    def convert(file_path, text, voice, pitch, speed, volume):
        try:
            neotts.tts_to_file(text, file_path, voice, pitch, speed, volume)
            return file_path
        except Exception, e:
            logging.error('caught exception in converting text to speech: %s', traceback.format_exc())
            return None

    @staticmethod
    def compress_audio_file(raw_file_path, compressed_file_path, log_file_path):
        command = FFMPEG_CONVERSION_COMMAND%({'input_file_path': raw_file_path,
                                              'output_file_path': compressed_file_path
                                             })
        exit_code = subprocess.call(shlex.split(command), env=ENVIRONMENT_VARIABLES)
        if exit_code != 0:
            message = 'error in compressing output audio, exit code: %d, check log at: %s' % (exit_code, log_file_path)
            logging.error(message)
            raise Exception(message)

    def get_raw_audio_file_path(self, request_hash):
        raw_audio_file_name = request_hash + RAW_AUDIO_FILE_EXTENSION
        return os.path.join(self.audio_data_cache_dir_path, raw_audio_file_name)

    def get_compressed_audio_file_path(self, request_hash):
        compressed_audio_file_name = request_hash + COMPRESSED_AUDIO_FILE_EXTENSION
        return os.path.join(self.audio_data_cache_dir_path, compressed_audio_file_name)

    def get_compression_log_file_path(self, request_hash):
        log_file_name = request_hash + LOG_FILE_EXTENSION
        return os.path.join(self.audio_data_cache_dir_path, log_file_name)

    def convert_text_file(self, text, voice, pitch, speed, volume):
        logging.info('received conversion request: %s, %s, %.1f, %.1f, %.1f', text, voice, pitch, speed, volume)

        request_hash = self.__class__.get_tts_to_file_request_hash(text, voice, pitch, speed, volume)
        compressed_audio_file_path = self.get_compressed_audio_file_path(request_hash)

        deferred = defer.Deferred()
        if os.path.isfile(compressed_audio_file_path):
            logging.info('found cached audio file for request at: %s, %s, %s, %.1f, %.1f, %.1f', compressed_audio_file_path, text,
                         voice, pitch, speed, volume)
            deferred.callback(compressed_audio_file_path)
            return deferred

        raw_audio_file_path = self.get_raw_audio_file_path(request_hash)
        log_file_path = self.get_compression_log_file_path(request_hash)
        future = self.executor.submit(self.__class__.convert, raw_audio_file_path, text, voice, pitch, speed, volume)

        def done_callback(_future):
            future.done()
            result = future.result()
            logging.info('conversion finished: %s, %s', raw_audio_file_path, result)

            if not result:
                deferred.callback(None)
            else:
                self.__class__.compress_audio_file(raw_audio_file_path, compressed_audio_file_path, log_file_path)
                deferred.callback(compressed_audio_file_path)

        future.add_done_callback(done_callback)

        return deferred


class BadRequest(Exception):
    pass

class InternalError(Exception):
    pass

class TTSService(object):
    app = klein.Klein()

    def __init__(self, tts_converter):
        self.tts_converter = tts_converter

    @app.handle_errors(BadRequest)
    def bad_request(self, request, failure):
        request.setResponseCode(400)
        return 'bad request: %s\n'%(failure.getErrorMessage())

    @app.handle_errors(InternalError)
    def internal_error(self, request, failure):
        request.setResponseCode(500)
        return 'internal error: %s\n'%(failure.getErrorMessage())

    @app.route("/tts", methods=['POST'])
    @defer.inlineCallbacks
    def tts(self, request):
        params = {
            'text': None,
            'voice': 'default',
            'pitch': -1,
            'speed': -1,
            'volume': -1
        }

        text_values = request.args.get('text', None)
        if not text_values or len(text_values) == 0:
            raise BadRequest("missing parameter 'text'")
        params['text'] = (text_values[0]).strip()
        if not params['text']:
            raise BadRequest("text may not be empty")

        voice_values = request.args.get('voice', None)
        if voice_values and len(voice_values) > 0:
            params['voice'] = voice_values[0]

        for param in ['pitch', 'speed', 'volume']:
            values = request.args.get(param, None)
            if values and len(values) > 0:
                try:
                    params[param] = float(values[0])
                except ValueError, e:
                    raise BadRequest(e.message)

        file_path = yield self.tts_converter.convert_text_file(**params)
        if not file_path:
            raise InternalError("TTS conversion failed")

        request.responseHeaders.setRawHeaders("Content-Type", [AUDIO_CONTENT_TYPE])
        request.responseHeaders.setRawHeaders("Content-Length", [os.stat(file_path).st_size])

        # TODO(shashank): chunked transfer
        with open(file_path) as source:
            defer.returnValue(source.read())


def start_server():
    port = int(sys.argv[1])
    neo_data_dir_path = sys.argv[2]
    audio_data_cache_dir_path = sys.argv[3]

    logging.basicConfig(level=logging.DEBUG)

    tts_converter = TTSConverter(neo_data_dir_path, audio_data_cache_dir_path)
    tts_service = TTSService(tts_converter)
    tts_service.app.run(host='0.0.0.0', port=port)

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print "usage: <executable> <port> <neo_data_dir_path> <audio_data_cache_dir_path>"
    else:
        start_server()


