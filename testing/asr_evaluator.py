# -*- coding: utf-8 -*-

import sys
import numpy
from itertools import *
import subprocess
import re
import random

def grouper(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return izip_longest(*args, fillvalue=fillvalue)

def get_hypotheses(samples):
    output = subprocess.check_output(["/Users/shashank/projects/iris/sphinx/pocketsphinx/build_x86/bin/pocketsphinx_continuous",
                                      "-hmm", "/Users/shashank/projects/iris/speechrec/SpeechRecResources_EN_US/AcousticModel_semi",
                                      "-dict", "/Users/shashank/projects/iris/speechrec/SpeechRecResources_EN_US/LanguageModel.dic",
                                      "-kws", "/Users/shashank/temp/kws.txt",
                                      "-infile", "/Users/shashank/temp/recording.wav",
                                      "-kws_plp", "1e-10", "-kws_threshold", "1e0", "-cmn", "none"])

    matches = re.findall(r'^0000\d+:\s(.+)', output, re.MULTILINE)
    if matches is None:
        return ('noise',)

    hyps = matches
    print 'before filtering', hyps
    hyps = [re.sub(r'^START ',  '', hyp.replace('BRIEFER', '')) for hyp in hyps if not hyp == '(null)']
    print 'after filtering', hyps
    if len(hyps) == 0:
        return ('noise',)
    return hyps

def process_audio_data_before_recognition(samples):
    # if there are more than one spikes remove the first
    # assuming it is the wakeup keyword
    print "input samples size:", len(samples)
    processed_samples = samples[:]

    WINDOW_SIZE = 256
    MIN_INTERVAL_FOR_VALID_UTTERANCE = 0.256
    VAD_MIN = 1600
    windows = list(grouper(samples, WINDOW_SIZE, 0))

    print 'windows', len(windows)

    last_window_avg = 0
    speech_start_window_indices = []
    speech_end_window_indices = []
    window_averages = []

    for index, window in enumerate(windows):
        total = 0
        for sample in window:
            total += sample ** 2

        cur_window_avg = (total/len(window)) ** 0.5
        window_averages.append(cur_window_avg)

        #print "cur_window_avg", cur_window_avg
        if last_window_avg < VAD_MIN and cur_window_avg >= VAD_MIN:
            speech_start_window_indices.append(index)

            print "window avg", cur_window_avg
        elif last_window_avg >= VAD_MIN and cur_window_avg < VAD_MIN:
            speech_end_window_indices.append(index)


        last_window_avg = cur_window_avg

    if len(speech_end_window_indices) < len(speech_start_window_indices):
        speech_end_window_indices.append(len(windows) - 1)

    print 'speech_start_window_indices', speech_start_window_indices, speech_end_window_indices
    print 'average across windows', sum(window_averages)/len(window_averages)
    # samples has too many ups and downs, assumed to be non-voice
    '''if len(speech_start_window_indices) > 10:
        for i in range(len(processed_samples)):
            processed_samples[i] = numpy.int16(0)
        return processed_samples'''

    merge_intervals_closer_than = (MIN_INTERVAL_FOR_VALID_UTTERANCE * 16000)/ WINDOW_SIZE
    speech_windows = []
    i = 0
    num_windows = len(speech_start_window_indices)
    while (i < num_windows):
        j = i
        while (j < num_windows - 1):
            if speech_start_window_indices[j + 1] - speech_end_window_indices[j] <= merge_intervals_closer_than:
                j += 1
            else:
                break
        speech_windows.append((speech_start_window_indices[i], speech_end_window_indices[j]))

        i = j + 1



    print "speech_windows", speech_windows
    '''if len(speech_windows) > 0:
        for i in range(0, speech_windows[:2][-1][0] * WINDOW_SIZE - 16 * WINDOW_SIZE):
            processed_samples[i] = numpy.int16(0)
        for i in range((speech_windows[-1][1] + 40) * WINDOW_SIZE, len(processed_samples)):
            processed_samples[i] = numpy.int16(0)
    '''

    print "output samples size:", len(processed_samples)
    #return processed_samples
    return processed_samples[(max(0, speech_windows[:2][-1][0] - 40)) * WINDOW_SIZE : (speech_windows[-1][1] + 20) * WINDOW_SIZE]

def create_file(a, b, data):
    samples = []
    print a, b, (int((a - 0.2) * 16000), int(b*16000))
    for i in range(int((a - 0.2) * 16000), int(b*16000)):
        samples.append(data[i])

    '''with open("/Users/shashank/temp/recording_raw.wav", "wb") as f:
        for sample in samples:
            f.write(sample)
'''
    #samples = process_audio_data_before_recognition(samples)

    with open("/Users/shashank/temp/recording.wav", "wb") as f:
        for sample in samples:
            f.write(sample)

    return samples

def get_segments(transcript_file_path):
    with open(transcript_file_path, 'r') as f:
        transcript = [l for l in f.read().split('\n') if l]

    # first silence is useless
    if transcript[0].startswith('silence:'):
        transcript = transcript[1:]

    rv = []
    for segment in list(grouper(transcript, 2)):
        assert segment[0].startswith('speech:'), ("first line should start with speech:", segment[0])
        assert segment[1].startswith('silence:'), ("second line should start with silence:", segment[1])
        start = float(segment[0].split('speech:')[1].split(',')[0])
        end = float(segment[1].split('silence:')[1].split(',')[0])
        truth = len(segment[1].split('(')) > 1 and segment[1].split('(')[1].split(',')[0] or 'N/A'

        rv.append((start, end, truth))

    return rv

def do_it(wav_file_path, transcript_file_path):
    data = numpy.memmap(wav_file_path, dtype='h', mode='r')
    
    hyps = []
    segments = get_segments(transcript_file_path)
    print segments
    for idx, (start, end, truth) in enumerate(segments[24:25]):
        print "processing segment", idx
        samples = create_file(start, end, data)
        avg_pow = (sum([s ** 2 for s in samples])/len(samples)) ** 0.5
        print 'avg_pow', avg_pow

        try:
            hyps.append((start, end, truth, get_hypotheses(samples), avg_pow))
        except subprocess.CalledProcessError as e:
            print 'error in getting hyp for start, end', start, end, 'ignoring'

    for idx, hyp in enumerate(hyps):
        print idx, hyp[0], hyp[1], hyp[2], hyp[3], hyp[4], hyp[2].upper().strip() == hyp[3][0].upper().strip() and '√' or 'X'

    success = len(filter(lambda x: x[2].upper().strip() == x[3][0].upper().strip(), hyps))
    print "success rate %d/%d = %.2f"%(success, len(hyps), (success * 100.0/len(hyps)))  


if __name__ == '__main__':
    do_it(sys.argv[1], sys.argv[2])
