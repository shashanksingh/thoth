//
//  main.m
//  cmd
//
//  Created by Shashank Singh on 1/17/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "VADetector.h"
#import "WITVad.h"
#include <Accelerate/Accelerate.h>

static const UInt8 WINDOW_SIZE = 128;
static const UInt8 STRIDE = 32;

static float getSpectralFlatnessMeasure(SInt16 *samples, long numSamples) {
    FFTSetup fftSetup = vDSP_create_fftsetup(round(log2(WINDOW_SIZE)), kFFTRadix2);

    float *windows = (float*)malloc(WINDOW_SIZE * sizeof(float));
    vDSP_hamm_window(windows, WINDOW_SIZE, 0);
    
    float *outputBuffer = (float*)malloc(WINDOW_SIZE * sizeof(float));
    float *totalPowerBuffer = (float*)malloc(WINDOW_SIZE/2 * sizeof(float));
    
    COMPLEX_SPLIT fftOutput;
    fftOutput.realp = (float *)malloc(WINDOW_SIZE/2 * sizeof(float));
    fftOutput.imagp = (float *)malloc(WINDOW_SIZE/2 * sizeof(float));
    
    float scale = (float) 1.0 / (2 * WINDOW_SIZE);
    
    UInt32 numWindows = 0;
    for (UInt32 i=0; i<numSamples - WINDOW_SIZE; i += STRIDE) {
        
        numWindows++;
        
        // convert SInt16 samples to float samples
        vDSP_vflt16(samples + i, 1, outputBuffer, 1, WINDOW_SIZE);
        
        // apply hanning windowing
        vDSP_vmul(outputBuffer, 1, windows, 1, outputBuffer, 1, WINDOW_SIZE);
        
        // split input into complex
        vDSP_ctoz((COMPLEX*)outputBuffer, 2, &fftOutput, 1, WINDOW_SIZE/2);
        
        // do the fft
        vDSP_fft_zrip(fftSetup, &fftOutput, 1, round(log2(WINDOW_SIZE)), FFT_FORWARD);
        
        // scale
        vDSP_vsmul(fftOutput.realp, 1, &scale, fftOutput.realp, 1, WINDOW_SIZE/2);
        vDSP_vsmul(fftOutput.imagp, 1, &scale, fftOutput.imagp, 1, WINDOW_SIZE/2);
        
        // zero out the nyquist value
        fftOutput.imagp[0] = 0.0;
        
        // get the power values
        vDSP_zvmags(&fftOutput, 1, outputBuffer, 1, WINDOW_SIZE/2);
        
        for (UInt32 j=0; j<WINDOW_SIZE/2; ++j) {
            totalPowerBuffer[j] += outputBuffer[j];
        }
    }
    
    printf("numWindows: %u\n", (unsigned int)numWindows);
    
    // take the simple average of powers for each frequency bin
    for (UInt32 i=0; i<WINDOW_SIZE/2; ++i) {
        totalPowerBuffer[i] /= numWindows;
        printf("totalPowerBuffer %u: %.2f\n", (unsigned int)i, totalPowerBuffer[i]);
    }
    
    double arithmeticMean = 0.0;
    double geometricMean = 0.0;
    
    for (UInt32 i=0; i<WINDOW_SIZE/2; ++i) {
        float power = fabs(totalPowerBuffer[i]);
        if (power != 0 && power == power) {
            arithmeticMean += power;
            geometricMean += log(power);
        }
    }
    
    arithmeticMean = arithmeticMean/(WINDOW_SIZE/2);
    geometricMean = exp(geometricMean/(WINDOW_SIZE/2));
    
    float sfm = fabs(10 * log10(geometricMean / arithmeticMean));
    NSLog(@"sfm: %.2f", sfm);
    return sfm;
}

static void getSamples(NSString *filePath, SInt16 **samples, long *numSamples) {
    NSURL *url = [NSURL fileURLWithPath:filePath];
    ExtAudioFileRef fileRef = NULL;
    OSStatus error = ExtAudioFileOpenURL((__bridge CFURLRef)url, &fileRef);
    if (error != noErr) {
        NSLog(@"error in reading file: %d", error);
        return;
    }
    
    AudioStreamBasicDescription clientASBD = {
        .mSampleRate = 16000,
        .mFormatID = kAudioFormatLinearPCM,
        .mFormatFlags = kAudioFormatFlagsNativeEndian | kAudioFormatFlagIsPacked | kAudioFormatFlagIsSignedInteger,
        .mBitsPerChannel = 16,
        .mChannelsPerFrame = 1,
        .mFramesPerPacket = 1,
        .mBytesPerFrame = 2,
        .mBytesPerPacket = 2
    };
    
    error = ExtAudioFileSetProperty(fileRef, kExtAudioFileProperty_ClientDataFormat, sizeof(clientASBD), &clientASBD);
    if(error != noErr) {
        NSLog(@"error in setting audio file property: %d", error);
        *samples = NULL;
        *numSamples = 0;
        return;
    }
    
    SInt64 numFrames = 0;
    UInt32 sizeSize = sizeof(numFrames);
    error = ExtAudioFileGetProperty(fileRef, kExtAudioFileProperty_FileLengthFrames, &sizeSize, &numFrames);
    if (error != noErr) {
        NSLog(@"error in getting audio file size: %d", error);
        *samples = NULL;
        *numSamples = 0;
        return;
    }
    
    NSLog(@"num frames in file: %lld", numFrames);
    
    size_t numBytes = numFrames * clientASBD.mBytesPerFrame;
    
    SInt16 *data = malloc(numBytes);
    AudioBuffer buffer = {
        .mNumberChannels = 1,
        .mDataByteSize = numBytes,
        .mData = data
    };
    
    
    AudioBufferList bufferList;
    bufferList.mNumberBuffers = 1;
    bufferList.mBuffers[0] = buffer;
    
    UInt32 framesRead = numFrames;
    error = ExtAudioFileRead(fileRef, &framesRead, &bufferList);
    if(error != noErr) {
        NSLog(@"error in reading audio data from file: %d", error);
        *samples = NULL;
        *numSamples = 0;
        return;
    }
    
    NSLog(@"frames of audio read: %d", framesRead);
    
    ExtAudioFileDispose(fileRef);
    *samples = data;
    NSLog(@"num frames: %d", numFrames);
    *numSamples = numFrames;
    
    return;
}

@interface WITHandler : NSObject <WITVadDelegate>
@end

@implementation WITHandler
-(void) vadStartedTalking {
    NSLog(@"started talking");
}
-(void) vadStoppedTalking {
    NSLog(@"stopped talking");
}
@end



int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
        
        
        
        SInt16 *samples = NULL;
        long numSamples = -1;
        
        
        
        WITVad *vad = [[WITVad alloc] init];
        WITHandler *witHandler = [[WITHandler alloc] init];
        vad.delegate = witHandler;
        
        NSData *samplesData;
        
        /*getSamples(@"/Users/shashank/projects/iris/testing/classifier/noise.wav", &samples, &numSamples);
        NSLog(@"samples: %d, numSamples: %l", samples[0], numSamples);
        
        samplesData = [NSData dataWithBytes:samples length:numSamples*2];
        [vad gotAudioSamples:samplesData];*/
        
        getSamples(@"/Users/shashank/projects/iris/testing/classifier/short_speech.wav", &samples, &numSamples);
        printf("start samples\n");
        for (UInt32 i=1; i<numSamples - 1; i++) {
            printf("%d ", samples[i]);
            if (abs(samples[i]) == 30000) {
                samples[i] = (samples[i - 1] + samples[i + 1])/2;
            }
        }
        printf("\nend samples\n");
        VADetector *detector = [[VADetector alloc] init];
        [detector getAutoCorrelationBasedVAD:samples numSamples:numSamples];
        [detector getSpectralFlatnessBasedVAD:samples numSamples:numSamples];
        [detector getZeroCrossingRateBasedVAD:samples numSamples:numSamples];
        [detector samplesHaveVoiceActivity:samples numSamples:numSamples withOutputMuted:NO];
        return 0;
        
        NSLog(@"samples: %d, numSamples: %l", samples[0], numSamples);
        getSpectralFlatnessMeasure(samples, numSamples);
        
        samplesData = [NSData dataWithBytes:samples length:numSamples*2];
        [vad gotAudioSamples:samplesData];
        
    }
    return 0;
}
