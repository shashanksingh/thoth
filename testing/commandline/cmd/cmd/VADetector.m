//
//  FrequencyVADetector.m
//  SpeechRec
//
//  Created by Shashank Singh on 12/18/14.
//  Copyright (c) 2014 Shashank Singh. All rights reserved.
//

#import "VADetector.h"
#include <Accelerate/Accelerate.h>

static const UInt16 WINDOW_SIZE = 128;
static const UInt8 WINDOW_STRIDE = 32;
static const Float32 ADJUST_0_DB = 1.5849e-13;

@interface VADetector()
@property (nonatomic) FFTSetup fftSetup;
@property (nonatomic) float *windows;
@property (nonatomic) float *outputBuffer;
@property (nonatomic) float *totalPowerBuffer;
@property (nonatomic) COMPLEX_SPLIT fftOutput;
@end

@implementation VADetector
@synthesize fftSetup, windows, outputBuffer, totalPowerBuffer, fftOutput;

-(VADetector*)init {
    self = [super init];
    if (self) {
        self.fftSetup = vDSP_create_fftsetup(round(log2(WINDOW_SIZE)), kFFTRadix2);
        
        self.windows = (float*)malloc(WINDOW_SIZE * sizeof(float));
        vDSP_hamm_window(self.windows, WINDOW_SIZE, 0);
        
        self.outputBuffer = (float*)malloc(WINDOW_SIZE * sizeof(float));
        self.totalPowerBuffer = (float*)malloc(WINDOW_SIZE/2 * sizeof(float));
        
        fftOutput.realp = (float *)malloc(WINDOW_SIZE/2 * sizeof(float));
        fftOutput.imagp = (float *)malloc(WINDOW_SIZE/2 * sizeof(float));
    }
    return self;
}

-(void)dealloc {
    if (self.windows) {
        free(self.windows);
    }
    if (self.outputBuffer) {
        free(self.outputBuffer);
    }
    if (fftOutput.realp) {
        free(fftOutput.realp);
    }
    if(fftOutput.imagp) {
        free(fftOutput.imagp);
    }
}

-(void)resetBuffers {
    memset(self.outputBuffer, 0, WINDOW_SIZE * sizeof(float));
    memset(self.totalPowerBuffer, 0, WINDOW_SIZE/2 * sizeof(float));
    memset(fftOutput.realp, 0, WINDOW_SIZE/2 * sizeof(float));
    memset(fftOutput.imagp, 0, WINDOW_SIZE/2 * sizeof(float));
}

-(BOOL)samplesHaveVoiceActivity:(SInt16*)samples numSamples:(UInt32)numSamples
                withOutputMuted:(BOOL)isOutputMuted {
    
    [self resetBuffers];
    
    if (numSamples < WINDOW_SIZE) {
        NSLog(@"Too few samples, at least %d samples required", WINDOW_SIZE);
        return YES;
    }
    
    long sum = 0;
    for (int i=0; i<numSamples; i++) {
        sum += samples[i];
    }
    printf("samples sum: %ld\n", sum);
    
    Float32 one = 1;
    float scale = (float) 1.0 / (2 * WINDOW_SIZE);
    
    UInt32 numWindows = 0;
    for (UInt32 i=0; i<numSamples - WINDOW_SIZE; i += WINDOW_STRIDE) {
        
        numWindows++;
        
        // convert SInt16 samples to float samples
        vDSP_vflt16(samples + i, 1, self.outputBuffer, 1, WINDOW_SIZE);
        
        // apply hanning windowing
        vDSP_vmul(self.outputBuffer, 1, self.windows, 1, self.outputBuffer, 1, WINDOW_SIZE);
        
        // split input into complex
        vDSP_ctoz((COMPLEX*)self.outputBuffer, 2, &fftOutput, 1, WINDOW_SIZE/2);
        
        // do the fft
        vDSP_fft_zrip(self.fftSetup, &fftOutput, 1, round(log2(WINDOW_SIZE)), FFT_FORWARD);
        
        // scale
        vDSP_vsmul(fftOutput.realp, 1, &scale, fftOutput.realp, 1, WINDOW_SIZE/2);
        vDSP_vsmul(fftOutput.imagp, 1, &scale, fftOutput.imagp, 1, WINDOW_SIZE/2);
        
        // zero out the nyquist value
        fftOutput.imagp[0] = 0.0;
        
        // get the power values
        vDSP_zvmags(&fftOutput, 1, self.outputBuffer, 1, WINDOW_SIZE/2);
        
        for (UInt32 j=0; j<WINDOW_SIZE/2; ++j) {
            self.totalPowerBuffer[j] += self.outputBuffer[j];
        }
    }
    
    printf("numWindows: %u\n", (unsigned int)numWindows);
    
    // take the simple average of powers for each frequency bin
    for (UInt32 i=0; i<WINDOW_SIZE/2; ++i) {
        self.totalPowerBuffer[i] /= numWindows;
        //printf("totalPowerBuffer %u: %.2f\n", (unsigned int)i, self.totalPowerBuffer[i]);
    }
    
    
    
    // in order to avoid taking log10 of zero, an adjusting factor is added in to make the minimum value equal -128dB
    vDSP_vsadd(self.totalPowerBuffer, 1, &ADJUST_0_DB, self.totalPowerBuffer, 1, WINDOW_SIZE/2);
    
    // convert the fft data to dB
    vDSP_vdbcon(self.totalPowerBuffer, 1, &one, self.totalPowerBuffer, 1, WINDOW_SIZE/2, 0);
    
    // move the origin so that all decible values are non-negative
    float minDecibleValue = +INFINITY;
    vDSP_minv(self.totalPowerBuffer, 1, &minDecibleValue, WINDOW_SIZE/2);
    minDecibleValue = -minDecibleValue;
    vDSP_vsadd(self.totalPowerBuffer, 1, &minDecibleValue, self.totalPowerBuffer, 1, WINDOW_SIZE/2);
    
    printf("decibels:\n");
    for (UInt32 i=0; i<WINDOW_SIZE/2; ++i) {
        printf("%.2f, ", self.totalPowerBuffer[i]);
        //self.totalPowerBuffer[i] = MAX(0, self.totalPowerBuffer[i]);
    }
    printf("\n\n");
    
    // assuming audio sampled at 16kHz we now have (16000/2)/(128/2) = 125hz frequency resolution
    // and 128/16 = 8ms time resolution
    float minDecible = 128;
    float maxDecible = -128;
    float thousandHzBuckets[8];
    float twoFiftyHzBuckets[32];
    for (UInt16 i=0; i<8; i++) {
        thousandHzBuckets[i] = 0;
    }
    for (UInt16 i=0; i<32; i++) {
        twoFiftyHzBuckets[i] = 0;
    }
    
    for (UInt32 i=0; i<WINDOW_SIZE/2; ++i) {
        float binDecible = self.totalPowerBuffer[i];
        //NSLog(@"binDecible: %.2f, minDecible: %.2f", binDecible, minDecible);
        minDecible = MIN(minDecible, binDecible);
        maxDecible = MAX(maxDecible, binDecible);
        
        
        thousandHzBuckets[(int)(i/8)] += binDecible;
        twoFiftyHzBuckets[(int)(i/2)] += binDecible;
    }
    
    
    // first 1000hz should have the maximum power
    for (UInt16 i=2; i<8; i++) {
        if (thousandHzBuckets[i] > thousandHzBuckets[0]) {
            NSLog(@"not speech, 1kHz bucket %d has more power than the first", i);
            return NO;
        }
    }
    
    // last 1000hz should have less power than any 0-4000hz
    for (UInt16 i=0; i <4; i++) {
        if (thousandHzBuckets[i] < thousandHzBuckets[7]) {
            //NSLog(@"not speech, 1kHz bucket %d has less power than the last; %.2f, %.2f", i, thousandHzBuckets[i], thousandHzBuckets[7]);
            //return NO;
        }
    }
    
    // there is at least one 1000hz bucket with a significant
    // fraction of first buckets power
    BOOL numNonFirst1000HzBucketWithPower = 0;
    for (UInt16 i=1; i<4; i++) {
        if (thousandHzBuckets[i] >= 0.5 * thousandHzBuckets[0]) {
            numNonFirst1000HzBucketWithPower++;
        }
    }
    if (numNonFirst1000HzBucketWithPower == 0) {
        NSLog(@"not speech, 1kHz numNonFirst1000HzBucketWithPower 0");
        return NO;
    }
    
    
    
    // at least two valleys in the graph under 4kHz for a valid
    // speech sample
    float valleyFactor = isOutputMuted ? 0.8 : 0.8;
    UInt8 numValleysLessThan4kHz = 0;
    for (UInt16 i=1; i<17; ++i) {
        NSLog(@"twoFiftyHzBuckets %d: %.2f", i, twoFiftyHzBuckets[i]);
        if (twoFiftyHzBuckets[i] < valleyFactor * twoFiftyHzBuckets[i - 1]
            && twoFiftyHzBuckets[i] < valleyFactor * twoFiftyHzBuckets[i + 1])
        {
            numValleysLessThan4kHz++;
        }
    }
    
    if (numValleysLessThan4kHz <  (isOutputMuted ? 1 : 2)) {
        //NSLog(@"not speech, numValleysLessThan4kHz: %d", numValleysLessThan4kHz);
        //return NO;
    }
    
    NSMutableArray *extrema = [NSMutableArray array];
    for (UInt32 i=0; i<32; ++i) {
        float thisBinDecible = self.totalPowerBuffer[i];
        float nextBinDecible = self.totalPowerBuffer[i + 1];
        
        BOOL isMinima = NO;
        BOOL isMaxima = NO;
        if (i == 0) {
            isMinima = thisBinDecible < nextBinDecible;
            isMaxima = thisBinDecible > nextBinDecible;
        } else {
            float lastBinDecible = self.totalPowerBuffer[i - 1];
            isMinima = thisBinDecible < lastBinDecible && thisBinDecible < nextBinDecible;
            isMaxima = thisBinDecible > lastBinDecible && thisBinDecible > nextBinDecible;
        }
        
        if (isMaxima || isMinima) {
            [extrema addObject:@(thisBinDecible)];
        }
    }
    
    numValleysLessThan4kHz = 0;
    for (UInt32 i=0; i<extrema.count; ++i) {
        NSNumber *extremum = extrema[i];
        NSLog(@"extremum: %.2f", extremum.floatValue);
        
        float bindDecible = extremum.floatValue;
        
        if (i==0) {
            NSNumber *nextExtremum = extrema[i + 1];
            if (bindDecible < valleyFactor * nextExtremum.floatValue) {
                numValleysLessThan4kHz++;
            }
        } else if (i == extrema.count - 1) {
            // last extrema being a minima is ignored
        } else {
            NSNumber *nextExtremum = extrema[i + 1];
            NSNumber *lastExtremum = extrema[i - 1];
            if (bindDecible < valleyFactor * lastExtremum.floatValue
                && bindDecible < valleyFactor * nextExtremum.floatValue) {
                numValleysLessThan4kHz++;
            }
        }
    }
    
    NSLog(@"numValleysLessThan4kHz: %d", numValleysLessThan4kHz);
    if (numValleysLessThan4kHz <  (isOutputMuted ? 1 : 2)) {
        NSLog(@"not speech, numValleysLessThan4kHz: %d", numValleysLessThan4kHz);
        return NO;
    }
    
    // at least one
    NSLog(@"special features: is speech");
    return YES;
}

-(BOOL)getAutoCorrelationBasedVAD:(SInt16*)samples numSamples:(UInt32)numSamples {
    
    // src: https://code.google.com/p/audacity/source/browse/audacity-src/trunk/src/FreqWindow.cpp
    
    if (numSamples < WINDOW_SIZE) {
        NSLog(@"Too few samples, at least %d samples required", WINDOW_SIZE);
        YES;
    }
    
    [self resetBuffers];
    
    float scale = (float) 1.0 / (2 * WINDOW_SIZE);
    
    memset(self.totalPowerBuffer, 0, WINDOW_SIZE/2);
    
    UInt32 numWindows = 0;
    
    for (UInt32 i=0; i<numSamples - WINDOW_SIZE; i += WINDOW_STRIDE) {
        
        numWindows++;
        
        // convert SInt16 samples to float samples
        vDSP_vflt16(samples + i, 1, self.outputBuffer, 1, WINDOW_SIZE);
        
        // apply hanning windowing
        vDSP_vmul(self.outputBuffer, 1, self.windows, 1, self.outputBuffer, 1, WINDOW_SIZE);
        
        // split input into complex
        vDSP_ctoz((COMPLEX*)self.outputBuffer, 2, &fftOutput, 1, WINDOW_SIZE/2);
        
        // do the fft
        vDSP_fft_zrip(self.fftSetup, &fftOutput, 1, round(log2(WINDOW_SIZE)), FFT_FORWARD);
        
        // scale
        vDSP_vsmul(fftOutput.realp, 1, &scale, fftOutput.realp, 1, WINDOW_SIZE/2);
        vDSP_vsmul(fftOutput.imagp, 1, &scale, fftOutput.imagp, 1, WINDOW_SIZE/2);
        
        // zero out the nyquist value
        fftOutput.imagp[0] = 0.0;
        
        // get the power values
        vDSP_zvmags(&fftOutput, 1, self.outputBuffer, 1, WINDOW_SIZE/2);
        
        for (UInt32 j=0; j<WINDOW_SIZE; ++j) {
            self.outputBuffer[j] = cbrtf(self.outputBuffer[j]);
        }
        
        // FFT again
        vDSP_ctoz((COMPLEX*)self.outputBuffer, 2, &fftOutput, 1, WINDOW_SIZE/2);
        
        // do the fft
        vDSP_fft_zrip(self.fftSetup, &fftOutput, 1, round(log2(WINDOW_SIZE)), FFT_FORWARD);
        
        // scale
        vDSP_vsmul(fftOutput.realp, 1, &scale, fftOutput.realp, 1, WINDOW_SIZE/2);
        vDSP_vsmul(fftOutput.imagp, 1, &scale, fftOutput.imagp, 1, WINDOW_SIZE/2);
        
        // zero out the nyquist value
        fftOutput.imagp[0] = 0.0;
        
        // take real part of result
        for (UInt32 j=0; j<WINDOW_SIZE/2; ++j) {
            self.totalPowerBuffer[j] += fftOutput.realp[j];
        }
    }
    
    printf("auto correlation vector:\n");
    for (UInt32 i=0; i<WINDOW_SIZE/2; ++i) {
        printf("%.2f ", self.totalPowerBuffer[i]);
    }
    
    for (UInt32 i=0; i<WINDOW_SIZE/2; ++i) {
        self.totalPowerBuffer[i] /= numWindows;
    }
    
    // Peak Pruning as described by Tolonen and Karjalainen, 2000
    for (UInt32 i=0; i<WINDOW_SIZE/2; ++i) {
        if (self.totalPowerBuffer[i] < 0.0) {
            self.totalPowerBuffer[i] = 0.0f;
        }
        self.outputBuffer[i] = self.totalPowerBuffer[i];
    }
    
    // Subtract a time-doubled signal (linearly interp.) from the original
    // (clipped) signal
    for (UInt32 i=0; i<WINDOW_SIZE/2; ++i) {
        if (i%2 == 0) {
            self.totalPowerBuffer[i] -= self.outputBuffer[i/2];
        } else {
            self.totalPowerBuffer[i] -= ((self.outputBuffer[i / 2] + self.outputBuffer[i / 2 + 1]) / 2);
        }
    }
    
    // Clip at zero again
    for (UInt32 i=0; i<WINDOW_SIZE/2; ++i) {
        if (self.totalPowerBuffer[i] < 0.0) {
            self.totalPowerBuffer[i] = 0.0f;
        }
    }
    
    float maxV = -INFINITY;
    float minV = +INFINITY;
    float sumV = 0;
    float sumFirstThird = 0;
    
    vDSP_maxv(self.totalPowerBuffer, 1, &maxV, WINDOW_SIZE/2);
    vDSP_minv(self.totalPowerBuffer, 1, &minV, WINDOW_SIZE/2);
    vDSP_sve(self.totalPowerBuffer, 1, &sumV, WINDOW_SIZE/2);
    vDSP_sve(self.totalPowerBuffer, 1, &sumFirstThird, WINDOW_SIZE/6);
    
    float sumLastTwoThirds = sumV - sumFirstThird;
    
    printf("auto correlation vector:\n");
    for (UInt32 i=0; i<WINDOW_SIZE/2; ++i) {
        printf("%.2f ", self.totalPowerBuffer[i]);
    }
    printf("\nmin,max,sum: %.2f, %.2f, %.2f", minV, maxV, sumV);
    printf("\n\n");
    
    if (sumV <= 0.9) {
        NSLog(@"sum autocorrelation less than threshold, not speech: %.2f", sumV);
    } else if (sumLastTwoThirds < sumFirstThird) {
        NSLog(@"not speech autocorrelation in last two thirds (%.2f) of window is less than first third (%.2f)", sumLastTwoThirds, sumFirstThird);
    }
    return sumV > 0.9;// && sumLastTwoThirds > sumFirstThird;
}

-(BOOL)getSpectralFlatnessBasedVAD:(SInt16*)samples numSamples:(UInt32)numSamples {
    [self resetBuffers];
    
    float scale = (float) 1.0 / (2 * WINDOW_SIZE);
    memset(self.totalPowerBuffer, 0, WINDOW_SIZE/2);
    
    UInt32 numWindows = 0;
    for (UInt32 i=0; i<numSamples - WINDOW_SIZE; i += WINDOW_STRIDE) {
        
        numWindows++;
        
        // convert SInt16 samples to float samples
        vDSP_vflt16(samples + i, 1, outputBuffer, 1, WINDOW_SIZE);
        
        // apply hanning windowing
        vDSP_vmul(outputBuffer, 1, windows, 1, outputBuffer, 1, WINDOW_SIZE);
        
        // split input into complex
        vDSP_ctoz((COMPLEX*)outputBuffer, 2, &fftOutput, 1, WINDOW_SIZE/2);
        
        // do the fft
        vDSP_fft_zrip(fftSetup, &fftOutput, 1, round(log2(WINDOW_SIZE)), FFT_FORWARD);
        
        // scale
        vDSP_vsmul(fftOutput.realp, 1, &scale, fftOutput.realp, 1, WINDOW_SIZE/2);
        vDSP_vsmul(fftOutput.imagp, 1, &scale, fftOutput.imagp, 1, WINDOW_SIZE/2);
        
        // zero out the nyquist value
        fftOutput.imagp[0] = 0.0;
        
        // get the power values
        vDSP_zvmags(&fftOutput, 1, outputBuffer, 1, WINDOW_SIZE/2);
        
        for (UInt32 j=0; j<WINDOW_SIZE/2; ++j) {
            totalPowerBuffer[j] += outputBuffer[j];
        }
    }
    
    printf("numWindows: %u\n", (unsigned int)numWindows);
    
    // take the simple average of powers for each frequency bin
    for (UInt32 i=0; i<WINDOW_SIZE/2; ++i) {
        totalPowerBuffer[i] /= numWindows;
        printf("totalPowerBuffer %u: %.2f\n", (unsigned int)i, totalPowerBuffer[i]);
    }
    
    double arithmeticMean = 0.0;
    double geometricMean = 0.0;
    
    for (UInt32 i=0; i<WINDOW_SIZE/2; ++i) {
        float power = fabs(totalPowerBuffer[i]);
        if (power != 0 && power == power && power < 10000000000) {
            arithmeticMean += power;
            geometricMean += log(power);
        }
    }
    
    arithmeticMean = arithmeticMean/(WINDOW_SIZE/2);
    geometricMean = exp(geometricMean/(WINDOW_SIZE/2));
    
    float sfm = fabs(10 * log10(geometricMean / arithmeticMean));
    printf("sfm: %.2f\n", sfm);
    
    BOOL isSpeech = sfm < 50.0;
    if (!isSpeech) {
        NSLog(@"spectral flatness measure above threshold, not speech: %.2f", sfm);
    }
    return isSpeech;
}

-(BOOL)getZeroCrossingRateBasedVAD:(SInt16*)samples numSamples:(UInt32)numSamples {
    UInt32 numZeroCrossing = 0;
    
    for (UInt32 i=1; i<numSamples; i++) {
        if (signbit(samples[i]) != signbit(samples[i - 1])) {
            numZeroCrossing++;
        }
    }
    
    // 10 ms windows
    UInt32 numWindows = numSamples/160;
    double avgNumZeroCrossing = (numZeroCrossing * 1000.0)/numSamples;
    printf("numZeroCrossing, avgNumZeroCrossing: %u, %.4f\n", numZeroCrossing, avgNumZeroCrossing);
    
    return YES;
}

@end
