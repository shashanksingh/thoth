//
//  VADetector.h
//  cmd
//
//  Created by Shashank Singh on 1/17/15.
//  Copyright (c) 2015 Shashank Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VADetector : NSObject
-(BOOL)getAutoCorrelationBasedVAD:(SInt16*)samples numSamples:(UInt32)numSamples;
-(BOOL)getSpectralFlatnessBasedVAD:(SInt16*)samples numSamples:(UInt32)numSamples;
-(BOOL)getZeroCrossingRateBasedVAD:(SInt16*)samples numSamples:(UInt32)numSamples;
-(BOOL)samplesHaveVoiceActivity:(SInt16*)samples numSamples:(UInt32)numSamples
                withOutputMuted:(BOOL)isOutputMuted;
@end
