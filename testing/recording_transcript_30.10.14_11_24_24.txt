silence: 0.03, 3544, 0
speech: 9.86, 255, 1787
silence: 11.39, 348, 22   (noise, √)
speech: 13.25, 1117, 6102 
silence: 14.91, 100, 428 (start, √)
speech: 17.54, 0, 4323 
silence: 19.04, 0, 0  (noise, √)
speech: 25.76, 282, 1616
silence: 30.40, 122, 142 (noise, X, next)
speech: 46.40, 1204, 7775 
silence: 48.51, 51, 76  (next, √)
speech: 79.39, 473, 3981
silence: 81.44, 139, 114 (next, √)
speech: 124.70, 557, 4186
silence: 126.82, 16, 57 (next, √)
speech: 156.45, 1, 1620
silence: 157.95, 5, 1 (noise, √)
speech: 164.77, 0, 3888
silence: 166.53, 140, 144 (next, X, go; next when go back changed to back)
speech: 219.74, 902, 1989
silence: 221.25, 0, 0 (next, was cut off, X, noise)
speech: 223.10, 169, 6991
silence: 225.18, 57, 71 (next, √)
speech: 251.42, 0, 2015
silence: 253.57, 130, 180 (next, √)
speech: 285.18, 1463, 1875
silence: 288.45, 788, 855 (noise, X, pause)
speech: 292.58, 1, 2543 
silence: 294.82, 665, 529 (next, X, back with prefix=2, resume with prefix=1, briefrr seems to be causing confusion, starting from after it works)
speech: 299.52, 740, 2329
silence: 302.11, 387, 449 (next, X, resume, same issue as above)
speech: 311.04, 684, 3622
silence: 313.18, 144, 111 (next, √)
speech: 321.57, 1120, 3889
silence: 323.71, 186, 239 (next, X, back, same as above, cutting briefrr out helps)
speech: 333.54, 1269, 2128
silence: 335.04, 1, 1 (noise, √)
speech: 335.26, 703, 1622 
silence: 336.77, 434, 434 (noise, X, prefix=0, suffix=2 fixed it, prefix=2 was including the previous noise into this window)
speech: 341.82, 1, 2745
silence: 344.06, 243, 221 (next, X, resume, cutting out briefer fixed it)
speech: 353.92, 424, 4571
silence: 356.13, 427, 607 (next, X, resume, next was cutoff at the end, ignoring such utterances my help with false positives, ps should be able to take care of these)
speech: 357.31, 1474, 2026
silence: 358.82, 130, 45  (noise, √)
speech: 362.56, 252, 2095
silence: 364.77, 262, 256 (next, X, back, resume, cutting out brieferr fixed it)
speech: 373.47, 22, 2503
silence: 378.43, 806, 660 (noise, X, resume, long noise , nearly 5 seconds, garbage trick might help, other heuristics might help too)
speech: 380.61, 0, 2381
silence: 382.14, 105, 120 (noise, √)
speech: 387.42, 459, 2471
silence: 389.50, 199, 153 (next, √)
speech: 395.49, 1006, 3587
silence: 397.63, 303, 360 (next, X, resume, removing brieferr helps)
speech: 403.87, 411, 3350
silence: 405.38, 510, 558 (noise, √)
speech: 449.38, 0, 2095
silence: 451.52, 518, 518 (next, √)
speech: 452.54, 770, 1723
silence: 454.24, 327, 307 (noise, X, resume, normal than usual noise with a bump)
speech: 454.66, 464, 3935
silence: 456.19, 347, 343 (noise, X, resume, normal than usual noise with a bump)
speech: 467.26, 24, 3030
silence: 469.54, 544, 845 (next, √, there was a briefrr prefix but it was broken)
speech: 479.58, 0, 4319
silence: 481.76, 195, 159 (next, √, there was a briefrr prefix but it was broken)
speech: 484.58, 733, 2916
silence: 486.18, 325, 262 (noise, X, resume, normal than usual noise with a bump)
speech: 493.06, 308, 2481
silence: 494.56, 0, 0     (next, √, there was no briefrr prefix, it was clear by itself)
speech: 500.54, 756, 1696
silence: 503.49, 0, 0     (noise, √, there might have been a muffled next there)
speech: 504.03, 248, 3246
silence: 505.57, 279, 259 (noise, √, normal than usual noise with a bump still worked fine)
speech: 574.18, 1309, 3808
silence: 576.22, 413, 210 (next, √, next sounded like nest, still recognized)
speech: 579.14, 1324, 1960
silence: 580.64, 0, 0  (noise, √)
speech: 584.32, 761, 3382
silence: 585.86, 254, 260 (noise, √)
speech: 602.46, 152, 3418
silence: 603.97, 1, 1 (noise, V)
speech: 637.22, 1318, 4072
silence: 639.20, 266, 375  (next, √, briefrr prefix was broken)
speech: 647.90, 1070, 2854
silence: 649.98, 0, 0  (next, X, pause, next was broken)
speech: 655.17, 118, 2606
silence: 657.34, 235, 148 (next, √, prefix was broken)
speech: 666.34, 14, 3669
silence: 668.42, 198, 143 (next, √, prefix was broken)
speech: 739.90, 31, 3489
silence: 742.14, 329, 305 (next, √, prefix was broken)
speech: 774.98, 32, 1833
silence: 776.48, 0, 0 (noise, √)
speech: 780.06, 651, 4749
silence: 781.66, 416, 260 (next, √, prefix was broken)
speech: 786.08, 23, 2454
silence: 788.16, 252, 248 (next, √, prefix was broken)
speech: 854.40, 613, 1946
silence: 856.00, 531, 424 (noise, √)
speech: 866.18, 1191, 2446
silence: 867.78, 80, 74 (next, √, no prefix)
speech: 969.44, 746, 2881
silence: 970.98, 84, 128 (next, X, prefix was there but both prefix and command were broken)
speech: 982.11, 211, 1999
silence: 983.65, 208, 219 (noise, √)
speech: 984.90, 14, 4130
silence: 987.14, 136, 246 (pause, √, prefix was there, command was a broken but worked)
speech: 989.47, 759, 3367
silence: 991.14, 122, 156 (resume, √, prefix present)
speech: 1051.68, 969, 1907
silence: 1053.18, 450, 121 (noise, √)
speech: 1056.51, 1447, 2267
silence: 1058.02, 0, 0 (noise, X, back, was at the end of the session)


