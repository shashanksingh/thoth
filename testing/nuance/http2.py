#!/usr/bin/env python
#vim: set fileencoding=utf8


"""
This example depends on the `requests` library
http://requests.readthedocs.org/en/latest/

    $ pip install requests

"""


from __future__ import print_function

import random
import requests
import time


APP_ID = "aid"
APP_KEY = "akey"


def main():
    headers = {
        'Accept': "audio/x-wav;codec=pcm;bit=16;rate=16000",
        'Content-Type': "text/plain"
    }

    params = {
        'appId': APP_ID,
        'appKey': APP_KEY,
        'id': "9CFFC4B1-6294-4122-B249-9441C0ECCE77",
        'voice': "Ava"
    }

    session = requests.session()
    session.headers.update(headers)
    session.params = params

    url = "http://postcatcher.in/catchers/547d8420b0b8eb0200000655"
    text = "This is a test {}.".format(random.randint(0, 10000))

    for i in xrange(0, 1):
        print("Attempt {0}... ".format(i + 1), end="")
        print("Text", text)

        t0 = time.time()
        r = session.post(url, data=text)
        print(r.content)
        t1 = time.time()

        print("{} ({}s)".format(r.status_code, round(t1 - t0, 2)))
        r.raise_for_status()


if __name__ == "__main__":
    main()
