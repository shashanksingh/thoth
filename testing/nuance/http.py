import sys
import requests
from StringIO import StringIO
import os

SERVER = "sandbox.nmdp.nuancemobility.net"
PATH = 'NMDPTTSCmdServlet/tts'
PORT = 443
APP_ID = "NMDPTRIAL_adisomani20141109161135"
APP_KEY = "149e8892e8f6e136a5851ae543098b34a60c47924b6cede6327c69322ca7bd404545d22d3bb364e1387ec67b8851af2e5df7c1a863ae8e952af7c7569f8596e4"

def do_tts(text_to_convert):
    url = 'http://%(server)s:%(port)d/%(path)s'%({
        'server': SERVER,
        'port': PORT,
        'path': PATH
    })
    params = {
        'appId': APP_ID,
        'appKey': APP_KEY,
        'ttsLang': 'en_US',
        'voice': 'Samantha',
        'id': '0000'
    }
    headers = {
        'Content-Type': 'text/plain',
        'Accept': 'audio/x-wav;codec=pcm;bit=16;rate=16000'
    }
    data = StringIO(text_to_convert)

    print "request url", url
    response = requests.post(url, params=params, headers=headers, data=data, verify=False)
    print "request finished"
    print response.content, response.status_code

    audio_data = StringIO(response.content)
    audio_data.seek(0, os.SEEK_END)
    print audio_data.tell()
    
    print dir(response)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "usage <script name> <text_to_convert>"
    else:
        do_tts(sys.argv[1])