import sys
import numpy
import math

def process_audio(samples):
    i = 0
    num_samples = len(samples)
    WINDOW_SIZE = 32
    contiguous_lows = 0

    while i < num_samples:
        j = i
        total = 0
        while j < min(num_samples, i + WINDOW_SIZE):
            value = samples[j]
            total += math.pow(value, 2)
            j += 1


        window_avg = math.sqrt(total/(j - i))
        if window_avg < 300:
            contiguous_lows += 1
        else:
            contiguous_lows = 0

        print "window_avg", window_avg, contiguous_lows

        if contiguous_lows >= 50:
            print "found wakeup"
            with open('/Users/shashank/Desktop/recordings/processed.wav', 'w+') as out:
                out.write(data[i:])
            return

        i = j

    print "didn't find wakeup"


if __name__ == "__main__":
    file_path = sys.argv[1]

    data = numpy.memmap(file_path, dtype='h', mode='r')
    process_audio(data)
