import sys
import numpy
from collections import deque
import math

WINDOW_SIZE = 512
VAD_MIN = 800
SPEECH_TO_SIL_FRAMES_MIN = WINDOW_SIZE
SIL_TO_SPEECH_SIL_FRAMES_MIN = 3 * WINDOW_SIZE
SIL_TO_SPEECH_SIL_FRAMES_MAX = 24000

def doIt():
    file_path = sys.argv[1]
    data = numpy.memmap(file_path, dtype='h', mode='r')

    #ignore first few samples, they are usually too high
    data = data[20:]

    num_samples = len(data)
    i = 0
    last_avg = -1
    silence_frames = 0
    speech_frames = 0
    is_silence = True
    threshold = -1
    speech_deque = deque([], 2)
    silence_deque = deque([], 32)
    non_increasing_count = 0


    while i < num_samples:
        j = i
        total = 0
        while j < min(num_samples, i + WINDOW_SIZE):
            total += data[j] ** 2
            j += 1
        
        current_avg = (total/(j - i)) ** 0.5
        speech_deque.append(current_avg)
        silence_deque.append(current_avg)

        avg_speech = sum(speech_deque)/len(speech_deque)
        avg_silence = sum(silence_deque)/len(silence_deque)

        if avg_speech >= 3000:
            speech_start_index = i - (len(speech_deque) * WINDOW_SIZE)
            if is_silence:
                print "speech: %.2f, %d, %d"%(speech_start_index/16000.0, last_avg, current_avg)
            is_silence = False
            non_increasing_count = 0
        elif not is_silence:
            if silence_deque[len(silence_deque) - 1] <= silence_deque[len(silence_deque) - 2]:
                non_increasing_count += 1
            else:
                non_increasing_count = 0
            if avg_silence < 300 or non_increasing_count >= 10:
                is_silence = True
                print "silence: %.2f, %d, %d"%(i/16000.0, last_avg, current_avg)

        #print 'avg', "%3.2f"%(i/16000.0), "%5d"%(avg_silence), "%5d"%(avg_speech)

        i = j

'''
        curr_window_is_high_engery = current_avg >= VAD_MIN
        last_window_was_high_energy = last_avg >= VAD_MIN

        if curr_window_is_high_engery != last_window_was_high_energy:
            if curr_window_is_high_engery:
                speech_frames = 0
            else:
                silence_frames = 0

                mi = SIL_TO_SPEECH_SIL_FRAMES_MIN
                ma = SIL_TO_SPEECH_SIL_FRAMES_MAX
                w = WINDOW_SIZE

                threshold = mi +  (((ma - mi)/w) * (speech_frames - w))
                threshold = min(ma, threshold)
                print "threshold", threshold


        if curr_window_is_high_engery:
            speech_frames += j - i
            if is_silence:
                if speech_frames >= SPEECH_TO_SIL_FRAMES_MIN:
                    is_silence = False
                    print "speech: %.2f, %d, %d"%(i/16000.0, last_avg, current_avg)

        else:
            silence_frames += j - i
            if not is_silence:

                if silence_frames >= threshold:
                    is_silence = True
                    print "silence: %.2f, %d, %d"%(i/16000.0, last_avg, current_avg)

        last_avg = current_avg
        i = j
'''

if __name__ == '__main__':
  doIt()

