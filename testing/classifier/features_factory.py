import features

def get_spectrum_features(samples):
    frames = features.sigproc.framesig(samples, 128, 1)
    power_spectrum = features.sigproc.logpowspec(frames, 128)
    num_freq_bins = len(power_spectrum[0])
    num_windows = len(power_spectrum)
    # avg over all windows for each bin
    power_spectrum = [sum([power_spectrum[j][i] for j in range(num_windows)])/num_windows for i in range(num_freq_bins)]
    min_pow = min(power_spectrum) * 1.0
    max_pow = max(power_spectrum) * 1.0

    return [(v - min_pow)/(max_pow - min_pow) for v in power_spectrum]
