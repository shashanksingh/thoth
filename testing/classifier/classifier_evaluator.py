import numpy
from sklearn.naive_bayes import GaussianNB
import common
import sys
import scipy.io.wavfile as wav
from features import mfcc
from features import logfbank

def get_feature_vecs(file_path):
    data =  numpy.memmap(file_path, dtype='h', mode='r')
    return logfbank(data)
    #return [map(float, line.split()) for line in common.get_features_vectors(data).split('\n') if line]

def train(speech_train_file_path, noise_train_file_path):
    speech_vecs = get_feature_vecs(speech_train_file_path)
    noise_vecs = get_feature_vecs(noise_train_file_path)

    all_feature_vecs = numpy.concatenate((speech_vecs, noise_vecs))
    all_classes = ['speech' for i in range(len(speech_vecs))] + ['noise' for i in range(len(noise_vecs))]

    gnb = GaussianNB()
    model = gnb.fit(all_feature_vecs, all_classes)
    prediction = model.predict(all_feature_vecs)
    print "incorrect", sum([all_classes[i] != prediction[i] for i in range(len(all_classes))])
    print "total", len(all_classes)
    return  model

def predict(model, test_file_path):
    test_vecs = get_feature_vecs(test_file_path)
    prediction = model.predict(test_vecs)
    noise_count = len([p for p in prediction if p == 'noise'])
    speech_count = len([p for p in prediction if p == 'speech'])
    print "noise", noise_count, (noise_count * 100.0)/(noise_count + speech_count)
    print "speech", speech_count, (speech_count * 100.0)/(noise_count + speech_count)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print 'usage: classifier_evaluator <speech_train_file_path> <noise_train_file_path> [test_file_path]'
    else:
        model = train(sys.argv[1], sys.argv[2])
        if len(sys.argv) > 3:
            predict(model, sys.argv[3])