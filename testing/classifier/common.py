import subprocess

def get_features_vectors(data):
    with open('_features.wav', 'w') as out:
        out.write(data)

    subprocess.check_output(["/Users/shashank/projects/iris/sphinx/pocketsphinx/build_x86/bin/sphinx_fe",
                                      "-remove_silence", "no",
                                      "-vad_prespeech", "1",
                                      "-vad_postspeech", "1",
                                      "-i", "_features.wav",
                                      "-wlen", "%.5f"%(1.024),
                                      "-nfft", "16384",
                                      "-o", "mfcc.mfc"])


    output = subprocess.check_output(["/Users/shashank/projects/iris/sphinx/pocketsphinx/build_x86/bin/sphinx_cepview",
                                      "-f", "mfcc.mfc",
                                      "-d", "13"])

    return output


def get_features_vectors2(data):
    with open('_features.wav', 'w') as out:
        out.write(data)

    subprocess.check_output(["/Users/shashank/projects/iris/sphinx/pocketsphinx/build_x86/bin/sphinx_fe",
                                      "-remove_silence", "no",
                                      "-vad_prespeech", "1",
                                      "-vad_postspeech", "1",
                                      "-i", "_features.wav",
                                      "-o", "mfcc.mfc"])


    output = subprocess.check_output(["/Users/shashank/projects/iris/sphinx/pocketsphinx/build_x86/bin/sphinx_cepview",
                                      "-f", "mfcc.mfc",
                                      "-d", "13"])

    return output
