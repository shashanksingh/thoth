#include "aquila/global.h"
#include "aquila/source/WaveFile.h"
#include "aquila/transform/FftFactory.h"
#include "aquila/tools/TextPlot.h"
#include "aquila/functions.h"
#include "aquila/source/window/HannWindow.h"
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>

std::vector<Aquila::SampleType> filterSilence(Aquila::WaveFile wav, unsigned int zero,
                                              unsigned int minSilenceWindow) {
    std::vector<Aquila::SampleType> rv;
    std::vector<Aquila::SampleType> tempBuffer;
    for (auto sample : wav) {
        if (abs(sample) <= zero) {
            tempBuffer.push_back(sample);
        } else {
            std::size_t tempBufferSize = tempBuffer.size();
            if (tempBufferSize > 0) {
                if (tempBufferSize <= minSilenceWindow) {
                    rv.reserve(rv.size() + distance(tempBuffer.begin(), tempBuffer.end()));
                    rv.insert(rv.end(), tempBuffer.begin(), tempBuffer.end());
                }
                tempBuffer.clear();
            }
            rv.push_back(sample);
        }
    }

    return rv;
}

void writeSpectrumToFile(const char *wavFilePath, const char *spectrumFilePath,
     unsigned int numSamplesInWindow, const char *prefix) {

    const std::size_t FFT_SIZE = 128;
    const std::size_t STRIDE = 160;

    Aquila::WaveFile wav(wavFilePath);
    std::ofstream outputFile;
    outputFile.open(spectrumFilePath);

    outputFile << "%\n% " << wav.getFilename() << "\n";

    unsigned int fileSize = wav.getWaveSize();
    unsigned int numSamples = (fileSize * 8)/wav.getBitsPerSample();
    std::cout << "Loaded file: " << wav.getFilename() << " " << numSamples << std::endl;

    auto filtered = filterSilence(wav, 10, 160);
    std::cout << "Filtered file: " << filtered.size() << std::endl;

    for (unsigned int k=0; k + STRIDE <= filtered.size(); k += STRIDE) {
        unsigned int blockEnd = std::min<unsigned int>(filtered.size(), k + numSamplesInWindow);

        auto fft = Aquila::FftFactory::getFft(FFT_SIZE);
        Aquila::HannWindow window(FFT_SIZE);
        std::vector<double> powerSpectrum(FFT_SIZE/2, 0.0);
        for (unsigned int i=k; i+FFT_SIZE<blockEnd; i+=FFT_SIZE) {

            std::transform(std::begin(filtered) + i,
                           std::begin(filtered) + i + FFT_SIZE,
                           window.begin(),
                           std::begin(filtered) + i,
                           std::multiplies<double>());

            Aquila::SpectrumType spectrum = fft->fft(&filtered[i]);
            for (unsigned int j=0; j<spectrum.size()/2; ++j) {
                auto complexValue = spectrum[j];
                powerSpectrum[j] += std::abs(complexValue);
            }
        }

        double maxPow = -1000000.0;
        double minPow = 1000000.0;
        for (unsigned int i=0; i<powerSpectrum.size(); ++i) {
            double totalPower = powerSpectrum[i];
            double averagePower = powerSpectrum[i]/powerSpectrum.size();
            double decibels = Aquila::dB(averagePower);

            powerSpectrum[i] = decibels;
            maxPow = std::max(decibels, maxPow);
            minPow = std::min(decibels, minPow);
        }

        for (unsigned int i=0; i<powerSpectrum.size(); ++i) {
            powerSpectrum[i] = (powerSpectrum[i] - minPow)/(maxPow - minPow);
        }

        if (strlen(prefix) > 0) {
            outputFile << prefix << " ";
        }

        for (double binPow: powerSpectrum) {
            outputFile << binPow << " ";
        }
        outputFile << "\n";
    }

    outputFile.close();
}

int main (int argc, char **argv) {
    if (argc < 4) {
        std::cout << "Usage: <programe name> <wav file path> <spectrum output file path> <num samples in window> [prefix]" << std::endl;
        return 0;
    }

    char *wavFilePath = argv[1];
    char *spectrumFilePath = argv[2];
    unsigned int numSamplesInWindow = -1;
    sscanf(argv[3], "%u", &numSamplesInWindow);
    const char *prefix = "";
    if (argc >= 5) {
        prefix = argv[4];
    }

    writeSpectrumToFile(wavFilePath, spectrumFilePath, numSamplesInWindow, prefix);

}
