import sys
import glob
import os
from subprocess import call
import re
import time
import argparse

DATA_DIR = "/Users/shashank/Desktop/sound_samples/"
NUM_SAMPLES_IN_WINDOW = 800

def get_arff_header(num_features):
    return '''%%
    @RELATION mfcc
    @ATTRIBUTE class {speech,noise}
    %s
    %%
    @DATA
    %%'''%('\n'.join(['@ATTRIBUTE f%d REAL'%i for i in range(num_features)]))

def compile_spectrum_generator():
    call(['g++', '-stdlib=libc++', '-std=gnu++11', 'spectrum_generator.cpp', '-I/Users/shashank/dev/lib/aquila/aquila-src/', '-L/Users/shashank/dev/lib/aquila/aquila-src/', '-L/Users/shashank/dev/lib/aquila/aquila-src/lib/', '-lAquila', '-lOoura_fft', '-o', 'spectrum_generator'])

def extract_features(speech_file_paths, noise_file_paths, arff_path):
    #compile_spectrum_generator()
    
    feature_file_paths = []
    for clazz, audio_file_paths in [["speech", speech_file_paths], ["noise", noise_file_paths]]:

        for audio_file_path in audio_file_paths:
            wav_file_name = os.path.basename(audio_file_path)
            feature_file_name = re.sub(r"\.wav$", ".txt", wav_file_name)
            feature_file_path = os.path.join(os.path.dirname(audio_file_path), feature_file_name)

            start_time = time.time()
            call(["./spectrum_generator", audio_file_path, feature_file_path,
                  str(NUM_SAMPLES_IN_WINDOW), clazz])
            time_taken = time.time() - start_time
            print 'processing time', int(round(time_taken))

            feature_file_paths.append(feature_file_path)

    arff_file = open(arff_path, 'w')
    arff_file.write(get_arff_header(64))

    for feature_file_path in feature_file_paths:
        feature_file = open(feature_file_path, 'r')
        arff_file.write(feature_file.read() + '\n')
        feature_file.close()

    arff_file.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--noise', action='append', help='path to noise wav file')
    parser.add_argument('-s', '--speech', action='append', help='path to speech wav file')
    parser.add_argument('-a', '--arff', help='path to output arff file')
    parser.add_argument('-f', '--feature', help='type of feature, {mfcc, spectrum}')

    options = parser.parse_args()

    speech_file_paths = []
    if options.speech:
        if options.speech == ['*']:
            speech_file_paths = glob.glob(os.path.join(DATA_DIR, 'speech/*.wav'))
        else:
            speech_file_paths = options.speech
    
    noise_file_paths = []
    if options.noise:
        if options.noise == ['*']:
            noise_file_paths = glob.glob(os.path.join(DATA_DIR, 'noise/*.wav'))
        else:
            noise_file_paths = options.noise

    if (len(speech_file_paths) == 0 and len(noise_file_paths) == 0):
        print "at least one audio file path needs to be provided"

    extract_features(speech_file_paths, noise_file_paths, options.arff)
 
