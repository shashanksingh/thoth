import sys
import os
import numpy
import argparse
from features import mfcc
import features_factory
import time

def get_header(feature):
    num_features = 13
    if feature == 'spectrum':
        num_features = 65

    return '''%%
    @RELATION mfcc
    @ATTRIBUTE class {speech,noise}
    %s
    %%
    @DATA
    %%'''%('\n'.join(['@ATTRIBUTE f%d REAL'%i for i in range(num_features)]))

def remove_silences_from_samples(samples, zero=10, min_silence_window=160):
    zeroes = []
    rv = []
    for sample in samples:
        if abs(sample) <= zero:
            zeroes.append(sample)
        else:
            num_zeroes = len(zeroes)
            if 0 < num_zeroes < min_silence_window:
                rv.extend(zeroes)
            zeroes = []
            rv.append(sample)

    return rv


def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def get_arff_data_for_audio_file(file_path, is_noise, feature_type=None):
    print "processing audio file", file_path

    samples = numpy.memmap(file_path, dtype='h', mode='r')
    start_time = time.time()
    samples = numpy.array(remove_silences_from_samples(samples))
    time_taken = time.time() - start_time
    print 'silence removal time', int(round(time_taken))

    if feature_type is None:
        feature_type = 'mfcc'

    start_time = time.time()
    rv = ''
    if feature_type == 'mfcc':
        feature_vecs = mfcc(samples)
        rv = '\n'.join(['%s %s'%(is_noise and 'noise' or 'speech', ' '.join(['%.5f'%v for v in attrs])) for attrs in feature_vecs])

    elif feature_type == 'spectrum':
        i = 0
        while i < len(samples) - 1600:
            chunk = samples[i:i+1600]
            feature_vec = features_factory.get_spectrum_features(chunk)
            rv += '%s %s'%(is_noise and 'noise' or 'speech', ' '.join(['%.5f'%v for v in feature_vec])) + '\n'
            i += 160

    time_taken = time.time() - start_time
    print 'feature extraction time', int(round(time_taken))
    return rv.strip()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--noise', action='append', help='path to noise wav file')
    parser.add_argument('-s', '--speech', action='append', help='path to speech wav file')
    parser.add_argument('-a', '--arff', help='path to output arff file')
    parser.add_argument('-f', '--feature', help='type of feature, {mfcc, spectrum}')

    options = parser.parse_args()

    speech_data = noise_data = ''
    if options.speech:
        print "processing speech files", options.speech
        speech_data = '\n'.join([get_arff_data_for_audio_file(audio_file_path, False, options.feature) for audio_file_path in options.speech])

    if options.noise:
        print "processing noise files", options.noise
        noise_data = '\n'.join([get_arff_data_for_audio_file(audio_file_path, True, options.feature) for audio_file_path in options.noise])

    arff_data = '%s\n%s\n%s'%(get_header(options.feature), speech_data, noise_data)
    with open(options.arff, 'w') as out_file:
        out_file.write(arff_data)

