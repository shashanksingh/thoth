#!/usr/bin/python

import SimpleHTTPServer
import SocketServer
import logging
import cgi

PORT = 14003

output_file = open("recording.wav", "w")

class ServerHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    def do_GET(self):
        self.send_response(405, "Method not allowed")

    def do_POST(self):
        logging.warning(self.headers)
        content_len = int(self.headers.getheader('content-length', 0))
        post_body = self.rfile.read(content_len)
        output_file.write(post_body)
        self.send_response(200, "OK")

Handler = ServerHandler
httpd = SocketServer.TCPServer(("", PORT), Handler)
httpd.serve_forever()

